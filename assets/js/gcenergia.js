$(document).ready(function () {
	let date = new Date(Date.now());

	$(".textarea").wysihtml5();

	document.addEventListener("keydown", function (event) {
		const key = event.key; // "ArrowRight", "ArrowLeft", "ArrowUp", or "ArrowDown"

		if (!!document.getElementById("facturas")) {
			if (key === "ArrowLeft") {
				location.assign(document.getElementById("ant").getAttribute("href"));
				//alert(document.getElementById("ant").getAttribute("href"));
			}

			if (key === "ArrowRight") {
				location.assign(document.getElementById("sgt").getAttribute("href"));
				//alert(document.getElementById("sgt").getAttribute("href"));
			}
		}
	});

	if (!!document.facturaForm) {
		const facturaMode = document.facturaForm.facturaMode;

		if (facturaMode && !!facturaMode.length) {
			facturaMode.forEach(function (element) {
				element.onchange = function () {
					const resumen = document.querySelectorAll(".resumen_doble");
					const doc = document.querySelectorAll(".documento_simple");

					if (this.value == "doble") {
						document.getElementById("monto_exento_libre").disabled = true;
						document.getElementById("cargo_otr_libre").disabled = true;
						document.getElementById("saldo_ant_libre").disabled = true;
						doc.forEach(function (el) {
							el.classList.add("oculto");
						});

						resumen.forEach(function (el) {
							el.classList.remove("oculto");
						});
					} else {
						document.getElementById("monto_exento_libre").disabled = false;
						document.getElementById("cargo_otr_libre").disabled = false;
						document.getElementById("saldo_ant_libre").disabled = false;
						doc.forEach(function (el) {
							el.classList.remove("oculto");
						});

						resumen.forEach(function (el) {
							el.classList.add("oculto");
						});
					}
				};
			});
		}
	}

	if (!!document.getElementById("ac_consumo"))
		document.getElementById("ac_consumo").addEventListener("blur", setValue);

	if (!!document.getElementById("fp_consumo"))
		document.getElementById("fp_consumo").addEventListener("blur", setValue);

	if (!!document.getElementById("ep_consumo"))
		document.getElementById("ep_consumo").addEventListener("blur", setValue);

	if (!!document.getElementById("ac_consumo_gen"))
		document
			.getElementById("ac_consumo_gen")
			.addEventListener("blur", setValue);

	if (!!document.getElementById("ep_consumo_gen"))
		document
			.getElementById("ep_consumo_gen")
			.addEventListener("blur", setValue);

	/* if (document.getElementById("#factura_form")) {
		document.querySelectorAll(".sumar").forEach(function (element) {
			element.onblur = function (e) {
				const target = e.target;
				console.log(target);
				setTotalServ(target.dataset.sum, target.dataset.result);
			};
		});

		document.querySelectorAll(".sumar_libre").forEach(function (element) {
			element.onblur = function (e) {
				const target = e.target;
				setTotalServ(target.dataset.sum, target.dataset.result);
			};
		});
	} */

	if (!!document.getElementById("tarB")) {
		document.getElementById("tarB").onblur = function () {
			if (this.value.replaceAll(" ", "") == "" || isNaN(this.value)) {
				this.value = 0;
			}
		};
	}

	if (!!document.getElementById("anno")) {
		document.getElementById("anno").onblur = function () {
			if (this.value.replaceAll(" ", "") == "" || isNaN(this.value)) {
				this.value = date.getFullYear();
			}
		};
	}

	if (!!document.getElementById("max_inv")) {
		document.getElementById("max_inv").onblur = function () {
			if (this.value.replaceAll(" ", "") === "" || isNaN(this.value)) {
				this.value = 0;
			}
		};
	}

	if (
		!!document.getElementById("regiones") &&
		!!document.getElementById("ciudades")
	) {
		let reg = document.getElementById("reg");
		let regiones = document.getElementById("regiones");
		let cid = document.getElementById("cid");

		regiones.onchange = function () {
			let ciudades = document.getElementById("ciudades");

			ciudades.value = 0;
			ciudades.innerHTML = "";

			if (!!regiones && regiones.value > 0) {
				$.getJSON(
					"https://apis.digital.gob.cl/dpa/regiones/" +
						regiones.value +
						"/comunas",
					function (result) {
						result.map((ciudad) => {
							let node = document.createElement("option");
							node.value = ciudad.codigo;
							node.text = ciudad.nombre;
							ciudades.add(node);
						});

						if (!!cid && cid.value > 0) {
							ciudades.value = cid.value;
							cid.value = 0;
						}

						if (!!result.length) ciudades.removeAttribute("disabled");
						else ciudades.setAttribute("disabled", "disabled");
					}
				);
			} else {
				ciudades.setAttribute("disabled", "disabled");
			}
		};

		$.getJSON("https://apis.digital.gob.cl/dpa/regiones", function (result) {
			result.map((region) => {
				let node = document.createElement("option");
				node.value = region.codigo;
				node.text = region.nombre;
				regiones.add(node);
			});

			if (!!reg && reg.value > 0) {
				regiones.value = reg.value;
				regiones.onchange();
			}
		});
	}

	if (!!document.getElementById("compania")) {
		let compania = document.getElementById("compania");
		compania.onchange = function () {
			let sector = document.getElementById("tarifario");

			sector.innerHTML = "";

			$.getJSON(
				location.protocol +
					"//" +
					location.hostname +
					"/gestion/get_sectores/?token=" +
					compania.value,
				function (result) {
					// console.log(result);

					Object.keys(result).forEach(function (key) {
						let node = document.createElement("option");
						node.value = result[key]["codigo"];
						node.text = result[key]["nombre"];

						sector.add(node);
					});

					if (result.length > 0) sector.removeAttribute("disabled");
					else sector.setAttribute("disabled", "disabled");
				}
			);
		};
	}

	$("#ajax-response").on("hidden.bs.modal", function (e) {
		// do something...

		document.getElementById("body").style.padding = 0;
	});

	$(".delete").click(function (event) {
		event.preventDefault();

		document.getElementById("token").value = $(this).attr("data-token");

		$("#deleteFacturaModal").modal("show");
	});

	$("#saveCuenta").click(function (event) {
		event.preventDefault();

		$("#saveCuentaModal").modal("show");
	});

	$("#savePassword").click(function (event) {
		event.preventDefault();

		$("#savePasswordModal").modal("show");
	});

	$("#saveAvatar").click(function (event) {
		event.preventDefault();

		$("#saveAvatarModal").modal("show");
	});

	$("#saveCliente").click(function (event) {
		event.preventDefault();

		$("#saveClienteModal").modal("show");
	});

	$("#saveClienteMessage").click(function (event) {
		event.preventDefault();

		$("#saveClienteMessageModal").modal("show");
	});

	$("#readXML").click(function (event) {
		event.preventDefault();

		$("#xmlFacturaModal").modal("show");
	});

	$("#saveFactura").click(function (event) {
		event.preventDefault();

		$("#saveFacturaModal").modal("show");
	});

	$("#saveFactura-n").click(function (event) {
		event.preventDefault();

		$("#saveFacturaModal").modal("show");
	});

	$("#saveSuministro").click(function (event) {
		event.preventDefault();

		$("#saveSuministroModal").modal("show");
	});

	$("#saveContrato").click(function (event) {
		event.preventDefault();

		$("#saveContratoModal").modal("show");
	});

	document.querySelectorAll(".editColumnas").forEach(function (element) {
		element.addEventListener("click", function (event) {
			event.preventDefault();

			const {
				dataset: { target, setTarget, cancelTarget },
				parentElement,
			} = event.target;

			document
				.querySelectorAll(`#${target} input[type='checkbox']`)
				.forEach(function (el) {
					el.classList.remove("hidden");
				});

			document.getElementById(setTarget).classList.remove("hidden");
			document.getElementById(cancelTarget).classList.remove("hidden");

			parentElement.classList.add("hidden");
		});
	});

	document.querySelectorAll(".cancelColumnas").forEach(function (element) {
		element.addEventListener("click", function (event) {
			event.preventDefault();

			const {
				dataset: { target, setTarget, editTarget, formTarget },
				parentElement,
			} = event.target;

			document
				.querySelectorAll(`#${target} input[type='checkbox']`)
				.forEach(function (el) {
					el.classList.add("hidden");
				});

			document.getElementById(setTarget).classList.add("hidden");
			parentElement.classList.add("hidden");

			document.getElementById(editTarget).classList.remove("hidden");

			document.getElementById(formTarget).reset();
		});
	});

	document.querySelectorAll(".setColumnas").forEach(function (element) {
		element.addEventListener("click", function (event) {
			event.preventDefault();

			const {
				dataset: { target },
			} = event.target;

			$(`#${target}`).modal("show");
		});
	});

	document.querySelectorAll(".saveColumnFactura").forEach(function (element) {
		element.addEventListener("click", function (event) {
			event.preventDefault();

			let el = event.target;

			$("#" + el.dataset.mdl).modal("hide");

			let tar = "#" + el.dataset.target;
			let target = document.getElementById(el.dataset.target);

			let form = $(tar);

			let tag = document.getElementById("ajax-content");

			$.ajax({
				url: target.getAttribute("action"),
				method: "POST",
				data: form.serialize(),
				dataType: "json",
				success: function (data) {
					console.log(data);

					if (data["status"] == "ok") {
						location.reload();
					} else {
						tag.innerHTML =
							"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

						Object.keys(data.content).forEach(function (key) {
							// Create a <li> node
							let node = document.createElement("LI");
							let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
							node.appendChild(textnode); // Append the text to <li>
							document.getElementById("elist").appendChild(node);
						});

						$("#ajax-response").modal("show");
					}
				},
				error: function (jqXHR, textStatus) {
					tag.innerHTML =
						"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

					$("#ajax-response").modal("show");
				},
			});
		});
	});

	document.querySelectorAll(".saveColumnRatio").forEach(function (element) {
		element.addEventListener("click", function (event) {
			event.preventDefault();

			let el = event.target;
			$("#" + el.dataset.mdl).modal("hide");

			let element = event.target;

			let tar = "#" + el.dataset.target;
			let target = document.getElementById(el.dataset.target);

			let form = $(tar);

			let tag = document.getElementById("ajax-content");

			$.ajax({
				url: target.getAttribute("action"),
				method: "POST",
				data: form.serialize(),
				dataType: "json",
				success: function (data) {
					console.log(data);

					if (data["status"] == "ok") {
						location.reload();
					} else {
						tag.innerHTML =
							"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

						Object.keys(data.content).forEach(function (key) {
							// Create a <li> node
							let node = document.createElement("LI");
							let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
							node.appendChild(textnode); // Append the text to <li>
							document.getElementById("elist").appendChild(node);
						});

						$("#ajax-response").modal("show");
					}
				},
				error: function (jqXHR, textStatus) {
					tag.innerHTML =
						"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

					$("#ajax-response").modal("show");
				},
			});
		});
	});

	$("#uploadXML").click(function (event) {
		$("#xmlFacturaModal").modal("hide");
	});

	$("#save").click(function (event) {
		if (!!document.getElementById("saveFacturaModal"))
			$("#saveFacturaModal").modal("hide");

		if (!!document.getElementById("saveSuministroModal"))
			$("#saveSuministroModal").modal("hide");

		if (!!document.getElementById("saveClienteModal"))
			$("#saveClienteModal").modal("hide");

		if (!!document.getElementById("saveCuentaModal"))
			$("#saveCuentaModal").modal("hide");

		if (!!document.getElementById("savePasswordModal"))
			$("#savePasswordModal").modal("hide");
	});

	$("#save2").click(function (event) {
		if (!!document.getElementById("savePasswordModal"))
			$("#savePasswordModal").modal("hide");

		if (!!document.getElementById("saveAvatarModal"))
			$("#saveAvatarModal").modal("hide");
	});

	$("#saveCliMsg").click(function (event) {
		if (!!document.getElementById("saveClienteMessageModal"))
			$("#saveClienteMessageModal").modal("hide");
	});

	$("#saveCon").click(function (event) {
		if (!!document.getElementById("saveContratoModal"))
			$("#saveContratoModal").modal("hide");
	});

	$("#addaho").click(function (event) {
		event.preventDefault();

		let tag = document.getElementById("aho-box");

		tag.classList.remove("hidden");
	});

	$("#adahoc").click(function (event) {
		event.preventDefault();

		let tag = document.getElementById("aho-box");

		tag.classList.add("hidden");

		document.getElementById("ahorro_form").reset();
	});

	$("#adahos").click(function (event) {
		event.preventDefault();

		$("#addAhorroModal").modal("show");
	});

	$("#addopt").click(function (event) {
		event.preventDefault();

		let tag = document.getElementById("opt-box");

		tag.classList.remove("hidden");
	});

	$("#adoptc").click(function (event) {
		event.preventDefault();

		let tag = document.getElementById("opt-box");

		tag.classList.add("hidden");

		document.getElementById("optimizacion_form").reset();
	});

	$("#adopts").click(function (event) {
		event.preventDefault();

		$("#addOptimizacionModal").modal("show");
	});

	$("#addconsum").click(function (event) {
		event.preventDefault();

		let tag = document.getElementById("consu-box");

		tag.classList.remove("hidden");

		tag.scrollIntoView();
	});

	$("#addconsum2").click(function (event) {
		event.preventDefault();

		let tag = document.getElementById("consu-box");

		tag.classList.remove("hidden");

		tag.scrollIntoView();
	});

	$("#adconsuc").click(function (event) {
		event.preventDefault();

		let tag = document.getElementById("consu-box");

		tag.classList.add("hidden");

		document.getElementById("consu_form").reset();
	});

	$("#adconsus").click(function (event) {
		event.preventDefault();

		$("#addConSumModal").modal("show");
	});

	$("#saveAhorro").click(function (event) {
		if (!!document.getElementById("addAhorroModal"))
			$("#addAhorroModal").modal("hide");
	});

	$("#saveOptimizacion").click(function (event) {
		if (!!document.getElementById("addOptimizacionModal"))
			$("#addOptimizacionModal").modal("hide");
	});

	$("#saveConSum").click(function (event) {
		if (!!document.getElementById("addConSumModal"))
			$("#addConSumModal").modal("hide");
	});

	document.querySelectorAll(".fav-add").forEach(function (element) {
		element.addEventListener("click", function () {
			event.preventDefault();

			document
				.getElementById("addFavorito")
				.setAttribute("data-target", element.getAttribute("data-target"));
			$("#addFavoritoModal").modal("show");
		});
	});

	document.querySelectorAll(".fav-remove").forEach(function (element) {
		element.addEventListener("click", function () {
			event.preventDefault();

			document
				.getElementById("deleteFavorito")
				.setAttribute("data-target", element.getAttribute("data-target"));
			$("#deleteFavoritoModal").modal("show");
		});
	});

	document.querySelectorAll(".mostrar").forEach(function (element) {
		element.addEventListener("click", function () {
			let target = "#" + element.getAttribute("data-tar");
			let target_c = "#" + element.getAttribute("data-tar-c");
			let target_i = "#" + element.getAttribute("data-tar-i");
			let target_f = "#" + element.getAttribute("data-tar-f");

			document.querySelectorAll(target_i + " input").forEach(function (item) {
				item.removeAttribute("disabled");
			});

			document.querySelector(target + " a.mostrar").classList.add("hidden");

			document.querySelector(target + " a.ocultar").classList.remove("hidden");

			document.querySelector(target_f).classList.remove("hidden");

			document.querySelector(target_c).classList.remove("hidden");

			document.querySelector(target_i).classList.remove("hidden");

			document.querySelector(target_i + "o").classList.remove("hidden");
		});
	});

	document.querySelectorAll(".ocultar").forEach(function (element) {
		element.addEventListener("click", function () {
			let target = "#" + element.getAttribute("data-tar");
			let target_c = "#" + element.getAttribute("data-tar-c");
			let target_i = "#" + element.getAttribute("data-tar-i");
			let target_f = "#" + element.getAttribute("data-tar-f");

			document.querySelectorAll(target_i + " input").forEach(function (item) {
				item.setAttribute("disabled", "disabled");
			});

			document.querySelector(target_f).classList.add("hidden");

			document.querySelector(target_c).classList.add("hidden");

			document.querySelector(target_i).classList.add("hidden");

			document.querySelector(target_i + "o").classList.add("hidden");

			document.querySelector(target + " a.ocultar").classList.add("hidden");

			document.querySelector(target + " a.mostrar").classList.remove("hidden");

			document.querySelector(target + "-form").reset();

			document.querySelector(
				target_i + " .ahorro_in span"
			).innerHTML = document
				.querySelector(target_i + " .ahorro_in")
				.getAttribute("data-value");

			document.querySelector(
				target_i + ' input[name="ahorro"]'
			).value = document
				.querySelector(target_i + " .ahorro_in")
				.getAttribute("data-value");

			document.querySelector(
				target_i + " .optimizacion_in span"
			).innerHTML = document
				.querySelector(target_i + " .optimizacion_in")
				.getAttribute("data-value");

			document.querySelector(
				target_i + ' input[name="optimizacion"]'
			).value = document
				.querySelector(target_i + " .optimizacion_in")
				.getAttribute("data-value");
		});
	});

	document.querySelectorAll(".irat").forEach(function (element) {
		element.addEventListener("click", function () {
			document
				.getElementById("saveIntervencion")
				.setAttribute("data-target", element.getAttribute("data-target"));

			$("#addIntervencionModal").modal("show");
		});
	});

	document.querySelectorAll(".edit").forEach(function (element) {
		element.addEventListener("click", function () {
			let target = "#" + element.getAttribute("data-tar");

			document.querySelectorAll(target + " input").forEach(function (item) {
				item.removeAttribute("disabled");
			});

			document.querySelectorAll(target + " select").forEach(function (item) {
				item.removeAttribute("disabled");
			});

			document.querySelector(target + " a.edit").classList.add("hidden");

			if (!!document.querySelector(target + " a.delete-ahorro"))
				document
					.querySelector(target + " a.delete-ahorro")
					.classList.add("hidden");

			if (!!document.querySelector(target + " a.delete-optimizacion"))
				document
					.querySelector(target + " a.delete-optimizacion")
					.classList.add("hidden");

			if (!!document.querySelector(target + " a.eaho"))
				document.querySelector(target + " a.eaho").classList.remove("hidden");

			if (!!document.querySelector(target + " a.eopt"))
				document.querySelector(target + " a.eopt").classList.remove("hidden");

			if (!!document.querySelector(target + " a.econsu"))
				document.querySelector(target + " a.econsu").classList.remove("hidden");

			document.querySelector(target + " a.cancel").classList.remove("hidden");
		});
	});

	document.querySelectorAll(".cancel").forEach(function (element) {
		element.addEventListener("click", function () {
			let target = "#" + element.getAttribute("data-tar");

			if (!!document.querySelector(target + " a.eaho"))
				document.querySelector(target + " a.eaho").classList.add("hidden");

			if (!!document.querySelector(target + " a.eopt"))
				document.querySelector(target + " a.eopt").classList.add("hidden");

			if (!!document.querySelector(target + " a.econsu"))
				document.querySelector(target + " a.econsu").classList.add("hidden");

			document.querySelector(target + " a.cancel").classList.add("hidden");

			document.querySelector(target + " a.edit").classList.remove("hidden");

			if (!!document.querySelector(target + " a.delete-ahorro"))
				document
					.querySelector(target + " a.delete-ahorro")
					.classList.remove("hidden");

			if (!!document.querySelector(target + " a.delete-optimizacion"))
				document
					.querySelector(target + " a.delete-optimizacion")
					.classList.remove("hidden");

			document.querySelectorAll(target + " input").forEach(function (item) {
				item.setAttribute("disabled", "disabled");
			});

			document.querySelectorAll(target + " select").forEach(function (item) {
				item.setAttribute("disabled", "disabled");
			});

			document.querySelector(target).reset();
		});
	});

	document.querySelectorAll(".eaho").forEach(function (element) {
		element.addEventListener("click", function () {
			document
				.getElementById("editAhorro")
				.setAttribute("data-tar", element.getAttribute("data-tar"));

			$("#editAhorroModal").modal("show");
		});
	});

	document.querySelectorAll(".eopt").forEach(function (element) {
		element.addEventListener("click", function () {
			document
				.getElementById("editOptimizacion")
				.setAttribute("data-tar", element.getAttribute("data-tar"));

			$("#editOptimizacionModal").modal("show");
		});
	});

	document.querySelectorAll(".econsu").forEach(function (element) {
		element.addEventListener("click", function () {
			document
				.getElementById("editConSum")
				.setAttribute("data-tar", element.getAttribute("data-tar"));

			$("#editConSumModal").modal("show");
		});
	});

	document.querySelectorAll(".delete-ahorro").forEach(function (element) {
		element.addEventListener("click", function () {
			document
				.getElementById("deleteAhorro")
				.setAttribute("data-tar", element.getAttribute("data-tar"));

			$("#deleteAhorroModal").modal("show");
		});
	});

	document.querySelectorAll(".delete-optimizacion").forEach(function (element) {
		element.addEventListener("click", function () {
			document
				.getElementById("deleteOptimizacion")
				.setAttribute("data-tar", element.getAttribute("data-tar"));

			$("#deleteOptimizacionModal").modal("show");
		});
	});

	$("#addFavorito").click(function (event) {
		let element = event.target;

		if (!!document.getElementById("addFavoritoModal"))
			$("#addFavoritoModal").modal("hide");

		let tag = document.getElementById("ajax-content");

		// console.log(element.getAttribute("data-target"));
		$.ajax({
			url: element.getAttribute("data-url"),
			method: "POST",
			data: { token: element.getAttribute("data-target") },
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					location.reload();
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});

					$("#ajax-response").modal("show");
				}
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#deleteFavorito").click(function (event) {
		let element = event.target;

		let tag = document.getElementById("ajax-content");

		if (!!document.getElementById("deleteFavoritoModal"))
			$("#deleteFavoritoModal").modal("hide");

		// console.log(element.getAttribute("data-target"));
		$.ajax({
			url: element.getAttribute("data-url"),
			method: "POST",
			data: { token: element.getAttribute("data-target") },
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					location.reload();
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});

					$("#ajax-response").modal("show");
				}
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#saveIntervencion").click(function (event) {
		$("#addIntervencionModal").modal("hide");

		let element = event.target;

		let tar = "#" + element.getAttribute("data-target");
		let target = document.getElementById(element.getAttribute("data-target"));
		let target_c = "#" + element.getAttribute("data-tar-c");
		let target_i = "#" + element.getAttribute("data-tar-i");

		let form = $(tar);

		let tag = document.getElementById("ajax-content");

		// console.log(element);

		$.ajax({
			url: target.getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					location.reload();
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});

					$("#ajax-response").modal("show");
				}
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#editAhorro").click(function (event) {
		$("#editAhorroModal").modal("hide");

		let target = document.getElementById(
			document.getElementById("editAhorro").getAttribute("data-tar")
		);

		let tar =
			"#" + document.getElementById("editAhorro").getAttribute("data-tar");

		let form = $(tar);

		let tag = document.getElementById("ajax-content");

		$.ajax({
			url: target.getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					tag.innerHTML =
						"<div class='callout callout-success'><h5 class='modal-title' id='ajax-response-label'>Exito!</h5><p>Contrato de ahorro modificado correctamente</p></div>";

					document.querySelector(tar + " a.eaho").classList.add("hidden");

					document.querySelector(tar + " a.cancel").classList.add("hidden");

					document.querySelector(tar + " a.edit").classList.remove("hidden");

					document
						.querySelector(tar + " a.delete-ahorro")
						.classList.remove("hidden");

					document.querySelectorAll(tar + " input").forEach(function (item) {
						item.setAttribute("disabled", "disabled");
					});
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});
				}

				$("#ajax-response").modal("show");
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#editOptimizacion").click(function (event) {
		$("#editOptimizacionModal").modal("hide");

		let target = document.getElementById(
			document.getElementById("editOptimizacion").getAttribute("data-tar")
		);

		let tar =
			"#" +
			document.getElementById("editOptimizacion").getAttribute("data-tar");

		let form = $(tar);

		let tag = document.getElementById("ajax-content");

		$.ajax({
			url: target.getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					tag.innerHTML =
						"<div class='callout callout-success'><h5 class='modal-title' id='ajax-response-label'>Exito!</h5><p>Contrato de optimización modificado correctamente</p></div>";

					document.querySelector(tar + " a.eopt").classList.add("hidden");

					document.querySelector(tar + " a.cancel").classList.add("hidden");

					document.querySelector(tar + " a.edit").classList.remove("hidden");

					document
						.querySelector(tar + " a.delete-optimizacion")
						.classList.remove("hidden");

					document.querySelectorAll(tar + " input").forEach(function (item) {
						item.setAttribute("disabled", "disabled");
					});
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});
				}

				$("#ajax-response").modal("show");
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#editConSum").click(function (event) {
		$("#editConSumModal").modal("hide");

		let target = document.getElementById(
			document.getElementById("editConSum").getAttribute("data-tar")
		);

		let tar =
			"#" + document.getElementById("editConSum").getAttribute("data-tar");

		let form = $(tar);

		let tag = document.getElementById("ajax-content");

		$.ajax({
			url: target.getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					tag.innerHTML =
						"<div class='callout callout-success'><h5 class='modal-title' id='ajax-response-label'>Exito!</h5><p>Contrato de suministro modificado correctamente</p></div>";

					document.querySelector(tar + " a.econsu").classList.add("hidden");

					document.querySelector(tar + " a.cancel").classList.add("hidden");

					document.querySelector(tar + " a.edit").classList.remove("hidden");

					document.querySelectorAll(tar + " input").forEach(function (item) {
						item.setAttribute("disabled", "disabled");
					});

					document.querySelectorAll(tar + " select").forEach(function (item) {
						item.setAttribute("disabled", "disabled");
					});
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});
				}

				$("#ajax-response").modal("show");
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#deleteAhorro").click(function (event) {
		$("#deleteAhorroModal").modal("hide");

		let tar = document.getElementById("deleteAhorro").getAttribute("data-tar");

		let target = document.getElementById(tar);

		let tag = document.getElementById("ajax-content");

		$.ajax({
			url: baseUrl("gestion/delete_ahorro"),
			method: "POST",
			data: {
				token: document.querySelector("#" + tar + " input[name='eaho']").value,
			},
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					tag.innerHTML =
						"<div class='callout callout-success'><h5 class='modal-title' id='ajax-response-label'>Exito!</h5><p>Contrato de ahorro eliminado correctamente</p></div>";
					target.remove();
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});
				}

				$("#ajax-response").modal("show");
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#deleteOptimizacion").click(function (event) {
		$("#deleteOptimizacionModal").modal("hide");

		let tar = document
			.getElementById("deleteOptimizacion")
			.getAttribute("data-tar");

		let target = document.getElementById(tar);

		let tag = document.getElementById("ajax-content");

		$.ajax({
			url: baseUrl("gestion/delete_optimizacion"),
			method: "POST",
			data: {
				token: document.querySelector("#" + tar + " input[name='eopt']").value,
			},
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					tag.innerHTML =
						"<div class='callout callout-success'><h5 class='modal-title' id='ajax-response-label'>Exito!</h5><p>Contrato de optimización eliminado correctamente</p></div>";
					target.remove();
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});
				}

				$("#ajax-response").modal("show");
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#deleteFactura").click(function (event) {
		let form = $("#factura_delete");
		//let element = document.getElementById(document.getElementById("token").value);
		let tag = document.getElementById("ajax-content");

		event.preventDefault();

		$("#deleteFacturaModal").modal("hide");

		$.ajax({
			url: document.getElementById("factura_delete").getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					tag.innerHTML =
						"<div class='callout callout-success'><h5 class='modal-title' id='ajax-response-label'>Exito!</h5><p>Factura eliminada correctamente</p></div>";
					//element.parentNode.removeChild(element);

					location.reload();
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});

					$("#ajax-response").modal("show");
				}
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});

		document.getElementById("token").value = "";
	});

	$("#factura-xml-form").submit(function (event) {
		event.preventDefault();

		let form = "#factura_form";

		let tag = document.getElementById("ajax-content");

		let fd = new FormData();

		let fileInputElement = document.getElementById("facturafileXML");

		fd.append("facturafileXML", fileInputElement.files[0]);

		$("html, body").css("cursor", "wait");

		$.ajax({
			url: document.getElementById("factura-xml-form").getAttribute("action"),
			method: "POST",
			data: fd,
			dataType: "json",
			processData: false,
			contentType: false,
			cache: false,
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					tag.innerHTML =
						"<div class='callout callout-success'><h5 class='modal-title' id='ajax-response-label'>Exito!</h5><p>Datos cargados correctamente</p></div>";

					document.getElementById("factura_form").reset();

					Object.keys(data.content).forEach(function (key) {
						switch (data.content[key]["name"]) {
							case "nro_factura":
								document.querySelector(
									form + " input[name='nro_factura']"
								).value = data.content[key]["value"];
								break;
							case "fec_doc":
								document.querySelector(form + " input[name='fec_doc']").value =
									data.content[key]["value"];
								break;
							case "fec_desde":
								document.querySelector(
									form + " input[name='fec_desde']"
								).value = data.content[key]["value"];
								break;
							case "fec_hasta":
								document.querySelector(
									form + " input[name='fec_hasta']"
								).value = data.content[key]["value"];
								break;
							case "fec_prox_lec":
								document.querySelector(
									form + " input[name='fec_prox_lec']"
								).value = data.content[key]["value"];
								break;
							case "ac_lec_ant":
								document.querySelector(
									form + " input[name='ac_lec_ant']"
								).value = data.content[key]["value"];
								break;
							case "ac_lec_act":
								document.querySelector(
									form + " input[name='ac_lec_act']"
								).value = data.content[key]["value"];
								break;
							case "ac_consumo":
								document.querySelector(
									form + " input[name='ac_consumo']"
								).value = data.content[key]["value"];
								document.querySelector(
									form + " input[name='ene_consumo']"
								).value = data.content[key]["value"];
								break;
							case "re_lec_ant":
								document.querySelector(
									form + " input[name='re_lec_ant']"
								).value = data.content[key]["value"];
								break;
							case "re_lec_act":
								document.querySelector(
									form + " input[name='re_lec_act']"
								).value = data.content[key]["value"];
								break;
							case "re_consumo":
								document.querySelector(
									form + " input[name='re_consumo']"
								).value = data.content[key]["value"];
								break;
							case "cargo_fijo":
								document.querySelector(
									form + " input[name='cargo_fijo']"
								).value = data.content[key]["value"];
								break;
							case "cargo_postal":
								document.querySelector(
									form + " input[name='cargo_postal']"
								).value = data.content[key]["value"];
								break;
							case "cargo_trans":
								document.querySelector(
									form + " input[name='cargo_trans']"
								).value = data.content[key]["value"];
								break;
							case "ene_precio":
								document.querySelector(
									form + " input[name='ene_precio']"
								).value = data.content[key]["value"];
								break;
							case "deman_fp_prec":
								document.querySelector(
									form + " input[name='deman_fp_prec']"
								).value = data.content[key]["value"];
								break;
							case "deman_ep_prec":
								document.querySelector(
									form + " input[name='deman_ep_prec']"
								).value = data.content[key]["value"];
								break;
							case "cargo_interes":
								document.querySelector(
									form + " input[name='cargo_interes']"
								).value = data.content[key]["value"];
								break;
							case "factor_potencia":
								document.querySelector(
									form + " input[name='factor_potencia']"
								).value = data.content[key]["value"];
								break;
							case "cargo_arriendo":
								document.querySelector(
									form + " input[name='cargo_arriendo']"
								).value = data.content[key]["value"];
								break;
							case "deman_ep_fact":
								document.querySelector(
									form + " input[name='deman_ep_fact']"
								).value = data.content[key]["value"];
								break;
							case "deman_fp_fact":
								document.querySelector(
									form + " input[name='deman_fp_fact']"
								).value = data.content[key]["value"];
								break;
							case "ep_consumo":
								document.querySelector(
									form + " input[name='ep_consumo']"
								).value = data.content[key]["value"];
								document.querySelector(
									form + " input[name='deman_ep_lec']"
								).value = data.content[key]["value"];
								break;
							case "fp_consumo":
								document.querySelector(
									form + " input[name='fp_consumo']"
								).value = data.content[key]["value"];
								document.querySelector(
									form + " input[name='deman_fp_lec']"
								).value = data.content[key]["value"];
								break;
							case "cargo_otros":
								document.querySelector(
									form + " input[name='cargo_otros']"
								).value = data.content[key]["value"];
								break;
							case "cargo_desc":
								document.querySelector(
									form + " input[name='cargo_desc']"
								).value = data.content[key]["value"];
								break;
							case "monto_exento":
								document.querySelector(
									form + " input[name='monto_exento']"
								).value = data.content[key]["value"];
								break;
							case "cargo_serv_pub":
								document.querySelector(
									form + " input[name='cargo_serv_pub']"
								).value = data.content[key]["value"];
								break;
							case "cargo_otr":
								document.querySelector(
									form + " input[name='cargo_otr']"
								).value = data.content[key]["value"];
								break;
							case "saldo_ant":
								document.querySelector(
									form + " input[name='saldo_ant']"
								).value = data.content[key]["value"];
								break;
							default:
								return false;
						}
						setTotalServ("sumar", "neto_ene");
					});
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});
				}

				$("#ajax-response").modal("show");

				$("html, body").css("cursor", "auto");
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";
				$("#ajax-response").modal("show");
				$("html, body").css("cursor", "auto");
			},
		});
	});

	$("#factura_form").submit(function (event) {
		event.preventDefault();

		let form = document.getElementById("factura_form");

		$("#saveFacturaModal").modal("hide");

		$("#ajax-respond").fadeOut(50);

		const mode = form.elements.namedItem("facturaMode");

		let fd = new FormData();

		let fileInputElement = document.getElementById("facturafile");
		let fileInputElement_gen = document.getElementById("facturafile_gen");
		let fileInputElement_libre = document.getElementById("facturafile_libre");

		if (!!mode) {
			if (mode.value == "simple") {
				fd.append("facturafile", fileInputElement_libre.files[0]);
			}

			if (mode.value == "doble") {
				fd.append("facturafile_gen", fileInputElement_gen.files[0]);
				fd.append("facturafile", fileInputElement.files[0]);
			}

			if (mode.value == "default") {
				fd.append("facturafile", fileInputElement.files[0]);
			}
		} else {
			fd.append("facturafile", fileInputElement.files[0]);
		}

		document
			.querySelectorAll("#factura_form input:not(.facturafile)")
			.forEach(function (element) {
				fd.append(element.name, element.value);
			});

		document
			.querySelectorAll("#factura_form select")
			.forEach(function (element) {
				fd.append(element.name, element.value);
			});

		fd.append("estado_analisis", form.elements.namedItem("estado").value);

		fd.append("estado_alerta", form.elements.namedItem("alerta").value);

		if (!!mode) {
			fd.append("factura_mode", mode.value);
		} else {
			fd.append("factura_mode", "default");
		}

		// console.log(fd);

		$.ajax({
			url: document.getElementById("factura_form").getAttribute("action"),
			method: "POST",
			data: fd,
			dataType: "json",
			processData: false,
			contentType: false,
			cache: false,
			success: function (data) {
				console.log(data);

				let tag = document.getElementById("ajax-respond");

				if (data["status"] == "ok") {
					//tag.innerHTML = "<div class='callout callout-success'><h4>Exito!</h4><p>Factura guardada correctamente</p></div>";
					// if(data["alert"].length > 0) {
					//     tag.innerHTML = "<div class='callout callout-warning'><h4>Alerta!</h4><p>La factura fue añadida, pero no Fue imposible subir el documento adjunto</p></div>";
					//     $("#ajax-respond").fadeIn(400);
					//     document.getElementById("factura_form").reset();
					// } else { }

					document.getElementById("factura_form").reset();
					window.history.back();
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h4>Error!</h4><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});

					$("#ajax-respond").fadeIn(400);
				}
			},
			error: function (jqXHR, textStatus) {
				console.log("Error: " + textStatus);
			},
		});
	});

	$("#avatar_form").submit(function (event) {
		event.preventDefault();

		let form = document.getElementById("avatar_form");

		let fd = new FormData();

		let fileInputElement = document.getElementById("avatarfile");

		let tag = document.getElementById("ajax-content");

		fd.append("avatarfile", fileInputElement.files[0]);

		document
			.querySelectorAll("#avatar_form input:not(#avatarfile)")
			.forEach(function (element) {
				fd.append(element.name, element.value);
			});

		$.ajax({
			url: document.getElementById("avatar_form").getAttribute("action"),
			method: "POST",
			data: fd,
			dataType: "json",
			processData: false,
			contentType: false,
			cache: false,
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					// tag.innerHTML = "<div class='callout callout-success'><h5 class='modal-title' id='ajax-response-label'>Exito!</h5><p>Foto de perfil actualizada correctamente</p></div>";
					location.reload();
					// document.getElementById("password_form").reset();
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});

					$("#ajax-response").modal("show");
				}
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#cuenta_form").submit(function (event) {
		event.preventDefault();

		let form = $(this);
		let tag = document.getElementById("ajax-content");

		$.ajax({
			url: document.getElementById("cuenta_form").getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					tag.innerHTML =
						"<div class='callout callout-success'><h5 class='modal-title' id='ajax-response-label'>Exito!</h5><p>Cuenta actualizada correctamente</p></div>";

					if (data["content"]) {
						if (data["cli_mode"]) {
							location.assign(
								baseUrl("gestion/cliente/?token=" + data["content"])
							);
						} else {
							location.assign(
								baseUrl("cuentas/view/?token=" + data["content"])
							);
						}
					}
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});
				}

				$("#ajax-response").modal("show");
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#password_form").submit(function (event) {
		event.preventDefault();

		let form = $(this);
		let tag = document.getElementById("ajax-content");

		$.ajax({
			url: document.getElementById("password_form").getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					tag.innerHTML =
						"<div class='callout callout-success'><h5 class='modal-title' id='ajax-response-label'>Exito!</h5><p>Contraseña actualizada correctamente</p></div>";

					document.getElementById("password_form").reset();
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});
				}

				$("#ajax-response").modal("show");
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#cliente_form").submit(function (event) {
		event.preventDefault();

		let form = $(this);
		let tag = document.getElementById("ajax-content");

		$.ajax({
			url: document.getElementById("cliente_form").getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					tag.innerHTML =
						"<div class='callout callout-success'><h5 class='modal-title' id='ajax-response-label'>Exito!</h5><p>Cliente actualizado correctamente</p></div>";

					if (data["content"]) {
						location.assign(
							baseUrl("gestion/cliente/?token=" + data["content"])
						);
					}
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});
				}

				$("#ajax-response").modal("show");
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#cliente_message_form").submit(function (event) {
		event.preventDefault();

		let form = $(this);
		let tag = document.getElementById("ajax-content");

		$.ajax({
			url: document
				.getElementById("cliente_message_form")
				.getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					tag.innerHTML =
						"<div class='callout callout-success'><h5 class='modal-title' id='ajax-response-label'>Exito!</h5><p>Mensaje actualizado correctamente</p></div>";

					if (data["content"]) {
						location.assign(
							baseUrl("gestion/cliente/?token=" + data["content"])
						);
					}
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});
				}

				$("#ajax-response").modal("show");
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#suministro_form").submit(function (event) {
		event.preventDefault();

		var form = $(this);

		$("#ajax-respond").fadeOut(50);

		$.ajax({
			url: document.getElementById("suministro_form").getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				let tag = document.getElementById("ajax-respond");

				if (data["status"] == "ok") {
					location.assign(
						baseUrl("gestion/suministro_edit/?token=" + data["content"])
					);
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h4>Error!</h4><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});

					$("#ajax-respond").fadeIn(400);
				}
			},
			error: function (jqXHR, textStatus) {
				console.log("Error: " + textStatus);
			},
		});
	});

	$("#suministro_edit_form").submit(function (event) {
		event.preventDefault();

		let form = $(this);

		let tag = document.getElementById("ajax-content");

		$.ajax({
			url: document
				.getElementById("suministro_edit_form")
				.getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					tag.innerHTML =
						"<div class='callout callout-success'><h5 class='modal-title' id='ajax-response-label'>Exito!</h5><p>Suministro actualizado correctamente</p></div>";
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});
				}

				$("#ajax-response").modal("show");
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#contrato_form").submit(function (event) {
		event.preventDefault();

		let form = $(this);

		let tag = document.getElementById("ajax-content");

		$.ajax({
			url: document.getElementById("contrato_form").getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				let alert = document.getElementById("alert-con");

				if (data["status"] == "ok") {
					tag.innerHTML =
						"<div class='callout callout-success'><h5 class='modal-title' id='ajax-response-label'>Exito!</h5><p>Contrato actualizado correctamente</p></div>";
					if (!!alert) {
						alert.remove();
					}
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});
				}

				$("#ajax-response").modal("show");
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#ahorro_form").submit(function (event) {
		event.preventDefault();

		let form = $(this);

		let tag = document.getElementById("ajax-content");

		$.ajax({
			url: document.getElementById("ahorro_form").getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					location.reload();
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});

					$("#ajax-response").modal("show");
				}
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#optimizacion_form").submit(function (event) {
		event.preventDefault();

		let form = $(this);

		let tag = document.getElementById("ajax-content");

		$.ajax({
			url: document.getElementById("optimizacion_form").getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					location.reload();
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});

					$("#ajax-response").modal("show");
				}
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});

	$("#consu_form").submit(function (event) {
		event.preventDefault();

		let form = $(this);

		let tag = document.getElementById("ajax-content");

		$.ajax({
			url: document.getElementById("consu_form").getAttribute("action"),
			method: "POST",
			data: form.serialize(),
			dataType: "json",
			success: function (data) {
				console.log(data);

				if (data["status"] == "ok") {
					location.reload();
				} else {
					tag.innerHTML =
						"<div id=error class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Error!</h5><ul id=elist></ul></div>";

					Object.keys(data.content).forEach(function (key) {
						// Create a <li> node
						let node = document.createElement("LI");
						let textnode = document.createTextNode(data.content[key]["msg"]); // Create a text node
						node.appendChild(textnode); // Append the text to <li>
						document.getElementById("elist").appendChild(node);
					});

					$("#ajax-response").modal("show");
				}
			},
			error: function (jqXHR, textStatus) {
				tag.innerHTML =
					"<div class='callout callout-danger'><h5 class='modal-title' id='ajax-response-label'>Server Error!</h5><p>Lo sentimos, ocurrio un error inesperado</p></div>";

				$("#ajax-response").modal("show");
			},
		});
	});
});

//// Functions ////

String.prototype.replaceAll = function (search, replacement) {
	var target = this;
	return target.replace(new RegExp(search, "g"), replacement);
};

function setValue(e) {
	const el = e.target;

	document.getElementById(el.dataset.target).value = el.value;
}

function setTotalServ(targetSum, targetResult) {
	let result = 0;
	let cargos = document.querySelectorAll("." + targetSum);

	cargos.forEach(function (item, index) {
		if (item.value.replaceAll(" ", "") == "" || isNaN(item.value)) {
			item.value = 0;
		}
		result += parseInt(item.value);
	});

	let targetEne = "neto_ene";
	let targetExtento = "monto_exento";
	let targetTotalNeto = "total_neto";
	let targetTotal = "total_fact";

	if (targetSum == "sumar_libre") {
		targetEne += "_gen";
		targetExtento += "_gen";
		targetTotalNeto += "_gen";
		targetTotal += "_gen";
	}

	const form = document.getElementById("factura_form");
	const mode = form.elements.namedItem("facturaMode");

	if (!!mode && mode.value == "simple") {
		targetExtento = "monto_exento_libre";
		targetTotalNeto = "total_neto_libre";
		targetTotal = "total_fact_libre";
	}

	document.getElementById(targetResult).value = result;
	setTotalNetoAndFact(targetEne, targetExtento, targetTotalNeto, targetTotal);
}

function setTotalNetoAndFact(
	targetEne,
	targetExtento,
	targetTotalNeto,
	targetTotal
) {
	const form = document.getElementById("factura_form");
	const mode = form.elements.namedItem("facturaMode");

	let exento = parseInt(document.getElementById(targetExtento).value);
	let neto =
		!!mode && mode.value == "simple"
			? parseInt(document.getElementById("neto_ene").value) +
			  parseInt(document.getElementById("neto_ene_gen").value) -
			  exento
			: parseInt(document.getElementById(targetEne).value) - exento;

	document.getElementById(targetTotalNeto).value = neto;

	document.getElementById(targetTotal).value =
		neto + exento + Math.round(neto * 0.19);

	document.getElementById("conf-neto").innerHTML = new Intl.NumberFormat(
		"de-DE"
	).format(neto);

	let targetOtros = "cargo_otr";
	let targetSaldo = "saldo_ant";
	let targetPagar = "total_pagar";

	if (targetEne == "neto_ene_gen") {
		targetOtros += "_gen";
		targetSaldo += "_gen";
		targetPagar += "_gen";
	}

	if (!!mode && mode.value == "simple") {
		targetOtros = "cargo_otr_libre";
		targetSaldo = "saldo_ant_libre";
		targetPagar = "total_pagar_libre";
	}

	setTotalPagar(targetTotal, targetOtros, targetSaldo, targetPagar);
}

function setTotalPagar(targetTotal, targetOtros, targetSaldo, targetPagar) {
	let total =
		parseInt(document.getElementById(targetTotal).value) +
		parseInt(document.getElementById(targetOtros).value) +
		parseInt(document.getElementById(targetSaldo).value);

	document.getElementById(targetPagar).value = total;

	document.getElementById("conf-tot").innerHTML = new Intl.NumberFormat(
		"de-DE"
	).format(total);

	const form = document.getElementById("factura_form");
	const mode = form.elements.namedItem("facturaMode");

	if (!!mode && mode.value == "doble") {
		setResumenLibre();
	}
}

function setResumenLibre() {
	const exento = parseInt(document.getElementById("monto_exento").value);
	const neto = parseInt(document.getElementById("total_neto").value);
	const factura = parseInt(document.getElementById("total_fact").value);
	const otros = parseInt(document.getElementById("cargo_otr").value);
	const anterior = parseInt(document.getElementById("saldo_ant").value);
	const pagar = parseInt(document.getElementById("total_pagar").value);

	const exento_gen = parseInt(
		document.getElementById("monto_exento_gen").value
	);
	const neto_gen = parseInt(document.getElementById("total_neto_gen").value);
	const factura_gen = parseInt(document.getElementById("total_fact_gen").value);
	const otros_gen = parseInt(document.getElementById("cargo_otr_gen").value);
	const anterior_gen = parseInt(document.getElementById("saldo_ant_gen").value);
	const pagar_gen = parseInt(document.getElementById("total_pagar_gen").value);

	const exento_libre = exento + exento_gen;
	const neto_libre = neto + neto_gen;
	const factura_libre = factura + factura_gen;
	const otros_libre = otros + otros_gen;
	const anterior_libre = anterior + anterior_gen;
	const pagar_libre = pagar + pagar_gen;

	document.getElementById("monto_exento_libre").value = exento_libre;
	document.getElementById("total_neto_libre").value = neto_libre;
	document.getElementById("total_fact_libre").value = factura_libre;
	document.getElementById("cargo_otr_libre").value = otros_libre;
	document.getElementById("saldo_ant_libre").value = anterior_libre;
	document.getElementById("total_pagar_libre").value = pagar_libre;
}

function setRatio(event) {
	let target = "#" + event.target.getAttribute("data-target");

	if (
		event.target.value.replaceAll(" ", "") == "" ||
		isNaN(event.target.value)
	) {
		event.target.value = 0;
	}

	// console.log(target);
	let energia = parseFloat(
		document.querySelector(target + ' input[name="energia"]').value
	);
	let fp = parseFloat(
		document.querySelector(target + ' input[name="fp"]').value
	);
	let ep = parseFloat(
		document.querySelector(target + ' input[name="ep"]').value
	);
	const cp = parseFloat(
		document.querySelector(target + ' input[name="cp"]').value
	);
	let produccion = parseFloat(
		document.querySelector(target + ' input[name="produccion"]').value
	);
	let indicador = parseFloat(
		document.querySelector(target + ' input[name="indicador"]').value
	);
	let fp_base = parseFloat(
		document.querySelector(target + ' input[name="fp_base"]').value
	);
	let ep_base = parseFloat(
		document.querySelector(target + ' input[name="ep_base"]').value
	);
	let indicador_base = parseFloat(
		document.querySelector(target + ' input[name="indicador_base"]').value
	);
	let varios_base = parseFloat(
		document.querySelector(target + ' input[name="varios_base"]').value
	);
	let fp_mes = parseFloat(
		document.querySelector(target + ' input[name="fp_mes"]').value
	);
	let ep_mes = parseFloat(
		document.querySelector(target + ' input[name="ep_mes"]').value
	);
	let varios_mes = parseInt(
		document.querySelector(target + ' input[name="varios_mes"]').value
	);
	let transmision = parseInt(
		document.querySelector(target + ' input[name="transmision_mes"]').value
	);
	let tarifa = parseFloat(
		document.querySelector(target + ' input[name="tarifa_mes"]').value
	);
	let cp_mes = parseFloat(
		document.querySelector(target + ' input[name="cp_mes"]').value
	);

	const tipo_tarifa = document.querySelector(
		target + ' input[name="tipo_tarifa"]'
	).value;

	let energia_gen = 0;
	let ep_gen = 0;
	let indicador_gen = 0;
	let ep_mes_gen = 0;
	let tec_r = 0;
	let armo_r = 0;
	let trans_r = 0;

	let tarA = 0;
	let tarB = 0;
	let tarBG = 0;
	let tarC = 0;
	let ahorro = 0;
	let optimizacion = 0;
	let ahorro_gen = 0;
	let optimizacion_gen = 0;
	let ahorro_dis = 0;
	let optimizacion_dis = 0;

	if (tipo_tarifa == "21") {
		energia_gen = parseFloat(
			document.querySelector(target + ' input[name="energia_gen"]').value
		);

		ep_gen = parseFloat(
			document.querySelector(target + ' input[name="ep_gen"]').value
		);

		indicador_gen = parseFloat(
			document.querySelector(target + ' input[name="indicador_gen"]').value
		);

		tec_r = parseFloat(
			document.querySelector(target + ' input[name="tec_r"]').value
		);

		armo_r = parseFloat(
			document.querySelector(target + ' input[name="armo_r"]').value
		);

		trans_r = parseFloat(
			document.querySelector(target + ' input[name="trans_r"]').value
		);

		tarA = (fp_base - fp_mes) * fp;
		tarB = (ep_base - ep_mes) * ep;
		tarBG = (ep_base - ep_mes_gen) * ep_gen;
		tarC = varios_base - varios_mes;

		ahorro_gen = tarBG;
		ahorro_dis = tarA + tarB + tarC;
		ahorro = Math.round(ahorro_gen + ahorro_dis);

		optimizacion_gen =
			(indicador_base - indicador_gen) *
			(energia_gen + tec_r + armo_r) *
			produccion;
		optimizacion_dis =
			(indicador_base - indicador) * (energia_gen + trans_r) * produccion;
		optimizacion = Math.round(optimizacion_gen + optimizacion_dis);

		document.querySelector(
			target + " .ahorro_in_gen span"
		).innerHTML = ahorro_gen;
		document.querySelector(
			target + ' input[name="ahorro_gen"]'
		).value = ahorro_gen;

		document.querySelector(
			target + " .ahorro_in_dis span"
		).innerHTML = ahorro_dis;
		document.querySelector(
			target + ' input[name="ahorro_dis"]'
		).value = ahorro_dis;

		document.querySelector(
			target + " .optimizacion_in_gen span"
		).innerHTML = optimizacion_gen;
		document.querySelector(
			target + ' input[name="optimizacion_gen"]'
		).value = optimizacion_gen;

		document.querySelector(
			target + " .optimizacion_in_dis span"
		).innerHTML = optimizacion_dis;
		document.querySelector(
			target + ' input[name="optimizacion_dis"]'
		).value = optimizacion_dis;
	} else {
		let mes = document.querySelector(target + ' input[name="mes"]').value;
		let etapa = document.querySelector(target + ' input[name="etapa"]').value;

		tarA = (fp_base - fp_mes) * fp;
		tarB =
			etapa == "2" ? ep_base * tarifa - ep_mes * ep : (ep_base - ep_mes) * ep;

		if (tipo_tarifa == "20") {
			tarB += cp_mes * cp;
		}

		tarC = varios_base - varios_mes;

		ahorro = Math.round(tarA + tarB + tarC);
		optimizacion = (indicador_base - indicador) * produccion * energia;

		mes = new Date(mes + "-01");

		let mes_c = new Date("2018-04-01");

		if (mes.getTime() >= mes_c.getTime()) {
			optimizacion += (indicador_base - indicador) * transmision;
		}

		optimizacion = Math.round(optimizacion);
	}

	// console.log(target + ' input[name="optimizacion"]');

	document.querySelector(target + " .ahorro_in span").innerHTML = ahorro;
	document.querySelector(target + ' input[name="ahorro"]').value = ahorro;

	document.querySelector(
		target + " .optimizacion_in span"
	).innerHTML = optimizacion;
	document.querySelector(
		target + ' input[name="optimizacion"]'
	).value = optimizacion;
}

function baseUrl(path) {
	return location.protocol + "//" + location.host + "/" + path;
}
