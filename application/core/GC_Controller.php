<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GC_Controller extends CI_Controller {
    protected $user;

    protected $is_auth;
    protected $auth_lvl;
    protected $redirect;

    public $data;

    public function __construct() {
        parent::__construct();
        $this->is_login();
        $this->data = new stdClass();

        if ($this->is_auth) {
            $this->load->model('User');
            $this->auth_lvl = $this->User->chek_permissions($this->encryption->decrypt($this->session->userdata('user')));
            $this->set_user();
            $this->data->user = $this->user;
			$this->data->auth = TRUE;
		}
    }

    protected function set_redirect($page = null, $action = null, $params = array()) {
        if(!empty($page)) {
            $this->redirect = $page;

            if(!empty($action)) {
                $this->redirect .= '/';
                $this->redirect .= $action;
            } 

            if(!empty($params)) {
                $this->redirect .= '/?';

                $i =  1;
                $lenght = count($params);

                foreach($params as $param) {
                    $this->redirect .= $param;

                    if($i < $lenght)
                        $this->redirect .= '&';


                    $i++;
                }
            }

            $this->redirect = base_url($this->redirect);
        } else {
            $this->redirect = base_url();
        }

        $this->redirect = base64_encode($this->redirect);
    }

    protected function _load_layout($template)
    {
        $this->load->view('layout/header');
        $this->load->view($template);
        $this->load->view('layout/footer');
    }

    protected function get_user() {
        return $this->user;
    }

    private function set_user() {
        $this->user = $this->User->get_user($this->encryption->decrypt($this->session->userdata('user')));

        $this->user->nombre = ucwords($this->user->nombre);
        $this->user->apellido = ucwords($this->user->apellido);
        $this->user->perfil = ucwords($this->user->perfil);
        $this->user->id = base64_encode($this->encryption->encrypt($this->user->id));
    }

    private function is_login()
    {
        $this->is_auth = $this->session->has_userdata('user');
    }
}