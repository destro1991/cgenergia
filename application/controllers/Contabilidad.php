<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contabilidad extends GC_Controller {
    private $cliente;
    private $clientes;
    private $suministros;

    private $mes_selected;
    private $contrato_selected;

    private $page;
    private $action;
    private $error;

    public function __construct(){
        parent::__construct();
        $this->page = "contabilidad";

        $this->load->model('Suministro');
        $this->load->model('Cliente');
    }

    public function index() {
        
        $this->action = "index";

        if($this->is_auth && ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2)) {

            $this->error = FALSE;

            $token = $this->encryption->decrypt(base64_decode($this->input->get('q')));
            $token = $this->security->xss_clean($token);

            try {

                $this->clientes = $this->Cliente->get_clientes_short();

                foreach($this->clientes as $cliente) {
                    $cliente->selected = $cliente->id == $token;
                    $cliente->id       = base64_encode($this->encryption->encrypt($cliente->id));
                }

            } catch(Exception $e) {
                $this->error = TRUE;
            }

            
            if(! empty($token)) {
                
                try{
                    $anno = $this->security->xss_clean($this->input->get('anno'));
                    $mes = $this->security->xss_clean($this->input->get('mes'));
                    $contrato = $this->security->xss_clean($this->input->get('contrato'));

                    $this->mes_selected = !empty($mes) ? $mes : 'ALL';

                    $this->contrato_selected = !empty($contrato) ? $contrato : 'ALL';

                    if(! empty($anno)) {

                        $this->suministros = empty($mes) 
                                             || $mes == 'ALL' ? $this->Suministro->get_suministros_cliente_improved_anual($token, $anno, $this->contrato_selected) : 
                                                                $this->Suministro->get_suministros_cliente_improved_mensual($token, ($anno.'-'.$mes), $this->contrato_selected);
    
                        $total_ahorro       = 0;
                        $total_optimizacion = 0;
                        $total_total        = 0;
                        $total_comision     = 0;
                        
                        $i = 1; 
                        foreach ($this->suministros as $suministro) {
                            $suministro->indice       = $i;

                            $suministro->id           = base64_encode($this->encryption->encrypt($suministro->id));
                            
                            $suministro->ahorro       = $suministro->ahorro < 0 ? 0 : $suministro->ahorro ;
                            
                            $suministro->optimizacion = $suministro->optimizacion < 0 ? 0 : $suministro->optimizacion;
            
                            $suministro->total        = $suministro->ahorro + $suministro->optimizacion;

                            $suministro->comision     = is_numeric($suministro->pct) ? round($suministro->total*($suministro->pct/100)) : 0;

                            $total_ahorro += $suministro->ahorro;
                    
                            $total_optimizacion += $suministro->optimizacion;
                    
                            $total_total += $suministro->total;
                        
                            $total_comision += $suministro->comision;
                            
                            $suministro->pct          = is_numeric($suministro->pct) ? number_format($suministro->pct, 0, ',', '.') . '%' : '-';

                            $suministro->comision     = is_numeric($suministro->comision) ? '$' . number_format($suministro->comision, 0, ',', '.') : '$' . 0;

                            $suministro->total        = is_numeric($suministro->total) ? '$' . number_format($suministro->total, 0, ',', '.') : '$' . 0;

                            $suministro->optimizacion = is_numeric($suministro->optimizacion) ? '$' . number_format($suministro->optimizacion, 0, ',', '.') : '$' . 0;
                        
                            $suministro->ahorro       = is_numeric($suministro->ahorro) ? '$' . number_format($suministro->ahorro, 0, ',', '.') : '$' . 0;
                            
                            $i++;
            
                        }

                        $total_ahorro       = is_numeric($total_ahorro)       ? '$' . number_format($total_ahorro, 0, ',', '.') : 0;
                        $total_optimizacion = is_numeric($total_optimizacion) ? '$' . number_format($total_optimizacion, 0, ',', '.') : 0;
                        $total_total        = is_numeric($total_total)        ? '$' . number_format($total_total, 0, ',', '.') : 0;
                        $total_comision     = is_numeric($total_comision)     ? '$' . number_format($total_comision, 0, ',', '.') : 0;

                        $this->data->total_ahorro       = $total_ahorro;
                        $this->data->total_optimizacion = $total_optimizacion;
                        $this->data->total_total        = $total_total;
                        $this->data->total_comision     = $total_comision;

                    } else {
                        $this->suministros = FALSE;
                    }

                }catch(Excepcion $e) {
                    $this->error = TRUE;
                }

            } else {
                $this->suministros = FALSE;
            }

            $this->data->filtrado = FALSE;

        } else {
            
            $this->set_redirect($this->page, $this->action);

            redirect(base_url('login/?redirect=' . $this->redirect));

        }

        if (!$this->error) {
            $annos = [];
            $anno_actual = (int) date('Y');
            $anno_plus = $anno_actual;

            $i = 0;
            while($i <= 20) {
                $annos[$i] = $anno_plus;
                $anno_plus--;
                $i++;
            }

            $this->data->annos = $annos;
            $this->data->anno_actual = $anno_actual;

            $this->data->mes_selected = $this->mes_selected;
            $this->data->contrato_selected = $this->contrato_selected;

            $this->data->clientes = $this->clientes;
            $this->data->suministros = $this->suministros;

            $this->data->export = !empty($this->suministros);
            
            $this->data->page = $this->action;

            $this->_load_layout('contabilidad/lista_suministros');

        } else {

            $this->_load_layout('errors/aplication/error_modulo');

        }

    }
}
