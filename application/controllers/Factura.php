<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Factura extends GC_Controller {
    private $page;
    private $action;
    private $error;
    private $datos;

    public function __construct(){
        parent::__construct();
        $this->page = "factura";
    }

    public function index() {
        
        redirect(base_url());

    }

    public function view_factura() {

        $this->action = "view_factura";
        $token = !empty($this->input->get('token')) ? $this->input->get('token') : FALSE;
        $_file = !empty($this->input->get('file')) ? $this->input->get('file') : '1';

        if (!$this->is_auth) {

            if(empty($token)) {
                $this->set_redirect();
            } else {
                $this->set_redirect($this->page, $this->action, array('token=' . $token));
            }

            redirect(base_url('login/?redirect=' . $this->redirect));
            
        }

        $this->error = !($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 3 || $this->auth_lvl->id_perfil == 4);
 
        if ($this->error) {

            $this->_load_layout('errors/aplication/error_modulo');

        } else {

            if(!empty($token)) {
                
                $token=$this->encryption->decrypt(base64_decode($token));

                $this->load->model('Suministro');

                $this->factura = $this->Suministro->get_factura($token);

                if(!empty($this->factura->id)) {
                    $_file_name = $_file == '2' ? $this->factura->archivo_gen : $this->factura->archivo;
                    $file = FCPATH . 'uploads/' . $_file_name;
                    $filename = $this->factura->suministro . '_factura_' . $this->factura->mes . '_' . $_file . '.pdf';
                    header('Content-type: application/pdf');
                    header('Content-Disposition: inline; filename="' . $filename . '"');
                    header('Content-Transfer-Encoding: binary');
                    header('Accept-Ranges: bytes');
                    @readfile($file);

                }

            } else {

                $this->_load_layout('errors/aplication/error_modulo');

            }

        }

    }

    public function load_xml() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);
        $this->datos = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);


        if(!empty($_FILES["facturafileXML"])) {

            if($_FILES['facturafileXML']['type'] == 'text/xml') {

                $xml_file = simplexml_load_file($_FILES['facturafileXML']['tmp_name']);

                // $xml_file=simplexml_load_string($note);

                // echo "type error";

            } else {

                $xml_file = FALSE;

            }
        
        } else {

            $xml_file = FALSE;
            
        }

        if ($xml_file === FALSE) {

            $this->error->append(['code' => 500, 'msg' => 'Error al cargar archivo']);

        } else {  

            $nro_factura = intval($xml_file->Documento[0]->Encabezado[0]->IdDoc[0]->Folio);
            $this->datos->append(['name'=>'nro_factura', 'value' => $nro_factura]);

            $fec_doc = new DateTime($xml_file->Documento[0]->Encabezado[0]->IdDoc[0]->FchEmis);
            $fec_doc = $fec_doc->format('Y-m-d');
            $this->datos->append(['name'=>'fec_doc', 'value' => $fec_doc]);

            $fec_desde = DateTime::createFromFormat('d/m/Y', $xml_file->DatosMedidor[0]->DatMed[0]->FecLecAnt);
            $fec_desde = $fec_desde->format('Y-m-d');
            $this->datos->append(['name'=>'fec_desde', 'value' => $fec_desde]);

            $fec_hasta = DateTime::createFromFormat('d/m/Y', $xml_file->DatosMedidor[0]->DatMed[0]->FecLecActual);
            $fec_hasta = $fec_hasta->format('Y-m-d');
            $this->datos->append(['name'=>'fec_hasta', 'value' => $fec_hasta]);

            $fec_prox_lec = $xml_file->DatosConsumoAdicional[0]->DatConsAdic[0]->Valor;
            $fec_prox_lec_mes = substr($fec_prox_lec, 3, 3);
            $fec_prox_lec = str_replace($fec_prox_lec_mes, $this->month_replace($fec_prox_lec_mes), $fec_prox_lec);
            $fec_prox_lec = DateTime::createFromFormat('d/m/Y', $fec_prox_lec);
            $fec_prox_lec = $fec_prox_lec->format('Y-m-d');
            $this->datos->append(['name'=>'fec_prox_lec', 'value' => $fec_prox_lec]);

            $ac_lec_ant = $xml_file->DatosMedidor[0]->DatMed[0]->LecAnt;
            $ac_lec_ant = preg_match( '/^[0-9]+(\.[0-9]{1,})?$/', $ac_lec_ant) ? $ac_lec_ant : 0;
            $this->datos->append(['name'=>'ac_lec_ant', 'value' => intval($ac_lec_ant)]);

            $ac_lec_act = $xml_file->DatosMedidor[0]->DatMed[0]->LecActual;
            $ac_lec_act = preg_match( '/^[0-9]+(\.[0-9]{1,})?$/', $ac_lec_act) ? $ac_lec_act : 0;
            $this->datos->append(['name'=>'ac_lec_act', 'value' => intval($ac_lec_act)]);

            $ac_consumo = $xml_file->DatosMedidor[0]->DatMed[0]->Consumo;
            $this->datos->append(['name'=>'ac_consumo', 'value' => intval($ac_consumo)]);

            $re_lec_ant = $xml_file->DatosMedidor[0]->DatMed[1]->LecAnt;
            $re_lec_ant = preg_match( '/^[0-9]+(\.[0-9]{1,})?$/', $re_lec_ant) ? $re_lec_ant : 0;
            $this->datos->append(['name'=>'re_lec_ant', 'value' => intval($re_lec_ant)]);

            $re_lec_act = $xml_file->DatosMedidor[0]->DatMed[1]->LecActual;
            $re_lec_act = preg_match( '/^[0-9]+(\.[0-9]{1,})?$/', $re_lec_act) ? $re_lec_act : 0;
            $this->datos->append(['name'=>'re_lec_act', 'value' => intval($re_lec_act)]);

            $re_consumo = $xml_file->DatosMedidor[0]->DatMed[1]->Consumo;
            $this->datos->append(['name'=>'re_consumo', 'value' => intval($re_consumo)]);

            $name = FALSE;
            $otros = 0;
            $descuentos = 0;
            
            $this->datos->append(['name'=>'cargo_otros', 'value' => $otros]);
            $this->datos->append(['name'=>'cargo_desc', 'value' => $descuentos]);

            foreach ($xml_file->Detalle2[0]->children() as $child) {

                if(isset($child->CdgItem[0])) {

                    if($child->CdgItem[0]->VlrCodigo == 1000060 || $child->CdgItem[0]->VlrCodigo == 2000060) {

                        $name = 'cargo_fijo';
                        
                    } elseif($child->CdgItem[0]->VlrCodigo == 100610) {

                        $name = 'cargo_postal';
                        
                    } elseif($child->CdgItem[0]->VlrCodigo == 1000033 || $child->CdgItem[0]->VlrCodigo == 2000033) {

                        $name = 'cargo_trans';

                    } elseif($child->CdgItem[0]->VlrCodigo == 1000114) {

                        $name = 'cargo_serv_pub';
                        
                    } elseif($child->CdgItem[0]->VlrCodigo == 1000032 || $child->CdgItem[0]->VlrCodigo == 2000032) {

                        $name = 'ene_precio';
                        
                    } elseif($child->CdgItem[0]->VlrCodigo == 1000044) {

                        $name = 'deman_fp_prec';
                        $deman_fp_fact = $child->NmbItem[0];
                        
                    } elseif($child->CdgItem[0]->VlrCodigo == 1000047 || $child->CdgItem[0]->VlrCodigo == 2000042) {

                        $name = 'deman_ep_prec';
                        $deman_ep_fact = $child->NmbItem[0];

                    } elseif($child->CdgItem[0]->VlrCodigo == 1000070) {

                        $name = 'factor_potencia';

                    } elseif($child->CdgItem[0]->VlrCodigo == 1000050) {

                        $name = 'cargo_arriendo';
                        
                    } elseif($child->CdgItem[0]->VlrCodigo == 400020) {

                        $name = 'cargo_interes';

                    } elseif($child->CdgItem[0]->VlrCodigo == '500C210' || $child->CdgItem[0]->VlrCodigo == '500C110') {

                        $name = 'cargo_desc';

                    } elseif($child->CdgItem[0]->VlrCodigo == 650010 || $child->CdgItem[0]->VlrCodigo == 100630 || $child->CdgItem[0]->VlrCodigo == 4004620 || $child->CdgItem[0]->VlrCodigo == 0) {

                        continue;

                    } else {

                        if(intval($child->MontoItem) >= 0 && $child->CdgItem[0]->VlrCodigo != '100C032') {

                            $name = 'cargo_otros';

                        } else {

                            $name = 'cargo_desc';

                        }

                    }

                    if(!empty($name)) {

                        if($name == 'cargo_otros') {

                            $otros += intval($child->MontoItem);
                            $this->datos->{11}['value'] = $otros;

                        } elseif ($name == 'cargo_desc') {

                            if(($child->CdgItem[0]->VlrCodigo == '100C032' || $child->CdgItem[0]->VlrCodigo == '500C210' || $child->CdgItem[0]->VlrCodigo == '500C110') && intval($child->MontoItem) > 0) {

                                $descuentos += (intval($child->MontoItem) * -1);

                            } else {

                                $descuentos += intval($child->MontoItem);

                            }

                            // $descuentos += intval($child->MontoItem);
                            $this->datos->{12}['value'] = $descuentos;

                        } else {

                            $this->datos->append(['name'=>$name, 'value' => intval($child->MontoItem)]);

                        }

                    }

                    $name = FALSE;

                }

            }

            if(!empty($deman_fp_fact)) {

                $deman_fp_fact = str_replace(',', '.', $deman_fp_fact);
                $deman_fp_fact = preg_replace('/[a-zA-zñÑáéíóú\(\)\s]/', '', $deman_fp_fact);
                $this->datos->append(['name'=>'deman_fp_fact', 'value' => floatval($deman_fp_fact)]);

            } else {

                $deman_fp_fact = 0;

            }

            if(!empty($deman_ep_fact)) {

                $deman_ep_fact = str_replace(',', '.', $deman_ep_fact);
                $deman_ep_fact = preg_replace('/[a-zA-zñÑáéíóú\(\)\s]/', '', $deman_ep_fact);
                $this->datos->append(['name'=>'deman_ep_fact', 'value' => floatval($deman_ep_fact)]);

            } else {

                $deman_ep_fact = 0;                

            }

            // echo 'pese por aqui';
            // exit;

            $ep_consumo = $xml_file->DatosConsumoAdicional[0]->DatConsAdic[1]->Valor;
            $ep_consumo  = str_ireplace('kw', '', $ep_consumo);
            $ep_consumo  = str_replace(' ', '', $ep_consumo);
            $ep_consumo  = str_replace('.', '', $ep_consumo);
            $ep_consumo  = str_replace(',', '.', $ep_consumo);
            $ep_consumo  = preg_match( '/^[0-9]+(\.[0-9]{1,})?$/', $ep_consumo) ? $ep_consumo : 0;
            $this->datos->append(['name'=>'ep_consumo', 'value' => floatval($ep_consumo)]);

            $fp_consumo = $xml_file->DatosConsumoAdicional[0]->DatConsAdic[2]->Valor;
            $fp_consumo  = str_ireplace('kw', '', $fp_consumo);
            $fp_consumo  = str_replace(' ', '', $fp_consumo);
            $fp_consumo  = str_replace('.', '', $fp_consumo);
            $fp_consumo  = str_replace(',', '.', $fp_consumo);
            $fp_consumo  = preg_match( '/^[0-9]+(\.[0-9]{1,})?$/', $fp_consumo) ? $fp_consumo : 0;
            $this->datos->append(['name'=>'fp_consumo', 'value' => floatval($fp_consumo)]);

            $monto_exento = intval($xml_file->Documento[0]->Encabezado[0]->Totales[0]->MntExe);
            $this->datos->append(['name'=>'monto_exento', 'value' => intval($monto_exento)]);

            $cargo_otr = intval($xml_file->Documento[0]->Encabezado[0]->Totales[0]->MontoNF);
            $this->datos->append(['name'=>'cargo_otr', 'value' => intval($cargo_otr)]);

            $saldo_ant = intval($xml_file->Documento[0]->Encabezado[0]->Totales[0]->SaldoAnterior);
            $this->datos->append(['name'=>'saldo_ant', 'value' => intval($saldo_ant)]);

            print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "content" => $this->datos)) );

        }       
    }

    private function month_replace($mes) {

        if($mes == 'ENE') {

            $mes = '01';

        } elseif($mes == 'FEB') {

            $mes = '02';

        } elseif($mes == 'MAR') {

            $mes = '03';
            
        } elseif($mes == 'ABR') {

            $mes = '04';
            
        } elseif($mes == 'MAY') {

            $mes = '05';
            
        } elseif($mes == 'JUN') {

            $mes = '06';
            
        } elseif($mes == 'JUL') {

            $mes = '07';
            
        } elseif($mes == 'AGO') {

            $mes = '08';
            
        } elseif($mes == 'SEP') {

            $mes = '09';
            
        } elseif($mes == 'OCT') {

            $mes = '10';
            
        } elseif($mes == 'NOV') {

            $mes = '11';
            
        } elseif($mes == 'DIC') {

            $mes = '12';
            
        }

        return $mes;
    }
}
