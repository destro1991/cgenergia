<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cuentas extends GC_Controller {
    private $cuenta;
    private $cliente;

    private $cuentas;

    private $perfiles;

    private $page;
    private $action;
    private $error;

    public function __construct(){
        parent::__construct();
        $this->page = "cuentas";

        $this->load->model('User');
    }

    public function index() {
        
        $this->action = null;

        if (!$this->is_auth) {

            $this->set_redirect($this->page);

            redirect(base_url('login/?redirect=' . $this->redirect));
            
        }

        $this->error = !($this->auth_lvl->id_perfil == 1);
 
        if ($this->error) {

            $this->_load_layout('errors/aplication/error_modulo');

        } else {

            if(empty($this->input->get('q'))) {

                $this->cuentas = $this->User->get_cuentas();

                $this->data->filtrado = FALSE;

            } else {

                $search = $this->security->xss_clean($this->input->get('q'));

                $this->cuentas = $this->User->get_cuentas_filtrado($search);

                $this->data->filtrado = TRUE;
                
            }

            foreach ($this->cuentas as $cuenta) {
                $cuenta->id = base64_encode($this->encryption->encrypt($cuenta->id));
            }

            $this->data->cuentas =  $this->cuentas;

            $this->data->page = $this->page;

            $this->_load_layout('cuenta/lista_cuentas');
        }
        
    }

    public function view() {

        $this->action = "view";
        $token = !empty($this->input->get('token')) ? $this->input->get('token') : FALSE;

        if (!$this->is_auth) {

            if(empty($token)) {
                $this->set_redirect();
            } else {
                $this->set_redirect($this->page, $this->action, array('token=' . $token));
            }

            redirect(base_url('login/?redirect=' . $this->redirect));
            
        }

        $this->error = !($this->auth_lvl->id_perfil == 1);
 
        if ($this->error) {

            $this->_load_layout('errors/aplication/error_modulo');

        } else {

            if(!empty($token)) {
                
                $token=$this->encryption->decrypt(base64_decode($token));

                $this->cuenta = $this->User->get_user($token);

                if(!empty($this->cuenta)) {
                    $this->cuenta->id = base64_encode($this->encryption->encrypt($this->cuenta->id));

                    $this->data->cuenta = $this->cuenta;

                    $this->data->page = $this->action;

                    $this->set_perfiles(TRUE);

                    $this->data->perfiles = $this->perfiles;

                    $this->_load_layout('cuenta/edit_cuenta');

                } else {

                    $this->_load_layout('errors/aplication/error_modulo');

                }

            } else {

                $this->_load_layout('errors/aplication/error_modulo');

            }
        }

    }

    public function new_account() {

        $this->action = "new_account";

        if (!$this->is_auth) {

            $this->set_redirect($this->page, $this->action);

            redirect(base_url('login/?redirect=' . $this->redirect));
            
        }

        $this->error = !($this->auth_lvl->id_perfil == 1);
 
        if ($this->error) {

            $this->_load_layout('errors/aplication/error_modulo');

        } else {

            $this->set_perfiles(TRUE);

            $this->data->perfiles = $this->perfiles;

            $this->_load_layout('cuenta/new_cuenta');

        }

    }

    public function view_cliente() {

        $this->action = "view_cliente";
        $token = !empty($this->input->get('token')) ? $this->input->get('token') : FALSE;

        if (!$this->is_auth) {

            if(empty($token)) {
                $this->set_redirect();
            } else {
                $this->set_redirect($this->page, $this->action, array('token=' . $token));
            }

            redirect(base_url('login/?redirect=' . $this->redirect));
            
        }

        $this->error = !($this->auth_lvl->id_perfil == 1 || $this->auth_lvl->id_perfil == 2);
 
        if ($this->error) {

            $this->_load_layout('errors/aplication/error_modulo');

        } else {

            if(!empty($token)) {
                
                $token=$this->encryption->decrypt(base64_decode($token));

                $this->cuenta = $this->User->get_user($token);

                if(!empty($this->cuenta)) {

                    $this->load->model('Cliente');

                    $this->cliente = $this->Cliente->get_cliente_user($this->cuenta->id);

                    $this->cliente->id = base64_encode($this->encryption->encrypt($this->cliente->id));

                    $this->data->cliente = !empty($this->cliente) ? $this->cliente : FALSE;

                    $this->cuenta->id = base64_encode($this->encryption->encrypt($this->cuenta->id));
                    
                    $this->data->cuenta = $this->cuenta;

                    $this->data->page = $this->action;

                    $this->_load_layout('cuenta/edit_cuenta_cliente');

                } else {

                    $this->_load_layout('errors/aplication/error_modulo');

                }

            } else {


                $this->_load_layout('errors/aplication/error_modulo');

            }
        }

    }

    public function new_cliente() {

        $this->action = "new_cliente";

        $token = !empty($this->input->get('token')) ? $this->input->get('token') : FALSE;

        if (!$this->is_auth) {

            if(empty($token)) {
                $this->set_redirect();
            } else {
                $this->set_redirect($this->page, $this->action, array('token=' . $token));
            }

            redirect(base_url('login/?redirect=' . $this->redirect));
            
        }

        $this->error = !($this->auth_lvl->id_perfil == 1 || $this->auth_lvl->id_perfil == 2);
 
        if ($this->error) {

            $this->_load_layout('errors/aplication/error_modulo');

        } else {

            if(!empty($token)) { 

                $token=$this->encryption->decrypt(base64_decode($token));

                $this->load->model('Cliente');

                $this->cliente = $this->Cliente->get_cliente_short($token);

                if(!empty($this->cliente)) {
                    $this->cliente->id = base64_encode($this->encryption->encrypt($this->cliente->id));
                    $this->cliente->nombre = strtoupper($this->cliente->nombre);

                    $this->data->cliente = $this->cliente;

                    $this->data->page = $this->action;

                    $this->_load_layout('cuenta/new_cuenta_cliente');

                } else {

                    $this->_load_layout('errors/aplication/error_modulo');

                }

            } else {

                $this->_load_layout('errors/aplication/error_modulo');

            }

        }

    }

    public function cuenta() {

        $this->action = "cuenta";

        if (!$this->is_auth) {

            $this->set_redirect($this->page, $this->action);

            redirect(base_url('login/?redirect=' . $this->redirect));
            
        }

        $this->_load_layout('cuenta/edit_cuenta_self');

    }

    public function settings() {

        $this->action = "cuenta";

        if (!$this->is_auth) {

            $this->set_redirect($this->page, $this->action);

            redirect(base_url('login/?redirect=' . $this->redirect));
            
        }

        $this->_load_layout('cuenta/config_cuenta');

    }

    //////////////////////////////////////////////////////////////////////////////////////////////////

    public function edit() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));
        
        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1) {
                
                $rut = $this->security->xss_clean($this->input->post('rut'));
                $nombre = ucwords($this->security->xss_clean($this->input->post('nombre')));

                if(empty($rut) || empty($nombre)) {

                    $this->error->append(['code' => 101, 
                                        'msg' => 'Indique nombre y rut']);
                    
                } else {
                    
                    $apellido = ucwords($this->security->xss_clean($this->input->post('apellido')));

                    $correo = $this->security->xss_clean($this->input->post('correo'));

                    $perfil = $this->security->xss_clean($this->input->post('perfil'));

                    $perfil = !empty($perfil) ? $perfil : 3;
                    
                    try {

                        $id_cuenta = $this->User->update_cuenta($token, $rut, $nombre, $apellido, $correo, $perfil);

                    } catch(Exception $e) {

                        $this->error->append(['code' => 102, 'msg' => 'Error al actualizar cuenta']);

                    }

                    if(!$id_cuenta) {

                        $this->error->append(['code' => 103, 'msg' => 'Error al actualizar cuenta']);

                    }
                }
            } else {

                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar cuenta']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "content" => FALSE)) );

    }

    public function add() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);
        
        if($this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1) {

                $rut = $this->security->xss_clean($this->input->post('rut'));
                $nombre = ucwords($this->security->xss_clean($this->input->post('nombre')));
                $password = $this->input->post('password');
                $password2 = $this->input->post('password2');

                if(empty($rut) || empty($nombre) || empty($password) || empty($password2)) {

                    $this->error->append(['code' => 101, 
                                        'msg' => 'Indique nombre, rut, y contraseña']);
                    
                } else {

                    if(preg_match_all("/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/", $password)) {

                        if($password === $password2) {

                            $apellido = ucwords($this->security->xss_clean($this->input->post('apellido')));
    
                            $correo = $this->security->xss_clean($this->input->post('correo'));
    
                            $perfil = $this->security->xss_clean($this->input->post('perfil'));

                            $password = md5($password);
                            
                            try {
    
                                $id_cuenta = $this->User->insert_cuenta($rut, $nombre, $apellido, $correo, $perfil, $password);
    
                            } catch(Exception $e) {
    
                                $this->error->append(['code' => 102, 'msg' => 'Error al ingresar cuenta']);
    
                            }
    
                            if(!$id_cuenta) {
    
                                $this->error->append(['code' => 103, 'msg' => 'Error al ingresar cuenta']);
    
                            } else {
    
                                $id_cuenta = base64_encode($this->encryption->encrypt($id_cuenta));
    
                            }
    
                        } else {
    
                            $this->error->append(['code' => 130, 'msg' => 'Las contraseñas no coinciden']);
    
                        }

                    } else {

                        $this->error->append(['code' => 140, 'msg' => 'Las contraseña debe contener como mínimo 8 caracteres, contemplando al menos una letra y un número']);

                    }
                }
            } else {

                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible ingresar cuenta']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "content" => $id_cuenta, "cli_mode" => FALSE)) );

    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    public function add_cliente() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));
        $id_cliente = 0;
        
        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1 || $this->auth_lvl->id_perfil == 2) {

                $rut = $this->security->xss_clean($this->input->post('rut'));
                $nombre = ucwords($this->security->xss_clean($this->input->post('nombre')));
                $password = $this->input->post('password');
                $password2 = $this->input->post('password2');

                if(empty($rut) || empty($nombre) || empty($password) || empty($password2)) {

                    $this->error->append(['code' => 101, 
                                        'msg' => 'Indique nombre, rut, y contraseña']);
                    
                } else {

                    if(preg_match_all("/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/", $password)) {

                        if($password === $password2) {

                            $apellido = ucwords($this->security->xss_clean($this->input->post('apellido')));
    
                            $correo = $this->security->xss_clean($this->input->post('correo'));
    
                            $perfil = 3;

                            $password = md5($password);

                            $this->load->model('Cliente');

                            $this->cliente = $this->Cliente->get_cliente_short($token);

                            if(!empty($this->cliente)) {

                                try {
    
                                    $id_cuenta = $this->User->insert_cuenta($rut, $nombre, $apellido, $correo, $perfil, $password);
        
                                } catch(Exception $e) {
        
                                    $this->error->append(['code' => 102, 'msg' => 'Error al ingresar cuenta']);
        
                                }
        
                                if(!$id_cuenta) {
        
                                    $this->error->append(['code' => 103, 'msg' => 'Error al ingresar cuenta']);
        
                                } else {

                                    $this->Cliente->set_cuenta_cliente($this->cliente->id, $id_cuenta);
                                    $id_cliente = base64_encode($this->encryption->encrypt($this->cliente->id));
        
                                }
            
                            } else {
            
                                $this->error->append(['code' => 150, 'msg' => 'No es posible crear la cuenta']);
            
                            }
    
                        } else {
    
                            $this->error->append(['code' => 130, 'msg' => 'Las contraseñas no coinciden']);
    
                        }

                    } else {

                        $this->error->append(['code' => 140, 'msg' => 'Las contraseña debe contener como mínimo 8 caracteres, contemplando al menos una letra y un número']);

                    }
                }
            } else {

                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible ingresar cuenta']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "content" => $id_cliente, "cli_mode" => TRUE)) );

    }

    public function edit_perfil() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);
        
        if($this->is_auth) {
            $nombre = ucwords($this->security->xss_clean($this->input->post('nombre')));

            if(empty($nombre)) {

                $this->error->append(['code' => 101, 
                                    'msg' => 'Indique nombre y rut']);
                
            } else {
                
                $apellido = ucwords($this->security->xss_clean($this->input->post('apellido')));

                $correo = $this->security->xss_clean($this->input->post('correo'));

                $id_user = $this->encryption->decrypt(base64_decode($this->user->id));
                
                try {

                    $id_cuenta = $this->User->update_perfil($id_user, $nombre, $apellido, $correo);

                } catch(Exception $e) {

                    $this->error->append(['code' => 102, 'msg' => 'Error al actualizar cuenta']);

                }

                if(!$id_cuenta) {

                    $this->error->append(['code' => 103, 'msg' => 'Error al actualizar cuenta']);

                }
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar cuenta']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "content" => FALSE)) );

    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    public function change_password() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));
        
        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1) {
                
                $password = $this->input->post('password');
                $password2 = $this->input->post('password2');

                if(empty($password) || empty($password2)) {

                    $this->error->append(['code' => 101, 
                                        'msg' => 'Indique contraseña']);
                    
                } else {
                    
                    if(preg_match_all("/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/", $password)) {

                        if($password === $password2) {

                            $password = md5($password);
                            
                            try {
    
                                $id_cuenta = $this->User->update_password($token, $password);
    
                            } catch(Exception $e) {
    
                                $this->error->append(['code' => 102, 'msg' => 'Error al actualizar contraseña']);
    
                            }
    
                            if(!$id_cuenta) {
    
                                $this->error->append(['code' => 103, 'msg' => 'Error al actualizar contraseña']);
    
                            }
    
                        } else {
    
                            $this->error->append(['code' => 130, 'msg' => 'Las contraseñas no coinciden']);
    
                        }

                    } else {

                        $this->error->append(['code' => 140, 'msg' => 'Las contraseña debe contener como mínimo 8 caracteres, contemplando al menos una letra y un número']);

                    }
                    
                }
            } else {

                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar contraseña']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "content" => FALSE)) );

    }

    public function change_password_self() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->encryption->decrypt(base64_decode($this->user->id));
        
        if($this->is_auth) {
            
            $password = $this->input->post('password');
            $password2 = $this->input->post('password2');

            if(empty($password) || empty($password2)) {

                $this->error->append(['code' => 101, 
                                    'msg' => 'Indique contraseña']);
                
            } else {
                
                if(preg_match_all("/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/", $password)) {

                    if($password === $password2) {

                        $password = md5($password);
                        
                        try {

                            $id_cuenta = $this->User->update_password($token, $password);

                        } catch(Exception $e) {

                            $this->error->append(['code' => 102, 'msg' => 'Error al actualizar contraseña']);

                        }

                        if(!$id_cuenta) {

                            $this->error->append(['code' => 103, 'msg' => 'Error al actualizar contraseña']);

                        }

                    } else {

                        $this->error->append(['code' => 130, 'msg' => 'Las contraseñas no coinciden']);

                    }

                } else {

                    $this->error->append(['code' => 140, 'msg' => 'Las contraseña debe contener como mínimo 8 caracteres, contemplando al menos una letra y un número']);

                }
                
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar contraseña']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "content" => FALSE)) );

    }

    public function change_avatar() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        if($this->is_auth) {

            if(!empty($_FILES["avatarfile"])) {

                $target_file = str_replace(" ", "_", trim($_FILES["avatarfile"]["name"]));
            
            } else {

                $target_file = "";

            }                    

            if (!empty($target_file)) {

                if($target_file != $this->user->avatar) {

                    $file = $this->do_upload('avatarfile');

                    if($file["status"] == "error") {

                        $this->error->append(['code' => 300, 'msg' => $file["content"]]);
                    
                    } else {

                        $file_name = $file["content"]["file_name"];

                        $id_user = $this->encryption->decrypt(base64_decode($this->user->id));

                        try {
    
                            $id_cuenta = $this->User->update_avatar($id_user, $file_name);

                        } catch(Exception $e) {

                            $this->error->append(['code' => 103, 'msg' => 'La imagen de perfil se actualizo con error']);

                        }

                        if(!$id_cuenta) {

                            $this->error->append(['code' => 104, 'msg' => 'La imagen de perfil se actualizo con error']);

                        }
                        
                    }

                }

            } else {

                $this->error->append(['code' => 101, 'msg' => 'No ha seleccionado archivo']);

            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar imagen de perfil']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "content" => FALSE)) );

    }

    private function do_upload($file) {
        $config['upload_path']          = './assets/img/';
        $config['allowed_types']        = 'jpg|png';
        $config['max_size']             = 4096;
        $config['max_width'] = '1920';
        $config['max_height'] = '1080';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($file)) {

            return array("status" => "error", "content" => $this->upload->display_errors('-','-'));

        } else {

            return array("status" => "ok", "content" => $this->upload->data());

        }
    }

    private function set_perfiles($fix) {

        $this->load->model('Perfil');

        if($fix) {
            $this->perfiles = $this->Perfil->get_all_fix();
        } else {

            $this->perfiles = $this->Perfil->get_all();

        }

    }
}