<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends GC_Controller {
    private $usuario;
    private $error;
    
    public function __construct(){
        parent::__construct();
        $this->error = FALSE;
    }
    
    public function index()
	{
        if (!$this->is_auth) {
            $this->push_errors();

            $this->data->redirect = empty($this->input->get('redirect')) ? base64_encode(base_url()) : $this->input->get('redirect');

            $this->load->view('login');
        } else {
            redirect(base_url());
        }
    }
    
    public function set_session() {
        $rut = $this->security->xss_clean($this->input->post('rut'));
        $password = $this->security->xss_clean($this->input->post('password'));

        if(empty($rut) || empty($password)) {
            $this->error = TRUE;
        } else {
            $this->load->model('User');

            $this->usuario = $this->User->get_user_password($rut, md5($password));

            if(!empty($this->usuario)) {

                $this->session->set_userdata('user', $this->encryption->encrypt($this->usuario->id));  
            } else {
                $this->error = TRUE; 
            }
        }

        $redirect = empty($this->input->post('redirect')) ? base_url() : base64_decode($this->input->post('redirect'));

        if($this->error) {
            $this->push_errors();

            $this->data->redirect = base64_encode($redirect);

            $this->load->view('login');
        } else {

            redirect($redirect);

        }
    }

    public function unset_session() {
        $this->session->unset_userdata('user');
        redirect(base_url());
    }

    public function set_session_ajax() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $rut = $this->security->xss_clean($this->input->post('rut'));
        $password = $this->security->xss_clean($this->input->post('password'));

        if(empty($rut) || empty($password)) {
            $this->error = TRUE;
        } else {
            $this->load->model('User');

            $this->usuario = $this->User->get_user_password($rut, md5($password));

            if(!empty($this->usuario)) {

                $this->session->set_userdata('user', $this->encryption->encrypt($this->usuario->id));  
            } else {
                $this->error->append(['code' => 100, 'msg' => 'Usuario o contraseña no válidos']);
            }
        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "content" => FALSE)) );
    }

    private function push_errors() {
        $this->data->error = $this->error;
    }
}