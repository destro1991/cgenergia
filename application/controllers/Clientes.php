<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends GC_Controller {
    private $cliente;

    private $page;
    private $action;
    private $error;

    public function __construct(){
        parent::__construct();
        $this->page = "clientes";

        $this->load->model('Cliente');
    }

    public function index() {
        
        redirect(base_url());

    }

    public function detail() {

    }

    public function view() {

        $this->action = "view";
        $token = !empty($this->input->get('token')) ? $this->input->get('token') : FALSE;

        if (!$this->is_auth) {

            if(empty($token)) {
                $this->set_redirect();
            } else {
                $this->set_redirect($this->page, $this->action, array('token=' . $token));
            }

            redirect(base_url('login/?redirect=' . $this->redirect));
            
        }

        $this->error = !($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2);
 
        if ($this->error) {

            $this->_load_layout('errors/aplication/error_modulo');

        } else {

            if(!empty($token)) {
                
                $token=$this->encryption->decrypt(base64_decode($token));

                $this->cliente = $this->Cliente->get_cliente($token);

                if(!empty($this->cliente)) {
                    $this->cliente->id = base64_encode($this->encryption->encrypt($this->cliente->id));

                    $this->cliente->nombre = strtoupper($this->cliente->nombre);

                    $this->cliente->message = preg_replace('/\n/', '<br>', $this->cliente->message);

                    $this->data->cliente = $this->cliente;

                    $this->data->page = $this->action;

                    $this->_load_layout('cliente/edit_cliente');

                } else {

                    $this->_load_layout('errors/aplication/error_modulo');

                }

            } else {

                $this->_load_layout('errors/aplication/error_modulo');

            }
        }

    }

    public function new_client() {

        $this->action = "new_client";

        if (!$this->is_auth) {

            $this->set_redirect($this->page, $this->action);

            redirect(base_url('login/?redirect=' . $this->redirect));
            
        }

        $this->error = !($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2);
 
        if ($this->error) {

            $this->_load_layout('errors/aplication/error_modulo');

        } else {

            $this->_load_layout('cliente/new_cliente');
        }

    }

    public function edit() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));
        
        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {
                
                $rut = $this->security->xss_clean($this->input->post('rut'));
                $nombre = $this->security->xss_clean($this->input->post('nombre'));

                if(empty($rut) || empty($nombre)) {

                    $this->error->append(['code' => 101, 
                                        'msg' => 'Indique nombre y rut del cliente']);
                    
                } else {
                    
                    $nombre = strtoupper($nombre);
                    
                    $contacto = $this->security->xss_clean($this->input->post('contacto'));

                    $correo = $this->security->xss_clean($this->input->post('correo'));

                    $telefono = $this->security->xss_clean($this->input->post('telefono'));

                    $web = $this->security->xss_clean($this->input->post('web'));
                    
                    try {

                        $id_cliente = $this->Cliente->update_cliente($token, $rut, $nombre, $contacto, $correo, $telefono, $web);

                    } catch(Exception $e) {

                        $this->error->append(['code' => 102, 'msg' => 'Error al actualizar cliente']);

                    }

                    if(!$id_cliente) {

                        $this->error->append(['code' => 103, 'msg' => 'Error al actualizar cliente']);

                    }
                }
            } else {

                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar cliente']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "content" => FALSE)) );

    }

    public function add() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);
        
        if($this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {

                $rut = $this->security->xss_clean($this->input->post('rut'));
                $nombre = $this->security->xss_clean($this->input->post('nombre'));

                if(empty($rut) || empty($nombre)) {

                    $this->error->append(['code' => 101, 
                                        'msg' => 'Indique nombre y rut del cliente']);
                    
                } else {
                    
                    $nombre = strtoupper($nombre);
                    
                    $contacto = $this->security->xss_clean($this->input->post('contacto'));

                    $correo = $this->security->xss_clean($this->input->post('correo'));

                    $telefono = $this->security->xss_clean($this->input->post('telefono'));

                    $web = $this->security->xss_clean($this->input->post('web'));
                    
                    try {

                        $id_cliente = $this->Cliente->insert_cliente($rut, $nombre, $contacto, $correo, $telefono, $web);

                    } catch(Exception $e) {

                        $this->error->append(['code' => 102, 'msg' => 'Error al ingresar cliente']);

                    }

                    if(!$id_cliente) {

                        $this->error->append(['code' => 103, 'msg' => 'Error al ingresar cliente']);

                    } else {

                        $id_cliente = base64_encode($this->encryption->encrypt($id_cliente));

                    }
                }
            } else {

                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible ingresar cliente']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "content" => $id_cliente)) );

    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public function set_message() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));
        
        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {
                
                //$message = $this->security->xss_clean($this->input->post('message'));
                //$message = preg_replace('/\s\s+/', ' ', $message);
                //$message = preg_replace('/\n\n+/', '/\n/', $message);
                $message = $this->input->post('message');

                if(empty($message)) {

                    $this->error->append(['code' => 101, 
                                        'msg' => 'Indique mensaje para cliente']);
                    
                } else {
                    
                    try {

                        $id_cliente = $this->Cliente->update_cliente_mensaje($token, $message);

                    } catch(Exception $e) {

                        $this->error->append(['code' => 102, 'msg' => 'Error al actualizar mensaje para el cliente']);

                    }

                    if(!$id_cliente) {

                        $this->error->append(['code' => 103, 'msg' => 'Error al actualizar mensaje para el cliente']);

                    }
                }
            } else {

                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar mensaje para el cliente']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "content" => FALSE)) );
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public function change_avatar() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));

        if(!empty($token) && $this->is_auth) {

            if(!empty($_FILES["avatarfile"])) {

                $target_file = str_replace(" ", "_", trim($_FILES["avatarfile"]["name"]));
            
            } else {

                $target_file = "";

            }                    

            if (!empty($target_file)) {
                
                $file_name = $this->security->xss_clean($this->input->post('file_name'));

                if($target_file != $file_name) {

                    $file = $this->do_upload('avatarfile');

                    if($file["status"] == "error") {

                        $this->error->append(['code' => 300, 'msg' => $file["content"]]);
                    
                    } else {

                        $file_name = $file["content"]["file_name"];

                        try {
    
                            $id_cliente = $this->Cliente->update_avatar($token, $file_name);

                        } catch(Exception $e) {

                            $this->error->append(['code' => 103, 'msg' => 'Avatar de cliente se actualizo con error']);

                        }

                        if(!$id_cliente) {

                            $this->error->append(['code' => 104, 'msg' => 'Avatar de cliente se actualizo con error']);

                        }
                        
                    }

                }

            } else {

                $this->error->append(['code' => 101, 'msg' => 'No ha seleccionado archivo']);

            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar avatar']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "content" => FALSE)) );

    }

    private function do_upload($file) {
        $config['upload_path']          = './assets/img/';
        $config['allowed_types']        = 'jpg|png';
        $config['max_size']             = 4096;
        $config['max_width'] = '1920';
        $config['max_height'] = '1080';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($file)) {

            return array("status" => "error", "content" => $this->upload->display_errors('-','-'));

        } else {

            return array("status" => "ok", "content" => $this->upload->data());

        }
    }
}