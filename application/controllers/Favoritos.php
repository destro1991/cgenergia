<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Favoritos extends GC_Controller {
    private $page;
    private $action;
    private $error;

    public function __construct(){
        parent::__construct();
        $this->page = "favoritos";

        $this->load->model('Favorito');
    }

    public function index() {
        
        redirect(base_url());

    }

    public function view() {


    }

    public function edit() {

        
    }

    public function add() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);
        
        if($this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 4) {

                $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));

                if(empty($token)) {

                    $this->error->append(['code' => 101, 
                                        'msg' => 'No es posible realizar la acción']);
                    
                } else {

                    $id_user = $this->encryption->decrypt(base64_decode($this->user->id));
                    
                    try {

                        $id_favorito = $this->Favorito->insert_favorito($id_user, $token);

                    } catch(Exception $e) {

                        $this->error->append(['code' => 102, 'msg' => 'Error al añadir favorito']);

                    }
                }
            } else {

                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible realizar la acción']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );

    }

    public function delete() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);
        
        if($this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 4) {

                $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));

                if(empty($token)) {

                    $this->error->append(['code' => 101, 
                                        'msg' => 'No es posible realizar la acción']);
                    
                } else {

                    $id_user = $this->encryption->decrypt(base64_decode($this->user->id));
                    
                    try {

                        $id_favorito = $this->Favorito->delete_favorito($id_user, $token);

                    } catch(Exception $e) {

                        $this->error->append(['code' => 102, 'msg' => 'Error al eliminar favorito']);

                    }

                    if(!$id_favorito) {

                        $this->error->append(['code' => 103, 'msg' => 'Error al eliminar favorito']);

                    } 
                }
            } else {

                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible realizar la acción']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );

    }

    ////////////////////////////////////////////////////////////////////////////////////////////

}