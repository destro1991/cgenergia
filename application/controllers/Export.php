<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends GC_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Cliente'); 
        $this->load->model('Suministro'); 
    }

    public function suministros(){
        $token = !empty($this->input->get('token')) ? $this->input->get('token') : FALSE;
        $mes   = date('Y-m');

        if(!empty($token)) {

            if($this->is_auth){

                if($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 3 || $this->auth_lvl->id_perfil == 4) {

                    $token = $this->encryption->decrypt(base64_decode($token));

                    $cliente = $this->Cliente->get_cliente_short($token);

                    if(!empty($cliente->id)) {
                        $suministros = $this->Suministro->get_suministros_cliente($cliente->id);

                        if( count($suministros) > 0 ) {

                            //Cargamos la librería de excel.
                            $this->load->library('excel'); $this->excel->setActiveSheetIndex(0);
                            $this->excel->getActiveSheet()->setTitle('Hoja1');
                            
                            //Contador de filas
                            $contador = 1;

                            //Le aplicamos ancho las columnas.
                            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
                            //Le aplicamos negrita a los títulos de la cabecera.
                            $this->excel->getActiveSheet()->getStyle('A' . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                            $this->excel->getActiveSheet()->setCellValue('A' . "{$contador}", 'Suministro');

                            //Le aplicamos ancho las columnas.
                            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                            //Le aplicamos negrita a los títulos de la cabecera.
                            $this->excel->getActiveSheet()->getStyle('B' . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                            $this->excel->getActiveSheet()->setCellValue('B' . "{$contador}", 'Nro. Cliente');

                            //Le aplicamos ancho las columnas.
                            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                            //Le aplicamos negrita a los títulos de la cabecera.
                            $this->excel->getActiveSheet()->getStyle('C' . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                            $this->excel->getActiveSheet()->setCellValue('C' . "{$contador}", 'Ult. Mes');

                            //Le aplicamos ancho las columnas.
                            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                            //Le aplicamos negrita a los títulos de la cabecera.
                            $this->excel->getActiveSheet()->getStyle('D' . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                            $this->excel->getActiveSheet()->setCellValue('D' . "{$contador}", 'Neto ($)');

                            //Le aplicamos ancho las columnas.
                            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                            //Le aplicamos negrita a los títulos de la cabecera.
                            $this->excel->getActiveSheet()->getStyle('E' . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                            $this->excel->getActiveSheet()->setCellValue('E' . "{$contador}", 'Total a Pagar ($)');
                            
                            //Le aplicamos ancho las columnas.
                            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                            //Le aplicamos negrita a los títulos de la cabecera.
                            $this->excel->getActiveSheet()->getStyle('F' . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                            $this->excel->getActiveSheet()->setCellValue('F' . "{$contador}", 'Ahorro ($)');                      
                            
                            //Definimos la data del cuerpo.        
                            foreach( $suministros as $suministro ) {

                                $suministro->last_month = empty($suministro->last_month) ? '-' : $suministro->last_month;
                        
                                $suministro->last_neto  = empty($suministro->last_neto) ? '-' : $suministro->last_neto;
                                // $suministro->last_neto  = number_format($suministro->last_neto, 0, ',', '.');
                                
                                $suministro->last_fact  = empty($suministro->last_fact) ? '-' : $suministro->last_fact;
                                // $suministro->last_fact  = number_format($suministro->last_fact, 0, ',', '.');
                                
                                $suministro->ahorro     = $this->auth_lvl->id_perfil == 3 && $suministro->ahorro < 0 ? 0 : $suministro->ahorro ;
                                // $suministro->ahorro     = number_format($suministro->ahorro, 0, ',', '.');

                                //Incrementamos una fila más, para ir a la siguiente.
                                $contador++;

                                //Informacion de las filas de la consulta.
                                    $this->excel->getActiveSheet()->setCellValue('A' . "{$contador}", $suministro->nombre);

                                    $this->excel->getActiveSheet()->setCellValue('B' . "{$contador}", $suministro->num_cli);

                                    $this->excel->getActiveSheet()->setCellValue('C' . "{$contador}", $suministro->last_month);

                                    $this->excel->getActiveSheet()->setCellValue('D' . "{$contador}", $suministro->last_neto);

                                    $this->excel->getActiveSheet()->setCellValue('E' . "{$contador}", $suministro->last_fact);

                                    $this->excel->getActiveSheet()->setCellValue('F' . "{$contador}", $suministro->ahorro);

                            }
                
                            //Le ponemos un nombre al archivo que se va a generar.
                            $archivo = "suministros_cliente_{$cliente->nombre}_{$mes}.xls";
                            header('Content-Type: application/vnd.ms-excel');
                            header('Content-Disposition: attachment;filename="'.$archivo.'"');
                            header('Cache-Control: max-age=0');
                            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                            
                            //Hacemos una salida al navegador con el archivo Excel.
                            $objWriter->save('php://output');

                        } else {

                            $this->_load_layout('errors/aplication/error_modulo');

                        }

                    } else {
                        $this->_load_layout('errors/aplication/error_modulo');
                    }

                } else {

                    $this->_load_layout('errors/aplication/error_modulo');

                }

            } else {

                $this->_load_layout('errors/aplication/error_modulo');

            }

        } else {

            $this->_load_layout('errors/aplication/error_modulo');

        }
    }

    public function factura() {

        $token = !empty($this->input->get('token')) ? $this->input->get('token') : FALSE;
        $anno = empty($this->security->xss_clean($this->input->get('y'))) ? date("Y") : $this->security->xss_clean($this->input->get('y'));

        if(!empty($token)) {

            if($this->is_auth){

                if($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 3 || $this->auth_lvl->id_perfil == 4) {

                    $token = $this->encryption->decrypt(base64_decode($token));

                    $suministro = $this->Suministro->get_suministro($token, $anno);

                    if(!empty($suministro->id)) {

                        $facturas = $this->Suministro->get_facturas_suministro($token, $anno);

                        if(count($facturas) > 0){
            
                            //Cargamos la librería de excel.
                            $this->load->library('excel'); $this->excel->setActiveSheetIndex(0);
                            $this->excel->getActiveSheet()->setTitle('Hoja1');
                            
                            //Contador de filas
                            $contador = 1;

                            $ascii = 65;

                            if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                $_ascii = $ascii + 1;
                                $char = chr($ascii);
                                $_char = chr($_ascii);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", '');

                                $this->excel->getActiveSheet()->mergeCells($char . "{$contador}" . ":" . $_char . "{$contador}");
                                $ascii = $_ascii + 1;


                                
                                $span = $this->cal_colspan([$suministro->factura_19, $suministro->factura_20], $this->auth_lvl->id_perfil);
                                if($span >= 0) {
                                    $_ascii = $ascii + $span;
                                    $char = chr($ascii);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Generación');
                                    
                                    if($_ascii != $ascii) {
                                        $_char = chr($_ascii);
                                        $this->excel->getActiveSheet()->mergeCells($char . "{$contador}" . ":" . $_char . "{$contador}");
                                    }
                                    $ascii = $_ascii + 1;
                                }


                                
                                $span = $this->cal_colspan([$suministro->factura_3, $suministro->factura_4], $this->auth_lvl->id_perfil);
                                if($span >= 0) {
                                    $_ascii = $ascii + $span;
                                    $char = chr($ascii);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Distribución');
                                    
                                    if($_ascii != $ascii) {
                                        $_char = chr($_ascii);
                                        $this->excel->getActiveSheet()->mergeCells($char . "{$contador}" . ":" . $_char . "{$contador}");
                                    }
                                    $ascii = $_ascii + 1;
                                }



                                $span = $this->cal_colspan([$suministro->factura_21, $suministro->factura_22], $this->auth_lvl->id_perfil);
                                if($span >= 0) {
                                    $_ascii = $ascii + $span;
                                    $char = chr($ascii);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Generación');
                                    
                                    if($_ascii != $ascii) {
                                        $_char = chr($_ascii);
                                        $this->excel->getActiveSheet()->mergeCells($char . "{$contador}" . ":" . $_char . "{$contador}");
                                    }
                                    $ascii = $_ascii + 1;
                                }



                                $span = $this->cal_colspan([$suministro->factura_5, $suministro->factura_6, $suministro->factura_7, $suministro->factura_8, $suministro->factura_17], $this->auth_lvl->id_perfil);
                                if($span >= 0) {
                                    $_ascii = $ascii + $span;
                                    $char = chr($ascii);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Distribución');
                                    
                                    if($_ascii != $ascii) {
                                        $_char = chr($_ascii);
                                        $this->excel->getActiveSheet()->mergeCells($char . "{$contador}" . ":" . $_char . "{$contador}");
                                    }
                                    $ascii = $_ascii + 1;
                                }



                                $span = $this->cal_colspan([$suministro->factura_23], $this->auth_lvl->id_perfil);
                                if($span >= 0) {
                                    $_ascii = $ascii + $span;
                                    $char = chr($ascii);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Generación');
                                    
                                    if($_ascii != $ascii) {
                                        $_char = chr($_ascii);
                                        $this->excel->getActiveSheet()->mergeCells($char . "{$contador}" . ":" . $_char . "{$contador}");
                                    }
                                    $ascii = $_ascii + 1;
                                }



                                $span = $this->cal_colspan([$suministro->factura_9, $suministro->factura_10, $suministro->factura_18], $this->auth_lvl->id_perfil);
                                if($span >= 0) {
                                    $_ascii = $ascii + $span;
                                    $char = chr($ascii);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Distribución');
                                    
                                    if($_ascii != $ascii) {
                                        $_char = chr($_ascii);
                                        $this->excel->getActiveSheet()->mergeCells($char . "{$contador}" . ":" . $_char . "{$contador}");
                                    }
                                    $ascii = $_ascii + 1;
                                }

                                $contador++;

                                $ascii = 65;
                            }                            
                            
                            if($suministro->factura_1 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Mes');
                                $ascii++;
                            }

                            if($suministro->factura_2 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Nro. Fact.');
                                $ascii++;
                            }

                            if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                if($suministro->factura_19 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Csmo. (kWh)');
                                    $ascii++;
                                }

                                if($suministro->factura_20 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Gasto. ($)');
                                    $ascii++;
                                }
                            }

                            if($suministro->factura_3 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Csmo. (kWh)');
                                $ascii++;
                            }

                            if($suministro->factura_4 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Gasto. ($)');
                                $ascii++;
                            }

                            if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                if($suministro->factura_20 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'PLHP (kW)');
                                    $ascii++;
                                }

                                if($suministro->factura_21 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'PFHP (kW)');
                                    $ascii++;
                                }
                            }

                            if($suministro->factura_5 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'PLFP (kW)');
                                $ascii++;
                            }

                            if($suministro->factura_6 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'PFFP (kW)');
                                $ascii++;
                            }

                            if($suministro->factura_7 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'PLHP (kW)');
                                $ascii++;
                            }

                            if($suministro->factura_8 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'PFHP (kW)');
                                $ascii++;
                            }

                            if($suministro->factura_17 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'CPHP (kW)');
                                $ascii++;
                            }

                            if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                if($suministro->factura_23 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'HP ($)');
                                    $ascii++;
                                }
                            }

                            if($suministro->factura_9 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'FP ($)');
                                $ascii++;
                            }

                            if($suministro->factura_10 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'HP ($)');
                                $ascii++;
                            }

                            if($suministro->factura_18 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'CP ($)');
                                $ascii++;
                            }

                            if($suministro->factura_11 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Req. ($)');
                                $ascii++;
                            }


                            if($suministro->factura_12 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Arr/tra ($)');
                                $ascii++;
                            }

                            if($suministro->factura_13 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'MFP ($)');
                                $ascii++;
                            }

                            if($suministro->factura_14 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'OC ($)');
                                $ascii++;
                            }

                            if($suministro->factura_15 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Neto ($)');
                                $ascii++;
                            }

                            if($suministro->factura_16 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Total ($)');
                                $ascii++;
                            }                          
                            
                            //Definimos la data del cuerpo.        
                            foreach($facturas as $factura){
                                $ascii = 65;

                                $factura->arr_tra = $factura->arriendo + $factura->transmision;

                                //Incrementamos una fila más, para ir a la siguiente.
                                $contador++;
                                //Informacion de las filas de la consulta.
                                if($suministro->factura_1 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->mes);
                                    $ascii++;
                                }

                                if($suministro->factura_2 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $value =  $factura->num_fact;

                                    if($factura->tipo_tarifa == 21 && !empty($factura->num_fact_gen) && $factura->num_fact_gen != $factura->num_fact) {
                                        $value = 'G-' . $factura->num_fact_gen . '/D-' . $factura->num_fact;
                                    }
                                    
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $value);
                                    $ascii++;
                                }

                                if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                    if($suministro->factura_19 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->consumo_ac_gen);
                                        $ascii++;
                                    }
    
                                    if($suministro->factura_4 == 20 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->energia_gen);
                                        $ascii++;
                                    }
                                }

                                if($suministro->factura_3 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->consumo_ac);
                                    $ascii++;
                                }

                                if($suministro->factura_4 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->energia);
                                    $ascii++;
                                }

                                if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                    if($suministro->factura_21 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->ep_pot_gen);
                                        $ascii++;
                                    }
    
                                    if($suministro->factura_22 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->ep_deman_fact_gen);
                                        $ascii++;
                                    }
                                }

                                if($suministro->factura_5 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->fp_pot);
                                    $ascii++;
                                }

                                if($suministro->factura_6 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->fp_deman_fact);
                                    $ascii++;
                                }

                                if($suministro->factura_7 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->ep_pot);
                                    $ascii++;
                                }

                                if($suministro->factura_8 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->ep_deman_fact);
                                    $ascii++;
                                }

                                if($suministro->factura_17 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->cp_consumo);
                                    $ascii++;
                                }

                                if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                    if($suministro->factura_23 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->ep_deman_prec_gen);
                                        $ascii++;
                                    }
                                }

                                if($suministro->factura_9 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->fp_deman_prec);
                                    $ascii++;
                                }

                                if($suministro->factura_10 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->ep_deman_prec);
                                    $ascii++;
                                }

                                if($suministro->factura_18 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->cp_precio);
                                    $ascii++;
                                }

                                if($suministro->factura_11 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->reliquidaciones);
                                    $ascii++;
                                }

                                if($suministro->factura_12 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->arr_tra);
                                    $ascii++;
                                }

                                if($suministro->factura_13 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->recargo_factor);
                                    $ascii++;
                                }

                                if($suministro->factura_14 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->otros_cargos);
                                    $ascii++;
                                }

                                if($suministro->factura_15 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->tot_neto);
                                    $ascii++;
                                }

                                if($suministro->factura_16 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $factura->tot_fac);
                                    $ascii++;
                                }
                            }
                
                            //Le ponemos un nombre al archivo que se va a generar.
                            $archivo = "facturas_suministro_{$suministro->nombre}_{$anno}.xls";
                            header('Content-Type: application/vnd.ms-excel');
                            header('Content-Disposition: attachment;filename="'.$archivo.'"');
                            header('Cache-Control: max-age=0');
                            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                            
                            //Hacemos una salida al navegador con el archivo Excel.
                            $objWriter->save('php://output');
                
                        }else{
                
                            $this->_load_layout('errors/aplication/error_modulo');
                
                        }

                    } else {

                        $this->_load_layout('errors/aplication/error_modulo');

                    }

                } else {

                    $this->_load_layout('errors/aplication/error_modulo');

                }

            } else {

                $this->_load_layout('errors/aplication/error_modulo');

            }

        } else {

            $this->_load_layout('errors/aplication/error_modulo');

        }
        
    }

    public function ratio() {
        $token = !empty($this->input->get('token')) ? $this->input->get('token') : FALSE;
        $anno = empty($this->security->xss_clean($this->input->get('y'))) ? date("Y") : $this->security->xss_clean($this->input->get('y'));

        if(!empty($token)) {

            if($this->is_auth){

                if($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 3 || $this->auth_lvl->id_perfil == 4) {

                    $token = $this->encryption->decrypt(base64_decode($token));

                    $suministro = $this->Suministro->get_suministro($token, $anno);

                    if(!empty($suministro->id)) {

                        $ratios = $this->Suministro->get_ratios($token, $anno);

                        if(count($ratios) > 0){

                            //Cargamos la librería de excel.
                            $this->load->library('excel'); $this->excel->setActiveSheetIndex(0);
                            $this->excel->getActiveSheet()->setTitle('Hoja1');
                            
                            //Contador de filas
                            $contador = 1;

                            $ascii = 65;
                            
                            if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                $char = chr($ascii);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", '');
                                $ascii++;


                                
                                $span = $this->cal_colspan([$suministro->ratio_11, $suministro->ratio_12, $suministro->ratio_13, $suministro->ratio_20, $suministro->ratio_21], $this->auth_lvl->id_perfil);
                                if($span >= 0) {
                                    $_ascii = $ascii + $span;
                                    $char = chr($ascii);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Generación');
                                    
                                    if($_ascii != $ascii) {
                                        $_char = chr($_ascii);
                                        $this->excel->getActiveSheet()->mergeCells($char . "{$contador}" . ":" . $_char . "{$contador}");
                                    }
                                    $ascii = $_ascii + 1;
                                }


                                
                                $span = $this->cal_colspan([$suministro->ratio_2, $suministro->ratio_3, $suministro->ratio_4, $suministro->ratio_10, $suministro->ratio_5, $suministro->ratio_19], $this->auth_lvl->id_perfil);
                                if($span >= 0) {
                                    $_ascii = $ascii + $span;
                                    $char = chr($ascii);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Distribución');
                                    
                                    if($_ascii != $ascii) {
                                        $_char = chr($_ascii);
                                        $this->excel->getActiveSheet()->mergeCells($char . "{$contador}" . ":" . $_char . "{$contador}");
                                    }
                                    $ascii = $_ascii + 1;
                                }



                                $span = $this->cal_colspan([$suministro->ratio_6], $this->auth_lvl->id_perfil);
                                if($span >= 0) {
                                    $_ascii = $ascii + $span;
                                    $char = chr($ascii);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", '');
                                    
                                    if($_ascii != $ascii) {
                                        $_char = chr($_ascii);
                                        $this->excel->getActiveSheet()->mergeCells($char . "{$contador}" . ":" . $_char . "{$contador}");
                                    }
                                    $ascii = $_ascii + 1;
                                }



                                $span = $this->cal_colspan([$suministro->ratio_14, $suministro->ratio_16, $suministro->ratio_18], $this->auth_lvl->id_perfil);
                                if($span >= 0) {
                                    $_ascii = $ascii + $span;
                                    $char = chr($ascii);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Generación');
                                    
                                    if($_ascii != $ascii) {
                                        $_char = chr($_ascii);
                                        $this->excel->getActiveSheet()->mergeCells($char . "{$contador}" . ":" . $_char . "{$contador}");
                                    }
                                    $ascii = $_ascii + 1;
                                }



                                $span = $this->cal_colspan([$suministro->ratio_7, $suministro->ratio_15,$suministro->ratio_17], $this->auth_lvl->id_perfil);
                                if($span >= 0) {
                                    $_ascii = $ascii + $span;
                                    $char = chr($ascii);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Distribución');
                                    
                                    if($_ascii != $ascii) {
                                        $_char = chr($_ascii);
                                        $this->excel->getActiveSheet()->mergeCells($char . "{$contador}" . ":" . $_char . "{$contador}");
                                    }
                                    $ascii = $_ascii + 1;
                                }

                                $contador++;

                                $ascii = 65;
                            }
                            
                            if($suministro->ratio_1 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Mes');
                                $ascii++;
                            }

                            if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                if($suministro->ratio_11 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Energía ($/kWh)');
                                    $ascii++;
                                }

                                if($suministro->ratio_21 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Mínimo Técnico ($/kWh)');
                                    $ascii++;
                                }

                                if($suministro->ratio_20 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Armonización ($/kWh)');
                                    $ascii++;
                                }

                                if($suministro->ratio_12 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Hora Punta ($/kWh)');
                                    $ascii++;
                                }

                                if($suministro->ratio_13 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                    //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Monómico ($/kWh)');
                                    $ascii++;
                                }
                            }

                            if($suministro->ratio_2 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Energía ($/kWh)');
                                $ascii++;
                            }

                            if(trim(strtolower ($suministro->tarifa)) == 'cliente libre'){
                                if($suministro->ratio_19 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                     //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Transmisión ($/kWh)');
                                    $ascii++;
                                }
                            }

                            if($suministro->ratio_3 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Fuera de Punta ($/kW)');
                                $ascii++;
                            }

                            if($suministro->ratio_4 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Hora Punta ($/kW)');
                                $ascii++;
                            }

                            if($suministro->ratio_10 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Compra Potencia ($/kW)');
                                $ascii++;
                            }

                            if($suministro->ratio_5 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Monómico ($/kWh)');
                                $ascii++;
                            }

                            if($suministro->ratio_6 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Proyectado (kWh)');
                                $ascii++;
                            }

                            if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                if($suministro->ratio_14 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                     //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Indicador (kWh/día)');
                                    $ascii++;
                                }

                                if($suministro->ratio_16 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                     //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Ahorro ($)');
                                    $ascii++;
                                }

                                if($suministro->ratio_18 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                     //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Optimización ($)');
                                    $ascii++;
                                }
                            }

                            if($suministro->ratio_7 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Indicador (kWh/día)');
                                $ascii++;
                            }

                            if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                if($suministro->ratio_15 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                     //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Ahorro ($)');
                                    $ascii++;
                                }
    
                                if($suministro->ratio_17 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $char = chr($ascii);
                                    //Le aplicamos ancho las columnas.
                                    $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                    //Le aplicamos negrita a los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                     //Definimos los títulos de la cabecera.
                                    $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Optimización ($)');
                                    $ascii++;
                                }
                            }

                            if($suministro->ratio_8 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Tot. Ahorro ($)');
                                $ascii++;
                            }

                            if($suministro->ratio_9 == 1 || $this->auth_lvl->id_perfil != 3) {
                                $char = chr($ascii);
                                //Le aplicamos ancho las columnas.
                                $this->excel->getActiveSheet()->getColumnDimension($char)->setWidth(20);
                                //Le aplicamos negrita a los títulos de la cabecera.
                                $this->excel->getActiveSheet()->getStyle($char . "{$contador}")->getFont()->setBold(true);
                                 //Definimos los títulos de la cabecera.
                                $this->excel->getActiveSheet()->setCellValue($char . "{$contador}", 'Tot. Optimización ($)');
                                $ascii++;
                            }                          
                            
                            //Definimos la data del cuerpo.        
                            foreach($ratios as $ratio){
                                $ascii = 65;
                                
                                $ratio->mod = (((int) substr($ratio->mes, 0, 4)) > 2018 ) && !empty($ratio->energia_in) && (($ratio->energia != $ratio->energia_in) ||  
                                            ($ratio->f_punta != $ratio->f_punta_in) ||
                                            ($ratio->e_punta != $ratio->e_punta_in) ||
                                            ($ratio->monomico != $ratio->monomico_in) ||
                                            ($ratio->produccion != $ratio->produccion_in) ||
                                            ($ratio->indicador != $ratio->indicador_in) ||
                                            ($ratio->ahorro != $ratio->ahorro_in) ||
                                            ($ratio->optimizacion != $ratio->optimizacion_in) || 
                                            ($ratio->cp_ratio != $ratio->cp_in));

                                if(!$ratio->mod) {
                                    
                                    if($this->auth_lvl->id_perfil == 3) {
                                        $ratio->ahorro = $ratio->ahorro < 0 ? 0 : $ratio->ahorro;
                                        $ratio->optimizacion = $ratio->optimizacion < 0 ? 0 : $ratio->optimizacion;
                                        
                                        if(trim(strtolower ($suministro->tarifa)) == 'cliente libre'){
                                            //*CLIENTE LIBRE
                                            $ratio->ahorro_gen = $ratio->ahorro_gen < 0 ? 0 : $ratio->ahorro_gen;
                                            $ratio->optimizacion_gen = $ratio->optimizacion_gen < 0 ? 0 : $ratio->optimizacion_gen;
                                            
                                            //*CLIENTE LIBRE
                                            $ratio->ahorro_dis = $ratio->ahorro_dis < 0 ? 0 : $ratio->ahorro_dis;
                                            $ratio->optimizacion_dis = $ratio->optimizacion_dis < 0 ? 0 : $ratio->optimizacion_dis;
                                        }
                                    }

                                    if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                        //*CLIENTE LIBRE
                                        $ratio->energia_str_gen = number_format($ratio->energia_gen, 2, ',', '.');
                                        $ratio->e_punta_str_gen = number_format($ratio->e_punta_gen, 2, ',', '.');
                                        $ratio->monomico_str_gen = number_format($ratio->monomico_gen, 2, ',', '.');
                                        $ratio->indicador_str_gen = number_format($ratio->indicador_gen, 2, ',', '.');
                                        $ratio->ahorro_str_gen = number_format($ratio->ahorro_gen, 0, ',', '.');
                                        $ratio->optimizacion_str_gen = number_format($ratio->optimizacion_gen, 0, ',', '.');
                                        $ratio->ahorro_str_dis = number_format($ratio->ahorro_dis, 0, ',', '.');
                                        $ratio->optimizacion_str_dis = number_format($ratio->optimizacion_dis, 0, ',', '.');
                                        $ratio->trans_ratio_str = number_format($ratio->trans_ratio, 0, ',', '.');
                                        $ratio->armo_ratio_str = number_format($ratio->armo_ratio, 0, ',', '.');
                                        $ratio->tec_ratio_str = number_format($ratio->tec_ratio, 0, ',', '.');
                                    }

                                    $ratio->energia_str = number_format($ratio->energia, 2, ',', '');
                                    $ratio->f_punta_str = number_format($ratio->f_punta, 2, ',', ''); 
                                    $ratio->e_punta_str = number_format($ratio->e_punta, 2, ',', ''); 
                                    $ratio->monomico_str = number_format($ratio->monomico, 2, ',', '');
                                    $ratio->produccion_str = number_format($ratio->produccion, 0, ',', '');
                                    $ratio->indicador_str = number_format($ratio->indicador, 2, ',', '');
                                    $ratio->ahorro_str = number_format($ratio->ahorro, 0, ',', '');
                                    $ratio->optimizacion_str = number_format($ratio->optimizacion, 0, ',', '');
                                    $ratio->cp_ratio_str = number_format($ratio->cp_ratio, 0, ',', '');

                                } else {

                                    if($this->auth_lvl->id_perfil == 3) {
                                        $ratio->ahorro_in = $ratio->ahorro_in < 0 ? 0 : $ratio->ahorro_in;
                                        $ratio->optimizacion_in = $ratio->optimizacion_in < 0 ? 0 : $ratio->optimizacion_in;

                                        if(trim(strtolower ($suministro->tarifa)) == 'cliente libre'){
                                            //*CLIENTE LIBRE
                                            $ratio->ahorro_in_gen = $ratio->ahorro_in_gen < 0 ? 0 : $ratio->ahorro_in_gen;
                                            $ratio->optimizacion_in_gen = $ratio->optimizacion_in_gen < 0 ? 0 : $ratio->optimizacion_in_gen;
                                            
                                            //*CLIENTE LIBRE
                                            $ratio->ahorro_in_dis = $ratio->ahorro_in_dis < 0 ? 0 : $ratio->ahorro_in_dis;
                                            $ratio->optimizacion_in_dis = $ratio->optimizacion_in_dis < 0 ? 0 : $ratio->optimizacion_in_dis;
                                        }
                                    }

                                    if(trim(strtolower ($suministro->tarifa)) == 'cliente libre'){
                                        //*CLIENTE LIBRE
                                        $ratio->energia_str_gen = number_format($ratio->energia_in_gen, 2, ',', '.');
                                        $ratio->e_punta_str_gen = number_format($ratio->e_punta_in_gen, 2, ',', '.');
                                        $ratio->monomico_str_gen = number_format($ratio->monomico_in_gen, 2, ',', '.');
                                        $ratio->indicador_str_gen = number_format($ratio->indicador_in_gen, 2, ',', '.');
                                        $ratio->ahorro_str_gen = number_format($ratio->ahorro_in_gen, 0, ',', '.');
                                        $ratio->optimizacion_str_gen = number_format($ratio->optimizacion_in_gen, 0, ',', '.');
                                        $ratio->ahorro_str_dis = number_format($ratio->ahorro_in_dis, 0, ',', '.');
                                        $ratio->optimizacion_str_dis = number_format($ratio->optimizacion_in_dis, 0, ',', '.');
                                        $ratio->trans_ratio_str = number_format($ratio->trans_ratio_in, 0, ',', '.');
                                        $ratio->armo_ratio_str = number_format($ratio->armo_ratio_in, 0, ',', '.');
                                        $ratio->tec_ratio_str = number_format($ratio->tec_ratio_in, 0, ',', '.');
                                    }

                                    $ratio->energia_str = number_format($ratio->energia_in, 2, ',', '');
                                    $ratio->f_punta_str = number_format($ratio->f_punta_in, 2, ',', ''); 
                                    $ratio->e_punta_str = number_format($ratio->e_punta_in, 2, ',', ''); 
                                    $ratio->monomico_str = number_format($ratio->monomico_in, 2, ',', '');
                                    $ratio->produccion_str = number_format($ratio->produccion_in, 0, ',', '');
                                    $ratio->indicador_str = number_format($ratio->indicador_in, 2, ',', '');
                                    $ratio->ahorro_str = number_format($ratio->ahorro_in, 0, ',', '');
                                    $ratio->optimizacion_str = number_format($ratio->optimizacion_in, 0, ',', '');
                                    $ratio->cp_ratio_str = number_format($ratio->cp_in, 0, ',', '');
                                }

                                //Incrementamos una fila más, para ir a la siguiente.
                                $contador++;
                                //Informacion de las filas de la consulta.
                                if($suministro->ratio_1 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->mes);
                                    $ascii++;
                                }

                                if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                    if($suministro->ratio_11 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->energia_str_gen);
                                        $ascii++;
                                    }

                                    if($suministro->ratio_21 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->tec_ratio_str);
                                        $ascii++;
                                    }

                                    if($suministro->ratio_20 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->armo_ratio_str);
                                        $ascii++;
                                    }

                                    if($suministro->ratio_12 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->e_punta_str_gen);
                                        $ascii++;
                                    }

                                    if($suministro->ratio_13 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->monomico_str);
                                        $ascii++;
                                    }
                                }

                                if($suministro->ratio_2 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->energia_str);
                                    $ascii++;
                                }

                                if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                    if($suministro->ratio_19 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->trans_ratio_str);
                                        $ascii++;
                                    }
                                }

                                if($suministro->ratio_3 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->f_punta_str);
                                    $ascii++;
                                }

                                if($suministro->ratio_4 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->e_punta_str);
                                    $ascii++;
                                }

                                if($suministro->ratio_10 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->cp_ratio_str);
                                    $ascii++;
                                }

                                if($suministro->ratio_5 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->monomico_str);
                                    $ascii++;
                                }

                                if($suministro->ratio_6 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->produccion_str);
                                    $ascii++;
                                }

                                if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {
                                    if($suministro->ratio_14 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->indicador_str_gen);
                                        $ascii++;
                                    }

                                    if($suministro->ratio_16 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->ahorro_str_gen);
                                        $ascii++;
                                    }

                                    if($suministro->ratio_18 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->optimizacion_str_gen);
                                        $ascii++;
                                    }
                                }

                                if($suministro->ratio_7 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->indicador_str);
                                    $ascii++;
                                }

                                if(trim(strtolower ($suministro->tarifa)) == 'cliente libre') {

                                    if($suministro->ratio_15 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->ahorro_str_dis);
                                        $ascii++;
                                    }

                                    if($suministro->ratio_17 == 1 || $this->auth_lvl->id_perfil != 3) {
                                        $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->optimizacion_str_dis);
                                        $ascii++;
                                    }
                                }

                                if($suministro->ratio_8 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->ahorro_str);
                                    $ascii++;
                                }

                                if($suministro->ratio_9 == 1 || $this->auth_lvl->id_perfil != 3) {
                                    $this->excel->getActiveSheet()->setCellValue(chr($ascii) . "{$contador}", $ratio->optimizacion_str);
                                    $ascii++;
                                }
                            }
                
                            //Le ponemos un nombre al archivo que se va a generar.
                            $archivo = "ratios_suministro_{$suministro->nombre}_{$anno}.xls";
                            header('Content-Type: application/vnd.ms-excel');
                            header('Content-Disposition: attachment;filename="'.$archivo.'"');
                            header('Cache-Control: max-age=0');
                            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                            
                            //Hacemos una salida al navegador con el archivo Excel.
                            $objWriter->save('php://output');
                
                        }else{
                
                            $this->_load_layout('errors/aplication/error_modulo');
                
                        }

                    } else {

                        $this->_load_layout('errors/aplication/error_modulo');

                    }

                } else {

                    $this->_load_layout('errors/aplication/error_modulo');

                }

            } else {

                $this->_load_layout('errors/aplication/error_modulo');

            }

        } else {

            $this->_load_layout('errors/aplication/error_modulo');

        }
    }

    public function suministros_contabilidad() {
        $token = $this->encryption->decrypt(base64_decode($this->input->get('q')));
        $token = $this->security->xss_clean($token);

        $anno = $this->security->xss_clean($this->input->get('anno'));
        $mes = $this->security->xss_clean($this->input->get('mes'));
        $contrato = $this->security->xss_clean($this->input->get('contrato'));

        if(! empty($token) && ! empty($anno) && ! empty($mes) && ! empty($contrato)) {

            if($this->is_auth){

                if($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {

                    $cliente = $this->Cliente->get_cliente_short($token);

                    if(! empty($cliente->id)) {

                        $suministros = $mes == 'ALL' ? $this->Suministro->get_suministros_cliente_improved_anual($token, $anno, $contrato) : 
                                                   $this->Suministro->get_suministros_cliente_improved_mensual($token, ($anno.'-'.$mes), $contrato);

                        if( count($suministros) > 0 ) {

                            //Cargamos la librería de excel.
                            $this->load->library('excel'); $this->excel->setActiveSheetIndex(0);
                            $this->excel->getActiveSheet()->setTitle('Hoja1');
                            
                            //Contador de filas
                            $contador = 1;

                            //Le aplicamos ancho las columnas.
                            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
                            //Le aplicamos negrita a los títulos de la cabecera.
                            $this->excel->getActiveSheet()->getStyle('A' . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                            $this->excel->getActiveSheet()->setCellValue('A' . "{$contador}", 'N°');

                            //Le aplicamos ancho las columnas.
                            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                            //Le aplicamos negrita a los títulos de la cabecera.
                            $this->excel->getActiveSheet()->getStyle('B' . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                            $this->excel->getActiveSheet()->setCellValue('B' . "{$contador}", 'Entidad');

                            //Le aplicamos ancho las columnas.
                            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
                            //Le aplicamos negrita a los títulos de la cabecera.
                            $this->excel->getActiveSheet()->getStyle('C' . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                            $this->excel->getActiveSheet()->setCellValue('C' . "{$contador}", 'Suministro');

                            //Le aplicamos ancho las columnas.
                            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                            //Le aplicamos negrita a los títulos de la cabecera.
                            $this->excel->getActiveSheet()->getStyle('D' . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                            $this->excel->getActiveSheet()->setCellValue('D' . "{$contador}", '% Ahorro');

                            //Le aplicamos ancho las columnas.
                            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                            //Le aplicamos negrita a los títulos de la cabecera.
                            $this->excel->getActiveSheet()->getStyle('E' . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                            $this->excel->getActiveSheet()->setCellValue('E' . "{$contador}", 'Ahorro');
                            
                            //Le aplicamos ancho las columnas.
                            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                            //Le aplicamos negrita a los títulos de la cabecera.
                            $this->excel->getActiveSheet()->getStyle('F' . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                            $this->excel->getActiveSheet()->setCellValue('F' . "{$contador}", 'Optimización ($)');
                            
                            //Le aplicamos ancho las columnas.
                            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
                            //Le aplicamos negrita a los títulos de la cabecera.
                            $this->excel->getActiveSheet()->getStyle('G' . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                            $this->excel->getActiveSheet()->setCellValue('G' . "{$contador}", 'Total Ahorro');

                            //Le aplicamos ancho las columnas.
                            $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
                            //Le aplicamos negrita a los títulos de la cabecera.
                            $this->excel->getActiveSheet()->getStyle('H' . "{$contador}")->getFont()->setBold(true);
                                //Definimos los títulos de la cabecera.
                            $this->excel->getActiveSheet()->setCellValue('H' . "{$contador}", 'Comisión');
                            
                            //Definimos la data del cuerpo.        
                            foreach( $suministros as $suministro ) {

                                $suministro->indice       = $contador;

                                $suministro->id           = base64_encode($this->encryption->encrypt($suministro->id));
                                
                                $suministro->ahorro       = $suministro->ahorro < 0 ? 0 : $suministro->ahorro ;
                                
                                $suministro->optimizacion = $suministro->optimizacion < 0 ? 0: $suministro->optimizacion;
                
                                $suministro->total        = $suministro->ahorro + $suministro->optimizacion;

                                $suministro->comision     = is_numeric($suministro->pct) ? round($suministro->total*($suministro->pct/100)) : 0;

                                $suministro->pct          = is_numeric($suministro->pct) ? number_format($suministro->pct, 0, ',', '.') . '%' : '-';

                                $suministro->comision     = is_numeric($suministro->comision) ? $suministro->comision : 0;

                                $suministro->total        = is_numeric($suministro->total) ? $suministro->total : 0;

                                $suministro->optimizacion = is_numeric($suministro->optimizacion) ? $suministro->optimizacion : 0;
                            
                                $suministro->ahorro       = is_numeric($suministro->ahorro) ? $suministro->ahorro : 0;

                                //Incrementamos una fila más, para ir a la siguiente.
                                $contador++;

                                //Informacion de las filas de la consulta.
                                $this->excel->getActiveSheet()->setCellValue('A' . "{$contador}", $suministro->indice);

                                $this->excel->getActiveSheet()->setCellValue('B' . "{$contador}", $suministro->nombre_cli);

                                $this->excel->getActiveSheet()->setCellValue('C' . "{$contador}", $suministro->nombre);

                                $this->excel->getActiveSheet()->setCellValue('D' . "{$contador}", $suministro->pct);

                                $this->excel->getActiveSheet()->setCellValue('E' . "{$contador}", $suministro->ahorro);

                                $this->excel->getActiveSheet()->setCellValue('F' . "{$contador}", $suministro->optimizacion);

                                $this->excel->getActiveSheet()->setCellValue('G' . "{$contador}", $suministro->total);

                                $this->excel->getActiveSheet()->setCellValue('H' . "{$contador}", $suministro->comision);

                            }
                
                            //Le ponemos un nombre al archivo que se va a generar.
                            $archivo = "contabilidad_{$cliente->nombre}_{$anno}_{$mes}_{$contrato}.xls";
                            header('Content-Type: application/vnd.ms-excel');
                            header('Content-Disposition: attachment;filename="'.$archivo.'"');
                            header('Cache-Control: max-age=0');
                            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                            
                            //Hacemos una salida al navegador con el archivo Excel.
                            $objWriter->save('php://output');

                        } else {

                            $this->_load_layout('errors/aplication/error_modulo');

                        }

                    } else {

                        $this->_load_layout('errors/aplication/error_modulo');

                    }

                } else {

                    $this->_load_layout('errors/aplication/error_modulo');

                }

            } else {

                $this->_load_layout('errors/aplication/error_modulo');

            }

        } else {

            $this->_load_layout('errors/aplication/error_modulo');

        }
    }

    private function cal_colspan(array $props = [], int $auth_lvl) {
        if($auth_lvl != 3) {
            return count($props) - 1;
        }

        $i = 0;
        foreach($props as $value) {
            if($value == 1) $i++;
        }

        return $i - 1;
    }
}