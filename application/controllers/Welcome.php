<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends GC_Controller {
	private $estadisticas;

	private $cliente;

	public function index()
	{
		$this->data->auth = $this->is_auth;

		if($this->is_auth) {

			$anno = date('Y');

			$user = $this->encryption->decrypt(base64_decode($this->user->id));

			$this->data->anno = $anno;

			$this->load->model('Cliente');

			if($this->auth_lvl->id_perfil != 3) {

				$this->clientes = $this->Cliente->get_clientes_favoritos($user, $anno);

				foreach ($this->clientes as $cliente) {

					$cliente->ahorro = (empty($cliente->ahorro)) ? 0 : $cliente->ahorro; 
					$cliente->optimizacion =  (empty($cliente->optimizacion))  ? 0 : $cliente->optimizacion;
					$cliente->neto =  empty($cliente->neto) ? 0 : $cliente->neto;

					$cliente->gestion = $cliente->ahorro + $cliente->optimizacion;
					$cliente->id = base64_encode($this->encryption->encrypt($cliente->id));
					$cliente->nombre = strtoupper($cliente->nombre);
					$cliente->ahorro = number_format($cliente->ahorro, 0, ',', '.');
					$cliente->optimizacion = number_format($cliente->optimizacion, 0, ',', '.');
					$cliente->neto = number_format($cliente->neto, 0, ',', '.');
					$cliente->gestion = number_format($cliente->gestion, 0, ',', '.');
					$cliente->total = number_format($cliente->total, 0, ',', '.');

				}

				$this->load->model('Estadistica');

				$this->estadisticas = $this->Estadistica->get_generales();

				$this->estadisticas->clientes = number_format($this->estadisticas->clientes, 0, ',', '.');

				$this->estadisticas->suministros = number_format($this->estadisticas->suministros, 0, ',', '.');

				$this->estadisticas->ahorro = number_format($this->estadisticas->ahorro, 0, ',', '.');

				$this->estadisticas->optimizacion = number_format($this->estadisticas->optimizacion, 0, ',', '.');

				$this->data->clientes = !empty($this->clientes) ? $this->clientes : FALSE ;

				$this->data->estadisticas = $this->estadisticas;

				$this->_load_layout('home');

			} else {

				$this->cliente = $this->Cliente->get_cliente_user($user);

				if(!empty($this->cliente->id)) { 

					$this->load->model('Suministro');

					$this->cliente->suministros = $this->Suministro->get_suministros_cliente_improved($this->cliente->id, $anno);

					foreach ($this->cliente->suministros as $suministro) {

						$suministro->ahorro = (empty($suministro->ahorro) || $suministro->ahorro < 0) ? 0 : $suministro->ahorro; 
						$suministro->optimizacion =  (empty($suministro->optimizacion) || $suministro->optimizacion < 0) ? 0 : $suministro->optimizacion;
						$suministro->neto =  empty($suministro->neto) ? 0 : $suministro->neto;

						$suministro->gestion = $suministro->ahorro + $suministro->optimizacion;
						$suministro->id = base64_encode($this->encryption->encrypt($suministro->id));
						$suministro->nombre = strtoupper($suministro->nombre);
						$suministro->ahorro = number_format($suministro->ahorro, 0, ',', '.');
						$suministro->optimizacion = number_format($suministro->optimizacion, 0, ',', '.');
						$suministro->neto = number_format($suministro->neto, 0, ',', '.');
						$suministro->gestion = number_format($suministro->gestion, 0, ',', '.');

					}

					$this->load->model('Estadistica');

					$this->estadisticas = $this->Estadistica->get_generales_cliente($this->cliente->id, $anno);

					$this->estadisticas->ahorro = $this->estadisticas->ahorro < 0 ? 0 : $this->estadisticas->ahorro ;

					$this->estadisticas->optimizacion = $this->estadisticas->optimizacion < 0 ? 0 : $this->estadisticas->optimizacion ;

					$this->estadisticas->suministros = number_format($this->estadisticas->suministros, 0, ',', '.');

					$this->estadisticas->ahorro = number_format($this->estadisticas->ahorro, 0, ',', '.');

					$this->estadisticas->optimizacion = number_format($this->estadisticas->optimizacion, 0, ',', '.');

					$this->estadisticas->neto = number_format($this->estadisticas->neto, 0, ',', '.');

					$this->estadisticas->facturado = number_format($this->estadisticas->facturado, 0, ',', '.');

					$this->data->cliente = !empty($this->cliente) ? $this->cliente : FALSE ;

					$this->data->estadisticas = $this->estadisticas;

					$this->_load_layout('home-cliente');

				} else {
					$this->_load_layout('errors/aplication/error_modulo');
				}

			}

		} else {

			redirect(base_url('login/'));

		}
	}

	public function test () {
		 
		$v = 4;
		$test = 'pi';

		// Remove whitespaces
		$test = preg_replace('/\s+/', '', $test);

		$number = '(?:\d+(?:[,.]\d+)?|pi|π)'; // What is a number
		$functions = '(?:sinh?|cosh?|tanh?|abs|acosh?|asinh?|atanh?|exp|log10|deg2rad|rad2deg|sqrt|ceil|floor|round)'; // Allowed PHP functions
		$operators = '[+\/*\^%-]'; // Allowed math operators
		$regexp = '/^(('.$number.'|'.$functions.'\s*\((?1)+\)|\((?1)+\))(?:'.$operators.'(?2))?)+$/'; // Final regexp, heavily using recursive patterns

		if (preg_match($regexp, $test))
		{
			$test = preg_replace('!pi|π!', 'pi()', $test); // Replace pi with pi function
			eval('$result = '.$test.';');
		}
		else
		{
			$result = false;
		}



		$test = '1+5';
		$v1 = 7; $v2 = 3;

		echo $result;
		echo '<br>';
		eval('$result = '.$test.'; $suma = ($v1 + $v2) * 2;');

		echo $result;
		echo '<br>';
		echo $suma;



	}
}
