<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gestion extends GC_Controller {
    private $clientes;
    private $suministros;
    private $ratios;
    private $companias;
    private $tarifas;
    private $analisis;

    private $cliente;
    private $suministro;

    private $page;
    private $action;
    private $error;

    public function __construct(){
        parent::__construct();
        $this->page = "gestion";
    }

    public function index() {
        
        redirect(base_url());

    }

    public function clientes() {
 
        $this->action = "clientes";

        if($this->is_auth){

            if($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 4) {

                $this->error = FALSE;

                $user = $this->encryption->decrypt(base64_decode($this->user->id));

                if(empty($this->input->get('q'))) {

                    $this->clientes = $this->User->get_clientes($user);

                    $this->data->filtrado = FALSE;

                } else {

                    $search = $this->security->xss_clean($this->input->get('q'));

                    $this->clientes = $this->User->get_clientes_filtrado($search, $user);

                    $this->data->filtrado = TRUE;
                    
                }

                foreach ($this->clientes as $cliente) {
                    $cliente->id = base64_encode($this->encryption->encrypt($cliente->id));
                }

                $this->data->clientes =  $this->clientes;

            } else {
                
                $this->error = TRUE;

            }

        } else {

            //$this->error = TRUE;
            
            $this->set_redirect($this->page, $this->action);

            redirect(base_url('login/?redirect=' . $this->redirect));

        }

        if (!$this->error) {
            
            $this->data->page = $this->action;

            $this->_load_layout('gestion/lista_clientes');

        } else {

            $this->_load_layout('errors/aplication/error_modulo');

        }
    }

    public function suministros() {

        $this->action = "suministros";

        if($this->is_auth){

            if($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 3 || $this->auth_lvl->id_perfil == 4) {

                $this->error = FALSE;

                $this->load->model('Suministro');

                try {                    

                    if(empty($this->input->get('q'))) {

                        if($this->auth_lvl->id_perfil == 3) {
                            $user = $this->encryption->decrypt(base64_decode($this->user->id));

                            $this->load->model('Cliente');

                            $cliente = $this->Cliente->get_cliente_user($user);

                            if(!empty($cliente->id)) {
                                
                                $this->suministros = $this->Suministro->get_suministros_cliente($cliente->id);

                                $cliente->id = base64_encode($this->encryption->encrypt($cliente->id));

                                $this->data->cliente = $cliente;

                            } else {

                                $this->error = TRUE;

                            }

                            $this->data->export = TRUE;

                        } else {

                            $this->suministros = $this->Suministro->get_suministros();

                            $this->data->export = FALSE;

                        }

                        $this->data->filtrado = FALSE;
    
                    } else {
    
                        $search = $this->security->xss_clean($this->input->get('q'));

                        if($this->auth_lvl->id_perfil == 3) {
                            $this->data->export = TRUE;

                            $user = $this->encryption->decrypt(base64_decode($this->user->id));

                            $this->load->model('Cliente');

                            $cliente = $this->Cliente->get_cliente_user($user);

                            if(!empty($cliente->id)) {

                                $this->suministros = $this->Suministro->get_suministros_cliente_filtrado($cliente->id, $search);

                            } else {

                                $this->error = TRUE;

                            }

                        } else {
                            $this->data->export = FALSE;

                            $this->suministros = $this->Suministro->get_suministros_filtrado($search);

                        }
    
                        $this->data->filtrado = TRUE;
                        
                    }

                    foreach ($this->suministros as $suministro) {
                        $suministro->id         = base64_encode($this->encryption->encrypt($suministro->id));
                        $suministro->id_cli     = base64_encode($this->encryption->encrypt($suministro->id_cli));
                        $suministro->last_month = empty($suministro->last_month) ? '-' : $suministro->last_month;
                        
                        $suministro->last_neto  = empty($suministro->last_neto) ? '-' : $suministro->last_neto;
                        
                        if(is_numeric($suministro->last_neto)) {
                            $suministro->last_neto  = number_format($suministro->last_neto, 0, ',', '.');
                        }
                        
                        $suministro->last_fact  = empty($suministro->last_fact) ? '-' : $suministro->last_fact;

                        if(is_numeric($suministro->last_fact)) {
                            $suministro->last_fact  = number_format($suministro->last_fact, 0, ',', '.');
                        }
                        
                        $suministro->ahorro     = $this->auth_lvl->id_perfil == 3 && $suministro->ahorro < 0 ? 0 : $suministro->ahorro ;
                        
                        if(is_numeric($suministro->ahorro)) {
                            $suministro->ahorro     = number_format($suministro->ahorro, 0, ',', '.');
                        }
                    }
    
                    $this->data->suministros =  $this->suministros;

                } catch(Exeception $e) {

                    $this->error = TRUE;

                }

            } else {
                
                $this->error = TRUE;

            }

        } else {

            $this->set_redirect($this->page, $this->action);

            redirect(base_url('login/?redirect=' . $this->redirect));

        }

        if (!$this->error) {
            
            $this->data->page = $this->action;

            $this->_load_layout('gestion/lista_suministros');

        } else {

            $this->_load_layout('errors/aplication/error_modulo');

        }

    }

    public function suministro() {
        $this->action = "suministro";
        $token = !empty($this->input->get('token')) ? $this->input->get('token') : FALSE;
        $anno = empty($this->security->xss_clean($this->input->get('y'))) ? date("Y") : $this->security->xss_clean($this->input->get('y'));
        $fac_class = "";

        if($this->is_auth) {

            if($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 3 || $this->auth_lvl->id_perfil == 4) {

                $this->error = FALSE;

                if(!empty($token)) {

                    $token = $this->encryption->decrypt(base64_decode($token));

                    $this->data->token = $token;

                    $this->load->model('Suministro');
                    
                    try {

                        $this->suministro = $this->Suministro->get_suministro($token, $anno);

                    } catch(Exception $e) {
                        
                        $this->error = TRUE;
                        $this->suministro = FALSE;

                    }

                    if(!empty($this->suministro)) {

                        $this->suministro->tarifa = empty($this->suministro->tarifa) ? "N/A" : $this->suministro->tarifa;

                        $this->suministro->etapa = empty($this->suministro->etapa) ? "N/A" : $this->suministro->etapa;

                        $this->suministro->anno_base_str = $this->suministro->anno_base ? $this->suministro->anno_base : "N/A" ;

                        $this->suministro->max1 = number_format($this->suministro->max1, 2, ',', '.');

                        $this->suministro->max2 = number_format($this->suministro->max2, 2, ',', '.');
                    
                        $this->data->anno = (int) $anno;

                        try {
                            //*CLIENTE LIBRE
                            $_facturas = $this->Suministro->get_facturas_suministro($this->suministro->id, $anno);
                        } catch(Exception $e) {
                            //*CLIENTE LIBRE
                            $_facturas=[];
                        }

                        $i = 0;
                        $tot_consumo_ac = 0;
                        $tot_energia = 0; 
                        $tot_fp_pot = 0; 
                        $tot_fp_deman_fact = 0;
                        $tot_ep_pot = 0;
                        $tot_ep_deman_fact = 0;
                        $tot_fp_deman_prec = 0;
                        $tot_ep_deman_prec = 0;
                        $tot_reliquidaciones = 0;
                        $tot_arr_tra = 0;
                        $tot_recargo_factor = 0;
                        $tot_otros_rec = 0;
                        $tot_tot_neto = 0;
                        $tot_tot_fac = 0;
                        $tot_cp_consumo = 0;
                        $tot_cp_precio = 0;

                        //*CLIENTE LIBRE
                        $tot_consumo_ac_gen = 0;
                        $tot_energia_gen = 0;
                        $tot_ep_pot_gen = 0;
                        $tot_ep_deman_fact_gen = 0;
                        $tot_ep_deman_prec_gen = 0;

                        foreach($_facturas as $factura) {                           
                            $factura->id = base64_encode($this->encryption->encrypt($factura->id));
                            $factura->arr_tra = $factura->arriendo + $factura->transmision;

                            if($factura->estado > 0) {
                                $fac_class .= "bg-blue";
                                $fac_class .= " ";
                            }

                            if($factura->alerta > 0) {
                                $fac_class .= "bg-red";
                                $fac_class .= " ";
                            }

                            if($factura->estado > 0 || $factura->alerta > 0) {
                                $fac_class .= "bg-hover";
                            }

                            $factura->fac_class = $fac_class;

                            $fac_class = "";

                            //$factura->remark = $factura->ep_pot == $this->suministro->max1 || $factura->ep_pot == $this->suministro->max2;
                            
                            //*CLIENTE LIBRE
                            if($factura->tipo_tarifa == 21) {
                                $factura->otros_cargos += $factura->otros_cargos_gen;
                                $factura->tot_neto = $factura->tot_neto_libre;
                                $factura->tot_fac = $factura->tot_fac_libre;
                            }

                            $tot_consumo_ac += $factura->consumo_ac;
                            $tot_energia += $factura->energia; 
                            $tot_fp_pot += $factura->fp_pot; 
                            $tot_fp_deman_fact += $factura->fp_deman_fact;
                            $tot_ep_pot += $factura->ep_pot;
                            $tot_ep_deman_fact += $factura->ep_deman_fact;
                            $tot_fp_deman_prec += $factura->fp_deman_prec;
                            $tot_ep_deman_prec += $factura->ep_deman_prec;
                            $tot_reliquidaciones += $factura->reliquidaciones;
                            $tot_arr_tra += $factura->arr_tra;
                            $tot_recargo_factor += $factura->recargo_factor;
                            $tot_otros_rec += $factura->otros_cargos;
                            $tot_tot_neto += $factura->tot_neto;
                            $tot_tot_fac += $factura->tot_fac;
                            $tot_cp_consumo += $factura->cp_consumo;
                            $tot_cp_precio += $factura->cp_precio;
                            
                            //*CLIENTE LIBRE
                            if($factura->tipo_tarifa == 21){
                                $tot_consumo_ac_gen += $factura->consumo_ac_gen;
                                $tot_energia_gen += $factura->energia_gen;
                                $tot_ep_pot_gen += $factura->ep_pot_gen;
                                $tot_ep_deman_fact_gen += $factura->ep_deman_fact_gen;
                                $tot_ep_deman_prec_gen += $factura->ep_deman_prec_gen;

                                $factura->consumo_ac_gen = number_format($factura->consumo_ac_gen, 0, ',', '.');
                                $factura->energia_gen = number_format($factura->energia_gen, 0, ',', '.');
                                $factura->ep_pot_gen = number_format($factura->ep_pot_gen, 2, ',', '.');
                                $factura->ep_deman_fact_gen = number_format($factura->ep_deman_fact_gen, 2, ',', '.');
                                $factura->ep_deman_prec_gen = number_format($factura->ep_deman_prec_gen, 0, ',', '.');
                            }
                            
                            $factura->consumo_ac = number_format($factura->consumo_ac, 0, ',', '.');
                            $factura->energia = number_format($factura->energia, 0, ',', '.'); 
                            $factura->fp_pot = number_format($factura->fp_pot, 2, ',', '.'); 
                            $factura->fp_deman_fact = number_format($factura->fp_deman_fact, 2, ',', '.');
                            $factura->ep_pot = number_format($factura->ep_pot, 2, ',', '.');
                            $factura->ep_deman_fact = number_format($factura->ep_deman_fact, 2, ',', '.');
                            $factura->fp_deman_prec = number_format($factura->fp_deman_prec, 0, ',', '.');
                            $factura->ep_deman_prec = number_format($factura->ep_deman_prec, 0, ',', '.');
                            $factura->reliquidaciones = number_format($factura->reliquidaciones, 0, ',', '.');
                            $factura->arr_tra = number_format($factura->arr_tra, 0, ',', '.');
                            $factura->recargo_factor = number_format($factura->recargo_factor, 0, ',', '.');
                            $factura->otros_cargos = number_format($factura->otros_cargos, 0, ',', '.');
                            $factura->tot_neto = number_format($factura->tot_neto, 0, ',', '.');
                            $factura->tot_fac = number_format($factura->tot_fac, 0, ',', '.');
                            
                            $factura->remark = $factura->ep_pot == $this->suministro->max1 || $factura->ep_pot == $this->suministro->max2;

                            $i++;
                        }

                        //*CLIENTE LIBRE
                        $this->suministro->facturas_libre = array_filter($_facturas, function($x) {
                            return $x->tipo_tarifa == 21;
                        });

                        $this->suministro->facturas = array_filter($_facturas, function($x) {
                            return $x->tipo_tarifa != 21;
                        });
                        
                        try {
                            $_ratios = $this->Suministro->get_ratios($this->suministro->id, $anno);
                        } catch(Exception $e) {
                            $_ratios = [];
                        }

                        $k = 0;
                        $tot_energia_r = 0;
                        $tot_f_punta = 0; 
                        $tot_e_punta = 0; 
                        $tot_monomico = 0;
                        $tot_produccion = 0;
                        $tot_indicador = 0;
                        $tot_ahorro = 0;
                        $tot_optimizacion = 0;
                        $tot_cp = 0;

                        //*CLIENTE LIBRE
                        $tot_energia_r_gen = 0;
                        $tot_e_punta_gen = 0;
                        $tot_monomico_gen = 0;
                        $tot_indicador_gen = 0;
                        $tot_ahorro_gen = 0;
                        $tot_optimizacion_gen = 0;
                        $tot_ahorro_dis = 0;
                        $tot_optimizacion_dis = 0;
                        $tot_trans_ratio = 0;
                        $tot_armo_ratio = 0;
                        $tot_tec_ratio = 0;

                        foreach($_ratios as $ratio) {
                            
                            $ratio->id = base64_encode($this->encryption->encrypt($ratio->id));

                            $ratio->mod = (((int) substr($ratio->mes, 0, 4)) > 2018 ) && !empty($ratio->energia_in) && (($ratio->energia != $ratio->energia_in) ||  
                                            ($ratio->f_punta != $ratio->f_punta_in) ||
                                            ($ratio->e_punta != $ratio->e_punta_in) ||
                                            ($ratio->monomico != $ratio->monomico_in) ||
                                            ($ratio->produccion != $ratio->produccion_in) ||
                                            ($ratio->indicador != $ratio->indicador_in) ||
                                            ($ratio->ahorro != $ratio->ahorro_in) ||
                                            ($ratio->optimizacion != $ratio->optimizacion_in) || 
                                            ($ratio->cp_ratio != $ratio->cp_in));
                            
                            $ratio->fp_mes_str = number_format($ratio->fp_mes, 2, ',', '.');
                            $ratio->ep_mes_str = number_format($ratio->ep_mes, 2, ',', '.');
                            $ratio->varios_mes_str = number_format($ratio->varios_mes, 2, ',', '.');
                            $ratio->tarifa_mes_str = number_format($ratio->tarifa_mes, 2, ',', '.');
                            $ratio->transmision_mes_str = number_format($ratio->transmision_mes, 2, ',', '.');
                            $ratio->cp_mes_str = number_format($ratio->cp_mes, 2, ',', '.');
                            $ratio->fp_base_str = number_format($ratio->fp_base, 2, ',', '.');
                            $ratio->ep_base_str = number_format($ratio->ep_base, 2, ',', '.');
                            $ratio->varios_base_str = number_format($ratio->varios_base, 2, ',', '.');

                            $ratio->indicador_base_str = number_format($ratio->indicador_base, 2, ',', '.');

                            //*CLIENTE LIBRE
                            $ratio ->ep_mes_str_gen = number_format($ratio->ep_mes_gen, 2, ',', '.');
                            $ratio->c_armo_mes_str = number_format($ratio->c_armo_mes, 2, ',', '.');
                            $ratio->c_tec_mes_str = number_format($ratio->c_tec_mes, 2, ',', '.');

                            $ratio->tar_a = $ratio->fp_base - $ratio->fp_mes;

                            //*CLIENTE LIBRE
                            if($ratio->tipo_tarifa == 21) {
                                $ratio->tar_b = $ratio->ep_base - $ratio->ep_mes;
                                $ratio->tar_bg = $ratio->ep_base - $ratio->ep_mes_gen;
                            } else {
                                if ($ratio->r_etapa == 1) {

                                    $ratio->tar_b = $ratio->ep_base - $ratio->ep_mes;
    
                                } else {
    
                                    $ratio->tar_b = $ratio->ep_base * $ratio->tarifa_mes;
    
                                }

                                $ratio->tar_bg = 0;
                            }
                            
                            $ratio->tar_c = $ratio->varios_base - $ratio->varios_mes;

                            if($this->auth_lvl->id_perfil == 3) {
                                $ratio->ahorro = $ratio->ahorro < 0 ? 0 : $ratio->ahorro;
                                $ratio->optimizacion = $ratio->optimizacion < 0 ? 0 : $ratio->optimizacion;

                                //*CLIENTE LIBRE
                                $ratio->ahorro_gen = $ratio->ahorro_gen < 0 ? 0 : $ratio->ahorro_gen;
                                $ratio->optimizacion_gen = $ratio->optimizacion_gen < 0 ? 0 : $ratio->optimizacion_gen;
                                
                                //*CLIENTE LIBRE
                                $ratio->ahorro_dis = $ratio->ahorro_dis < 0 ? 0 : $ratio->ahorro_dis;
                                $ratio->optimizacion_dis = $ratio->optimizacion_dis < 0 ? 0 : $ratio->optimizacion_dis;
                            }

                            if(!$ratio->mod) {
                                $ratio->energia_str = number_format($ratio->energia, 2, ',', '.');
                                $ratio->f_punta_str = number_format($ratio->f_punta, 2, ',', '.'); 
                                $ratio->e_punta_str = number_format($ratio->e_punta, 2, ',', '.'); 
                                $ratio->monomico_str = number_format($ratio->monomico, 2, ',', '.');
                                $ratio->produccion_str = number_format($ratio->produccion, 0, ',', '.');
                                $ratio->indicador_str = number_format($ratio->indicador, 2, ',', '.');
                                $ratio->ahorro_str = number_format($ratio->ahorro, 0, ',', '.');
                                $ratio->optimizacion_str = number_format($ratio->optimizacion, 0, ',', '.');
                                $ratio->cp_ratio_str = number_format($ratio->cp_ratio, 0, ',', '.');

                                //*CLIENTE LIBRE
                                $ratio->energia_str_gen = number_format($ratio->energia_gen, 2, ',', '.');
                                $ratio->e_punta_str_gen = number_format($ratio->e_punta_gen, 2, ',', '.');
                                $ratio->monomico_str_gen = number_format($ratio->monomico_gen, 2, ',', '.');
                                $ratio->indicador_str_gen = number_format($ratio->indicador_gen, 2, ',', '.');
                                $ratio->ahorro_str_gen = number_format($ratio->ahorro_gen, 0, ',', '.');
                                $ratio->optimizacion_str_gen = number_format($ratio->optimizacion_gen, 0, ',', '.');
                                $ratio->ahorro_str_dis = number_format($ratio->ahorro_dis, 0, ',', '.');
                                $ratio->optimizacion_str_dis = number_format($ratio->optimizacion_dis, 0, ',', '.');
                                $ratio->trans_ratio_str = number_format($ratio->trans_ratio, 2, ',', '.');
                                $ratio->armo_ratio_str = number_format($ratio->armo_ratio, 2, ',', '.');
                                $ratio->tec_ratio_str = number_format($ratio->tec_ratio, 2, ',', '.');

                                $tot_energia_r += $ratio->energia;
                                $tot_f_punta += $ratio->f_punta; 
                                $tot_e_punta += $ratio->e_punta; 
                                $tot_monomico += $ratio->monomico;
                                $tot_produccion += $ratio->produccion;
                                $tot_indicador += $ratio->indicador;
                                $tot_ahorro += $ratio->ahorro;
                                $tot_optimizacion += $ratio->optimizacion;
                                $tot_cp += $ratio->cp_ratio;

                                $ratio->tar_a *= $ratio->f_punta;

                                if($ratio->tipo_tarifa == 21) {
                                    //*CLIENTE LIBRE
                                    $tot_energia_r_gen += $ratio->energia_gen;
                                    $tot_e_punta_gen += $ratio->e_punta_gen;
                                    $tot_monomico_gen += $ratio->monomico_gen;
                                    $tot_indicador_gen += $ratio->indicador_gen;
                                    $tot_ahorro_gen += $ratio->ahorro_gen;
                                    $tot_optimizacion_gen += $ratio->optimizacion_gen;
                                    $tot_ahorro_dis += $ratio->ahorro_dis;
                                    $tot_optimizacion_dis += $ratio->optimizacion_dis;
                                    $tot_trans_ratio += $ratio->trans_ratio;
                                    $tot_armo_ratio += $ratio->armo_ratio;
                                    $tot_tec_ratio += $ratio->tec_ratio;

                                    $ratio->tar_b *= $ratio->e_punta;
                                    $ratio->tar_bg *= $ratio->e_punta_gen;
                                } else {
                                    if ($ratio->r_etapa == 1) {

                                        $ratio->tar_b *= $ratio->e_punta;
        
                                    } else {
        
                                        $ratio->tar_b -= ($ratio->ep_mes * $ratio->e_punta);
        
                                    }

                                    if($ratio->tipo_tarifa == 20) {
                                        $ratio->tar_b += ($ratio->cp_mes * $ratio->cp_ratio);
                                    }
                                }

                            } else {

                                if($this->auth_lvl->id_perfil == 3) {
                                    $ratio->ahorro_in = $ratio->ahorro_in < 0 ? 0 : $ratio->ahorro_in;
                                    $ratio->optimizacion_in = $ratio->optimizacion_in < 0 ? 0 : $ratio->optimizacion_in;

                                    //*CLIENTE LIBRE
                                    $ratio->ahorro_in_gen = $ratio->ahorro_in_gen < 0 ? 0 : $ratio->ahorro_in_gen;
                                    $ratio->optimizacion_in_gen = $ratio->optimizacion_in_gen < 0 ? 0 : $ratio->optimizacion_in_gen;
                                    
                                    //*CLIENTE LIBRE
                                    $ratio->ahorro_in_dis = $ratio->ahorro_in_dis < 0 ? 0 : $ratio->ahorro_in_dis;
                                    $ratio->optimizacion_in_dis = $ratio->optimizacion_in_dis < 0 ? 0 : $ratio->optimizacion_in_dis;
                                }

                                $ratio->energia_str = number_format($ratio->energia_in, 2, ',', '.');
                                $ratio->f_punta_str = number_format($ratio->f_punta_in, 2, ',', '.'); 
                                $ratio->e_punta_str = number_format($ratio->e_punta_in, 2, ',', '.'); 
                                $ratio->monomico_str = number_format($ratio->monomico_in, 2, ',', '.');
                                $ratio->produccion_str = number_format($ratio->produccion_in, 0, ',', '.');
                                $ratio->indicador_str = number_format($ratio->indicador_in, 2, ',', '.');
                                $ratio->ahorro_str = number_format($ratio->ahorro_in, 0, ',', '.');
                                $ratio->optimizacion_str = number_format($ratio->optimizacion_in, 0, ',', '.');
                                $ratio->cp_ratio_str = number_format($ratio->cp_in, 0, ',', '.');

                                //*CLIENTE LIBRE
                                $ratio->energia_str_gen = number_format($ratio->energia_in_gen, 2, ',', '.');
                                $ratio->e_punta_str_gen = number_format($ratio->e_punta_in_gen, 2, ',', '.');
                                $ratio->monomico_str_gen = number_format($ratio->monomico_in_gen, 2, ',', '.');
                                $ratio->indicador_str_gen = number_format($ratio->indicador_in_gen, 2, ',', '.');
                                $ratio->ahorro_str_gen = number_format($ratio->ahorro_in_gen, 0, ',', '.');
                                $ratio->optimizacion_str_gen = number_format($ratio->optimizacion_in_gen, 0, ',', '.');
                                $ratio->ahorro_str_dis = number_format($ratio->ahorro_in_dis, 0, ',', '.');
                                $ratio->optimizacion_str_dis = number_format($ratio->optimizacion_in_dis, 0, ',', '.');
                                $ratio->trans_ratio_str = number_format($ratio->trans_ratio_in, 2, ',', '.');
                                $ratio->armo_ratio_str = number_format($ratio->armo_ratio_in, 2, ',', '.');
                                $ratio->tec_ratio_str = number_format($ratio->tec_ratio_in, 2, ',', '.');

                                $tot_energia_r += $ratio->energia_in;
                                $tot_f_punta += $ratio->f_punta_in; 
                                $tot_e_punta += $ratio->e_punta_in; 
                                $tot_monomico += $ratio->monomico_in;
                                $tot_produccion += $ratio->produccion_in;
                                $tot_indicador += $ratio->indicador_in;
                                $tot_ahorro += $ratio->ahorro_in;
                                $tot_optimizacion += $ratio->optimizacion_in;
                                $tot_cp += $ratio->cp_in;

                                $ratio->tar_a *= $ratio->f_punta_in;

                                if($ratio->tipo_tarifa == 21) {
                                    //*CLIENTE LIBRE
                                    $tot_energia_r_gen += $ratio->energia_in_gen;
                                    $tot_e_punta_gen += $ratio->e_punta_in_gen;
                                    $tot_monomico_gen += $ratio->monomico_in_gen;
                                    $tot_indicador_gen += $ratio->indicador_in_gen;
                                    $tot_ahorro_gen += $ratio->ahorro_in_gen;
                                    $tot_optimizacion_gen += $ratio->optimizacion_in_gen;
                                    $tot_ahorro_dis += $ratio->ahorro_in_dis;
                                    $tot_optimizacion_dis += $ratio->optimizacion_in_dis;
                                    $tot_trans_ratio += $ratio->trans_ratio_in;
                                    $tot_armo_ratio += $ratio->armo_ratio_in;
                                    $tot_tec_ratio += $ratio->tec_ratio_in;

                                    $ratio->tar_b *= $ratio->e_punta_in;
                                    $ratio->tar_bg *= $ratio->e_punta_in_gen;
                                } else {
                                    if ($ratio->r_etapa == 1) {

                                        $ratio->tar_b *= $ratio->e_punta_in;
        
                                    } else {
        
                                        $ratio->tar_b -= ($ratio->ep_mes * $ratio->e_punta_in);
        
                                    }

                                    if($ratio->tipo_tarifa == 20) {
                                        $ratio->tar_b += ($ratio->cp_mes * $ratio->cp_in);
                                    }
                                }
                            }

                            $ratio->tar_a_str = number_format($ratio->tar_a, 2, ',', '.');
                            $ratio->tar_b_str = number_format($ratio->tar_b, 2, ',', '.');
                            $ratio->tar_c_str = number_format($ratio->tar_c, 2, ',', '.');

                            //*CLIENTE LIBRE
                            $ratio->tar_bg_str = number_format($ratio->tar_bg, 2, ',', '.');

                            $k++;
                        }

                        //*CLIENTE LIBRE
                        $this->data->ratios_libre = array_filter($_ratios, function($x) {
                            return $x->tipo_tarifa == 21;
                        });

                        $this->data->ratios = array_filter($_ratios, function($x) {
                            return $x->tipo_tarifa != 21;
                        });

                        $this->suministro->id = base64_encode($this->encryption->encrypt($this->suministro->id));

                        $this->data->suministro = $this->suministro;

                        $this->data->suministro->tot_consumo_ac = number_format($tot_consumo_ac, 0, ',', '.');
                        $this->data->suministro->tot_energia = number_format($tot_energia, 0, ',', '.'); 
                        $this->data->suministro->tot_fp_pot = number_format($tot_fp_pot, 2, ',', '.'); 
                        $this->data->suministro->tot_fp_deman_fact = number_format($tot_fp_deman_fact, 2, ',', '.');
                        $this->data->suministro->tot_ep_pot = number_format($tot_ep_pot, 2, ',', '.');
                        $this->data->suministro->tot_ep_deman_fact = number_format($tot_ep_deman_fact, 2, ',', '.');
                        $this->data->suministro->tot_fp_deman_prec = number_format($tot_fp_deman_prec, 0, ',', '.');
                        $this->data->suministro->tot_ep_deman_prec = number_format($tot_ep_deman_prec, 0, ',', '.');
                        $this->data->suministro->tot_reliquidaciones = number_format($tot_reliquidaciones, 0, ',', '.');
                        $this->data->suministro->tot_arr_tra = number_format($tot_arr_tra, 0, ',', '.');
                        $this->data->suministro->tot_recargo_factor = number_format($tot_recargo_factor, 0, ',', '.');
                        $this->data->suministro->tot_otros_rec = number_format($tot_otros_rec, 0, ',', '.');
                        $this->data->suministro->tot_tot_neto = number_format($tot_tot_neto, 0, ',', '.');
                        $this->data->suministro->tot_tot_fac = number_format($tot_tot_fac, 0, ',', '.');
                        $this->data->suministro->tot_cp_consumo = number_format($tot_cp_consumo, 0, ',', '.');
                        $this->data->suministro->tot_cp_precio = number_format($tot_cp_precio, 0, ',', '.');
                        
                        //*CLIENTE LIBRE
                        $this->data->suministro->tot_consumo_ac_gen = number_format($tot_consumo_ac_gen, 0, ',', '.');
                        $this->data->suministro->tot_energia_gen = number_format($tot_energia_gen, 0, ',', '.');
                        $this->data->suministro->tot_ep_pot_gen = number_format($tot_ep_pot_gen, 0, ',', '.');
                        $this->data->suministro->tot_ep_deman_fact_gen = number_format($tot_ep_deman_fact_gen, 0, ',', '.');
                        $this->data->suministro->tot_ep_deman_prec_gen = number_format($tot_ep_deman_prec_gen, 0, ',', '.');


                        if($i > 0) {
                            $this->data->suministro->prom_consumo_ac = number_format($tot_consumo_ac / $i, 0, ',', '.');
                            $this->data->suministro->prom_energia = number_format($tot_energia / $i, 0, ',', '.'); 
                            $this->data->suministro->prom_fp_pot = number_format($tot_fp_pot / $i, 2, ',', '.'); 
                            $this->data->suministro->prom_fp_deman_fact = number_format($tot_fp_deman_fact / $i, 2, ',', '.');
                            $this->data->suministro->prom_ep_pot = number_format($tot_ep_pot / $i, 2, ',', '.');
                            $this->data->suministro->prom_ep_deman_fact = number_format($tot_ep_deman_fact / $i, 2, ',', '.');
                            $this->data->suministro->prom_fp_deman_prec = number_format($tot_fp_deman_prec / $i, 0, ',', '.');
                            $this->data->suministro->prom_ep_deman_prec = number_format($tot_ep_deman_prec / $i, 0, ',', '.');
                            $this->data->suministro->prom_reliquidaciones = number_format($tot_reliquidaciones / $i, 0, ',', '.');
                            $this->data->suministro->prom_arr_tra = number_format($tot_arr_tra / $i, 0, ',', '.');
                            $this->data->suministro->prom_recargo_factor = number_format($tot_recargo_factor / $i, 0, ',', '.');
                            $this->data->suministro->prom_otros_rec = number_format($tot_otros_rec / $i, 0, ',', '.');
                            $this->data->suministro->prom_tot_neto = number_format($tot_tot_neto / $i, 0, ',', '.');
                            $this->data->suministro->prom_tot_fac = number_format($tot_tot_fac / $i, 0, ',', '.');
                            $this->data->suministro->prom_cp_consumo = number_format($tot_cp_consumo / $i, 0, ',', '.');
                            $this->data->suministro->prom_cp_precio = number_format($tot_cp_precio / $i, 0, ',', '.');

                            //*CLIENTE LIBRE
                            $this->data->suministro->prom_consumo_ac_gen = number_format($tot_consumo_ac_gen / $i, 0, ',', '.');
                            $this->data->suministro->prom_energia_gen = number_format($tot_energia_gen / $i, 0, ',', '.');
                            $this->data->suministro->prom_ep_pot_gen = number_format($tot_ep_pot_gen / $i, 0, ',', '.');
                            $this->data->suministro->prom_ep_deman_fact_gen = number_format($tot_ep_deman_fact_gen / $i, 0, ',', '.');
                            $this->data->suministro->prom_ep_deman_prec_gen = number_format($tot_ep_deman_prec_gen / $i, 0, ',', '.');
                        }

                        $this->data->suministro->ratio = new stdClass();

                        $this->data->suministro->ratio->tot_energia = number_format($tot_energia_r, 2, ',', '.');
                        $this->data->suministro->ratio->tot_f_punta = number_format($tot_f_punta, 2, ',', '.'); 
                        $this->data->suministro->ratio->tot_e_punta = number_format($tot_e_punta, 2, ',', '.'); 
                        $this->data->suministro->ratio->tot_monomico = number_format($tot_monomico, 2, ',', '.');
                        $this->data->suministro->ratio->tot_produccion = number_format($tot_produccion, 0, ',', '.');
                        $this->data->suministro->ratio->tot_indicador = number_format($tot_indicador, 2, ',', '.');
                        $this->data->suministro->ratio->tot_ahorro = number_format($tot_ahorro, 0, ',', '.');
                        $this->data->suministro->ratio->tot_optimizacion = number_format($tot_optimizacion, 0, ',', '.');
                        $this->data->suministro->ratio->tot_cp = number_format($tot_cp, 0, ',', '.');

                        //*CLIENTE LIBRE
                        $this->data->suministro->ratio->tot_energia_gen = number_format($tot_energia_r_gen, 2, ',', '.');
                        $this->data->suministro->ratio->tot_e_punta_gen = number_format($tot_e_punta_gen, 2, ',', '.');
                        $this->data->suministro->ratio->tot_monomico_gen = number_format($tot_monomico_gen, 2, ',', '.');
                        $this->data->suministro->ratio->tot_indicador_gen = number_format($tot_indicador_gen, 2, ',', '.');
                        $this->data->suministro->ratio->tot_ahorro_gen = number_format($tot_ahorro_gen, 0, ',', '.');
                        $this->data->suministro->ratio->tot_optimizacion_gen = number_format($tot_optimizacion_gen, 0, ',', '.');
                        $this->data->suministro->ratio->tot_ahorro_dis = number_format($tot_ahorro_dis, 0, ',', '.');
                        $this->data->suministro->ratio->tot_optimizacion_dis = number_format($tot_optimizacion_dis, 0, ',', '.');
                        $this->data->suministro->ratio->tot_trans_ratio = number_format($tot_trans_ratio, 2, ',', '.');
                        $this->data->suministro->ratio->tot_armo_ratio = number_format($tot_armo_ratio, 2, ',', '.');
                        $this->data->suministro->ratio->tot_tec_ratio = number_format($tot_tec_ratio, 2, ',', '.');

                        if($k > 0) {
                            $this->data->suministro->ratio->prom_energia = number_format($tot_energia_r / $k, 2, ',', '.');
                            $this->data->suministro->ratio->prom_f_punta = number_format($tot_f_punta / $k, 2, ',', '.'); 
                            $this->data->suministro->ratio->prom_e_punta = number_format($tot_e_punta / $k, 2, ',', '.'); 
                            $this->data->suministro->ratio->prom_monomico = number_format($tot_monomico / $k, 2, ',', '.');
                            $this->data->suministro->ratio->prom_produccion = number_format($tot_produccion / $k, 0, ',', '.');
                            $this->data->suministro->ratio->prom_indicador = number_format($tot_indicador / $k, 2, ',', '.');
                            $this->data->suministro->ratio->prom_ahorro = number_format($tot_ahorro / $k, 0, ',', '.');
                            $this->data->suministro->ratio->prom_optimizacion = number_format($tot_optimizacion / $k, 0, ',', '.');
                            $this->data->suministro->ratio->prom_cp = number_format($tot_cp / $k, 0, ',', '.');

                                //*CLIENTE LIBRE
                            $this->data->suministro->ratio->prom_energia_gen = number_format($tot_energia_r_gen / $k, 2, ',', '.');
                            $this->data->suministro->ratio->prom_e_punta_gen = number_format($tot_e_punta_gen / $k, 2, ',', '.');
                            $this->data->suministro->ratio->prom_monomico_gen = number_format($tot_monomico_gen / $k, 2, ',', '.');
                            $this->data->suministro->ratio->prom_indicador_gen = number_format($tot_indicador_gen / $k, 2, ',', '.');
                            $this->data->suministro->ratio->prom_ahorro_gen = number_format($tot_ahorro_gen / $k, 0, ',', '.');
                            $this->data->suministro->ratio->prom_optimizacion_gen = number_format($tot_optimizacion_gen / $k, 0, ',', '.');
                            $this->data->suministro->ratio->prom_ahorro_dis = number_format($tot_ahorro_dis / $k, 0, ',', '.');
                            $this->data->suministro->ratio->prom_optimizacion_dis = number_format($tot_optimizacion_dis / $k, 0, ',', '.');
                            $this->data->suministro->ratio->prom_trans_ratio = number_format($tot_trans_ratio / $k, 2, ',', '.');
                            $this->data->suministro->ratio->prom_armo_ratio = number_format($tot_armo_ratio / $k, 2, ',', '.');
                            $this->data->suministro->ratio->prom_tec_ratio = number_format($tot_tec_ratio / $k, 2, ',', '.');
                        }
                    } else {
                        $this->data->suministro = FALSE;
                    }

                } else {

                    $this->data->suministro = FALSE;
                }

            } else {
                
                $this->error = TRUE;

            }

        } else {

            if(empty($token)) {
                $this->set_redirect();
            } else {
                $this->set_redirect($this->page, $this->action, array('y=' . $anno, 'token=' . $token));
            }

            redirect(base_url('login/?redirect=' . $this->redirect));

        }

        if (!$this->error) {

            $this->_load_layout('gestion/detail_suministro');

        } else {

            $this->_load_layout('errors/aplication/error_modulo');

        }

    }

    public function cliente() {

        $this->action = "cliente";
        $this->data->filtrado = FALSE;
        $this->data->export = TRUE;

        if($this->is_auth) {

            if($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 4) {

                $this->error = FALSE;

                if(!empty($this->input->get('token'))) {

                    $token=$this->encryption->decrypt(base64_decode($this->input->get('token')));

                    $user = $this->encryption->decrypt(base64_decode($this->user->id));

                    $this->cliente = $this->User->get_cliente($token, $user);

                    if(!empty($this->cliente->id)) {

                        $this->cliente->contacto = empty($this->cliente->contacto) ? "N/A" : $this->cliente->contacto;
                        $this->cliente->correo = empty($this->cliente->correo) ? "N/A" : $this->cliente->correo;
                        $this->cliente->num_tel = empty($this->cliente->num_tel) ? "N/A" : $this->cliente->num_tel;
                        $this->cliente->id_user = empty($this->cliente->id_user) ? FALSE : base64_encode($this->encryption->encrypt($this->cliente->id_user));
                        
                        $this->load->model('Suministro');

                        if(empty($this->input->get('q'))) {

                            $this->suministros = $this->Suministro->get_suministros_cliente($this->cliente->id);

                        } else {

                            $search = $this->security->xss_clean($this->input->get('q'));

                            $this->suministros = $this->Suministro->get_suministros_cliente_filtrado($this->cliente->id, $search);

                            $this->data->filtrado = TRUE;

                        }
                        

                        foreach ($this->suministros as $suministro) {
                            $suministro->id = base64_encode($this->encryption->encrypt($suministro->id));
                            $suministro->id_cli = base64_encode($this->encryption->encrypt($suministro->id_cli));
                            $suministro->last_month = empty($suministro->last_month) ? '-' : $suministro->last_month;
                            
                            $suministro->last_neto  = empty($suministro->last_neto) ? '-' : $suministro->last_neto;
                            
                            if(is_numeric($suministro->last_neto)) {
                                $suministro->last_neto = number_format($suministro->last_neto, 0, ',', '.');
                            }
                            
                            $suministro->last_fact = empty($suministro->last_fact) ? '-' : $suministro->last_fact;

                            if(is_numeric($suministro->last_fact)) {
                                $suministro->last_fact = number_format($suministro->last_fact, 0, ',', '.');
                            }
                            
                            $suministro->ahorro = $this->auth_lvl->id_perfil == 3 && $suministro->ahorro < 0 ? 0 : $suministro->ahorro ;
                            
                            if(is_numeric($suministro->ahorro)) {
                                $suministro->ahorro = number_format($suministro->ahorro, 0, ',', '.');
                            }   

                        }
        
                        $this->data->suministros =  $this->suministros;

                        $this->cliente->id = base64_encode($this->encryption->encrypt($this->cliente->id));
                        $this->data->cliente = $this->cliente;

                    }

                } else {

                    $this->data->cliente = FALSE;
                }

            } else {
                
                $this->error = TRUE;

            }

        } else {

            if(empty($token)) {
                $this->set_redirect();
            } else {
                $this->set_redirect($this->page, $this->action, array('token=' . $token));
            }

            redirect(base_url('login/?redirect=' . $this->redirect));

        }

        if (!$this->error) {

            $this->data->page = $this->action;
            $this->_load_layout('gestion/detail_cliente');

        } else {

            $this->_load_layout('errors/aplication/error_modulo');

        }
    }

    public function nuevo_suministro() {
        
        $this->action = "nuevo_suministro";
        $token=$this->encryption->decrypt(base64_decode($this->input->get('token')));

        if (!$this->is_auth) {

            if(empty($token)) {
                $this->set_redirect();
            } else {
                $this->set_redirect($this->page, $this->action, array('token=' . $token));
            }

            redirect(base_url('login/?redirect=' . $this->redirect));
            
        }

        $this->error = !($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2);
 
        if ($this->error) {
            
            $this->_load_layout('errors/aplication/error_modulo');

        } else {

            if(!empty($this->input->get('token'))) {

                $this->cliente = $this->User->get_cliente_suministro($token);

                $this->cliente->nombre = strtoupper($this->cliente->nombre);

                $this->data->cliente = $this->cliente;

                $this->data->token = base64_encode($this->encryption->encrypt($token));

                $this->data->anno = date('Y');

            } else {

                $this->data->token = FALSE;
            }

            $this->set_companias();

            $this->data->companias = $this->companias;

            $this->data->page = $this->action;

            $this->_load_layout('gestion/nuevo_suministro');

        }

    }

    public function suministro_edit() {
        $this->action = "suministro_edit";
        $token = !empty($this->input->get('token')) ? $this->input->get('token') : FALSE;
        $anno = date("Y");

        if($this->is_auth) {

            if($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {

                $this->error = FALSE;

                if(!empty($token)) {

                    $token = $this->encryption->decrypt(base64_decode($token));

                    $this->data->token = $token;

                    $this->load->model('Suministro');
                    
                    try {

                        $this->suministro = $this->Suministro->get_suministro($token, $anno);

                    } catch(Exception $e) {
                        
                        $this->error = TRUE;
                        $this->suministro = FALSE;

                    }

                    if(!empty($this->suministro)) {
                        $this->suministro->con_mod = empty($this->suministro->fec_ini_con) ? 0 : 1;

                        try {
                            
                            $this->load->model('Ahorro');
                            $this->suministro->ahorro = $this->Ahorro->get_contratos_ahorro($this->suministro->id);

                            foreach($this->suministro->ahorro as $ahorro) {
                                $ahorro->id = base64_encode($this->encryption->encrypt($ahorro->id));
                            }

                        } catch (Exception $e) {
                            $this->error = TRUE;
                            $this->suministro->ahorro = FALSE;
                        }

                        try {
                            
                            $this->load->model('Optimizacion');
                            $this->suministro->optimizacion = $this->Optimizacion->get_contratos_optimizacion($this->suministro->id);

                            foreach($this->suministro->optimizacion as $optimizacion) {
                                $optimizacion->id = base64_encode($this->encryption->encrypt($optimizacion->id));
                            }

                        } catch (Exception $e) {
                            $this->error = TRUE;
                            $this->suministro->optimizacion = FALSE;
                        }

                        try {
                            
                            $this->load->model('Suministro');
                            $this->suministro->periodos = $this->Suministro->get_contratos_suministro($this->suministro->id);

                            foreach($this->suministro->periodos as $periodo) {
                                $periodo->id = base64_encode($this->encryption->encrypt($periodo->id));
                                $periodo->id_suministro = base64_encode($this->encryption->encrypt($periodo->id_suministro));
                            }

                        } catch (Exception $e) {
                            $this->error = TRUE;
                            $this->suministro->periodos = FALSE;
                        }

                        $this->set_tarifas();

                        $this->data->tarifas = $this->tarifas;

                        $this->set_companias();

                        $this->data->companias = $this->companias;

                        $this->load->model('Compania');

                        $sectores = $this->Compania->get_sectores($this->suministro->compania_id);

                        foreach ($sectores as $sector) {
                            $sector->codigo = base64_encode($this->encryption->encrypt($sector->codigo));
                        }

                        $this->data->sectores = $sectores;

                        $this->suministro->id = base64_encode($this->encryption->encrypt($this->suministro->id));
                        
                        $this->data->suministro = $this->suministro;

                        $this->data->page = $this->action;

                        $this->data->anno = date('Y');
                    } else {
                        $this->data->suministro = FALSE; 
                    }
                } else {

                    $this->data->suministro = FALSE;
                }

            } else {
                
                $this->error = TRUE;

            }

        } else {

            if(empty($token)) {
                $this->set_redirect();
            } else {
                $this->set_redirect($this->page, $this->action, array('y=' . $anno, 'token=' . $token));
            }

            redirect(base_url('login/?redirect=' . $this->redirect));

        }

        if (!$this->error) {

            $this->_load_layout('gestion/edit_suministro');

        } else {

            $this->_load_layout('errors/aplication/error_modulo');

        }
    }

    public function nueva_factura() {
        $this->action = "nueva_factura";
        $token = !empty($this->input->get('token')) ? $this->input->get('token') : FALSE;

        if (!$this->is_auth) {

            if(empty($token)) {
                $this->set_redirect();
            } else {
                $this->set_redirect($this->page, $this->action, array('token=' . $token));
            }

            redirect(base_url('login/?redirect=' . $this->redirect));
            
        }

        $this->error = !($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 4);
 
        if ($this->error) {

            $this->load->view('errors/aplication/error_modulo');

        } else {

            if(!empty($token)) {
                
                $token=$this->encryption->decrypt(base64_decode($token));

                $this->load->model('Suministro');

                $this->suministro = $this->Suministro->get_contrato_suministro_activo($token);

                if(!empty($this->suministro)) {

                    $this->suministro->nombre = strtoupper($this->suministro->nombre);

                    $this->suministro->id_con = base64_encode($this->encryption->encrypt($this->suministro->id_con));

                    $this->suministro->id_suministro = base64_encode($this->encryption->encrypt($this->suministro->id_suministro));

                    $this->data->suministro = $this->suministro;

                    $this->set_tipo_analisis();

                    $this->data->tipo_ana = $this->analisis;

                    //$this->data->mes = date('Y-m', strtotime('-1 month'));
                    $this->data->mes = date('Y-m');

                    $this->data->page = $this->action;

                    if($this->suministro->id_tarifa == 21) {
                        $this->_load_layout('gestion/nueva_factura_libre');
                    } else {
                        $this->_load_layout('gestion/nueva_factura');
                    }

                } else {
                    $this->load->view('errors/aplication/error_modulo');
                }

            } else {
                $this->load->view('errors/aplication/error_modulo');
            }
        }
    }

    public function factura() {
        $this->action = "factura";
        $token = !empty($this->input->get('token')) ? $this->input->get('token') : FALSE;

        if (!$this->is_auth) {

            if(empty($token)) {
                $this->set_redirect();
            } else {
                $this->set_redirect($this->page, $this->action, array('token=' . $token));
            }

            redirect(base_url('login/?redirect=' . $this->redirect));
            
        }

        $this->error = !($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 4);
 
        if ($this->error) {

            $this->_load_layout('errors/aplication/error_modulo');

        } else {

            if(!empty($token)) {
                
                $token=$this->encryption->decrypt(base64_decode($token));

                $this->load->model('Suministro');

                $this->factura = $this->Suministro->get_factura($token);

                $this->factura->suministro = strtoupper($this->factura->suministro);



                if($this->factura->factura_mode != 'default') {
                    $this->factura->total_factura_libre = $this->factura->tot_neto_libre + $this->factura->tot_exento_libre + round($this->factura->tot_neto_libre * 0.19, 0);

                    if($this->factura->tot_fac_libre == 0) {
                        $this->factura->tot_fac = $this->factura->total_factura_libre + $this->factura->otros_libre + $this->factura->saldo_anterior_libre;
                    }
                    
                    if($this->factura->factura_mode == 'doble') {
                        $this->factura->total_serv = $this->factura->tot_neto + $this->factura->tot_exento;
                        $this->factura->total_factura = $this->factura->total_serv + round($this->factura->tot_neto * 0.19, 0);

                        $this->factura->total_serv_gen = $this->factura->tot_neto_gen + $this->factura->tot_exento_gen;
                        $this->factura->total_factura_gen = $this->factura->total_serv_gen + round($this->factura->tot_neto_gen * 0.19, 0);

                        if($this->factura->tot_fac == 0) {
                            $this->factura->tot_fac = $this->factura->total_factura + $this->factura->otros + $this->factura->saldo_anterior;
                        }
        
                        if($this->factura->tot_fac_gen == 0) {
                            $this->factura->tot_fac = $this->factura->total_factura_gen + $this->factura->otros_gen + $this->factura->saldo_anterior_gen;
                        }
                    } else {
                        $this->factura->total_serv = $this->factura->tot_neto + $this->factura->tot_exento;
                        $this->factura->total_serv_gen = $this->factura->tot_neto_gen + $this->factura->tot_exento_gen;
                        
                        $this->factura->total_factura = 0;
                        $this->factura->total_factura_gen = 0;
                    }
                } else {
                    $this->factura->total_serv = $this->factura->tot_neto + $this->factura->tot_exento;
                    $this->factura->total_factura = $this->factura->total_serv + round($this->factura->tot_neto * 0.19, 0);

                    $this->factura->total_factura_gen = 0;
                    $this->factura->total_factura_libre = 0;

                    if($this->factura->tot_fac == 0) {
                        $this->factura->tot_fac = $this->factura->total_factura + $this->factura->otros + $this->factura->saldo_anterior;
                    }
                }

                $this->factura->id_suministro = base64_encode($this->encryption->encrypt($this->factura->id_suministro));
                $this->factura->id = base64_encode($this->encryption->encrypt($this->factura->id));

                $this->data->factura = $this->factura;

                $this->set_tipo_analisis();

                $this->data->tipo_ana = $this->analisis;

                //$this->data->mes = date('Y-m', strtotime('-1 month'));
                $this->data->mes = date('Y-m');

                $this->data->page = $this->action;

                if($this->factura->tipo_tarifa == 21) {
                    $this->_load_layout('gestion/detail_factura_libre');
                } else {
                    $this->_load_layout('gestion/detail_factura');
                }


            } else {
                $this->_load_layout('errors/aplication/error_modulo');
            }
        }
    }

    public function agregar_factura() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);
        $this->alert = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $error_lectura = FALSE;

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));

        if(!empty($token) && $this->is_auth) {

            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 4) {

                $nro_factura = $this->security->xss_clean($this->input->post('nro_factura'));
                $fec_doc = $this->security->xss_clean($this->input->post('fec_doc'));
                $fec_desde = $this->security->xss_clean($this->input->post('fec_desde'));
                $fec_hasta = $this->security->xss_clean($this->input->post('fec_hasta'));
                $fec_prox_lec = $this->security->xss_clean($this->input->post('fec_prox_lec'));
                $valor_produccion = $this->security->xss_clean($this->input->post('valor_produccion'));
                $mes = $this->security->xss_clean($this->input->post('mes'));
                $mode = $this->security->xss_clean($this->input->post('factura_mode'));
                $tarifa = $this->security->xss_clean($this->input->post('tarifa'));
                $tarifa = is_numeric($tarifa) ? $tarifa : 0 ;

                if($mode == 'doble') {
                    $nro_factura_gen = $this->security->xss_clean($this->input->post('nro_factura_gen'));
                    $fec_doc_gen = $this->security->xss_clean($this->input->post('fec_doc_gen'));
                    $fec_desde_gen = $this->security->xss_clean($this->input->post('fec_desde_gen'));
                    $fec_hasta_gen = $this->security->xss_clean($this->input->post('fec_hasta_gen'));
                    $fec_prox_lec_gen = $this->security->xss_clean($this->input->post('fec_prox_lec_gen'));
                } else if($mode == 'simple') {
                    $nro_factura = $this->security->xss_clean($this->input->post('nro_factura_libre'));
                    $fec_doc = $this->security->xss_clean($this->input->post('fec_doc_libre'));
                    $fec_desde = $this->security->xss_clean($this->input->post('fec_desde_libre'));
                    $fec_hasta = $this->security->xss_clean($this->input->post('fec_hasta_libre'));
                    $fec_prox_lec = $this->security->xss_clean($this->input->post('fec_prox_lec_libre'));

                    $nro_factura_gen = $nro_factura;
                    $fec_doc_gen = $fec_doc;
                    $fec_desde_gen= $fec_desde;
                    $fec_hasta_gen = $fec_hasta;
                    $fec_prox_lec_gen = $fec_prox_lec;
                } else {
                    $nro_factura_gen = $nro_factura;
                    $fec_doc_gen = $fec_doc;
                    $fec_desde_gen= $fec_desde;
                    $fec_hasta_gen = $fec_hasta;
                    $fec_prox_lec_gen = $fec_prox_lec;
                }

                if (empty($nro_factura) || empty($fec_doc) || empty($fec_desde) || empty($fec_hasta) || empty($fec_prox_lec) || empty($mes)) {

                    $this->error->append(['code' => 101, 'msg' => 'Indique Nro., fecha, período, y fecha de proxima lectura para el documento / ' . $nro_factura . '-' . $fec_doc . '-' . $fec_desde . '-' .$fec_hasta . '-' . $fec_prox_lec . '-' . $mes]);
                    
                } else {
                    
                    $this->load->model('Suministro');
                    
                    try {

                        $suministro = $this->Suministro->get_suministro_last_month($token);

                    } catch(Exeption $e) {

                        $suministro = FALSE;
                        $this->error->append(['code' => 150, 'msg' => 'Error al ingresar factura']);

                    }

                    $ac_lec_ant = $this->security->xss_clean($this->input->post('ac_lec_ant'));
                    $ac_lec_ant = is_numeric($ac_lec_ant) ? $ac_lec_ant : 0 ;

                    $ac_lec_act = $this->security->xss_clean($this->input->post('ac_lec_act'));
                    $ac_lec_act = is_numeric($ac_lec_act) ? $ac_lec_act : 0 ;

                    $ac_consumo = $this->security->xss_clean($this->input->post('ene_consumo'));
                    $ac_consumo = is_numeric($ac_consumo) ? $ac_consumo : 0 ;

                    $re_lec_ant = $this->security->xss_clean($this->input->post('re_lec_ant'));
                    $re_lec_ant = is_numeric($re_lec_ant) ? $re_lec_ant : 0 ;

                    $re_lec_act = $this->security->xss_clean($this->input->post('re_lec_act'));
                    $re_lec_act = is_numeric($re_lec_act) ? $re_lec_act : 0 ;

                    $re_consumo = $this->security->xss_clean($this->input->post('re_consumo'));
                    $re_consumo = is_numeric($re_consumo ) ? $re_consumo : 0 ;


                    $ac_lec_ant_gen = $this->security->xss_clean($this->input->post('ac_lec_ant_gen'));
                    $ac_lec_ant_gen = is_numeric($ac_lec_ant_gen) ? $ac_lec_ant_gen : 0 ;

                    $ac_lec_act_gen = $this->security->xss_clean($this->input->post('ac_lec_act_gen'));
                    $ac_lec_act_gen = is_numeric($ac_lec_act_gen) ? $ac_lec_act_gen : 0 ;

                    $ac_consumo_gen = $this->security->xss_clean($this->input->post('ene_consumo_gen'));
                    $ac_consumo_gen = is_numeric($ac_consumo_gen) ? $ac_consumo_gen : 0 ;

                    $re_lec_ant_gen = $this->security->xss_clean($this->input->post('re_lec_ant_gen'));
                    $re_lec_ant_gen = is_numeric($re_lec_ant_gen) ? $re_lec_ant_gen : 0 ;

                    $re_lec_act_gen = $this->security->xss_clean($this->input->post('re_lec_act_gen'));
                    $re_lec_act_gen = is_numeric($re_lec_act_gen) ? $re_lec_act_gen : 0 ;

                    $re_consumo_gen = $this->security->xss_clean($this->input->post('re_consumo_gen'));
                    $re_consumo_gen = is_numeric($re_consumo_gen ) ? $re_consumo_gen : 0 ;


                    $fp_consumo = $this->security->xss_clean($this->input->post('fp_consumo'));
                    $fp_consumo = is_numeric($fp_consumo) ? $fp_consumo : 0 ;

                    $ep_consumo = $this->security->xss_clean($this->input->post('ep_consumo'));
                    $ep_consumo = is_numeric($ep_consumo) ? $ep_consumo : 0 ;


                    $ep_consumo_gen = $this->security->xss_clean($this->input->post('ep_consumo_gen'));
                    $ep_consumo_gen = is_numeric($ep_consumo_gen) ? $ep_consumo_gen : 0 ;


                    $cargo_fijo = $this->security->xss_clean($this->input->post('cargo_fijo'));
                    $cargo_fijo = is_numeric($cargo_fijo) ? $cargo_fijo : 0 ;

                    $cargo_postal = $this->security->xss_clean($this->input->post('cargo_postal'));
                    $cargo_postal = is_numeric($cargo_postal) ? $cargo_postal : 0 ;

                    $ene_precio = $this->security->xss_clean($this->input->post('ene_precio'));
                    $ene_precio = is_numeric($ene_precio) ? $ene_precio : 0 ;
                    

                    $ene_precio_gen = $this->security->xss_clean($this->input->post('ene_precio_gen'));
                    $ene_precio_gen = is_numeric($ene_precio_gen) ? $ene_precio_gen : 0 ;


                    $deman_fp_fact = $this->security->xss_clean($this->input->post('deman_fp_fact'));
                    $deman_fp_fact = is_numeric($deman_fp_fact) ? $deman_fp_fact : 0 ;

                    $deman_fp_prec = $this->security->xss_clean($this->input->post('deman_fp_prec'));
                    $deman_fp_prec = is_numeric($deman_fp_prec) ? $deman_fp_prec : 0 ;

                    $deman_ep_fact = $this->security->xss_clean($this->input->post('deman_ep_fact'));
                    $deman_ep_fact = is_numeric($deman_ep_fact) ? $deman_ep_fact : 0 ;
                    
                    $deman_ep_prec = $this->security->xss_clean($this->input->post('deman_ep_prec'));
                    $deman_ep_prec = is_numeric($deman_ep_prec) ? $deman_ep_prec : 0 ;

                    $factor_potencia = $this->security->xss_clean($this->input->post('factor_potencia'));
                    $factor_potencia = is_numeric($factor_potencia) ? $factor_potencia : 0 ;


                    $deman_ep_fact_gen = $this->security->xss_clean($this->input->post('deman_ep_fact_gen'));
                    $deman_ep_fact_gen = is_numeric($deman_ep_fact_gen) ? $deman_ep_fact_gen : 0 ;
                    
                    $deman_ep_prec_gen = $this->security->xss_clean($this->input->post('deman_ep_prec_gen'));
                    $deman_ep_prec_gen = is_numeric($deman_ep_prec_gen) ? $deman_ep_prec_gen : 0 ;


                    $cargo_arriendo = $this->security->xss_clean($this->input->post('cargo_arriendo'));
                    $cargo_arriendo = is_numeric($cargo_arriendo) ? $cargo_arriendo : 0 ;

                    $cargo_trans = $this->security->xss_clean($this->input->post('cargo_trans'));
                    $cargo_trans = is_numeric($cargo_trans) ? $cargo_trans : 0 ;

                    $cargo_interes = $this->security->xss_clean($this->input->post('cargo_interes'));
                    $cargo_interes = is_numeric($cargo_interes) ? $cargo_interes : 0 ;

                    $cargo_domi = $this->security->xss_clean($this->input->post('cargo_domi'));
                    $cargo_domi = is_numeric($cargo_domi) ? $cargo_domi : 0 ;

                    $cargo_corte = $this->security->xss_clean($this->input->post('cargo_corte'));
                    $cargo_corte = is_numeric($cargo_corte) ? $cargo_corte : 0 ;



                    $cargo_serv_pub = $this->security->xss_clean($this->input->post('cargo_serv_pub'));
                    $cargo_serv_pub = is_numeric($cargo_serv_pub) ? $cargo_serv_pub : 0 ;

                    $cargo_lgse = $this->security->xss_clean($this->input->post('cargo_lgse'));
                    $cargo_lgse = is_numeric($cargo_lgse) ? $cargo_lgse : 0 ;

                    $cargo_min_tec = $this->security->xss_clean($this->input->post('cargo_min_tec'));
                    $cargo_min_tec = is_numeric($cargo_min_tec) ? $cargo_min_tec : 0 ;

                    $cargo_serv_com = $this->security->xss_clean($this->input->post('cargo_serv_com'));
                    $cargo_serv_com = is_numeric($cargo_serv_com) ? $cargo_serv_com : 0 ;

                    $cargo_armonizacion = $this->security->xss_clean($this->input->post('cargo_armonizacion'));
                    $cargo_armonizacion = is_numeric($cargo_armonizacion) ? $cargo_armonizacion : 0 ;



                    $cargo_otros = $this->security->xss_clean($this->input->post('cargo_otros'));
                    $cargo_otros = is_numeric($cargo_otros) ? $cargo_otros : 0 ;

                    $cargo_reliq = $this->security->xss_clean($this->input->post('cargo_reliq'));
                    $cargo_reliq = is_numeric($cargo_reliq) ? $cargo_reliq : 0 ;

                    $cargo_desc = $this->security->xss_clean($this->input->post('cargo_desc'));
                    $cargo_desc = is_numeric($cargo_desc) ? $cargo_desc : 0 ;


                    $cargo_otros_gen = $this->security->xss_clean($this->input->post('cargo_otros_gen'));
                    $cargo_otros_gen = is_numeric($cargo_otros_gen) ? $cargo_otros_gen : 0 ;

                    $cargo_interes_gen = $this->security->xss_clean($this->input->post('cargo_interes_gen'));
                    $cargo_interes_gen = is_numeric($cargo_interes_gen) ? $cargo_interes_gen : 0 ;

                    $cargo_desc_gen = $this->security->xss_clean($this->input->post('cargo_desc_gen'));
                    $cargo_desc_gen = is_numeric($cargo_desc_gen) ? $cargo_desc_gen : 0 ;

                    
                    if($mode != 'default') {
                        $monto_exento_libre = $this->security->xss_clean($this->input->post('monto_exento_libre'));
                        $monto_exento_libre = is_numeric($monto_exento_libre) ? $monto_exento_libre : 0 ;

                        $total_neto_libre = $this->security->xss_clean($this->input->post('total_neto_libre'));
                        $total_neto_libre = is_numeric($total_neto_libre) ? $total_neto_libre : 0 ;

                        $cargo_otr_libre = $this->security->xss_clean($this->input->post('cargo_otr_libre'));
                        $cargo_otr_libre = is_numeric($cargo_otr_libre) ? $cargo_otr_libre : 0 ;

                        $saldo_ant_libre = $this->security->xss_clean($this->input->post('saldo_ant_libre'));
                        $saldo_ant_libre = is_numeric($saldo_ant_libre) ? $saldo_ant_libre : 0 ;

                        $total_pagar_libre = $this->security->xss_clean($this->input->post('total_pagar_libre'));
                        $total_pagar_libre = is_numeric($total_pagar_libre) ? $total_pagar_libre : 0 ;

                        if($mode == 'simple') {
                            $total_neto = $this->security->xss_clean($this->input->post('neto_ene'));
                            $total_neto = is_numeric($total_neto) ? $total_neto : 0 ;
                            
                            $total_neto_gen = $total_neto_libre;

                            $monto_exento_gen = $monto_exento = 0;
                            $cargo_otr_gen = $cargo_otr = 0;
                            $saldo_ant_gen = $saldo_ant = 0;
                            $total_pagar_gen = $total_pagar = 0;
                        } else {
                            $monto_exento = $this->security->xss_clean($this->input->post('monto_exento'));
                            $monto_exento = is_numeric($monto_exento) ? $monto_exento : 0 ;

                            $total_neto = $this->security->xss_clean($this->input->post('total_neto'));
                            $total_neto = is_numeric($total_neto) ? $total_neto : 0 ;

                            $cargo_otr = $this->security->xss_clean($this->input->post('cargo_otr'));
                            $cargo_otr = is_numeric($cargo_otr) ? $cargo_otr : 0 ;

                            $saldo_ant = $this->security->xss_clean($this->input->post('saldo_ant'));
                            $saldo_ant = is_numeric($saldo_ant) ? $saldo_ant : 0 ;

                            $total_pagar = $this->security->xss_clean($this->input->post('total_pagar'));
                            $total_pagar = is_numeric($total_pagar) ? $total_pagar : 0 ;


                            $monto_exento_gen = $this->security->xss_clean($this->input->post('monto_exento_gen'));
                            $monto_exento_gen = is_numeric($monto_exento_gen) ? $monto_exento_gen : 0 ;

                            $total_neto_gen = $this->security->xss_clean($this->input->post('total_neto_gen'));
                            $total_neto_gen = is_numeric($total_neto_gen) ? $total_neto_gen : 0 ;

                            $cargo_otr_gen = $this->security->xss_clean($this->input->post('cargo_otr_gen'));
                            $cargo_otr_gen = is_numeric($cargo_otr_gen) ? $cargo_otr_gen : 0 ;

                            $saldo_ant_gen = $this->security->xss_clean($this->input->post('saldo_ant_gen'));
                            $saldo_ant_gen = is_numeric($saldo_ant_gen) ? $saldo_ant_gen : 0 ;

                            $total_pagar_gen = $this->security->xss_clean($this->input->post('total_pagar_gen'));
                            $total_pagar_gen = is_numeric($total_pagar_gen) ? $total_pagar_gen : 0 ;
                        }
                    } else {
                        $monto_exento = $this->security->xss_clean($this->input->post('monto_exento'));
                        $monto_exento = is_numeric($monto_exento) ? $monto_exento : 0 ;

                        $total_neto = $this->security->xss_clean($this->input->post('total_neto'));
                        $total_neto = is_numeric($total_neto) ? $total_neto : 0 ;

                        $cargo_otr = $this->security->xss_clean($this->input->post('cargo_otr'));
                        $cargo_otr = is_numeric($cargo_otr) ? $cargo_otr : 0 ;

                        $saldo_ant = $this->security->xss_clean($this->input->post('saldo_ant'));
                        $saldo_ant = is_numeric($saldo_ant) ? $saldo_ant : 0 ;

                        $total_pagar = $this->security->xss_clean($this->input->post('total_pagar'));
                        $total_pagar = is_numeric($total_pagar) ? $total_pagar : 0 ;

                        $monto_exento_libre = $monto_exento_gen = 0;
                        $total_neto_libre = $total_neto_gen = 0;
                        $cargo_otr_libre = $cargo_otr_gen = 0;
                        $saldo_ant_libre = $saldo_ant_gen = 0;
                        $total_pagar_libre = $total_pagar_gen = 0;
                    }


                    $etapa = $this->security->xss_clean($this->input->post('etapa'));

                    $tarB = $this->security->xss_clean($this->input->post('tarB'));
                    $tarB = is_numeric($tarB) ? $tarB : 0 ;

                    $estado_ana = $this->security->xss_clean($this->input->post('estado_analisis'));
                    $estado_ana = is_numeric($estado_ana) ? $estado_ana : 0 ;

                    $cp_consumo = $this->security->xss_clean($this->input->post('cp_consumo'));
                    $cp_consumo = is_numeric($cp_consumo) ? $cp_consumo : 0 ;

                    $cp_precio = $this->security->xss_clean($this->input->post('cp_precio'));
                    $cp_precio = is_numeric($cp_precio) ? $cp_precio : 0 ;

                    $fec_ana = date('Y-m-d');
                    
                    $tipo_ana = $this->security->xss_clean($this->input->post('tipo_ana'));
                    
                    $obs_in = $this->security->xss_clean($this->input->post('obs_in'));
                    
                    $obs_cli = $this->security->xss_clean($this->input->post('obs_cli'));

                    $estado_alert = $this->security->xss_clean($this->input->post('estado_alerta'));
                    
                    $obs_alert = $this->security->xss_clean($this->input->post('obs_alert'));

                    $file_name = "";
                    $file_name_gen = "";

                    if(!empty($_FILES["facturafile"])) {

                        $target_file = str_replace(" ", "_", trim($_FILES["facturafile"]["name"]));
                    
                    } else {

                        $target_file = "";

                    }

                    if(!empty($_FILES["facturafile_gen"])) {

                        $target_file_gen = str_replace(" ", "_", trim($_FILES["facturafile_gen"]["name"]));
                    
                    } else {

                        $target_file_gen = "";

                    }

                    if($ac_lec_ant > 0 && $ac_lec_act > 0  && $ac_lec_ant > $ac_lec_act) {
                        $error_lectura = TRUE;
                        $this->error->append(['code' => 225, 'msg' => '(Energía Activa) El valor para "Lectura Anterior" debe ser menor que el de "Lectura Actual"']);
                    }

                    if($re_lec_ant > 0 && $re_lec_act > 0 && $re_lec_ant > $re_lec_act) {
                        $error_lectura = TRUE;
                        $this->error->append(['code' => 325, 'msg' => '(Energía Reactiva) El valor para "Lectura Anterior" debe ser menor que el de "Lectura Actual"']);
                    }
                    
                    if (! $error_lectura) {

                        if (!empty($target_file)) {

                            $file = $this->do_upload('facturafile');
        
                            if($file["status"] == "error") {

                                $this->alert->append(['code' => 300, 'msg' => $file["content"]]);
                            
                            } else {
        
                                $file_name = $file["content"]["file_name"];
        
                            }

                        }

                        if (!empty($target_file_gen)) {

                            $file_gen = $this->do_upload('facturafile_gen');
        
                            if($file_gen["status"] == "error") {

                                $this->alert->append(['code' => 300, 'msg' => $file_gen["content"]]);
                            
                            } else {
        
                                $file_name_gen = $file_gen["content"]["file_name"];
        
                            }

                        }

                                        
                        if(!empty($suministro)) {

                            $last_month = strtotime($suministro->last_month . "-01 00:00:00");
                            $fecha = strtotime($mes . "-01 00:00:00");

                            // if($fecha > $last_month ) {
                        

                                try {     
                                    
                                $id_factura = $this->Suministro->insert_factura($token, $nro_factura, $fec_doc, $fec_desde,
                                                                        $fec_hasta, $fec_prox_lec, $valor_produccion, 
                                                                        $mes, $ac_lec_ant, $ac_lec_act, $ac_consumo, 
                                                                        $re_lec_ant, $re_lec_act, $re_consumo,
                                                                        $fp_consumo, $ep_consumo, $cargo_fijo,
                                                                        $cargo_postal, $ene_precio, $deman_fp_fact,
                                                                        $deman_fp_prec, $deman_ep_fact, $deman_ep_prec,
                                                                        $factor_potencia, $cargo_arriendo, $cargo_trans,
                                                                        $cargo_interes, $cargo_domi, $cargo_corte, $cargo_serv_pub,
                                                                        $cargo_otros, $cargo_reliq, $cargo_desc,
                                                                        $monto_exento, $total_neto, $cargo_otr, $saldo_ant,
                                                                        $total_pagar, $etapa, $tarB, $file_name, $cp_consumo, $cp_precio,
                                                                        $nro_factura_gen, $fec_doc_gen, $fec_desde_gen, $fec_hasta_gen,
                                                                        $fec_prox_lec_gen, $ac_lec_ant_gen, $ac_lec_act_gen,
                                                                        $ac_consumo_gen, $re_lec_ant_gen, $re_lec_act_gen, 
                                                                        $re_consumo_gen, $ep_consumo_gen, $ene_precio_gen,
                                                                        $deman_ep_fact_gen, $deman_ep_prec_gen, 
                                                                        $cargo_lgse, $cargo_min_tec, $cargo_serv_com,
                                                                        $cargo_armonizacion, $total_neto_gen, $cargo_interes_gen,
                                                                        $cargo_desc_gen, $cargo_otros_gen, 
                                                                        $cargo_otr_gen, $total_pagar_gen, $monto_exento_gen,
                                                                        $saldo_ant_gen, $file_name_gen, $tarifa,
                                                                        $total_neto_libre, $cargo_otr_libre, $total_pagar_libre,
                                                                        $monto_exento_libre, $saldo_ant_libre, $mode);
                            
                                } catch(Exception $e) {

                                    $this->error->append(['code' => 102, 'msg' => 'Error al ingresar factura']);
                
                                }

                                if(empty($id_factura)) {

                                    $this->error->append(['code' => 103, 'msg' => 'Error al ingresar factura']);
                
                                } else {

                                    try {     
                                    
                                        $id = $this->Suministro->insert_analisis($id_factura, $estado_ana, $fec_ana,
                                                                                    $tipo_ana, $obs_in, $obs_cli);
                                    
                                    } catch(Exception $e) {
        
                                        $this->error->append(['code' => 115, 'msg' => 'Factura ingresada con error (análisis[115])']);
                    
                                    }

                                    // if(empty($id)) {
                                    //     $this->error->append(['code' => 116, 'msg' => 'Factura ingresada con error (análisis)']);
                                    // }

                                    try {     
                                    
                                        $id = $this->Suministro->insert_alerta($id_factura, $estado_alert, $obs_alert);
                                    
                                    } catch(Exception $e) {
        
                                        $this->error->append(['code' => 117, 'msg' => 'Factura ingresada con error (alerta[117])']);
                    
                                    }

                                    // if(empty($id)) {
                                    //     $this->error->append(['code' => 118, 'msg' => 'Factura ingresada con error (alerta)']);
                                    // }

                                }
                            // } else {

                            //     $this->error->append(['code' => 105, 'msg' => 'Ya existe una factura para el período seleccionado']);

                            // }

                        } else {

                            $this->error->append(['code' => 150, 'msg' => 'Error al ingresar factura']);

                        }
                    }
                }
            } else {
                $this->error->append(['code' => 110, 'msg' => 'No tienes permisos para realizar esta operación']);
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible registrar factura']);
        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "alert" => $this->alert)) );

    }

    public function edit_factura() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);
        $this->alert = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $error_lectura = FALSE;

        // $token = $this->encryption->decrypt(base64_decode($this->input->post('token')));

        $token = $this->encryption->decrypt(base64_decode($this->input->post('token')));

        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 4) {
                $nro_factura = $this->security->xss_clean($this->input->post('nro_factura'));
                $fec_doc = $this->security->xss_clean($this->input->post('fec_doc'));
                $fec_desde = $this->security->xss_clean($this->input->post('fec_desde'));
                $fec_hasta = $this->security->xss_clean($this->input->post('fec_hasta'));
                $fec_prox_lec = $this->security->xss_clean($this->input->post('fec_prox_lec'));
                $mes = $this->security->xss_clean($this->input->post('mes'));
                $mode = $this->security->xss_clean($this->input->post('factura_mode'));
                $tarifa = $this->security->xss_clean($this->input->post('tarifa'));

                if($mode == 'doble') {
                    $nro_factura_gen = $this->security->xss_clean($this->input->post('nro_factura_gen'));
                    $fec_doc_gen = $this->security->xss_clean($this->input->post('fec_doc_gen'));
                    $fec_desde_gen = $this->security->xss_clean($this->input->post('fec_desde_gen'));
                    $fec_hasta_gen = $this->security->xss_clean($this->input->post('fec_hasta_gen'));
                    $fec_prox_lec_gen = $this->security->xss_clean($this->input->post('fec_prox_lec_gen'));
                } else if($mode == 'simple') {
                    $nro_factura = $this->security->xss_clean($this->input->post('nro_factura_libre'));
                    $fec_doc = $this->security->xss_clean($this->input->post('fec_doc_libre'));
                    $fec_desde = $this->security->xss_clean($this->input->post('fec_desde_libre'));
                    $fec_hasta = $this->security->xss_clean($this->input->post('fec_hasta_libre'));
                    $fec_prox_lec = $this->security->xss_clean($this->input->post('fec_prox_lec_libre'));

                    $nro_factura_gen = $nro_factura;
                    $fec_doc_gen = $fec_doc;
                    $fec_desde_gen= $fec_desde;
                    $fec_hasta_gen = $fec_hasta;
                    $fec_prox_lec_gen = $fec_prox_lec;
                } else {
                    $nro_factura_gen = $nro_factura;
                    $fec_doc_gen = $fec_doc;
                    $fec_desde_gen= $fec_desde;
                    $fec_hasta_gen = $fec_hasta;
                    $fec_prox_lec_gen = $fec_prox_lec;
                }

                if (empty($nro_factura) || empty($nro_factura_gen) || empty($fec_doc) || empty($fec_doc_gen) || empty($fec_desde) || empty($fec_desde_gen) || empty($fec_hasta) || empty($fec_hasta_gen) || empty($fec_prox_lec) || empty($fec_prox_lec_gen) || empty($mes)) {

                    $this->error->append(['code' => 101, 'msg' => 'Indique Nro., fecha, período, y fecha de proxima lectura para el documento']);
                    
                } else {

                    $valor_produccion = $this->security->xss_clean($this->input->post('valor_produccion'));
                    $valor_produccion = is_numeric($valor_produccion) ? $valor_produccion : 0 ;
                    
                    $ac_lec_ant = $this->security->xss_clean($this->input->post('ac_lec_ant'));
                    $ac_lec_ant = is_numeric($ac_lec_ant) ? $ac_lec_ant : 0 ;

                    $ac_lec_act = $this->security->xss_clean($this->input->post('ac_lec_act'));
                    $ac_lec_act = is_numeric($ac_lec_act) ? $ac_lec_act : 0 ;

                    $ac_consumo = $this->security->xss_clean($this->input->post('ene_consumo'));
                    $ac_consumo = is_numeric($ac_consumo) ? $ac_consumo : 0 ;

                    $re_lec_ant = $this->security->xss_clean($this->input->post('re_lec_ant'));
                    $re_lec_ant = is_numeric($re_lec_ant) ? $re_lec_ant : 0 ;

                    $re_lec_act = $this->security->xss_clean($this->input->post('re_lec_act'));
                    $re_lec_act = is_numeric($re_lec_act) ? $re_lec_act : 0 ;

                    $re_consumo = $this->security->xss_clean($this->input->post('re_consumo'));
                    $re_consumo = is_numeric($re_consumo ) ? $re_consumo : 0 ;


                    
                    $ac_lec_ant_gen = $this->security->xss_clean($this->input->post('ac_lec_ant_gen'));
                    $ac_lec_ant_gen = is_numeric($ac_lec_ant_gen) ? $ac_lec_ant_gen : 0 ;

                    $ac_lec_act_gen = $this->security->xss_clean($this->input->post('ac_lec_act_gen'));
                    $ac_lec_act_gen = is_numeric($ac_lec_act_gen) ? $ac_lec_act_gen : 0 ;

                    $ac_consumo_gen = $this->security->xss_clean($this->input->post('ene_consumo_gen'));
                    $ac_consumo_gen = is_numeric($ac_consumo_gen) ? $ac_consumo_gen : 0 ;

                    $re_lec_ant_gen = $this->security->xss_clean($this->input->post('re_lec_ant_gen'));
                    $re_lec_ant_gen = is_numeric($re_lec_ant_gen) ? $re_lec_ant_gen : 0 ;

                    $re_lec_act_gen = $this->security->xss_clean($this->input->post('re_lec_act_gen'));
                    $re_lec_act_gen = is_numeric($re_lec_act_gen) ? $re_lec_act_gen : 0 ;

                    $re_consumo_gen = $this->security->xss_clean($this->input->post('re_consumo_gen'));
                    $re_consumo_gen = is_numeric($re_consumo_gen ) ? $re_consumo_gen : 0 ;



                    $fp_consumo = $this->security->xss_clean($this->input->post('fp_consumo'));
                    $fp_consumo = is_numeric($fp_consumo) ? $fp_consumo : 0 ;

                    $ep_consumo = $this->security->xss_clean($this->input->post('ep_consumo'));
                    $ep_consumo = is_numeric($ep_consumo) ? $ep_consumo : 0 ;



                    $ep_consumo_gen = $this->security->xss_clean($this->input->post('ep_consumo_gen'));
                    $ep_consumo_gen = is_numeric($ep_consumo_gen) ? $ep_consumo_gen : 0 ;



                    $cargo_fijo = $this->security->xss_clean($this->input->post('cargo_fijo'));
                    $cargo_fijo = is_numeric($cargo_fijo) ? $cargo_fijo : 0 ;

                    $cargo_postal = $this->security->xss_clean($this->input->post('cargo_postal'));
                    $cargo_postal = is_numeric($cargo_postal) ? $cargo_postal : 0 ;

                    $ene_precio = $this->security->xss_clean($this->input->post('ene_precio'));
                    $ene_precio = is_numeric($ene_precio) ? $ene_precio : 0 ;



                    $ene_precio_gen = $this->security->xss_clean($this->input->post('ene_precio_gen'));
                    $ene_precio_gen = is_numeric($ene_precio_gen) ? $ene_precio_gen : 0 ;



                    $deman_fp_fact = $this->security->xss_clean($this->input->post('deman_fp_fact'));
                    $deman_fp_fact = is_numeric($deman_fp_fact) ? $deman_fp_fact : 0 ;

                    $deman_fp_prec = $this->security->xss_clean($this->input->post('deman_fp_prec'));
                    $deman_fp_prec = is_numeric($deman_fp_prec) ? $deman_fp_prec : 0 ;

                    $deman_ep_fact = $this->security->xss_clean($this->input->post('deman_ep_fact'));
                    $deman_ep_fact = is_numeric($deman_ep_fact) ? $deman_ep_fact : 0 ;
                    
                    $deman_ep_prec = $this->security->xss_clean($this->input->post('deman_ep_prec'));
                    $deman_ep_prec = is_numeric($deman_ep_prec) ? $deman_ep_prec : 0 ;

                    $factor_potencia = $this->security->xss_clean($this->input->post('factor_potencia'));
                    $factor_potencia = is_numeric($factor_potencia) ? $factor_potencia : 0 ;



                    $deman_ep_fact_gen = $this->security->xss_clean($this->input->post('deman_ep_fact_gen'));
                    $deman_ep_fact_gen = is_numeric($deman_ep_fact_gen) ? $deman_ep_fact_gen : 0 ;
                    
                    $deman_ep_prec_gen = $this->security->xss_clean($this->input->post('deman_ep_prec_gen'));
                    $deman_ep_prec_gen = is_numeric($deman_ep_prec_gen) ? $deman_ep_prec_gen : 0 ;



                    $cargo_arriendo = $this->security->xss_clean($this->input->post('cargo_arriendo'));
                    $cargo_arriendo = is_numeric($cargo_arriendo) ? $cargo_arriendo : 0 ;

                    $cargo_trans = $this->security->xss_clean($this->input->post('cargo_trans'));
                    $cargo_trans = is_numeric($cargo_trans) ? $cargo_trans : 0 ;

                    $cargo_interes = $this->security->xss_clean($this->input->post('cargo_interes'));
                    $cargo_interes = is_numeric($cargo_interes) ? $cargo_interes : 0 ;

                    $cargo_domi = $this->security->xss_clean($this->input->post('cargo_domi'));
                    $cargo_domi = is_numeric($cargo_domi) ? $cargo_domi : 0 ;



                    $cargo_corte = $this->security->xss_clean($this->input->post('cargo_corte'));
                    $cargo_corte = is_numeric($cargo_corte) ? $cargo_corte : 0 ;

                    $cargo_lgse = $this->security->xss_clean($this->input->post('cargo_lgse'));
                    $cargo_lgse = is_numeric($cargo_lgse) ? $cargo_lgse : 0 ;

                    $cargo_min_tec = $this->security->xss_clean($this->input->post('cargo_min_tec'));
                    $cargo_min_tec = is_numeric($cargo_min_tec) ? $cargo_min_tec : 0 ;

                    $cargo_serv_com = $this->security->xss_clean($this->input->post('cargo_serv_com'));
                    $cargo_serv_com = is_numeric($cargo_serv_com) ? $cargo_serv_com : 0 ;

                    $cargo_armonizacion = $this->security->xss_clean($this->input->post('cargo_armonizacion'));
                    $cargo_armonizacion = is_numeric($cargo_armonizacion) ? $cargo_armonizacion : 0 ;



                    $cargo_serv_pub = $this->security->xss_clean($this->input->post('cargo_serv_pub'));
                    $cargo_serv_pub = is_numeric($cargo_serv_pub) ? $cargo_serv_pub : 0 ;

                    $cargo_otros = $this->security->xss_clean($this->input->post('cargo_otros'));
                    $cargo_otros = is_numeric($cargo_otros) ? $cargo_otros : 0 ;

                    $cargo_reliq = $this->security->xss_clean($this->input->post('cargo_reliq'));
                    $cargo_reliq = is_numeric($cargo_reliq) ? $cargo_reliq : 0 ;

                    $cargo_desc = $this->security->xss_clean($this->input->post('cargo_desc'));
                    $cargo_desc = is_numeric($cargo_desc) ? $cargo_desc : 0 ;



                    $cargo_otros_gen = $this->security->xss_clean($this->input->post('cargo_otros_gen'));
                    $cargo_otros_gen = is_numeric($cargo_otros_gen) ? $cargo_otros_gen : 0 ;

                    $cargo_interes_gen = $this->security->xss_clean($this->input->post('cargo_interes_gen'));
                    $cargo_interes_gen = is_numeric($cargo_interes_gen) ? $cargo_interes_gen : 0 ;

                    $cargo_desc_gen = $this->security->xss_clean($this->input->post('cargo_desc_gen'));
                    $cargo_desc_gen = is_numeric($cargo_desc_gen) ? $cargo_desc_gen : 0 ;




                    if($mode != 'default') {
                        $monto_exento_libre = $this->security->xss_clean($this->input->post('monto_exento_libre'));
                        $monto_exento_libre = is_numeric($monto_exento_libre) ? $monto_exento_libre : 0 ;

                        $total_neto_libre = $this->security->xss_clean($this->input->post('total_neto_libre'));
                        $total_neto_libre = is_numeric($total_neto_libre) ? $total_neto_libre : 0 ;

                        $cargo_otr_libre = $this->security->xss_clean($this->input->post('cargo_otr_libre'));
                        $cargo_otr_libre = is_numeric($cargo_otr_libre) ? $cargo_otr_libre : 0 ;

                        $saldo_ant_libre = $this->security->xss_clean($this->input->post('saldo_ant_libre'));
                        $saldo_ant_libre = is_numeric($saldo_ant_libre) ? $saldo_ant_libre : 0 ;

                        $total_pagar_libre = $this->security->xss_clean($this->input->post('total_pagar_libre'));
                        $total_pagar_libre = is_numeric($total_pagar_libre) ? $total_pagar_libre : 0 ;

                        if($mode == 'simple') {
                            $total_neto = $this->security->xss_clean($this->input->post('neto_ene'));
                            $total_neto = is_numeric($total_neto) ? $total_neto : 0 ;
                            
                            $total_neto_gen = $total_neto_libre;

                            $monto_exento_gen = $monto_exento = 0;
                            $cargo_otr_gen = $cargo_otr = 0;
                            $saldo_ant_gen = $saldo_ant = 0;
                            $total_pagar_gen = $total_pagar = 0;
                        } else {
                            $monto_exento = $this->security->xss_clean($this->input->post('monto_exento'));
                            $monto_exento = is_numeric($monto_exento) ? $monto_exento : 0 ;

                            $total_neto = $this->security->xss_clean($this->input->post('total_neto'));
                            $total_neto = is_numeric($total_neto) ? $total_neto : 0 ;

                            $cargo_otr = $this->security->xss_clean($this->input->post('cargo_otr'));
                            $cargo_otr = is_numeric($cargo_otr) ? $cargo_otr : 0 ;

                            $saldo_ant = $this->security->xss_clean($this->input->post('saldo_ant'));
                            $saldo_ant = is_numeric($saldo_ant) ? $saldo_ant : 0 ;

                            $total_pagar = $this->security->xss_clean($this->input->post('total_pagar'));
                            $total_pagar = is_numeric($total_pagar) ? $total_pagar : 0 ;


                            $monto_exento_gen = $this->security->xss_clean($this->input->post('monto_exento_gen'));
                            $monto_exento_gen = is_numeric($monto_exento_gen) ? $monto_exento_gen : 0 ;

                            $total_neto_gen = $this->security->xss_clean($this->input->post('total_neto_gen'));
                            $total_neto_gen = is_numeric($total_neto_gen) ? $total_neto_gen : 0 ;

                            $cargo_otr_gen = $this->security->xss_clean($this->input->post('cargo_otr_gen'));
                            $cargo_otr_gen = is_numeric($cargo_otr_gen) ? $cargo_otr_gen : 0 ;

                            $saldo_ant_gen = $this->security->xss_clean($this->input->post('saldo_ant_gen'));
                            $saldo_ant_gen = is_numeric($saldo_ant_gen) ? $saldo_ant_gen : 0 ;

                            $total_pagar_gen = $this->security->xss_clean($this->input->post('total_pagar_gen'));
                            $total_pagar_gen = is_numeric($total_pagar_gen) ? $total_pagar_gen : 0 ;
                        }
                    } else {
                        $monto_exento = $this->security->xss_clean($this->input->post('monto_exento'));
                        $monto_exento = is_numeric($monto_exento) ? $monto_exento : 0 ;

                        $total_neto = $this->security->xss_clean($this->input->post('total_neto'));
                        $total_neto = is_numeric($total_neto) ? $total_neto : 0 ;

                        $cargo_otr = $this->security->xss_clean($this->input->post('cargo_otr'));
                        $cargo_otr = is_numeric($cargo_otr) ? $cargo_otr : 0 ;

                        $saldo_ant = $this->security->xss_clean($this->input->post('saldo_ant'));
                        $saldo_ant = is_numeric($saldo_ant) ? $saldo_ant : 0 ;

                        $total_pagar = $this->security->xss_clean($this->input->post('total_pagar'));
                        $total_pagar = is_numeric($total_pagar) ? $total_pagar : 0 ;

                        $monto_exento_libre = $monto_exento_gen = 0;
                        $total_neto_libre = $total_neto_gen = 0;
                        $cargo_otr_libre = $cargo_otr_gen = 0;
                        $saldo_ant_libre = $saldo_ant_gen = 0;
                        $total_pagar_libre = $total_pagar_gen = 0;
                    }




                    $tarB = $this->security->xss_clean($this->input->post('tarB'));
                    $tarB = is_numeric($tarB) ? $tarB : 0 ;

                    $estado_ana = $this->security->xss_clean($this->input->post('estado_analisis'));
                    $estado_ana = is_numeric($estado_ana) ? $estado_ana : 0 ;

                    $cp_consumo = $this->security->xss_clean($this->input->post('cp_consumo'));
                    $cp_consumo = is_numeric($cp_consumo) ? $cp_consumo : 0 ;

                    $cp_precio = $this->security->xss_clean($this->input->post('cp_precio'));
                    $cp_precio = is_numeric($cp_precio) ? $cp_precio : 0 ;

                    $last_estado_ana = $this->security->xss_clean($this->input->post('last_estado'));

                    $fec_ana = $estado_ana != $last_estado_ana ? date('Y-m-d') : $this->security->xss_clean($this->input->post('last_mod'));
                    
                    $tipo_ana = $this->security->xss_clean($this->input->post('tipo_ana'));
                    
                    $obs_in = $this->security->xss_clean($this->input->post('obs_in'));
                    
                    $obs_cli = $this->security->xss_clean($this->input->post('obs_cli'));

                    $estado_alert = $this->security->xss_clean($this->input->post('estado_alerta'));
                    
                    $obs_alert = $this->security->xss_clean($this->input->post('obs_alert'));

                    $file_name = $this->security->xss_clean($this->input->post('file_name'));
                    $file_name_gen = $this->security->xss_clean($this->input->post('file_name_gen'));

                    if(!empty($_FILES["facturafile"])) {

                        $target_file = str_replace(" ", "_", trim($_FILES["facturafile"]["name"]));
                    
                    } else {

                        $target_file = "";

                    }

                    if(!empty($_FILES["facturafile_gen"])) {

                        $target_file_gen = str_replace(" ", "_", trim($_FILES["facturafile_gen"]["name"]));
                    
                    } else {

                        $target_file_gen = "";

                    }
                    
                    if($ac_lec_ant > 0 && $ac_lec_act > 0 && $ac_lec_ant > $ac_lec_act) {
                        $error_lectura = TRUE;
                        $this->error->append(['code' => 225, 'msg' => '(Energía Activa) El valor para "Lectura Anterior" debe ser menor que el de "Lectura Actual"']);
                    }

                    if($re_lec_ant > 0 && $re_lec_act > 0 && $re_lec_ant > $re_lec_act) {
                        $error_lectura = TRUE;
                        $this->error->append(['code' => 325, 'msg' => '(Energía Reactiva) El valor para "Lectura Anterior" debe ser menor que el de "Lectura Actual"']);
                    }

                    if(! $error_lectura) {
                        if (!empty($target_file)) {

                            if($target_file != $file_name) {
        
                                $file = $this->do_upload('facturafile');
        
                                if($file["status"] == "error") {
        
                                    $this->alert->append(['code' => 300, 'msg' => $file["content"]]);
                            
                                } else {
            
                                    $file_name = $file["content"]["file_name"];
            
                                }
        
                            }

                        }

                        if($tarifa == '21' && $mode == 'doble') {

                            if (!empty($target_file_gen)) {
                                
                                if($target_file_gen != $file_name_gen){
                                    
                                    $file_gen = $this->do_upload('facturafile_gen');
                                    
                                    if($file_gen["status"] == "error") {
                                        
                                        $this->alert->append(['code' => 300, 'msg' => $file_gen["content"]]);
                                        
                                    } else {
                                        
                                        $file_name_gen = $file_gen["content"]["file_name"];
                                        
                                    }
                                }
                                
                            }
                        }

                        $this->load->model('Suministro');

                        try {     

                            $id_factura = $this->Suministro->update_factura($token, $valor_produccion, 
                                                                    $ac_lec_ant, $ac_lec_act, $ac_consumo, 
                                                                    $re_lec_ant, $re_lec_act, $re_consumo,
                                                                    $fp_consumo, $ep_consumo, $cargo_fijo,
                                                                    $cargo_postal, $ene_precio, $deman_fp_fact,
                                                                    $deman_fp_prec, $deman_ep_fact, $deman_ep_prec,
                                                                    $factor_potencia, $cargo_arriendo, $cargo_trans,
                                                                    $cargo_interes, $cargo_domi, $cargo_corte, $cargo_serv_pub,
                                                                    $cargo_otros, $cargo_reliq, $cargo_desc,
                                                                    $monto_exento, $total_neto, $cargo_otr, $saldo_ant,
                                                                    $total_pagar, $tarB, $nro_factura, $fec_doc, $fec_desde,
                                                                    $fec_hasta, $fec_prox_lec, $mes, $file_name, $cp_consumo, $cp_precio,
                                                                    $nro_factura_gen, $fec_doc_gen, $fec_desde_gen, $fec_hasta_gen,
                                                                    $fec_prox_lec_gen,
                                                                    $ac_lec_ant_gen, $ac_lec_act_gen,
                                                                    $ac_consumo_gen, $re_lec_ant_gen, $re_lec_act_gen, 
                                                                    $re_consumo_gen, $ep_consumo_gen, $ene_precio_gen,
                                                                    $deman_ep_fact_gen, $deman_ep_prec_gen, 
                                                                    $cargo_lgse, $cargo_min_tec, $cargo_serv_com,
                                                                    $cargo_armonizacion, $total_neto_gen, $cargo_interes_gen,
                                                                    $cargo_desc_gen, $cargo_otros_gen, 
                                                                    $cargo_otr_gen, $total_pagar_gen, $monto_exento_gen,
                                                                    $saldo_ant_gen, $file_name_gen, 
                                                                    $total_neto_libre, $cargo_otr_libre, $total_pagar_libre,
                                                                    $monto_exento_libre, $saldo_ant_libre, $mode);
                    
                        } catch(Exception $e) {

                            $this->error->append(['code' => 102, 'msg' => 'Error al actualizar factura']);

                        }

                        if(empty($id_factura)) {

                            $this->error->append(['code' => 103, 'msg' => 'Error al actualizar factura']);

                        } else {

                            try {     
                                    
                                $id_factura = $this->Suministro->update_analisis($token, $estado_ana, $fec_ana,
                                                                                    $tipo_ana, $obs_in, $obs_cli);
                        
                            } catch(Exception $e) {
        
                                $this->error->append(['code' => 104, 'msg' => 'Factura actualizada con errores']);
        
                            } 

                            if(empty($id_factura)) {

                                $this->error->append(['code' => 105, 'msg' => 'Factura actualizada con errores (análisis)']);
                            
                            }

                            try {     
                                    
                                $id_factura = $this->Suministro->update_alerta($token, $estado_alert, $obs_alert);
                        
                            } catch(Exception $e) {
        
                                $this->error->append(['code' => 106, 'msg' => 'Factura actualizada con errores (alerta)']);
        
                            }

                            if(empty($id_factura)) {

                                $this->error->append(['code' => 107, 'msg' => 'Factura actualizada con errores']);

                            }
                        }
                    }
                }
            } else {
                $this->error->append(['code' => 110, 'msg' => 'No tienes permisos para realizar esta operación']);
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar factura']);
        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok", "alert" => $this->alert)) );

    }

    public function delete_factura() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));

        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2 || $this->auth_lvl->id_perfil == 4) {

                try {     

                    $this->load->model('Suministro');
                        
                    $id_factura = $this->Suministro->delete_factura($token);
            
                } catch(Exception $e) {

                    $this->error->append(['code' => 102, 'msg' => 'Error al eliminar factura']);

                }

            } else {

                $this->error->append(['code' => 110, 'msg' => 'No tienes permisos para realizar esta operación']);

            }
        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible eliminar factura']);
        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );
    }

    public function agregar_suministro() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));

        $id_suministro = FALSE;
        
        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {

                $tarifario = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('tarifario'))));
                $nombre = $this->security->xss_clean($this->input->post('nombre'));
                $numcli = $this->security->xss_clean($this->input->post('numcli'));

                if(empty($tarifario) || empty($nombre) || empty($numcli)) {

                    $this->error->append(['code' => 101, 
                                        'msg' => 'Indique sector tarifario, nombre y número de cliente']);
                    
                } else {

                    $nombre = strtoupper($nombre);

                    $direccion = $this->security->xss_clean($this->input->post('direccion'));

                    $regiones = $this->security->xss_clean($this->input->post('region'));
                    $regiones = is_numeric($regiones) ? $regiones : 0;

                    $ciudades =  !empty($this->input->post('ciudad')) ? $this->security->xss_clean($this->input->post('ciudad')) : 0;
                    $ciudades = is_numeric($ciudades) ? $ciudades : 0;

                    $observacion = $this->security->xss_clean($this->input->post('observacion'));
                    $contacto = $this->security->xss_clean($this->input->post('contacto'));
                    $cargo = $this->security->xss_clean($this->input->post('cargo'));
                    $correo = $this->security->xss_clean($this->input->post('correo'));
                    $telefono = $this->security->xss_clean($this->input->post('telefono'));
                    $fec_ter_sum = $this->security->xss_clean($this->input->post('fec_ter_sum'));
                    $fec_cm_tarif = $this->security->xss_clean($this->input->post('fec_cm_tarif'));
                    $max_inv = $this->security->xss_clean($this->input->post('max_inv'));
                    $anno_base = $this->security->xss_clean($this->input->post('anno_base'));
                    
                    try {

                        $this->load->model('Suministro');

                        $id_suministro = $this->Suministro->insert_sumisnitro($token, $tarifario, $nombre, $numcli, $direccion, $regiones, 
                                                                                $ciudades, $observacion, $contacto, $cargo, $correo, 
                                                                                $telefono, $fec_ter_sum, $fec_cm_tarif, $max_inv, 
                                                                                $anno_base);

                    } catch(Exception $e) {

                        $this->error->append(['code' => 102, 'msg' => 'Error al ingresar suministro']);

                    }

                    if(!$id_suministro) {

                        $this->error->append(['code' => 103, 'msg' => 'Error al ingresar suministro']);

                    } else {
                        $id_suministro = base64_encode($this->encryption->encrypt($id_suministro));
                    }
                }
            } else {

                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible registrar suministro']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error)) : json_encode(array("status" => "ok", "content" => $id_suministro)) );

    }

    public function edit_suministro() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));
        
        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {

                $tarifario = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('tarifario'))));
                $nombre = $this->security->xss_clean($this->input->post('nombre'));
                $numcli = $this->security->xss_clean($this->input->post('numcli'));

                if(empty($tarifario) || empty($nombre) || empty($numcli)) {

                    $this->error->append(['code' => 101, 
                                        'msg' => 'Indique sector tarifario, nombre y número de cliente']);
                    
                } else {
                    
                    $nombre = strtoupper($nombre);
                    
                    $direccion = $this->security->xss_clean($this->input->post('direccion'));

                    $regiones = $this->security->xss_clean($this->input->post('region'));
                    $regiones = is_numeric($regiones) ? $regiones : 0;

                    $ciudades =  !empty($this->input->post('ciudad')) ? $this->security->xss_clean($this->input->post('ciudad')) : 0;
                    $ciudades = is_numeric($ciudades) ? $ciudades : 0; 

                    $observacion = $this->security->xss_clean($this->input->post('observacion'));
                    $contacto = $this->security->xss_clean($this->input->post('contacto'));
                    $cargo = $this->security->xss_clean($this->input->post('cargo'));
                    $correo = $this->security->xss_clean($this->input->post('correo'));
                    $telefono = $this->security->xss_clean($this->input->post('telefono'));
                    $fec_ter_sum = $this->security->xss_clean($this->input->post('fec_ter_sum'));
                    $fec_cm_tarif = $this->security->xss_clean($this->input->post('fec_cm_tarif'));
                    $max_inv = $this->security->xss_clean($this->input->post('max_inv'));
                    $anno_base = $this->security->xss_clean($this->input->post('anno_base'));
                    
                    try {

                        $this->load->model('Suministro');

                        $id_suministro = $this->Suministro->update_suministro($token, $tarifario, $nombre, $numcli, $direccion, $regiones, 
                                                                                $ciudades, $observacion, $contacto, $cargo, $correo, 
                                                                                $telefono, $fec_ter_sum, $fec_cm_tarif, $max_inv, 
                                                                                $anno_base);

                    } catch(Exception $e) {

                        $this->error->append(['code' => 102, 'msg' => 'Error al actualizar suministro']);

                    }

                    if(!$id_suministro) {

                        $this->error->append(['code' => 103, 'msg' => 'Error al actualizar suministro']);

                    }
                }
            } else {

                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar suministro']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );
    }

    public function edit_contrato() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('tcon'))));
        
        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {

                $fec_ini_gc = $this->security->xss_clean($this->input->post('fec_ini_gc'));
                $fec_ter_gc = $this->security->xss_clean($this->input->post('fec_ter_gc'));
                $fec_ini_save = $this->security->xss_clean($this->input->post('fec_ini_save'));

                if(empty($fec_ini_gc) || empty($fec_ter_gc) || empty($fec_ini_save)) {

                    $this->error->append(['code' => 101, 
                                        'msg' => 'Indique fecha de inicio, termino, e inicio de ahorro']);
                    
                } else {

                    $pct_con = $this->security->xss_clean($this->input->post('pct_con'));
                    $pct_con = is_numeric($pct_con) ? $pct_con : 0;

                    $estado =  !empty($this->input->post('estado')) ? $this->security->xss_clean($this->input->post('estado')) : 0;
                    $estado = $estado == 1 || $estado == 0 ? $estado : 0;

                    $observacion = $this->security->xss_clean($this->input->post('observacion3'));

                    $con_mod = $this->security->xss_clean($this->input->post('con_mod'));
                    
                    try {

                        if($con_mod == 1) {

                            $this->load->model('Suministro');

                            $id_suministro = $this->Suministro->update_contrato($token, $fec_ini_gc, $fec_ter_gc, 
                                                                                $fec_ini_save, $pct_con, $estado, $observacion);

                        } else {

                            $this->load->model('Contrato');

                            $id_suministro = $this->Contrato->insert_contrato($token, $fec_ini_gc, $fec_ter_gc, $observacion, 
                                                                $fec_ini_save, $pct_con, $estado);
                        }

                    } catch(Exception $e) {

                        $this->error->append(['code' => 102, 'msg' => 'Error al actualizar contrato']);

                    }

                    if(!$id_suministro) {

                        $this->error->append(['code' => 103, 'msg' => 'Error al actualizar contrato']);

                    }
                }
            } else {

                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            
            }

        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar contrato']);

        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );
    }

    public function add_contrato_suministro() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $id_suministro = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('adconsu'))));
        
        if(!empty($id_suministro) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {
                $fec_ini_con_sum = $this->security->xss_clean($this->input->post('fec_ini_con_sum'));
                $fec_ter_con_sum = $this->security->xss_clean($this->input->post('fec_ter_con_sum'));
                
                if(!empty($fec_ini_con_sum) && !empty($fec_ter_con_sum)) {
                    
                    $this->load->model('Suministro');

                    try {

                        $con = $this->Suministro->check_contrato_suministro_activo(0, $id_suministro, $fec_ini_con_sum, $fec_ter_con_sum);

                    } catch (Exception $e) {

                        $this->error->append(['code' => 300, 
                                                'msg' => 'Imposible validar contrato de suministro']);

                    }

                    if($con->total > 0) {

                        $this->error->append(['code' => 301, 
                                                'msg' => 'Ya existe un contrato asociado para el periodo seleccionado']);
                    } else {

                        $tipo_tarifa = $this->security->xss_clean($this->input->post('tipo_tarifa'));
                        $potencia = $this->security->xss_clean($this->input->post('potencia'));
                        $potencia = is_numeric($potencia) ? $potencia : 0 ;

                        $ac_num = $this->security->xss_clean($this->input->post('ac_num'));
                        $ac_prop = $this->security->xss_clean($this->input->post('ac_prop'));
                        $ac_cons =  $this->security->xss_clean($this->input->post('ac_cons'));
                        $ac_cons = is_numeric($ac_cons) ? $ac_cons : 0 ;
                        $re_num = $this->security->xss_clean($this->input->post('re_num'));
                        $re_prop = $this->security->xss_clean($this->input->post('re_prop'));
                        $re_cons = $this->security->xss_clean($this->input->post('re_cons'));
                        $re_cons = is_numeric($re_cons) ? $re_cons : 0 ;
                        $fp_num = $this->security->xss_clean($this->input->post('fp_num'));
                        $fp_prop = $this->security->xss_clean($this->input->post('fp_prop'));
                        $fp_cons = $this->security->xss_clean($this->input->post('fp_cons'));
                        $fp_cons = is_numeric($fp_cons) ? $fp_cons : 0 ;
                        $ep_num =  $this->security->xss_clean($this->input->post('ep_num'));
                        $ep_prop = $this->security->xss_clean($this->input->post('ep_prop'));
                        $ep_cons = $this->security->xss_clean($this->input->post('ep_cons'));
                        $ep_cons = is_numeric($ep_cons) ? $ep_cons : 0 ;

                        try {

                            $id_con = $this->Suministro->insert_contrato_suministro($id_suministro, $tipo_tarifa, $potencia, 
                                                                                    $fec_ini_con_sum, $fec_ter_con_sum,
                                                                                    $ac_num, $ac_prop, $ac_cons,
                                                                                    $re_num, $re_prop, $re_cons,
                                                                                    $fp_num, $fp_prop, $fp_cons,
                                                                                    $ep_num, $ep_prop, $ep_cons);

                        } catch(Exception $e) {
                            $this->error->append(['code' => 107, 
                                                    'msg' => 'Error al ingresar contrato de suministro']);
                        }

                        if(!$id_con) {

                            $this->error->append(['code' => 103, 'msg' => 'Error al ingresar contrato de suministro']);

                        }
                    }
                } else {
                    $this->error->append(['code' => 108, 
                                                'msg' => 'Indique fecha de inicio y termino para el contrato de suministro']);
                }
            } else {
                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            }
        } else {
            $this->error->append(['code' => 100, 'msg' => 'Imposible ingresar contrato de suministro']);
        }
        
        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );
    }

    public function edit_contrato_suministro() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $id = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('ecosu'))));
        $id_suministro = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('ecosusu'))));
        
        if(!empty($id) && !empty($id_suministro) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {
                $fec_ini_con_sum = $this->security->xss_clean($this->input->post('fec_ini_con_sum'));
                $fec_ter_con_sum = $this->security->xss_clean($this->input->post('fec_ter_con_sum'));
                
                if(!empty($fec_ini_con_sum) && !empty($fec_ter_con_sum)) {
                    $this->load->model('Suministro');

                    try {

                        $con = $this->Suministro->check_contrato_suministro_activo($id, $id_suministro, $fec_ini_con_sum, $fec_ter_con_sum);

                    } catch (Exception $e) {

                        $this->error->append(['code' => 300, 
                                                'msg' => 'Imposible validar contrato de suministro']);

                    }

                    if($con->total > 0) {

                        $this->error->append(['code' => 301, 
                                                'msg' => 'Ya existe un contrato asociado para el periodo seleccionado']);
                    } else {

                        $tipo_tarifa = $this->security->xss_clean($this->input->post('tipo_tarifa'));
                        $potencia = $this->security->xss_clean($this->input->post('potencia'));
                        $potencia = is_numeric($potencia) ? $potencia : 0 ;

                        $ac_num = $this->security->xss_clean($this->input->post('ac_num'));
                        $ac_prop = $this->security->xss_clean($this->input->post('ac_prop'));
                        $ac_cons =  $this->security->xss_clean($this->input->post('ac_cons'));
                        $ac_cons = is_numeric($ac_cons) ? $ac_cons : 0 ;
                        $re_num = $this->security->xss_clean($this->input->post('re_num'));
                        $re_prop = $this->security->xss_clean($this->input->post('re_prop'));
                        $re_cons = $this->security->xss_clean($this->input->post('re_cons'));
                        $re_cons = is_numeric($re_cons) ? $re_cons : 0 ;
                        $fp_num = $this->security->xss_clean($this->input->post('fp_num'));
                        $fp_prop = $this->security->xss_clean($this->input->post('fp_prop'));
                        $fp_cons = $this->security->xss_clean($this->input->post('fp_cons'));
                        $fp_cons = is_numeric($fp_cons) ? $fp_cons : 0 ;
                        $ep_num =  $this->security->xss_clean($this->input->post('ep_num'));
                        $ep_prop = $this->security->xss_clean($this->input->post('ep_prop'));
                        $ep_cons = $this->security->xss_clean($this->input->post('ep_cons'));
                        $ep_cons = is_numeric($ep_cons) ? $ep_cons : 0 ;

                        try {
                            
                            $id_con = $this->Suministro->update_contrato_suministro($id, $tipo_tarifa, $potencia, 
                                                                                    $fec_ini_con_sum, $fec_ter_con_sum,
                                                                                    $ac_num, $ac_prop, $ac_cons,
                                                                                    $re_num, $re_prop, $re_cons,
                                                                                    $fp_num, $fp_prop, $fp_cons,
                                                                                    $ep_num, $ep_prop, $ep_cons);

                        } catch(Exception $e) {
                            $this->error->append(['code' => 107, 
                                                    'msg' => 'Error al ingresar contrato de suministro']);
                        }

                        if(!$id_con) {

                            $this->error->append(['code' => 103, 'msg' => 'Error al ingresar contrato de suministro']);

                        }
                    }
                } else {
                    $this->error->append(['code' => 108, 
                                                'msg' => 'Indique fecha de inicio y termino para el contrato de suministro']);
                }
            } else {
                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            }
        } else {
            $this->error->append(['code' => 100, 'msg' => 'Imposible ingresar contrato de suministro']);
        }
        
        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );
    }

    public function add_ahorro() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $id_suministro = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('adaho'))));
        
        if(!empty($id_suministro) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {
                $fec_ini_con_save = $this->security->xss_clean($this->input->post('fec_ini_con_save'));
                $fec_ter_con_save = $this->security->xss_clean($this->input->post('fec_ter_con_save'));

                if(!empty($fec_ini_con_save) && !empty($fec_ter_con_save)) {
                    $pct_con_save = $this->security->xss_clean($this->input->post('pct_con_save'));
                    $pct_con_save = is_numeric($pct_con_save) ? $pct_con_save : 0 ;

                    try {

                        $this->load->model('Ahorro');
                        $id_ahorro = $this->Ahorro->insert_contrato($id_suministro, 
                                                                    $fec_ini_con_save, 
                                                                    $fec_ter_con_save, 
                                                                    $pct_con_save);

                    } catch(Exception $e) {
                        $this->error->append(['code' => 107, 
                                                'msg' => 'Error al ingresar contrato de ahorro']);
                    }

                    if(!$id_ahorro) {

                        $this->error->append(['code' => 103, 'msg' => 'Error al ingresar contrato de ahorro']);

                    }
                } else {
                    $this->error->append(['code' => 108, 
                                                'msg' => 'Indique fecha de inicio y termino para el contrato de ahorro']);
                }
            } else {
                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            }
        } else {
            $this->error->append(['code' => 100, 'msg' => 'Imposible ingresar contrato de ahorro']);
        }
        
        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );
    }

    public function edit_ahorro() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('eaho'))));
        
        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {
                $fec_ini_con_save = $this->security->xss_clean($this->input->post('fec_ini_con_save'));
                $fec_ter_con_save = $this->security->xss_clean($this->input->post('fec_ter_con_save'));

                if(!empty($fec_ini_con_save) && !empty($fec_ter_con_save)) {
                    $pct_con_save = $this->security->xss_clean($this->input->post('pct_con_save'));
                    $pct_con_save = is_numeric($pct_con_save) ? $pct_con_save : 0 ;

                    try {

                        $this->load->model('Ahorro');
                        $id = $this->Ahorro->update_contrato($token,
                                                                    $fec_ini_con_save, 
                                                                    $fec_ter_con_save, 
                                                                    $pct_con_save);

                    } catch(Exception $e) {
                        $this->error->append(['code' => 107, 
                                                'msg' => 'Error al actualizar contrato de ahorro']);
                    }

                    if(!$id) {

                        $this->error->append(['code' => 103, 'msg' => 'Error al actualizar contrato de ahorro '.$id]);

                    }
                } else {
                    $this->error->append(['code' => 108, 
                                                'msg' => 'Indique fecha de inicio y termino para el contrato de ahorro']);
                }
            } else {
                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            }
        } else {
            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar contrato de ahorro']);
        }
        
        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );

    }

    public function delete_ahorro() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));

        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {

                try {     

                    $this->load->model('Ahorro');
                        
                    $id_factura = $this->Ahorro->delete_contrato($token);
            
                } catch(Exception $e) {

                    $this->error->append(['code' => 102, 'msg' => 'Error al eliminar contrato de ahorro']);

                }

            } else {

                $this->error->append(['code' => 110, 'msg' => 'No tienes permisos para realizar esta operación']);

            }
        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible eliminar contrato de ahorro']);
        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );
    }

    public function add_optimizacion() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $id_suministro = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('adopt'))));
        
        if(!empty($id_suministro) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {
                $fec_ini_con_opt = $this->security->xss_clean($this->input->post('fec_ini_con_opt'));
                $fec_ter_con_opt = $this->security->xss_clean($this->input->post('fec_ter_con_opt'));

                if(!empty($fec_ini_con_opt) && !empty($fec_ter_con_opt)) {
                    $pct_con_opt = $this->security->xss_clean($this->input->post('pct_con_opt'));
                    $pct_con_opt = is_numeric($pct_con_opt) ? (int) $pct_con_opt : 0 ;

                    try {

                        $this->load->model('Optimizacion');
                        $id_ahorro = $this->Optimizacion->insert_contrato($id_suministro, 
                                                                    $fec_ini_con_opt, 
                                                                    $fec_ter_con_opt, 
                                                                    $pct_con_opt);

                    } catch(Exception $e) {
                        $this->error->append(['code' => 107, 
                                                'msg' => 'Error al ingresar contrato de optimización']);
                    }
                    
                    if(!$id_ahorro) {

                        $this->error->append(['code' => 103, 'msg' => 'Error al ingresar contrato de optimización']);

                    }
                } else {
                    $this->error->append(['code' => 108, 
                                                'msg' => 'Indique fecha de inicio y termino para el contrato de optimización']);
                }
            } else {
                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            }
        } else {
            $this->error->append(['code' => 100, 'msg' => 'Imposible ingresar contrato de optimización']);
        }
        
        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );
    }

    public function edit_optimizacion() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('eopt'))));
        
        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {
                $fec_ini_con_opt = $this->security->xss_clean($this->input->post('fec_ini_con_opt'));
                $fec_ter_con_opt = $this->security->xss_clean($this->input->post('fec_ter_con_opt'));

                if(!empty($fec_ini_con_opt) && !empty($fec_ter_con_opt)) {
                    $pct_con_opt = $this->security->xss_clean($this->input->post('pct_con_opt'));
                    $pct_con_opt = is_numeric($pct_con_opt) ? $pct_con_opt : 0 ;

                    try {

                        $this->load->model('Optimizacion');
                        $id = $this->Optimizacion->update_contrato($token,
                                                                    $fec_ini_con_opt, 
                                                                    $fec_ter_con_opt, 
                                                                    $pct_con_opt);

                    } catch(Exception $e) {
                        $this->error->append(['code' => 107, 
                                                'msg' => 'Error al actualizar contrato de optimización']);
                    }

                    if(!$id) {

                        $this->error->append(['code' => 103, 'msg' => 'Error al actualizar contrato de optimización']);

                    }
                } else {
                    $this->error->append(['code' => 108, 
                                                'msg' => 'Indique fecha de inicio y termino para el contrato de optimización']);
                }
            } else {
                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            }
        } else {
            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar contrato de optimización']);
        }
        
        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );

    }

    public function delete_optimizacion() {
        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));

        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {

                try {     

                    $this->load->model('Optimizacion');
                        
                    $id_factura = $this->Optimizacion->delete_contrato($token);
            
                } catch(Exception $e) {

                    $this->error->append(['code' => 102, 'msg' => 'Error al eliminar contrato de optimización']);

                }

            } else {

                $this->error->append(['code' => 110, 'msg' => 'No tienes permisos para realizar esta operación']);

            }
        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible eliminar contrato de optimización']);
        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );
    }

    public function set_intervencion() {
        // print_r(json_encode($_POST));

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));

        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {
                
                $energia = $this->security->xss_clean($this->input->post('energia'));
                $energia = is_numeric($energia) ? $energia : 0 ;

                $fp = $this->security->xss_clean($this->input->post('fp'));
                $fp = is_numeric($fp) ? $fp : 0 ;

                $ep = $this->security->xss_clean($this->input->post('ep'));
                $ep = is_numeric($ep) ? $ep : 0 ;

                $cp = $this->security->xss_clean($this->input->post('cp'));
                $cp = is_numeric($cp) ? $cp : 0 ;

                $monomico = $this->security->xss_clean($this->input->post('monomico'));
                $monomico = is_numeric($monomico) ? $monomico : 0 ;

                $produccion = $this->security->xss_clean($this->input->post('produccion'));
                $produccion = is_numeric($produccion) ? $produccion : 0 ;

                $indicador = $this->security->xss_clean($this->input->post('indicador'));
                $indicador = is_numeric($indicador) ? $indicador : 0 ;

                $ahorro = $this->security->xss_clean($this->input->post('ahorro'));
                $ahorro = is_numeric($ahorro) ? $ahorro : 0 ;

                $optimizacion = $this->security->xss_clean($this->input->post('optimizacion'));
                $optimizacion = is_numeric($optimizacion) ? $optimizacion : 0 ;

                $observacion = $this->security->xss_clean($this->input->post('observacion'));

                //*CLIENTE LIBRE
                $energia_gen = $this->security->xss_clean($this->input->post('energia_gen'));
                $energia_gen = is_numeric($energia_gen) ? $energia_gen : 0 ;

                $tec_r = $this->security->xss_clean($this->input->post('tec_r'));
                $tec_r = is_numeric($tec_r) ? $tec_r : 0 ;

                $armo_r = $this->security->xss_clean($this->input->post('armo_r'));
                $armo_r = is_numeric($armo_r) ? $armo_r : 0 ;

                $ep_gen = $this->security->xss_clean($this->input->post('ep_gen'));
                $ep_gen = is_numeric($ep_gen) ? $ep_gen : 0 ;

                $monomico_gen = $this->security->xss_clean($this->input->post('monomico_gen'));
                $monomico_gen = is_numeric($monomico_gen) ? $monomico_gen : 0 ;

                $trans_r = $this->security->xss_clean($this->input->post('trans_r'));
                $trans_r = is_numeric($trans_r) ? $trans_r : 0 ;

                $indicador_gen = $this->security->xss_clean($this->input->post('indicador_gen'));
                $indicador_gen = is_numeric($indicador_gen) ? $indicador_gen : 0 ;

                $ahorro_gen = $this->security->xss_clean($this->input->post('ahorro_gen'));
                $ahorro_gen = is_numeric($ahorro_gen) ? $ahorro_gen : 0 ;

                $optimizacion_gen = $this->security->xss_clean($this->input->post('optimizacion_gen'));
                $optimizacion_gen = is_numeric($optimizacion_gen) ? $optimizacion_gen : 0 ;

                $ahorro_dis = $this->security->xss_clean($this->input->post('ahorro_dis'));
                $ahorro_dis = is_numeric($ahorro_dis) ? $ahorro_dis : 0 ;

                $optimizacion_dis = $this->security->xss_clean($this->input->post('optimizacion_dis'));
                $optimizacion_dis = is_numeric($optimizacion_dis) ? $optimizacion_dis : 0 ;

                $this->load->model('Ratio');

                try {     
                        
                    $id_ratio = $this->Ratio->check_ratio($token);
            
                } catch(Exception $e) {

                    $this->error->append(['code' => 102, 'msg' => 'Error al procesar la intervención']);

                }

                if($id_ratio->total > 0) {

                    try {     
                        
                        $id_ratio = $this->Ratio->check_intervencion($token);
                
                    } catch(Exception $e) {
    
                        $this->error->append(['code' => 104, 'msg' => 'Error al procesar la intervención']);
    
                    }

                    if($id_ratio->total > 0) {

                        try {     
                        
                            $ratio = $this->Ratio->update_intervencion($token, $energia, $fp, $ep, $monomico, 
                                                                        $produccion, $indicador, $ahorro, $optimizacion,
                                                                        $observacion, $cp, $energia_gen, $tec_r,
                                                                        $armo_r, $ep_gen, $monomico_gen, $trans_r,
                                                                        $indicador_gen, $ahorro_gen, $optimizacion_gen,
                                                                        $ahorro_dis, $optimizacion_dis);
                    
                        } catch(Exception $e) {
        
                            $this->error->append(['code' => 105, 'msg' => 'Error al ingresar la intervención']);
        
                        }

                    } else {

                        try {     
                        
                            $ratio = $this->Ratio->insert_intervencion($token, $energia, $fp, $ep, $monomico, 
                                                                        $produccion, $indicador, $ahorro, $optimizacion,
                                                                        $observacion, $cp, $energia_gen, $tec_r,
                                                                        $armo_r, $ep_gen, $monomico_gen, $trans_r,
                                                                        $indicador_gen, $ahorro_gen, $optimizacion_gen,
                                                                        $ahorro_dis, $optimizacion_dis);
                    
                        } catch(Exception $e) {
        
                            $this->error->append(['code' => 106, 'msg' => 'Error al ingresar la intervención']);
        
                        }

                    }

                } else {

                    $this->error->append(['code' => 103, 'msg' => 'Error datos no válidos']);

                }

            } else {

                $this->error->append(['code' => 110, 'msg' => 'No tienes permisos para realizar esta operación']);

            }
        } else {

            $this->error->append(['code' => 100, 'msg' => 'Imposible procesar intervención']);
        }

        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );
    }

    public function set_columnas_factura() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));
        
        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {
                
                $factura_3 = empty($this->input->post('consumo')) ? 0 : 1;
                
                $factura_4 = empty($this->input->post('gasto')) ? 0 : 1;
                
                $factura_5 = empty($this->input->post('pl_fp')) ? 0 : 1;

                $factura_6 = empty($this->input->post('pf_fp')) ? 0 : 1;
                
                $factura_7 = empty($this->input->post('pl_ep')) ? 0 : 1;
                
                $factura_8 = empty($this->input->post('pf_ep')) ? 0 : 1;
                
                $factura_9 = empty($this->input->post('fp_precio')) ? 0 : 1;
                
                $factura_10 = empty($this->input->post('ep_precio')) ? 0 : 1;

                $factura_11 = empty($this->input->post('req')) ? 0 : 1;
                
                $factura_12 = empty($this->input->post('ah_ta')) ? 0 : 1;
                
                $factura_13 = empty($this->input->post('factor')) ? 0 : 1;
                
                $factura_14 = empty($this->input->post('oc')) ? 0 : 1;
                
                $factura_15 = empty($this->input->post('neto')) ? 0 : 1;

                $factura_16 = empty($this->input->post('total')) ? 0 : 1;
                
                $factura_17= empty($this->input->post('cp_consumo')) ? 0 : 1;

                $factura_18 = empty($this->input->post('cp_precio')) ? 0 : 1;

                $factura_19 = empty($this->input->post('consumo_gen')) ? 0 : 1;
                
                $factura_20 = empty($this->input->post('gasto_gen')) ? 0 : 1;

                $factura_21 = empty($this->input->post('pl_ep_gen')) ? 0 : 1;
                
                $factura_22 = empty($this->input->post('pf_ep_gen')) ? 0 : 1;

                $factura_23 = empty($this->input->post('ep_precio_gen')) ? 0 : 1;

                try {

                    $this->load->model('Web');
                    $id = $this->Web->update_columnas_factura($token,
                                                                $factura_3, 
                                                                $factura_4, 
                                                                $factura_5,
                                                                $factura_6,
                                                                $factura_7,
                                                                $factura_8,
                                                                $factura_9,
                                                                $factura_10,
                                                                $factura_11,
                                                                $factura_12,
                                                                $factura_13,
                                                                $factura_14,
                                                                $factura_15,
                                                                $factura_16,
                                                                $factura_17,
                                                                $factura_18,
                                                                $factura_19,
                                                                $factura_20,
                                                                $factura_21,
                                                                $factura_22,
                                                                $factura_23);

                } catch(Exception $e) {
                    $this->error->append(['code' => 107, 
                                            'msg' => 'Error al actualizar la visualización de columnas']);
                }

                if(!$id) {

                    $this->error->append(['code' => 103, 'msg' => 'Error al actualizar la visualización de columnas']);

                }
            } else {
                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            }
        } else {
            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar la visualización de columnas']);
        }
        
        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );

    }

    public function set_columnas_ratio() {

        $this->error = new ArrayObject(array(),ArrayObject::ARRAY_AS_PROPS);

        $token = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->post('token'))));
        
        if(!empty($token) && $this->is_auth) {
            if ($this->auth_lvl->id_perfil == 1  || $this->auth_lvl->id_perfil == 2) {
                
                $ratio_2 = empty($this->input->post('ratio_2')) ? 0 : 1;
                
                $ratio_3 = empty($this->input->post('ratio_3')) ? 0 : 1;
                
                $ratio_4 = empty($this->input->post('ratio_4')) ? 0 : 1;
               
                $ratio_5 = empty($this->input->post('ratio_5')) ? 0 : 1;
                
                $ratio_6 = empty($this->input->post('ratio_6')) ? 0 : 1;
                
                $ratio_7 = empty($this->input->post('ratio_7')) ? 0 : 1;
                
                $ratio_8 = empty($this->input->post('ratio_8')) ? 0 : 1;
                
                $ratio_9 = empty($this->input->post('ratio_9')) ? 0 : 1;

                $ratio_10 = empty($this->input->post('ratio_10')) ? 0 : 1;

                $ratio_11 = empty($this->input->post('ratio_11')) ? 0 : 1;
                $ratio_12 = empty($this->input->post('ratio_12')) ? 0 : 1;
                $ratio_13 = empty($this->input->post('ratio_13')) ? 0 : 1;
                $ratio_14 = empty($this->input->post('ratio_14')) ? 0 : 1;
                $ratio_15 = empty($this->input->post('ratio_15')) ? 0 : 1;
                $ratio_16 = empty($this->input->post('ratio_16')) ? 0 : 1;
                $ratio_17 = empty($this->input->post('ratio_17')) ? 0 : 1;
                $ratio_18 = empty($this->input->post('ratio_18')) ? 0 : 1;
                $ratio_19 = empty($this->input->post('ratio_19')) ? 0 : 1;
                $ratio_20 = empty($this->input->post('ratio_20')) ? 0 : 1;
                $ratio_21 = empty($this->input->post('ratio_21')) ? 0 : 1;
               
                try {

                    $this->load->model('Web');
                    $id = $this->Web->update_columnas_ratio($token,
                                                                $ratio_2, 
                                                                $ratio_3,
                                                                $ratio_4,
                                                                $ratio_5,
                                                                $ratio_6,
                                                                $ratio_7,
                                                                $ratio_8,
                                                                $ratio_9,
                                                                $ratio_10,
                                                                $ratio_11,
                                                                $ratio_12,
                                                                $ratio_13,
                                                                $ratio_14,
                                                                $ratio_15,
                                                                $ratio_16,
                                                                $ratio_17,
                                                                $ratio_18,
                                                                $ratio_19,
                                                                $ratio_20,
                                                                $ratio_21);

                } catch(Exception $e) {
                    $this->error->append(['code' => 107, 
                                            'msg' => 'Error al actualizar la visualización de columnas']);
                }

                if(!$id) {

                    $this->error->append(['code' => 103, 'msg' => 'Error al actualizar la visualización de columnas']);

                }
            } else {
                $this->error->append(['code' => 120, 'msg' => 'No tienes permisos para realizar esta operación']);
            }
        } else {
            $this->error->append(['code' => 100, 'msg' => 'Imposible actualizar la visualización de columnas']);
        }
        
        print_r( $this->error->count() > 0 ? json_encode(array("status" => "error", "content" => $this->error) ) : json_encode(array("status" => "ok")) );

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private function add_gccontrato($id_suministro, $fec_ini_gc, $fec_ter_gc, $observacion3, 
                                    $fec_ini_save, $pct_con, $estado) {
                                         
        if(empty($id_suministro)) {

            if (empty($fec_ini_gc) || empty($fec_ter_gc) || (empty($pct_con) || $pct_con < 1) 
                || ($estado =! 0 && $estado =!1)) {
                return FALSE;
            } else {

                try {

                    $this->load->model('Contrato');
                    $query = $this->Contrato->insert_contrato($id_suministro, $fec_ini_gc, $fec_ter_gc, 
                                                    $observacion3, $fec_ini_save, $pct_con, $estado);

                    return $query ? $query : 'error';

                } catch(Exception $e) {
                    
                    return FALSE;
    
                }

            }

        } else {

            return FALSE;

        }

    }

    private function add_contrato_ahorro($id_suministro, $fec_ini_con_save, $fec_ter_con_save,
                                            $pct_con_save) {
                                        
        if(empty($id_suministro)) {

            if (empty($fec_ini_con_save) || empty($fec_ter_con_save) 
                || (empty($pct_con_save) || $pct_con_save < 1)) {

                return FALSE;

            } else {

                try {

                    $this->load->model('Ahorro');
                    $query = $this->Ahorro->insert_contrato($id_suministro, 
                                                                $fec_ini_con_save, 
                                                                $fec_ter_con_save, 
                                                                $pct_con_save);

                    return $query ? $query : 'error';

                } catch(Exception $e) {
                    
                    return FALSE;
    
                }

            }

        } else {

            return FALSE;

        }

    }

    private function add_contrato_optimizacion($id_suministro, $fec_ini_con_opt, $fec_ter_con_opt,
                                            $pct_con_opt) {
                                        
        if(empty($id_suministro)) {

            if (empty($fec_ini_con_opt) || empty($fec_ter_con_opt) 
                || (empty($pct_con_opt) || $pct_con_opt < 1)) {

                return FALSE;

            } else {

                try {

                    $this->load->model('Optimizacion');
                    $query = $this->Optimizacion->insert_contrato($id_suministro, 
                                                                $fec_ini_con_opt, 
                                                                $fec_ter_con_opt, 
                                                                $pct_con_opt);

                    return $query ? $query : 'error';

                } catch(Exception $e) {
                    
                    return FALSE;
    
                }

            }

        } else {

            return FALSE;

        }

    }

    private function do_upload($file) {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'pdf';
        $config['max_size']             = 4096;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($file)) {

            return array("status" => "error", "content" => $this->upload->display_errors());

        } else {

            return array("status" => "ok", "content" => $this->upload->data());

        }
    }

    public function test () {
        //var_dump($_POST);

        print_r(json_encode($_POST));
        // echo "</br>";
        // print_r(json_encode($_FILES));
    }

    private function set_companias() {

        $this->load->model('Compania');

        $this->companias = $this->Compania->get_companias();

        foreach ($this->companias as $compania) {
            $compania->id = base64_encode($this->encryption->encrypt($compania->id));
        }

    }

    private function set_tarifas() {

        $this->load->model('Tarifa');

        $this->tarifas = $this->Tarifa->get_tarifas();

    }

    private function set_tipo_analisis() {

        $this->load->model('Suministro');

        $this->analisis = $this->Suministro->get_analisis();

    }

    public function get_sectores() {

        $id_compania = $this->security->xss_clean($this->encryption->decrypt(base64_decode($this->input->get('token'))));

        $this->load->model('Compania');

        $sectores = $this->Compania->get_sectores($id_compania);

        foreach ($sectores as $sector) {
            $sector->codigo = base64_encode($this->encryption->encrypt($sector->codigo));
        }

        print_r(json_encode($sectores));

    }
}