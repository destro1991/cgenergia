<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Favorito extends CI_Model { 

    public function insert_favorito($user, $cliente) {

        $sql = "INSERT INTO gc_favoritos
        VALUES (?,?);";
        $query = $this->db->query($sql, array(strip_tags($user), 
                                                strip_tags($cliente)));

        return $query ? $this->db->insert_id('gc_favoritos') : FALSE;

    }

    public function delete_favorito($user, $cliente) {

        $sql = "delete from gc_favoritos
                where id_user = ? and id_cli = ?;";

        $query = $this->db->query($sql, array(strip_tags($user), strip_tags($cliente)));

        return $query;

    }
}