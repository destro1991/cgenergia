<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contrato extends CI_Model {

    public function get_contrato_suministro($id_suministro) {
        $sql = "SELECT * 
                FROM gc_contrato
                where id_suministro = ?;";
        $query = $this->db->query($sql, array(strip_tags($id_suministro))); 
        
        return $query->row();
    }

    public function insert_contrato($id_suministro, $fec_ini_gc, $fec_ter_gc, $observacion, 
                                        $fec_ini_save, $pct_con, $estado) {

        $sql = "INSERT INTO gc_contrato
                VALUES (?,?,?,?,?,?,?);";
        $query = $this->db->query($sql, array(strip_tags($id_suministro), 
                                            strip_tags($fec_ini_gc), 
                                            strip_tags($fec_ter_gc), 
                                            strip_tags($fec_ini_save),
                                            strip_tags($pct_con),
                                            strip_tags($estado),
                                            strip_tags($observacion)));
        
        return $query ? $this->db->insert_id('gc_contrato') : FALSE;

    }

}