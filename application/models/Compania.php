<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compania extends CI_Model {

    public function get_companias() {

        $sql = "SELECT id, nombre FROM gc_compania;";
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    public function get_sectores($id) {

        $sql = "SELECT id as codigo, nombre FROM gc_sec_tarifa where id_compania = ?;";
        $query = $this->db->query($sql, array(strip_tags($id)));
        
        return $query->result();
    }

}