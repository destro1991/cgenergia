<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Model {

    public function get_clientes_short() {
        $sql = "SELECT id, nombre FROM gc_cliente;";
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    public function get_clientes_favoritos($user, $anno) {
        $user = strip_tags($user);
        $anno = strip_tags($anno);

        $sql = "SELECT c.id, c.nombre, c.avatar,
                        (select count(*) from gc_suministro where id_cli = c.id) as total,
                        (SELECT sum(if(i.ahorro_in is null, r.ahorro, i.ahorro_in))
                            FROM gc_ratio r
                            left join gc_ratio_intervencion i on i.id_ratio = r.id
                            where r.id_suministro in  (select id from gc_suministro where id_cli = c.id)
                            and SUBSTR(mes, 1, 4) = ?
                            order by mes) as ahorro,
                        (SELECT sum(if(i.optimizacion_in is null, r.optimizacion, i.optimizacion_in))
                            FROM gc_ratio r
                            left join gc_ratio_intervencion i on i.id_ratio = r.id
                            where r.id_suministro in  (select id from gc_suministro where id_cli = c.id)
                            and SUBSTR(mes, 1, 4) = ?
                            order by mes) as optimizacion,
                            (select sum(tot_neto) 
								from gc_factura 
								where SUBSTR(mes, 1, 4) = ?
								and id_con in (select id 
												from gc_contrato_suministro 
												where id_suministro in (select id 
																		from gc_suministro
																		where id_cli = c.id))) as neto
                FROM gc_cliente c
                where id in (select id_cli 
                                from gc_favoritos
                                where id_user = ?);";
        $query = $this->db->query($sql, array($anno, $anno, $anno, $user));
        
        return $query->result();

    }

    public function get_cliente($id) {
        $sql = "SELECT * FROM gc_cliente where id = ?;";
        $query = $this->db->query($sql, array(strip_tags($id)));
        
        return $query->row();
    }

    public function get_cliente_nombre($nombre) {
        $nombre = strip_tags($nombre);

        $sql = "SELECT id, nombre FROM gc_cliente where nombre = '$nombre' order by id limit 1;";
        $query = $this->db->query($sql);
        
        return $query->row();
    }

    public function get_cliente_short($id) {
        $sql = "SELECT id, nombre FROM gc_cliente where id = ?;";
        $query = $this->db->query($sql, array(strip_tags($id)));
        
        return $query->row();
    }
    
    public function get_cliente_user($id) {
        $sql = "SELECT id, nombre, message FROM gc_cliente where id_user = ?;";
        $query = $this->db->query($sql, array(strip_tags($id)));
        
        return $query->row();
    }

    public function insert_cliente($rut, $nombre, $contacto, 
                                    $correo, $telefono, $sitio) {

        $sql = "INSERT INTO gc_cliente
        VALUES (null,?,?,?,?,?,?,null,null,null);";
        $query = $this->db->query($sql, array(strip_tags($rut), 
                                                strip_tags($nombre), 
                                                strip_tags($contacto),
                                                strip_tags($correo),
                                                strip_tags($telefono),
                                                strip_tags($sitio)));

        return $query ? $this->db->insert_id('gc_cliente') : FALSE;

    }

    public function update_cliente($id, $rut, $nombre, $contacto, 
                                    $correo, $telefono, $sitio) {

        $sql = "update gc_cliente
                set 
                rut = ?,
                nombre = ?,
                contacto = ?,
                correo = ?,
                num_tel = ?,
                sitio_web = ?
                where id = ?;";

        $query = $this->db->query($sql, array(strip_tags($rut), 
                                                strip_tags($nombre), 
                                                strip_tags($contacto),
                                                strip_tags($correo),
                                                strip_tags($telefono),
                                                strip_tags($sitio),
                                                strip_tags($id)));

        return $query;

    }

    public function update_cliente_mensaje($id, $message) {

        $sql = "update gc_cliente
                set 
                message = ?
                where id = ?;";

        $query = $this->db->query($sql, array($message,
                                              strip_tags($id)));

        return $query;

    }

    public function set_cuenta_cliente($id, $user) {

        $sql = "update gc_cliente
        set 
        id_user = ?
        where id = ?;";

        $query = $this->db->query($sql, array(strip_tags($user), 
                                                strip_tags($id)));

        return $query;

    }

    public function update_avatar($id, $avatar) {

        $sql = "update gc_cliente
                set 
                avatar = ?
                where id = ?;";

        $query = $this->db->query($sql, array(strip_tags($avatar),
                                                strip_tags($id)));

        return $query;

    }
}