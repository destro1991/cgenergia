<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ratio extends CI_Model {
    public function check_ratio($id) {
        $sql = "SELECT count(id) as total
                FROM gc_ratio 
                where id = ?;";
        $query = $this->db->query($sql, array(strip_tags($id)));
        
        return $query->row();
    }

    public function check_intervencion($id) {
        $sql = "SELECT count(id_ratio) as total
                FROM gc_ratio_intervencion 
                where id_ratio = ?;";
        $query = $this->db->query($sql, array(strip_tags($id)));
        
        return $query->row();
    }

    public function insert_intervencion($token, $energia_in, $f_punta_in, $e_punta_in, $monomico_in, 
                                        $produccion_in, $indicador_in, $ahorro, $optimizacion, $observacion, $cp,
                                        $energia_gen, $tec_r,
                                        $armo_r, $ep_gen, $monomico_gen, $trans_r,
                                        $indicador_gen, $ahorro_gen, $optimizacion_gen,
                                        $ahorro_dis, $optimizacion_dis) {

        $sql = "INSERT INTO gc_ratio_intervencion (
            id_ratio,
            energia_in,
            f_punta_in,
            e_punta_in,
            monomico_in,
            produccion_in,
            indicador_in,
            ahorro_in,
            optimizacion_in,
            observacion,
            cp_in,
            energia_in_gen,
            e_punta_in_gen,
            monomico_in_gen,
            indicador_in_gen,
            ahorro_in_gen,
            ahorro_in_dis,
            optimizacion_in_gen,
            optimizacion_in_dis,
            trans_ratio_in,
            armo_ratio_in,
            tec_ratio_in
        )
                VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $query = $this->db->query($sql, array(strip_tags($token),
                                            strip_tags($energia_in), 
                                            strip_tags($f_punta_in), 
                                            strip_tags($e_punta_in), 
                                            strip_tags($monomico_in),
                                            strip_tags($produccion_in),
                                            strip_tags($indicador_in),
                                            strip_tags($ahorro),
                                            strip_tags($optimizacion),
                                            strip_tags($observacion),
                                            strip_tags($cp),
                                            strip_tags($energia_gen),
                                            strip_tags($ep_gen),
                                            strip_tags($monomico_gen),
                                            strip_tags($indicador_gen),
                                            strip_tags($ahorro_gen),
                                            strip_tags($ahorro_dis),
                                            strip_tags($optimizacion_gen),
                                            strip_tags($optimizacion_dis),
                                            strip_tags($trans_r),
                                            strip_tags($armo_r),
                                            strip_tags($tec_r)));
        
        return $query ? $this->db->insert_id('gc_ratio_intervencion') : FALSE;

    }

    public function update_intervencion($token, $energia_in, $f_punta_in, $e_punta_in, $monomico_in, 
                                        $produccion_in, $indicador_in, $ahorro, $optimizacion, $observacion, $cp, 
                                        $energia_gen, $tec_r,
                                        $armo_r, $ep_gen, $monomico_gen, $trans_r,
                                        $indicador_gen, $ahorro_gen, $optimizacion_gen,
                                        $ahorro_dis, $optimizacion_dis) {
    
        $sql = "update gc_ratio_intervencion
                set energia_in = ?,
                f_punta_in = ?,
                e_punta_in = ?,
                monomico_in = ?,
                produccion_in = ?,
                indicador_in = ?,
                ahorro_in = ?,
                optimizacion_in = ?,
                observacion = ?,
                cp_in = ?,
                energia_in_gen = ?,
                e_punta_in_gen = ?,
                monomico_in_gen = ?,
                indicador_in_gen = ?,
                ahorro_in_gen = ?,
                ahorro_in_dis = ?,
                optimizacion_in_gen = ?,
                optimizacion_in_dis = ?,
                trans_ratio_in = ?,
                armo_ratio_in = ?,
                tec_ratio_in = ?
                where id_ratio = ?;";

        $query = $this->db->query($sql, array(strip_tags($energia_in), 
                                        strip_tags($f_punta_in), 
                                        strip_tags($e_punta_in), 
                                        strip_tags($monomico_in),
                                        strip_tags($produccion_in),
                                        strip_tags($indicador_in),
                                        strip_tags($ahorro),
                                        strip_tags($optimizacion),
                                        strip_tags($observacion),
                                        strip_tags($cp),
                                        strip_tags($energia_gen),
                                        strip_tags($ep_gen),
                                        strip_tags($monomico_gen),
                                        strip_tags($indicador_gen),
                                        strip_tags($ahorro_gen),
                                        strip_tags($ahorro_dis),
                                        strip_tags($optimizacion_gen),
                                        strip_tags($optimizacion_dis),
                                        strip_tags($trans_r),
                                        strip_tags($armo_r),
                                        strip_tags($tec_r),
                                        strip_tags($token))); 
        
        return $query;
        
    }
}