<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CI_Model {

    public function get_all() {

        $sql = "SELECT * FROM gc_perfil;";
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    public function get_all_fix() {

        $sql = "SELECT * FROM gc_perfil where id <> 3;";
        $query = $this->db->query($sql);
        
        return $query->result();
    }

}