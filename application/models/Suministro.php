<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suministro extends CI_Model {

    public function get_analisis() {

        $sql = "SELECT id, nombre FROM gc_tipo_analisis;";
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    public function get_suministros() {
        $sql = "SELECT s.*, c.nombre as nombre_cli,
                    (SELECT 
                        if(i.ahorro_in is null, r.ahorro, i.ahorro_in) ahorro
                    FROM gc_ratio r
                    left join gc_ratio_intervencion i on i.id_ratio = r.id
                    where 
                    r.id_suministro = s.id
                    and r.mes = s.last_month 
                    order by r.id desc limit 1) ahorro 
                FROM gc_suministro s
                JOIN gc_cliente c on c.id = s.id_cli;";
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    public function get_suministros_cliente($id_cliente) {
        $sql = "SELECT s.*,
                        (SELECT 
                            if(i.ahorro_in is null, r.ahorro, i.ahorro_in) ahorro
                        FROM gc_ratio r
                        left join gc_ratio_intervencion i on i.id_ratio = r.id
                        where 
                        r.id_suministro = s.id
                        and r.mes = s.last_month 
                        order by r.id desc limit 1) ahorro 
                FROM gc_suministro s 
                where s.id_cli = ?;";
        $query = $this->db->query($sql, array(strip_tags($id_cliente)));
        
        return $query->result();
    }

    public function get_suministros_cliente_improved($cliente, $anno) {
        $cliente = strip_tags($cliente);
        $anno = strip_tags($anno);

        $sql = "SELECT c.id, c.nombre,
                        (SELECT sum(if(i.ahorro_in is null, r.ahorro, i.ahorro_in))
                            FROM gc_ratio r
                            left join gc_ratio_intervencion i on i.id_ratio = r.id
                            where r.id_suministro = c.id
                            and SUBSTR(mes, 1, 4) = ?
                            order by mes) as ahorro,
                        (SELECT sum(if(i.optimizacion_in is null, r.optimizacion, i.optimizacion_in))
                            FROM gc_ratio r
                            left join gc_ratio_intervencion i on i.id_ratio = r.id
                            where r.id_suministro = c.id
                            and SUBSTR(mes, 1, 4) = ?
                            order by mes) as optimizacion,
                            (select sum(tot_neto) 
								from gc_factura 
								where SUBSTR(mes, 1, 4) = ?
								and id_con in (select id 
												from gc_contrato_suministro 
												where id_suministro = c.id)) as neto
                FROM gc_suministro c
                where id_cli = ?
                order by c.nombre;";
        $query = $this->db->query($sql, array($anno, $anno, $anno, $cliente));
        
        return $query->result();
    }

    public function get_suministros_cliente_improved_anual($cliente, $anno, $filtro) {
        $cliente = strip_tags($cliente);
        $anno = strip_tags($anno);
        $filtro = strip_tags($filtro);
        
        $sql_1 = "select cli.nombre as nombre_cli, s.id, s.nombre, ch.pct,
            (SELECT sum(if(i.ahorro_in is null, r.ahorro, i.ahorro_in))
                FROM gc_ratio r
                left join gc_ratio_intervencion i on i.id_ratio = r.id
                where r.id_suministro = s.id
                and id_factura in (select fac.id from gc_factura fac
                                    where fac.id_con in (select cs.id 
                                                    from gc_contrato_suministro cs 
                                                    where cs.id_suministro = s.id)
                                    and SUBSTR(fac.mes, 1, 4) = $anno
                                    and fac.mes between DATE_FORMAT(ch.fec_ini, '%Y-%m') and DATE_FORMAT(ch.fec_ter, '%Y-%m'))
                order by mes) as ahorro,
            (SELECT sum(if(i.optimizacion_in is null, r.optimizacion, i.optimizacion_in))
                FROM gc_ratio r
                left join gc_ratio_intervencion i on i.id_ratio = r.id
                where r.id_suministro = s.id
                and id_factura in (select fac.id from gc_factura fac 
                                    where fac.id_con in (select cs.id 
                                                    from gc_contrato_suministro cs 
                                                    where cs.id_suministro = s.id)
                                    and SUBSTR(fac.mes, 1, 4) = $anno
                                    and fac.mes between DATE_FORMAT(ch.fec_ini, '%Y-%m') and DATE_FORMAT(ch.fec_ter, '%Y-%m'))
                order by mes) as optimizacion
            from gc_suministro s
            join gc_contrato_ahorro ch on ch.id_suministro = s.id
            JOIN gc_cliente cli on cli.id = s.id_cli
            where s.id_cli = $cliente
            and $anno between SUBSTR(ch.fec_ini, 1, 4) and SUBSTR(ch.fec_ter, 1, 4)";

        $sql_2 = "select cli.nombre as nombre_cli, s.id, s.nombre, null as pct, 0 as ahorro, 0 as optimizacion
            from gc_suministro s
            JOIN gc_cliente cli on cli.id = s.id_cli
            where s.id_cli = $cliente and s.id not in (select distinct s.id
                                            from gc_suministro s
                                            join gc_contrato_ahorro ch on ch.id_suministro = s.id
                                            where s.id_cli = $cliente
                                            and $anno between SUBSTR(ch.fec_ini, 1, 4) and SUBSTR(ch.fec_ter, 1, 4)
                                            order by id)
            order by nombre, id;";
        
        if($filtro == '1') {
            $sql = $sql_1 . ' order by nombre, id';
        } else if ($filtro == '2') {
            $sql = $sql_2;
        } else {
            $sql = $sql_1 . ' union ' . $sql_2; 
        }
        
        $query = $this->db->query($sql, array($anno, $anno));
        
        return $query->result();
    }

    public function get_suministros_cliente_improved_mensual($cliente, $date, $filtro) {
        $cliente = strip_tags($cliente);
        $date = strip_tags($date);
        $filtro = strip_tags($filtro);

        $sql_1 = "select cli.nombre as nombre_cli, s.id, s.nombre, ch.pct,
                        (SELECT sum(round(if(i.ahorro_in is null, r.ahorro, i.ahorro_in),0))
                		FROM gc_ratio r
                		left join gc_ratio_intervencion i on i.id_ratio = r.id
                		where r.id_suministro = s.id
                		and id_factura in (select fac.id from gc_factura fac
                							where fac.id_con in (select cs.id 
                											from gc_contrato_suministro cs 
                											where cs.id_suministro = s.id)
                							and fac.mes = '$date')
                		) as ahorro ,        
                		(SELECT sum(round(if(i.optimizacion_in is null, r.optimizacion, i.optimizacion_in),0))
                		FROM gc_ratio r
                		left join gc_ratio_intervencion i on i.id_ratio = r.id
                		LEFT JOIN gc_contrato_optimizacion cho on cho.id_suministro = r.id_suministro
                		where r.id_suministro = s.id
                		and r.mes = '$date' 
                        and id_factura in (select fac.id from gc_factura fac
                							where fac.id_con in (select cs.id 
                											from gc_contrato_suministro cs 
                											where cs.id_suministro = s.id)
                							and fac.mes = '$date')
                		and '$date' between SUBSTR(cho.fec_ini, 1, 7) and SUBSTR(cho.fec_ter, 1, 7)
                		) as optimizacion
            from gc_suministro s
            join gc_contrato_ahorro ch on ch.id_suministro = s.id
            JOIN gc_cliente cli on cli.id = s.id_cli
            where s.id_cli = $cliente
            and '$date' between SUBSTR(ch.fec_ini, 1, 7) and SUBSTR(ch.fec_ter, 1, 7)";

        $sql_2 = "select cli.nombre as nombre_cli, s.id, s.nombre, null as pct, 0 as ahorro, 0 as optimizacion
        from gc_suministro s
        JOIN gc_cliente cli on cli.id = s.id_cli
        where s.id_cli = $cliente and s.id not in (select id_suministro 
                                    from gc_contrato_ahorro 
                                    where '$date' between SUBSTR(fec_ini, 1, 7) 
                                                and SUBSTR(fec_ter, 1, 7))
        order by nombre, id;";

        if($filtro == '1') {
            $sql = $sql_1 . ' order by nombre, id';
        } else if ($filtro == '2') {
            $sql = $sql_2;
        } else {
            $sql = $sql_1 . ' union ' . $sql_2; 
        }
        
        $query = $this->db->query($sql, array($cliente));
        
        return $query->result();
    }

    public function get_suministros_filtrado($filtro) {
        $filtro = strip_tags($filtro);
        $sql = "SELECT s.*,
                        (SELECT 
                            if(i.ahorro_in is null, r.ahorro, i.ahorro_in) ahorro
                        FROM gc_ratio r
                        left join gc_ratio_intervencion i on i.id_ratio = r.id
                        where 
                        r.id_suministro = s.id
                        and r.mes = s.last_month 
                        order by r.id desc limit 1) ahorro 
                FROM gc_suministro s 
                where s.nombre like '%$filtro%';";
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    public function get_suministros_cliente_filtrado($id_cliente, $filtro) {
        $filtro = strip_tags($filtro);

        $sql = "SELECT s.*,
                        (SELECT 
                            if(i.ahorro_in is null, r.ahorro, i.ahorro_in) ahorro
                        FROM gc_ratio r
                        left join gc_ratio_intervencion i on i.id_ratio = r.id
                        where 
                        r.id_suministro = s.id
                        and r.mes = s.last_month 
                        order by r.id desc limit 1) ahorro 
                FROM gc_suministro s 
                where s.id_cli = ? and s.nombre like '%$filtro%';";
        $query = $this->db->query($sql, array(strip_tags($id_cliente)));
        
        return $query->result();
    }

    public function get_suministro($id, $anno) {
        $id = strip_tags($id);
        $sql = "SELECT s.*, g.fec_ini as fec_ini_con, g.fec_ter as fec_ter_con, g.fec_cambio, g.pct_contrato, g.publicacion, g.observacion as obs_con, c.nombre as cliente, z.nombre as zona, o.nombre as compania, o.id as compania_id, 
                        (select t.nombre
                        from gc_contrato_suministro c
                        left join gc_tipo_tarifa t on c.id_tarifa = t.id
                        where c.id_suministro = ? and now() between c.fec_ini and c.fec_ter) as tarifa,
                        (select count(*)
                        from gc_contrato_suministro c
                        where c.id_suministro = ? and now() between c.fec_ini and c.fec_ter) as sumi,
                        max_ep_fact_anno($id,$anno,-1) as max1,
                        max_ep_fact_anno($id,$anno, max_ep_fact_anno($id,$anno,-1)) as max2 
                FROM gc_suministro s 
                left join gc_contrato g on g.id_suministro = s.id
                join gc_cliente c on s.id_cli = c.id
                left join gc_sec_tarifa z on s.id_tarifa = z.id
                left join gc_compania o on z.id_compania = o.id
                where s.id = ?;";
        $query = $this->db->query($sql, array($id, $id, $id));
        
        return $query->row();
    }

    public function get_suministro_last_month($id) {
        $sql = "SELECT last_month
                FROM gc_suministro 
                where id = (select id_suministro from gc_contrato_suministro where id = ?);";
        $query = $this->db->query($sql, array(strip_tags($id))); 
        
        return $query->row();
    }

    public function get_ratios($id, $anno) {
        $anno = strip_tags($anno);
        $id = strip_tags($id);

        $sql = "SELECT r.*, i.energia_in, i.f_punta_in, i.e_punta_in, i.monomico_in, i.produccion_in, 
                        i.indicador_in, i.ahorro_in, i.optimizacion_in, i.observacion, i.cp_in,
                        i.energia_in_gen, i.e_punta_in_gen, i.monomico_in_gen, i.indicador_in_gen,
                        i.ahorro_in_gen, i.optimizacion_in_gen, i.ahorro_in_dis, i.optimizacion_in_dis,
                        i.trans_ratio_in, i.armo_ratio_in, i.tec_ratio_in,
                        (select f.etapa 
                            from gc_factura f 
                            where f.id_con in (select c.id 
                                                from gc_contrato_suministro c 
                                                where c.id_suministro = ?)
			                and f.mes = r.mes
                            order by id
                            limit 1) as r_etapa
                FROM gc_ratio r
                left join gc_ratio_intervencion i on i.id_ratio = r.id
                where id_suministro = ?
                and SUBSTR(mes, 1, 4) = $anno
                order by mes, id;";

        $query = $this->db->query($sql, array($id, $id)); 
        
        return $query->result();
    }

    public function get_facturas_suministro($id, $anno) {
        $anno = strip_tags($anno);
        $sql = "SELECT f.*, a.estado, r.estado as alerta 
                FROM gc_factura f
                join gc_analisis_factura a on f.id = a.id_factura
                join gc_alerta_factura r on f.id = r.id_factura
                where f.id_con in (select c.id 
                                from gc_contrato_suministro c 
                                where c.id_suministro = ?)
                and SUBSTR(mes, 1, 4) = $anno
                order by mes, id;";
        $query = $this->db->query($sql, array(strip_tags($id))); 
        
        return $query->result();
    }

    public function get_contratos_suministro($id) {
        $sql = "select *
                from gc_contrato_suministro
                where id_suministro = ?;";
        $query = $this->db->query($sql, array(strip_tags($id))); 
        
        return $query->result();
    }

    public function get_factura($id) {

        $sql = "SELECT f.*, a.*, r.estado as alerta, r.observacion as obs_alert, s.nombre as suministro, s.id as id_suministro
                FROM gc_factura f
                join gc_analisis_factura a on f.id = a.id_factura
                join gc_alerta_factura r on f.id = r.id_factura
                join gc_contrato_suministro c on f.id_con = c.id
                join gc_suministro s on c.id_suministro = s.id
                where f.id = ?;";
        $query = $this->db->query($sql, array(strip_tags($id))); 
        
        return $query->row();

    }

    public function get_contrato_suministro_activo($id) {
        $sql = "select c.id id_con, c.id_tarifa id_tarifa, s.id id_suministro, s.nombre, s.last_month, s.etapa
        from gc_contrato_suministro c
        join gc_suministro s on c.id_suministro = s.id
        where c.id_suministro = ? and now() between c.fec_ini and c.fec_ter;";

        $query = $this->db->query($sql, array(strip_tags($id))); 
        
        return $query->row();
    }

    public function check_contrato_suministro_activo($id, $id_suministro, $fec_ini, $fec_ter) {
        $sql = "select count(*) as total
                from gc_contrato_suministro c
                where c.id <> ? and c.id_suministro = ? 
                and ((c.fec_ini between str_to_date(?, '%Y-%m-%d') and str_to_date(?,  '%Y-%m-%d'))
                        or (c.fec_ter between str_to_date(?,  '%Y-%m-%d') and str_to_date(?,  '%Y-%m-%d')));";

        $query = $this->db->query($sql, array(strip_tags($id),
                                                strip_tags($id_suministro),
                                                strip_tags($fec_ini),
                                                strip_tags($fec_ter),
                                                strip_tags($fec_ini),
                                                strip_tags($fec_ter)));
        
        return $query->row();
    }

    public function insert_factura($token, $nro_factura, $fec_doc, $fec_desde,
                                    $fec_hasta, $fec_prox_lec, $valor_produccion, 
                                    $mes, $ac_lec_ant, $ac_lec_act, $ac_consumo, 
                                    $re_lec_ant, $re_lec_act, $re_consumo,
                                    $fp_consumo, $ep_consumo, $cargo_fijo,
                                    $cargo_postal, $ene_precio, $deman_fp_fact,
                                    $deman_fp_prec, $deman_ep_fact, $deman_ep_prec,
                                    $factor_potencia, $cargo_arriendo, $cargo_trans,
                                    $cargo_interes, $cargo_domi, $cargo_corte, $cargo_serv_pub,
                                    $cargo_otros, $cargo_reliq, $cargo_desc,
                                    $monto_exento, $total_neto, $cargo_otr, $saldo_ant,
                                    $total_pagar, $etapa, $tarB, $file, $cp_consumo, $cp_precio,
                                    $nro_factura_gen, $fec_doc_gen, $fec_desde_gen, $fec_hasta_gen,
                                    $fec_prox_lec_gen, $ac_lec_ant_gen, $ac_lec_act_gen,
                                    $ac_consumo_gen, $re_lec_ant_gen, $re_lec_act_gen, 
                                    $re_consumo_gen, $ep_consumo_gen, $ene_precio_gen,
                                    $deman_ep_fact_gen, $deman_ep_prec_gen,
                                    $cargo_lgse, $cargo_min_tec, $cargo_serv_com,
                                    $cargo_armonizacion, $total_neto_gen, $cargo_interes_gen,
                                    $cargo_desc_gen, $cargo_otros_gen, 
                                    $cargo_otr_gen, $total_pagar_gen, $monto_exento_gen,
                                    $saldo_ant_gen, $file_name_gen, $tarifa,
                                    $total_neto_libre, $cargo_otr_libre, $total_pagar_libre,
                                    $monto_exento_libre, $saldo_ant_libre, $mode) {

        $sql = "insert into gc_factura (
            num_fact,
            fec_doc,
            fec_ini,
            fec_ter,
            fec_prox,
            mes,
            produccion,
            lec1_activa,
            lec2_activa,
            lec1_reactiva,
            lec2_reactiva,
            consumo_re,
            consumo_ac,
            fp_pot,
            ep_pot,
            cargo_fijo,
            cargo_despacho,
            energia,
            fp_deman_fact,
            fp_deman_prec,
            ep_deman_fact,
            ep_deman_prec,
            recargo_factor,
            arriendo,
            transmision,
            interes,
            domicilio,
            corte,
            serv_pub,
            reliquidaciones,
            otros_desc,
            otros_cargos,
            id_con,
            tot_neto,
            otros,
            tot_fac,
            tot_exento,
            saldo_anterior,
            etapa,
            tarB,
            archivo,
            cp_consumo,
            cp_precio,
            num_fact_gen,
            fec_doc_gen,
            fec_ini_gen,
            fec_ter_gen,
            fec_prox_gen,
            lec1_activa_gen,
            lec2_activa_gen,
            lec1_reactiva_gen,
            lec2_reactiva_gen,
            consumo_ac_gen,
            consumo_re_gen,
            ep_pot_gen,
            energia_gen,
            ep_deman_fact_gen,
            ep_deman_prec_gen,
            cargo_lgse,
            cargo_tecnico,
            cargo_ser_com,
            cargo_armonizacion,
            tot_neto_gen,
            interes_gen,
            otros_desc_gen,
            otros_cargos_gen,
            otros_gen,
            tot_fac_gen,
            tot_exento_gen,
            saldo_anterior_gen,
            archivo_gen,
            tipo_tarifa,
            tot_neto_libre,
            otros_libre,
            tot_fac_libre,
            tot_exento_libre,
            saldo_anterior_libre,
            factura_mode
                )
                values 
                (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $query = $this->db->query($sql, array(strip_tags($nro_factura),
                                                strip_tags($fec_doc),
                                                strip_tags($fec_desde),
                                                strip_tags($fec_hasta),
                                                strip_tags($fec_prox_lec),
                                                strip_tags($mes),
                                                strip_tags($valor_produccion),
                                                strip_tags($ac_lec_ant),
                                                strip_tags($ac_lec_act),
                                                strip_tags($re_lec_ant),
                                                strip_tags($re_lec_act),
                                                strip_tags($re_consumo),
                                                strip_tags($ac_consumo),
                                                strip_tags($fp_consumo),
                                                strip_tags($ep_consumo),
                                                strip_tags($cargo_fijo),
                                                strip_tags($cargo_postal),
                                                strip_tags($ene_precio),
                                                strip_tags($deman_fp_fact),
                                                strip_tags($deman_fp_prec),
                                                strip_tags($deman_ep_fact),
                                                strip_tags($deman_ep_prec),
                                                strip_tags($factor_potencia),
                                                strip_tags($cargo_arriendo),
                                                strip_tags($cargo_trans),
                                                strip_tags($cargo_interes),
                                                strip_tags($cargo_domi),
                                                strip_tags($cargo_corte),
                                                strip_tags($cargo_serv_pub),
                                                strip_tags($cargo_reliq),
                                                strip_tags($cargo_desc),
                                                strip_tags($cargo_otros),
                                                strip_tags($token),
                                                strip_tags($total_neto),
                                                strip_tags($cargo_otr),
                                                strip_tags($total_pagar),
                                                strip_tags($monto_exento),
                                                strip_tags($saldo_ant),
                                                strip_tags($etapa),
                                                strip_tags($tarB),
                                                strip_tags($file),
                                                strip_tags($cp_consumo),
                                                strip_tags($cp_precio),
                                                strip_tags($nro_factura_gen),
                                                strip_tags($fec_doc_gen),
                                                strip_tags($fec_desde_gen),
                                                strip_tags($fec_hasta_gen),
                                                strip_tags($fec_prox_lec_gen),
                                                strip_tags($ac_lec_ant_gen),
                                                strip_tags($ac_lec_act_gen),
                                                strip_tags($re_lec_ant_gen),
                                                strip_tags($re_lec_act_gen),
                                                strip_tags($ac_consumo_gen),
                                                strip_tags($re_consumo_gen),
                                                strip_tags($ep_consumo_gen),
                                                strip_tags($ene_precio_gen),
                                                strip_tags($deman_ep_fact_gen),
                                                strip_tags($deman_ep_prec_gen),
                                                strip_tags($cargo_lgse),
                                                strip_tags($cargo_min_tec),
                                                strip_tags($cargo_serv_com),
                                                strip_tags($cargo_armonizacion),
                                                strip_tags($total_neto_gen),
                                                strip_tags($cargo_interes_gen),
                                                strip_tags($cargo_desc_gen),
                                                strip_tags($cargo_otros_gen),
                                                strip_tags($cargo_otr_gen),
                                                strip_tags($total_pagar_gen),
                                                strip_tags($monto_exento_gen),
                                                strip_tags($saldo_ant_gen), 
                                                strip_tags($file_name_gen),
                                                strip_tags($tarifa),
                                                strip_tags($total_neto_libre), 
                                                strip_tags($cargo_otr_libre), 
                                                strip_tags($total_pagar_libre),
                                                strip_tags($monto_exento_libre), 
                                                strip_tags($saldo_ant_libre),
                                                strip_tags($mode)));
        
        return $query ? $this->db->insert_id('gc_factura') : FALSE;
    }

    public function update_factura($token, $valor_produccion, 
                                    $ac_lec_ant, $ac_lec_act, $ac_consumo, 
                                    $re_lec_ant, $re_lec_act, $re_consumo,
                                    $fp_consumo, $ep_consumo, $cargo_fijo,
                                    $cargo_postal, $ene_precio, $deman_fp_fact,
                                    $deman_fp_prec, $deman_ep_fact, $deman_ep_prec,
                                    $factor_potencia, $cargo_arriendo, $cargo_trans,
                                    $cargo_interes, $cargo_domi, $cargo_corte, $cargo_serv_pub,
                                    $cargo_otros, $cargo_reliq, $cargo_desc,
                                    $monto_exento, $total_neto, $cargo_otr, $saldo_ant,
                                    $total_pagar, $tarB, $nro_factura, $fec_doc, $fec_desde,
                                    $fec_hasta, $fec_prox_lec, $mes, $file, $cp_consumo, $cp_precio,
                                    $nro_factura_gen, $fec_doc_gen, $fec_desde_gen, $fec_hasta_gen,
                                    $fec_prox_lec_gen,
                                    $ac_lec_ant_gen, $ac_lec_act_gen,
                                    $ac_consumo_gen, $re_lec_ant_gen, $re_lec_act_gen, 
                                    $re_consumo_gen, $ep_consumo_gen, $ene_precio_gen,
                                    $deman_ep_fact_gen, $deman_ep_prec_gen, 
                                    $cargo_lgse, $cargo_min_tec, $cargo_serv_com,
                                    $cargo_armonizacion, $total_neto_gen, $cargo_interes_gen,
                                    $cargo_desc_gen, $cargo_otros_gen, 
                                    $cargo_otr_gen, $total_pagar_gen, $monto_exento_gen,
                                    $saldo_ant_gen, $file_name_gen, 
                                    $total_neto_libre, $cargo_otr_libre, $total_pagar_libre,
                                    $monto_exento_libre, $saldo_ant_libre, $mode) {
    
        $sql = "update gc_factura 
                set 
                produccion = ?,
                lec1_activa = ?,
                lec2_activa = ?,
                lec1_reactiva = ?,
                lec2_reactiva = ?,
                consumo_re = ?,
                consumo_ac = ?,
                fp_pot = ?,
                ep_pot = ?,
                cargo_fijo = ?,
                cargo_despacho = ?,
                energia = ?,
                fp_deman_fact = ?,
                fp_deman_prec = ?,
                ep_deman_fact = ?,
                ep_deman_prec = ?,
                recargo_factor = ?,
                arriendo = ?,
                transmision = ?,
                interes = ?,
                domicilio = ?,
                corte = ?,
                serv_pub = ?,
                reliquidaciones = ?,
                otros_desc = ?,
                otros_cargos = ?,
                tot_neto = ?,
                otros = ?,
                tot_fac = ?,
                tot_exento = ?,
                saldo_anterior = ?,
                tarB = ?,
                num_fact = ?,
                fec_doc = ?,
                fec_ini = ?,
                fec_ter = ?,
                fec_prox = ?,
                mes = ?,
                archivo = ?,
                cp_consumo = ?,
                cp_precio = ?,
                num_fact_gen = ?,
                fec_doc_gen = ?,
                fec_ini_gen = ?,
                fec_ter_gen = ?,
                fec_prox_gen = ?,
                lec1_activa_gen = ?,
                lec2_activa_gen = ?,
                lec1_reactiva_gen = ?,
                lec2_reactiva_gen = ?,
                consumo_ac_gen = ?,
                consumo_re_gen = ?,
                ep_pot_gen = ?,
                energia_gen = ?,
                ep_deman_fact_gen = ?,
                ep_deman_prec_gen = ?,
                cargo_lgse = ?,
                cargo_tecnico = ?,
                cargo_ser_com = ?,
                cargo_armonizacion = ?,
                tot_neto_gen = ?,
                interes_gen = ?,
                otros_desc_gen = ?,
                otros_cargos_gen = ?,
                otros_gen = ?,
                tot_fac_gen = ?,
                tot_exento_gen = ?,
                saldo_anterior_gen = ?,
                archivo_gen = ?,
                tot_neto_libre = ?,
                otros_libre = ?,
                tot_fac_libre = ?,
                tot_exento_libre = ?,
                saldo_anterior_libre = ?,
                factura_mode = ?
                where id = ?;";

        $query = $this->db->query($sql, array(strip_tags($valor_produccion),
                                                strip_tags($ac_lec_ant),
                                                strip_tags($ac_lec_act),
                                                strip_tags($re_lec_ant),
                                                strip_tags($re_lec_act),
                                                strip_tags($re_consumo),
                                                strip_tags($ac_consumo),
                                                strip_tags($fp_consumo),
                                                strip_tags($ep_consumo),
                                                strip_tags($cargo_fijo),
                                                strip_tags($cargo_postal),
                                                strip_tags($ene_precio),
                                                strip_tags($deman_fp_fact),
                                                strip_tags($deman_fp_prec),
                                                strip_tags($deman_ep_fact),
                                                strip_tags($deman_ep_prec),
                                                strip_tags($factor_potencia),
                                                strip_tags($cargo_arriendo),
                                                strip_tags($cargo_trans),
                                                strip_tags($cargo_interes),
                                                strip_tags($cargo_domi),
                                                strip_tags($cargo_corte),
                                                strip_tags($cargo_serv_pub),
                                                strip_tags($cargo_reliq),
                                                strip_tags($cargo_desc),
                                                strip_tags($cargo_otros),
                                                strip_tags($total_neto),
                                                strip_tags($cargo_otr),
                                                strip_tags($total_pagar),
                                                strip_tags($monto_exento),
                                                strip_tags($saldo_ant),
                                                strip_tags($tarB),
                                                strip_tags($nro_factura),
                                                strip_tags($fec_doc),
                                                strip_tags($fec_desde),
                                                strip_tags($fec_hasta),
                                                strip_tags($fec_prox_lec),
                                                strip_tags($mes),
                                                strip_tags($file),
                                                strip_tags($cp_consumo),
                                                strip_tags($cp_precio),
                                                strip_tags($nro_factura_gen),
                                                strip_tags($fec_doc_gen),
                                                strip_tags($fec_desde_gen),
                                                strip_tags($fec_hasta_gen),
                                                strip_tags($fec_prox_lec_gen),
                                                strip_tags($ac_lec_ant_gen),
                                                strip_tags($ac_lec_act_gen),
                                                strip_tags($re_lec_ant_gen),
                                                strip_tags($re_lec_act_gen),
                                                strip_tags($ac_consumo_gen),
                                                strip_tags($re_consumo_gen),
                                                strip_tags($ep_consumo_gen),
                                                strip_tags($ene_precio_gen),
                                                strip_tags($deman_ep_fact_gen),
                                                strip_tags($deman_ep_prec_gen),
                                                strip_tags($cargo_lgse),
                                                strip_tags($cargo_min_tec), 
                                                strip_tags($cargo_serv_com),
                                                strip_tags($cargo_armonizacion),
                                                strip_tags($total_neto_gen),
                                                strip_tags($cargo_interes_gen),
                                                strip_tags($cargo_desc_gen),
                                                strip_tags($cargo_otros_gen),
                                                strip_tags($cargo_otr_gen),
                                                strip_tags($total_pagar_gen),
                                                strip_tags($monto_exento_gen),
                                                strip_tags($saldo_ant_gen),
                                                strip_tags($file_name_gen),
                                                strip_tags($total_neto_libre),
                                                strip_tags($cargo_otr_libre),
                                                strip_tags($total_pagar_libre),
                                                strip_tags($monto_exento_libre),
                                                strip_tags($saldo_ant_libre),
                                                strip_tags($mode),
                                                strip_tags($token)));
        
        return $query;
        
    }

    public function delete_factura($id) {

        $sql = "delete from gc_factura where id = ?;";

        $query = $this->db->query($sql, array(strip_tags($id))); 

        return $query;

    }

    public function insert_sumisnitro($token, $tarifario, $nombre, $numcli, $direccion, $regiones, 
                                        $ciudades, $observacion, $contacto, $cargo, $correo, 
                                        $telefono, $fec_ter_sum, $fec_cm_tarif, $max_inv, 
                                        $anno_base) {

        $sql = "INSERT INTO gc_suministro (
            nombre,
            num_cli,
            observacion,
            id_tarifa,
            direccion,
            region,
            ciudad,
            fec_ter,
            fec_mod,
            limit_inv,
            anno_base,
            id_cli,
            nom_contacto,
            cargo_contacto,
            email_contacto,
            telefono_contacto
        )
                VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $query = $this->db->query($sql, array(strip_tags($nombre), 
                                            strip_tags($numcli), 
                                            strip_tags($observacion), 
                                            strip_tags($tarifario),
                                            strip_tags($direccion),
                                            strip_tags($regiones),
                                            strip_tags($ciudades),
                                            strip_tags($fec_ter_sum),
                                            strip_tags($fec_cm_tarif),
                                            strip_tags($max_inv),
                                            strip_tags($anno_base),
                                            strip_tags($token),
                                            strip_tags($contacto),
                                            strip_tags($cargo),
                                            strip_tags($correo),
                                            strip_tags($telefono)));
        
        return $query ? $this->db->insert_id('gc_suministro') : FALSE;

    }

    public function update_suministro($token, $tarifario, $nombre, $numcli, $direccion, $regiones, 
                                        $ciudades, $observacion, $contacto, $cargo, $correo, 
                                        $telefono, $fec_ter_sum, $fec_cm_tarif, $max_inv, 
                                        $anno_base) {
    
        $sql = "update gc_suministro
                set nombre = ?,
                num_cli = ?,
                observacion = ?,
                id_tarifa = ?,
                direccion = ?,
                region = ?,
                ciudad = ?,
                fec_ter = ?,
                fec_mod = ?,
                limit_inv = ?,
                anno_base = ?,
                nom_contacto = ?,
                cargo_contacto = ?,
                email_contacto = ?,
                telefono_contacto = ?
                where id = ?;";

        $query = $this->db->query($sql, array(strip_tags($nombre),
                                                strip_tags($numcli),
                                                strip_tags($observacion),
                                                strip_tags($tarifario),
                                                strip_tags($direccion),
                                                strip_tags($regiones),
                                                strip_tags($ciudades),
                                                strip_tags($fec_ter_sum),
                                                strip_tags($fec_cm_tarif),
                                                strip_tags($max_inv),
                                                strip_tags($anno_base),
                                                strip_tags($contacto),
                                                strip_tags($cargo),
                                                strip_tags($correo),
                                                strip_tags($telefono),
                                                strip_tags($token))); 
        
        return $query;
        
    }

    public function update_contrato($token, $fec_ini_gc, $fec_ter_gc, 
                                    $fec_ini_save, $pct_con, $estado, $observacion) {
    
        $sql = "update gc_contrato
                set fec_ini = ?,
                fec_ter = ?,
                fec_cambio = ?,
                pct_contrato = ?,
                publicacion = ?,
                observacion = ?
                where id_suministro = ?;";

        $query = $this->db->query($sql, array(strip_tags($fec_ini_gc),
                                                strip_tags($fec_ter_gc),
                                                strip_tags($fec_ini_save),
                                                strip_tags($pct_con),
                                                strip_tags($estado),
                                                strip_tags($observacion),
                                                strip_tags($token))); 
        
        return $query;
        
    }

    public function update_analisis($token, $estado_ana, $fec_ana,
                                    $tipo_ana, $obs_in, $obs_cli) {
    
        $sql = "update gc_analisis_factura
                set estado = ?,
                fecha = ?,
                id_tipo = ?,
                obs_interna = ?,
                obs_cliente = ?
                where id_factura = ?;";

        $query = $this->db->query($sql, array(strip_tags($estado_ana),
                                                strip_tags($fec_ana),
                                                strip_tags($tipo_ana),
                                                strip_tags($obs_in),
                                                strip_tags($obs_cli),
                                                strip_tags($token))); 
        
        return $query;
        
    }

    public function update_alerta($token, $estado_alert, $obs_alert) {
    
        $sql = "update gc_alerta_factura
                set estado = ?,
                observacion = ?
                where id_factura = ?;";

        $query = $this->db->query($sql, array(strip_tags($estado_alert),
                                                strip_tags($obs_alert),
                                                strip_tags($token))); 
        
        return $query;
        
    }

    public function insert_analisis($id, $estado_ana, $fec_ana,
                                    $tipo_ana, $obs_in, $obs_cli) {

        $sql = "INSERT INTO gc_analisis_factura
                VALUES (?,?,?,?,?,?);";
        $query = $this->db->query($sql, array(strip_tags($id), 
                                            strip_tags($estado_ana), 
                                            strip_tags($fec_ana), 
                                            strip_tags($tipo_ana),
                                            strip_tags($obs_in),
                                            strip_tags($obs_cli)));
        
        return $query ? $this->db->insert_id('gc_analisis_factura') : FALSE;

    }

    public function insert_alerta($id, $estado_alert, $obs_alert) {

        $sql = "INSERT INTO gc_alerta_factura
                VALUES (?,?,?);";
        $query = $this->db->query($sql, array(strip_tags($id), 
                                            strip_tags($estado_alert), 
                                            strip_tags($obs_alert)));
        
        return $query ? $this->db->insert_id('gc_alerta_factura') : FALSE;

    }

    public function insert_contrato_suministro($id_suministro, $id_tarifa, $potencia, 
                                                $fec_ini, $fec_ter,
                                                $ac_num, $ac_prop, $ac_cons,
                                                $re_num, $re_prop, $re_cons,
                                                $fp_num, $fp_prop, $fp_cons,
                                                $ep_num, $ep_prop, $ep_cons) {

        $sql = "INSERT INTO gc_contrato_suministro
                VALUES (null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $query = $this->db->query($sql, array(strip_tags($id_suministro), 
                                            strip_tags($id_tarifa), 
                                            strip_tags($potencia), 
                                            strip_tags($fec_ini),
                                            strip_tags($fec_ter),
                                            strip_tags($ac_num),
                                            strip_tags($ac_prop),
                                            strip_tags($ac_cons),
                                            strip_tags($re_num),
                                            strip_tags($re_prop),
                                            strip_tags($re_cons),
                                            strip_tags($fp_num),
                                            strip_tags($fp_prop),
                                            strip_tags($fp_cons),
                                            strip_tags($ep_num),
                                            strip_tags($ep_prop),
                                            strip_tags($ep_cons)));
        
        return $query ? $this->db->insert_id('gc_contrato_suministro') : FALSE;

    }

    public function update_contrato_suministro($id, $id_tarifa, $potencia, 
                                                $fec_ini, $fec_ter,
                                                $ac_num, $ac_prop, $ac_cons,
                                                $re_num, $re_prop, $re_cons,
                                                $fp_num, $fp_prop, $fp_cons,
                                                $ep_num, $ep_prop, $ep_cons) {

        $sql = "update gc_contrato_suministro
                set
                id_tarifa = ?,
                potencia = ?,
                fec_ini = ?,
                fec_ter = ?,
                ac_num = ?,
                ac_prop = ?,
                ac_cons = ?,
                re_num = ?,
                re_prop = ?,
                re_cons = ?,
                fp_num = ?,
                fp_prop = ?,
                fp_cons = ?,
                ep_num = ?,
                ep_prop = ?,
                ep_cons = ?
                where id = ?;";
        $query = $this->db->query($sql, array(strip_tags($id_tarifa), 
                                            strip_tags($potencia), 
                                            strip_tags($fec_ini),
                                            strip_tags($fec_ter),
                                            strip_tags($ac_num),
                                            strip_tags($ac_prop),
                                            strip_tags($ac_cons),
                                            strip_tags($re_num),
                                            strip_tags($re_prop),
                                            strip_tags($re_cons),
                                            strip_tags($fp_num),
                                            strip_tags($fp_prop),
                                            strip_tags($fp_cons),
                                            strip_tags($ep_num),
                                            strip_tags($ep_prop),
                                            strip_tags($ep_cons),
                                            strip_tags($id)));
        
        return $query;

    }
}