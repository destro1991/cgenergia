<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ahorro extends CI_Model {

    public function get_contratos_ahorro($id_suministro) {
        $sql = "SELECT id, fec_ini, fec_ter, pct 
                FROM gc_contrato_ahorro
                where id_suministro = ?;";
        $query = $this->db->query($sql, array(strip_tags($id_suministro))); 
        
        return $query->result(); 
    }

    public function insert_contrato($id_suministro, $fec_ini_con_save, $fec_ter_con_save, 
                                            $pct_con_save) {

        $sql = "INSERT INTO gc_contrato_ahorro
                VALUES (null,?,?,?,?);";
        $query = $this->db->query($sql, array(strip_tags($id_suministro), 
                                            strip_tags($fec_ini_con_save), 
                                            strip_tags($fec_ter_con_save), 
                                            strip_tags($pct_con_save)));
        
        return $query ? $this->db->insert_id('gc_contrato_ahorro') : FALSE;

    }

    public function update_contrato($id, $fec_ini_con_save, $fec_ter_con_save, 
                                            $pct_con_save) {

        $sql = "update gc_contrato_ahorro
                set fec_ini = ?,
                fec_ter = ?,
                pct = ?
                where id = ?;";

        $query = $this->db->query($sql, array(strip_tags($fec_ini_con_save), 
                                            strip_tags($fec_ter_con_save), 
                                            strip_tags($pct_con_save),
                                            strip_tags($id)));
        
        return $query;

    }

    public function delete_contrato($id) {

        $sql = "delete from gc_contrato_ahorro where id = ?;";

        $query = $this->db->query($sql, array(strip_tags($id)));
        
        return $query;

    }

}