<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarifa extends CI_Model {

    public function get_tarifas() {

        $sql = "SELECT id, nombre FROM gc_tipo_tarifa;";
        $query = $this->db->query($sql);
        
        return $query->result();
    }

}