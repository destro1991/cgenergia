<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Model {

    public function update_columnas_factura($id,$factura_3, 
                                                $factura_4, 
                                                $factura_5,
                                                $factura_6,
                                                $factura_7,
                                                $factura_8,
                                                $factura_9,
                                                $factura_10,
                                                $factura_11,
                                                $factura_12,
                                                $factura_13,
                                                $factura_14,
                                                $factura_15,
                                                $factura_16,
                                                $factura_17,
                                                $factura_18,
                                                $factura_19,
                                                $factura_20,
                                                $factura_21,
                                                $factura_22,
                                                $factura_23) {
        strip_tags($id);
        $factura_3 = strip_tags($factura_3);
        $factura_4 = strip_tags($factura_4);
        $factura_5 = strip_tags($factura_5);
        $factura_6 = strip_tags($factura_6);
        $factura_7 = strip_tags($factura_7);
        $factura_8 = strip_tags($factura_8);
        $factura_9 = strip_tags($factura_9);
        $factura_10 = strip_tags($factura_10);
        $factura_11 = strip_tags($factura_11);
        $factura_12 = strip_tags($factura_12);
        $factura_13 = strip_tags($factura_13);
        $factura_14 = strip_tags($factura_14);
        $factura_15 = strip_tags($factura_15);
        $factura_16 = strip_tags($factura_16);
        $factura_17 = strip_tags($factura_17);
        $factura_18 = strip_tags($factura_18);
        $factura_18 = strip_tags($factura_19);
        $factura_18 = strip_tags($factura_20);
        $factura_18 = strip_tags($factura_21);
        $factura_18 = strip_tags($factura_22);
        $factura_18 = strip_tags($factura_23);

        $sql = "update gc_suministro
                set
                factura_3 = ?, 
                factura_4 = ?, 
                factura_5 = ?,
                factura_6 = ?,
                factura_7 = ?,
                factura_8 = ?,
                factura_9 = ?,
                factura_10 = ?,
                factura_11 = ?,
                factura_12 = ?,
                factura_13 = ?,
                factura_14 = ?,
                factura_15 = ?,
                factura_16 = ?,
                factura_17 = ?,
                factura_18 = ?,
                factura_19 = ?,
                factura_20 = ?,
                factura_21 = ?,
                factura_22 = ?,
                factura_23 = ?
                where id = ?";
        $query = $this->db->query($sql, array($factura_3, 
                                        $factura_4, 
                                        $factura_5,
                                        $factura_6,
                                        $factura_7,
                                        $factura_8,
                                        $factura_9,
                                        $factura_10,
                                        $factura_11,
                                        $factura_12,
                                        $factura_13,
                                        $factura_14,
                                        $factura_15,
                                        $factura_16,
                                        $factura_17,
                                        $factura_18,
                                        $factura_19,
                                        $factura_20,
                                        $factura_21,
                                        $factura_22,
                                        $factura_23,
                                        $id));
        
        return $query;
    }

    public function update_columnas_ratio($id,$ratio_2, 
                                                $ratio_3, 
                                                $ratio_4,
                                                $ratio_5,
                                                $ratio_6,
                                                $ratio_7,
                                                $ratio_8,
                                                $ratio_9,
                                                $ratio_10,
                                                $ratio_11,
                                                $ratio_12,
                                                $ratio_13,
                                                $ratio_14,
                                                $ratio_15,
                                                $ratio_16,
                                                $ratio_17,
                                                $ratio_18,
                                                $ratio_19,
                                                $ratio_20,
                                                $ratio_21) {

        $id = strip_tags($id);
        $ratio_2 = strip_tags($ratio_2);
        $ratio_3 = strip_tags($ratio_3);
        $ratio_4 = strip_tags($ratio_4);
        $ratio_5 = strip_tags($ratio_5);
        $ratio_6 = strip_tags($ratio_6);
        $ratio_7 = strip_tags($ratio_7);
        $ratio_8 = strip_tags($ratio_8);
        $ratio_9 = strip_tags($ratio_9);
        $ratio_10 = strip_tags($ratio_10);
        $ratio_11 = strip_tags($ratio_11);
        $ratio_12 = strip_tags($ratio_12);
        $ratio_13 = strip_tags($ratio_13);
        $ratio_14 = strip_tags($ratio_14);
        $ratio_15 = strip_tags($ratio_15);
        $ratio_16 = strip_tags($ratio_16);
        $ratio_17 = strip_tags($ratio_17);
        $ratio_18 = strip_tags($ratio_18);
        $ratio_19 = strip_tags($ratio_19);
        $ratio_20 = strip_tags($ratio_20);
        $ratio_21 = strip_tags($ratio_21);

        $sql = "update gc_suministro
                set
                ratio_2 = ?, 
                ratio_3 = ?, 
                ratio_4 = ?,
                ratio_5 = ?,
                ratio_6 = ?,
                ratio_7 = ?,
                ratio_8 = ?,
                ratio_9 = ?,
                ratio_10 = ?,
                ratio_11 = ?,
                ratio_12 = ?,
                ratio_13 = ?,
                ratio_14 = ?,
                ratio_15 = ?,
                ratio_16 = ?,
                ratio_17 = ?,
                ratio_18 = ?,
                ratio_19 = ?,
                ratio_20 = ?,
                ratio_21 = ?
                where id = ?";
        $query = $this->db->query($sql, array($ratio_2,
                                                $ratio_3,
                                                $ratio_4,
                                                $ratio_5,
                                                $ratio_6,
                                                $ratio_7,
                                                $ratio_8,
                                                $ratio_9,
                                                $ratio_10,
                                                $ratio_11,
                                                $ratio_12,
                                                $ratio_13,
                                                $ratio_14,
                                                $ratio_15,
                                                $ratio_16,
                                                $ratio_17,
                                                $ratio_18,
                                                $ratio_19,
                                                $ratio_20,
                                                $ratio_21,
                                                $id));
        
        return $query;
    }
}