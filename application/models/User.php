<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {
    public function get_cuentas() {
        $sql = "SELECT u.id, u.rut, u.nombre, u.apellido, u.correo, p.perfil 
                FROM gc_user u
                join gc_perfil p on u.id_perfil = p.id
                where u.id_perfil <> 3;";
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    public function get_cuentas_filtrado($filtro) {
        $filtro = strip_tags($filtro);

        $sql = "SELECT u.id, u.rut, u.nombre, u.apellido, u.correo, p.perfil 
                FROM gc_user u
                join gc_perfil p on u.id_perfil = p.id
                where u.id_perfil <> 3 and (nombre like '%$filtro%' or rut like '%$filtro%');";
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    public function get_user($id) {
        $sql = "SELECT u.id, u.rut, u.nombre, u.apellido, u.correo, u.fec_in, u.avatar, u.id_perfil, p.perfil
        FROM gc_user u
        join gc_perfil p on u.id_perfil = p.id
        where u.id = ?;";
        $query = $this->db->query($sql, array(strip_tags($id)));
        
        return $query->row();
    }

    public function get_user_password($rut, $password) {
        $sql = "SELECT id FROM gc_user where rut = ? and pass = ?;";
        $query = $this->db->query($sql, array(strip_tags($rut), strip_tags($password)));
        $row = $query->row();

        $row = $query->num_rows() > 0 ? $query->row() : FALSE;

        return $row;
    }

    public function chek_permissions($id) {
        $sql = "SELECT id_perfil FROM gc_user where id = ?;";
        $query = $this->db->query($sql, array(strip_tags($id)));
        
        return $query->row();
    }

    public function get_clientes($user) {
        $sql = "SELECT c.id, c.rut, c.nombre, c.correo,
                        (select count(*) 
                        from gc_favoritos 
                        where id_user = ? 
                        and id_cli = c.id) as fav
                FROM gc_cliente c;";
        $query = $this->db->query($sql, array(strip_tags($user)));
        
        return $query->result();
    }

    public function get_clientes_filtrado($filtro, $user) {
        $filtro = strip_tags($filtro);
        $sql = "SELECT c.id, c.rut, c.nombre, c.correo,
                        (select count(*) 
                        from gc_favoritos 
                        where id_user = ? 
                        and id_cli = c.id) as fav
                FROM gc_cliente c
                where nombre 
                like '%$filtro%';";
        $query = $this->db->query($sql, array(strip_tags($user)));
        
        return $query->result();
    }

    public function get_cliente($id, $user) {
        $id = strip_tags($id);
        $user = strip_tags($user);
        
        $sql = "SELECT c.*, 
                        (select count(*) from gc_suministro where id_cli = ?) as total,
                        (select count(*) 
                            from gc_favoritos 
                            where id_user = ? 
                            and id_cli = c.id) as fav
                FROM gc_cliente c
                where id = ?;";
        $query = $this->db->query($sql, array($id, $user, $id));
        
        return $query->row();
    }

    public function get_cliente_suministro($id) {
        $id = strip_tags($id);
        
        $sql = "SELECT c.*
                FROM gc_cliente c
                where id = ?;";
        $query = $this->db->query($sql, array($id));
        
        return $query->row();
    }

    public function update_cuenta($id, $rut, $nombre, $apellido, 
                                    $correo, $perfil) {

        $sql = "update gc_user
                set 
                rut = ?,
                nombre = ?,
                apellido = ?,
                correo = ?,
                id_perfil = ?
                where id = ?;";

        $query = $this->db->query($sql, array(strip_tags($rut), 
                                                strip_tags($nombre), 
                                                strip_tags($apellido),
                                                strip_tags($correo),
                                                strip_tags($perfil),
                                                strip_tags($id)));

        return $query;

    }

    public function update_perfil($id, $nombre, $apellido, 
                                    $correo) {

        $sql = "update gc_user
                set 
                nombre = ?,
                apellido = ?,
                correo = ?
                where id = ?;";

        $query = $this->db->query($sql, array(strip_tags($nombre), 
                                                strip_tags($apellido),
                                                strip_tags($correo),
                                                strip_tags($id)));

        return $query;

    }

    public function update_password($id, $password) {

        $sql = "update gc_user
                set 
                pass = ?
                where id = ?;";

        $query = $this->db->query($sql, array(strip_tags($password),
                                                strip_tags($id)));

        return $query;

    }

    public function update_avatar($id, $avatar) {

        $sql = "update gc_user
                set 
                avatar = ?
                where id = ?;";

        $query = $this->db->query($sql, array(strip_tags($avatar),
                                                strip_tags($id)));

        return $query;

    }

    public function insert_cuenta($rut, $nombre, $apellido, 
                                    $correo, $perfil, $password) {

        $sql = "INSERT INTO gc_user (rut, nombre, apellido, correo, id_perfil, pass, fec_in)
        VALUES (?,?,?,?,?,?,now());";
        $query = $this->db->query($sql, array(strip_tags($rut), 
                                                strip_tags($nombre), 
                                                strip_tags($apellido),
                                                strip_tags($correo),
                                                strip_tags($perfil),
                                                strip_tags($password)));

        return $query ? $this->db->insert_id('gc_user') : FALSE;

    }
}