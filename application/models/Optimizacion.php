<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Optimizacion extends CI_Model {

    public function get_contratos_optimizacion($id_suministro) {
        $sql = "SELECT id, fec_ini, fec_ter, pct
                FROM gc_contrato_optimizacion
                where id_suministro = ?;";
        $query = $this->db->query($sql, array(strip_tags($id_suministro))); 
        
        return $query->result(); 
    }

    public function insert_contrato($id_suministro, $fec_ini_con_opt, $fec_ter_con_opt, 
                                            $pct_con_opt) {

        $sql = "INSERT INTO gc_contrato_optimizacion
                VALUES (null,?,?,?,?);";
        $query = $this->db->query($sql, array(strip_tags($id_suministro), 
                                            strip_tags($fec_ini_con_opt), 
                                            strip_tags($fec_ter_con_opt), 
                                            $pct_con_opt));
        
        return $query ? $this->db->insert_id('gc_contrato_optimizacion') : FALSE;

    }

    public function update_contrato($id, $fec_ini_con_opt, $fec_ter_con_opt, 
                                            $pct_con_opt) {

        $sql = "update gc_contrato_optimizacion
                set fec_ini = ?,
                fec_ter = ?,
                pct = ?
                where id = ?;";

        $query = $this->db->query($sql, array(strip_tags($fec_ini_con_opt), 
                                            strip_tags($fec_ter_con_opt), 
                                            strip_tags($pct_con_opt),
                                            strip_tags($id)));
        
        return $query;

    }

    public function delete_contrato($id) {

        $sql = "delete from gc_contrato_optimizacion where id = ?;";

        $query = $this->db->query($sql, array(strip_tags($id)));
        
        return $query;

    }

}