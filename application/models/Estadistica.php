<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estadistica extends CI_Model {

    public function get_generales() {
        $sql = "select count_clientes_function() as clientes, 
                    count_suministros_function() as suministros,
                    total_ahorro_function() as ahorro,
                    total_optimizacion_function() as optimizacion;";
        $query = $this->db->query($sql);
        
        return $query->row();

    }

    public function get_generales_cliente($cliente, $anno) {
        $cliente = strip_tags($cliente);
        $anno = strip_tags($anno);

        $sql = "select count_suministros_cliente_function(?) as suministros,
                    total_ahorro_cliente_function(?,?) as ahorro,
                    total_optimizacion_cliente_function(?,?) as optimizacion,
                    total_neto_cliente_function(?,?) as neto,
                    total_factura_cliente_function(?,?) as facturado;";
        $query = $this->db->query($sql, array($cliente, 
                                              $cliente, $anno, 
                                              $cliente, $anno, 
                                              $cliente, $anno, 
                                              $cliente, $anno));
        
        return $query->row();

    }
}