<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión
        <small>Lista de Cuentas</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active"> <i class="fa fa-user-circle-o"></i> Cuentas</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-xs-2 col-sm-4">
                    <h3 class="box-title">Cuentas</h3>
                </div>
                <div class="col-xs-2">
                    <a href="<?=base_url('cuentas/new_account')?>" type="submit" class="btn btn-primary btn-flat margin-left">
                        <i class="fa fa-plus"></i> Nueva Cuenta
                    </a>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <form id="filtrarCun" action="<?=base_url('cuentas/')?>" method="get">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Buscar..." <?php if(!empty($_GET['q'])) : ?> value="<?=$_GET['q'];?>" <?php endif; ?>>
                            <span class="input-group-btn">
                                <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
                <?php if($this->data->filtrado):?>
                <div class="col-xs-2">
                    <a href="<?=base_url('cuentas/')?>" type="submit" class="btn btn-default btn-flat margin-left">
                        <i class="fa fa-window-close"></i> Mostrar Todos
                    </a>
                </div>
                <?php endif;?>
            </div>
        </div>
        <div class="box-body">
            
            <?php if (!empty($this->data->cuentas)) : ?>
            <table id="example2" class="table table-hover">
                <thead>
                    <tr>
                        <th>Rut</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Correo</th>
                        <th>Perfil</th>                   
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->data->cuentas as $cuenta) : ?>

                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col col-xs-7 col-sm-9 col-md-10">
                                        <span><?=$cuenta->rut;?></span>
                                    </div>
                                </div>
                            </td>
                            <td><?=$cuenta->nombre;?></td>
                            <td><?=$cuenta->apellido;?></td>
                            <td><?=$cuenta->correo;?></td>
                            <td style="word-wrap: break-word;">
                                <div class="row">
                                    <div class="col col-xs-8 col-sm-9">
                                        <span><?=$cuenta->perfil;?></span>
                                    </div>

                                    <div class="col col-xs-4 col-sm-3">
                                        <a href="<?=base_url('cuentas/view/?token=' . $cuenta->id);?>" title="Editar">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </div>

                                </div>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Rut</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Correo</th>
                        <th>Perfil</th>
                    </tr>
                </tfoot>
            </table>
            <?php else: ?>
                <h4><em> (No se encontraron resultados) </em></h4>
            <?php endif; ?>  
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  