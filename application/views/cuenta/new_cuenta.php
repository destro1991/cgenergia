<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Modal -->
<div id="ajax-response" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ajax-response-label" aria-hidden="true">
    <div class="modal-dialog">
        <div id="ajax-content" class="modal-content">
            
        </div>
    </div>
</div>
<!-- Modal -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Cuentas
        <small>Nueva Cuenta</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="<?=base_url('cuentas/');?>"><i class="fa fa-edit"></i> Cuentas</a></li>
        <li class="active">Agregar Cuenta</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <form id="cuenta_form" action="<?=base_url('cuentas/add')?>" method="post">
      <!-- Modal -->
        <div class="modal fade" id="saveCuentaModal" tabindex="-1" role="dialog" aria-labelledby="saveCuentaModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="saveCuentaModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                </div>
                <div class="modal-body">
                    <p>¿Realmente desea ingresar una nueva cuenta?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button id="save" type="submit" class="btn btn-primary">Sí</button>
                </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
            <div class="col-xs-4 col-sm-8">
                <h3 class="box-title">Ingresando cuenta</h3>
            </div>
            <div class="col-xs-6 col-sm-4 text-right">
                <a href="<?=base_url('cuentas/');?>" class="text-18 margin-left" title="Cancelar">
                    <i class="fa fa-window-close"></i>
                </a>
            </div>
        </div>
        <div class="box-body">
        <div class="row">
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                <div class="row">
                    <fieldset>
                    <legend>Perfil</legend>
                    
                    <div class="col col-xs-12 col-md-4">
                        <div class="form-group">
                            <label>
                                Rut:
                                <input name="rut" type="text" class="form-control" placeholder="12345678-k" pattern="^[0-9]+[-|‐]{1}[0-9kK]{1}$" required="required">
                            </label>
                        </div>
                    </div>
                        
                    <div class="col col-xs-12 col-md-4">
                        <div class="form-group">
                            <label>
                                Nombre:
                                <input name="nombre" type="text" class="form-control" placeholder="nombre" required="required">
                            </label>
                        </div>
                    </div>
                    
                    <div class="col col-xs-12 col-md-4">
                        <div class="form-group">
                            <label>
                                Apellido:
                                <input name="apellido" type="text" class="form-control" placeholder="apellido">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-4">
                        <div class="form-group">
                            <label>
                                Correo: 
                                <input name="correo" type="email" class="form-control" placeholder="mail@ejemplo.com" pattern="^[-\w.%+]{1,64}@(?:[A-Za-z0-9-]{1,63}\.){1,125}[A-Za-z]{2,63}$">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-4">
                        <div class="form-group">
                            <label>
                                Perfil:
                                <select id="perfil" name="perfil"  class="form-control" required="required">
                                    <?php foreach($this->data->perfiles as $perfil) :?>
                                        <option value="<?=$perfil->id;?>"><?=$perfil->perfil?></option>
                                    <?php endforeach;?>
                                </select>
                            </label>
                        </div>
                    </div> 
                    </fieldset>
                </div>

                <div class="row">
                    <fieldset>
                    <legend>Seguridad</legend>
                    
                    <div class="col col-xs-12 col-md-4">
                        <div class="form-group">
                            <label>
                                Contraseña:
                                <input name="password" type="password" class="form-control" placeholder="contraseña" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" required="required">
                            </label>
                        </div>
                    </div>
                        
                    <div class="col col-xs-12 col-md-4">
                        <div class="form-group">
                            <label>
                                Confirmar contraseña:
                                <input name="password2" type="password" class="form-control" placeholder="contraseña" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" required="required">
                            </label>
                        </div>
                    </div>
                    </fieldset>
                </div>
            </div>
        </div>        
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="row">
                <div class="col col-xs-8 col-xs-offset-1 col-sm-offset-2">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group text-right">
                                <button id="saveCuenta" type="button" class="btn btn-primary btn-flat">
                                    <i class="fa fa-floppy-o"></i> Guardar
                                </button>
                                <a href="<?=base_url('cuentas/');?>" class="btn btn-default btn-flat margin-left">
                                    <i class="fa fa-window-close"></i> Cerrar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->