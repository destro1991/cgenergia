<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Modal -->
<div id="ajax-response" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ajax-response-label" aria-hidden="true">
    <div class="modal-dialog">
        <div id="ajax-content" class="modal-content">
            
        </div>
    </div>
</div>
<!-- Modal -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Cuentas
        <small>Configuración</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Configuración</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <?php if(empty($this->data->user)) :?>
        <div class="callout callout-danger">
            <h4><i class="fa fa-exclamation-circle"></i> Error</h4>

            <p>Imposible recuperar la información de la cuenta</p>
        </div>
    <?php else :?>
      
    <form id="password_form" action="<?=base_url('cuentas/change_password_self')?>" method="post">
      <!-- Modal -->
        <div class="modal fade" id="savePasswordModal" tabindex="-1" role="dialog" aria-labelledby="savePasswordModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="savePasswordModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                </div>
                <div class="modal-body">
                    <p>¿Realmente desea modificar la contraseña?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button id="save2" type="submit" class="btn btn-primary">Sí</button>
                </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-xs-4 col-sm-8">
                <h3 class="box-title">Seguridad</h3>
            </div>
            <div class="col-xs-6 col-sm-4 text-right">
                <a href="<?=base_url('cuentas/');?>" class="text-18 margin-left" title="Cancelar">
                    <i class="fa fa-window-close"></i>
                </a>
            </div>
        </div>
        <div class="box-body">
        <div class="row">
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col col-xs-12 col-md-4">
                        <div class="form-group">
                            <label>
                                Contraseña:
                                <input name="password" type="password" class="form-control" placeholder="contraseña" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" required="required">
                            </label>
                        </div>
                    </div>
                        
                    <div class="col col-xs-12 col-md-4">
                        <div class="form-group">
                            <label>
                                Confirmar contraseña:
                                <input name="password2" type="password" class="form-control" placeholder="contraseña" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" required="required">
                            </label>
                        </div>
                    </div> 
                </div>
            </div>
        </div>        
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="row">
                <div class="col col-xs-8 col-xs-offset-1 col-sm-offset-2">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group text-right">
                                <button id="savePassword" type="button" class="btn btn-primary btn-flat">
                                    <i class="fa fa-floppy-o"></i> Guardar
                                </button>
                                <a href="<?=base_url('cuentas/');?>" class="btn btn-default btn-flat margin-left">
                                    <i class="fa fa-window-close"></i> Cerrar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      </form>

    <?php endif;?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->