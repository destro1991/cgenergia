<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Contabiliad
        <small>Listado de suministros</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active"> <i class="fa fa-edit"></i> Suministros</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="box-title">Filtro</h3>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <form id="filtrarSum" action="<?=base_url('contabilidad')?>" method="get">
                        <div class="row">
                            <div class="col-xs-10">
                                <div class="form-group">
                                    <label>
                                        Cliente:
                                        <div class="row-fluid">
                                        <select name="q" class="selectpicker" data-live-search="true">
                                            <?php foreach($this->data->clientes as $cliente) :?>
                                                <option value="<?=$cliente->id;?>" <?php if($cliente->selected):?> selected <?php endif;?>><?=$cliente->nombre;?></option>
                                            <?php endforeach;?>
                                        </select>
                                        </div>
                                    </label>
                                    <label>
                                        Año:
                                        <select id="anno" name="anno"  class="form-control" required="required">
                                            <?php foreach($this->data->annos as $anno) :?>
                                                <option value="<?=$anno;?>" <?php if((empty($_GET['anno']) && $anno == $this->data->anno_actual) || (!empty($_GET['anno']) && $anno == $_GET['anno'])) :?>selected<?php endif;?>><?=$anno;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </label>
                                    <label>
                                        Mes:
                                        <select id="mes" name="mes"  class="form-control" required="required">
                                            <option value="ALL" <?php if($this->data->mes_selected == 'ALL') :?>selected<?php endif;?>>Todos</option>
                                            <option value="01"  <?php if($this->data->mes_selected == '01') :?>selected<?php endif;?>>Enero</option>
                                            <option value="02"  <?php if($this->data->mes_selected == '02') :?>selected<?php endif;?>>Febrero</option>
                                            <option value="03"  <?php if($this->data->mes_selected == '03') :?>selected<?php endif;?>>Marzo</option>
                                            <option value="04"  <?php if($this->data->mes_selected == '04') :?>selected<?php endif;?>>Abril</option>
                                            <option value="05"  <?php if($this->data->mes_selected == '05') :?>selected<?php endif;?>>Mayo</option>
                                            <option value="06"  <?php if($this->data->mes_selected == '06') :?>selected<?php endif;?>>Junio</option>
                                            <option value="07"  <?php if($this->data->mes_selected == '07') :?>selected<?php endif;?>>Julio</option>
                                            <option value="08"  <?php if($this->data->mes_selected == '08') :?>selected<?php endif;?>>Agosto</option>
                                            <option value="09"  <?php if($this->data->mes_selected == '09') :?>selected<?php endif;?>>Septiembre</option>
                                            <option value="10"  <?php if($this->data->mes_selected == '10') :?>selected<?php endif;?>>Octubre</option>
                                            <option value="11"  <?php if($this->data->mes_selected == '11') :?>selected<?php endif;?>>Noviembre</option>
                                            <option value="12"  <?php if($this->data->mes_selected == '12') :?>selected<?php endif;?>>Diciembre</option>
                                        </select>
                                    </label>
                                    <label>
                                        Contrato de Ahorro:
                                        <select id="contrato" name="contrato"  class="form-control" required="required">
                                            <option value="ALL" <?php if($this->data->contrato_selected == 'ALL') :?>selected<?php endif;?>>Todos</option>
                                            <option value="1"   <?php if($this->data->contrato_selected == '1') :?>selected<?php endif;?>>Contrato</option>
                                            <option value="2"   <?php if($this->data->contrato_selected == '2') :?>selected<?php endif;?>>Sin Contrato</option>
                                        </select>
                                    </label>
                                    <button type="submit" id="search-btn" class="btn btn-primary btn-flat margin-left">
                                        <i class="fa fa-search"></i> Buscar
                                    </button>
                                </div>
                            </div>
                        </div>                        
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-xs-10">
                    <h3 class="box-title">Suministros</h3>
                </div>
                <div class="col-xs-2">
                    <?php if($this->data->export) : ?>
                        <a href="<?=base_url('export/suministros_contabilidad/?q=' . $_GET['q'] . '&anno=' . $_GET['anno'] . '&mes=' . $_GET['mes'] . '&contrato=' . $_GET['contrato']);?>" class="btn btn-default btn-flat margin-left">
                            <i class="fa fa-file-excel-o"></i> Exportar
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="box-body">
            <?php if (!empty($this->data->suministros)) : ?>
            <table id="contsumi" class="table dark table-hover" data-page-length="30">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>Entidad</th>
                        <th>Suministro</th>
                        <th>% Ahorro</th>
                        <th>Ahorro</th>
                        <th>Optimización</th>
                        <th>Total Ahorro</th>
                        <th>Comisión</th>                   
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->data->suministros as $suministro) : ?>

                        <tr>
                            <td><b><?=$suministro->indice;?></b></td>
                            <td><?=$suministro->nombre_cli;?></td>
                            <td><?=$suministro->nombre;?></td>
                            <td><?=$suministro->pct;?></td>
                            <td><?=$suministro->ahorro;?></td>
                            <td><?=$suministro->optimizacion;?></td>
                            <td><?=$suministro->total;?></td>
                            <td><?=$suministro->comision;?></td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3"></td>
                        <td><b>Totales:</b></td>
                        <td><b><?=$this->data->total_ahorro;?></b></td>
                        <td><b><?=$this->data->total_optimizacion;?></b></td>
                        <td><b><?=$this->data->total_total;?></b></td>
                        <td><b><?=$this->data->total_comision;?></b></td>
                    </tr>
                    <tr>
                        <th>N°</th>
                        <th>Entidad</th>
                        <th>Suministro</th>
                        <th>% Ahorro</th>
                        <th>Ahorro</th>
                        <th>Optimización</th>
                        <th>Total Ahorro</th>
                        <th>Comisión</th>
                    </tr>
                </tfoot>
            </table>
            <?php else: ?>
                <h4><em> (No se encontraron resultados) </em></h4>
            <?php endif; ?>  
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  