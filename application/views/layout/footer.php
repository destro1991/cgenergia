<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <footer class="main-footer" <?php if(!$this->data->auth):?>style="margin-left:0 !important;"<?php endif;?>>
    <div class="pull-right hidden-xs">
      <b>Page rendered in</b> {elapsed_time}  <b>Version</b> 1.5
    </div>
    <strong>Copyright &copy; 2018-2019 <a href="https://gcenergia.cl">GC Energía</a>. </strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?=base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!--Inputmask-->
<script src="<?=base_url();?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?=base_url();?>assets/plugins/input-mask/jquery.inputmask.regex.extensions.js"></script>
<script src="<?=base_url();?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- SlimScroll -->
<script src="<?=base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url();?>assets/dist/js/adminlte.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?=base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- APP -->
<script src="<?=base_url();?>assets/dist/js/app.min.js"></script>
<script src="<?=base_url();?>assets/js/gcenergia.js"></script>
<?php 
if(!empty($this->data->page)) : 
  if($this->data->page == 'clientes' || $this->data->page == 'suministros' || $this->data->page == 'cuentas') :
?>
    <!-- page script -->
    <script>
      $(function () {
        $('#example2').DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : false,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false,
          language: {
              decimal: ",",
          },
          "columnDefs": [
            { "orderable": false, "targets": 6 }
          ]
        })
      })
    </script>
<?php 
  endif;
  if($this->data->page == 'cliente') :
?>
    <script>
      $(function () {
        $('#example3').DataTable({
          'paging'      : false,
          'lengthChange': false,
          'searching'   : false,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true,
          language: {
              decimal: ",",
          },
          "columnDefs": [
            { "orderable": false, "targets": 6 }
          ]
        })
      })
    </script>
<?php
  endif;
endif;
?>
</body>
</html>
