<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Home
        <small>Plataforma de Ahorro</small>
      </h1>
      <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Inicio</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="callout callout-info">
        <h4>!Bienvenido!</h4>

        <p>
          Esta es la plataforma de Gestión Energética proporcionada por GC Energía.
        </p>
      </div>

      <?php if(empty($this->data->clientes)) : ?>

      <div class="callout callout-success">
        <h4>Información</h4>

        <p>
            Aquí se desplegará tu lista de clientes favoritos.

            Aún no tienes elementos marcados como favoritos.
        </p>
      </div>

      <?php endif; ?>

      <div class="row">
        <div class="col col-xs-12">
            <!-- Default box -->
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Estadísticas Generales</h3>
              </div>
              <div class="box-body">
                <div class="row">
                      <div class="col col-xs-12">
                          <ul class="detail">
                              <li class="margin-left"><b>Clientes:</b> <?=$this->data->estadisticas->clientes;?></li>
                              <li class="margin-left"><b>Suministros:</b> <?=$this->data->estadisticas->suministros;?></li>
                              <li class="margin-left"><b>Total en ahorros:</b> $ <?=$this->data->estadisticas->ahorro;?></li>
                              <li class="margin-left"><b>Total en optimización:</b> $ <?=$this->data->estadisticas->optimizacion;?></li>
                          </ul>
                      </div>
                  </div>
                </div>
              <!-- /.box-body -->
              <div class="box-footer">
              </div>
              <!-- /.box-footer-->
            </div>
            <!-- /.box -->
        </div>
        <?php if(!empty($this->data->clientes)) : ?>
        <?php foreach ($this->data->clientes as $cliente) : ?>
        <div class="col col-xs-12 col-md-4">
            <!-- Default box -->
            <div class="box box-primary">
              <div class="box-header with-border text-center">
                <div class="row">
                  <div class="col col-xs-12">
                    <a href="<?=base_url('gestion/cliente/?token=' . $cliente->id)?>" class="box-title" title="Ver más"><b><?=$cliente->nombre?> <?=$this->data->anno?></b></a>
                  </div>
                  <div class="col col-xs-12">
                    <img class="img-circle" src="<?=base_url('assets/img/'. $cliente->avatar);?>" height="150px" width="150px" alt="Avatar Cliente">
                  </div>
                  <div class="col col-xs-12">
                    <span class="text-14"><b>$ <?=$cliente->gestion?></b><span>
                  </div>
                </div>                
              </div>
              <div class="box-body">
                <p></p>
                <ul class="favorite-data">
                  <li> 
                    <div class="row">
                      <div class="col col-xs-5 text-right">
                        <b>Suministros:</b>
                      </div>

                      <div class="col col-xs-7 text-left">
                        <?=$cliente->total?>
                      </div>
                    </div>
                   </li>
                  <li>
                    <div class="row">
                      <div class="col col-xs-5 text-right">
                        <b>Ahorro:</b>
                      </div>
                      <div class="col col-xs-7">
                        $ <?=$cliente->ahorro?>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="row">
                      <div class="col col-xs-5 text-right">
                        <b>Optimización:</b> 
                      </div>
                      <div class="col col-xs-7">
                        $ <?=$cliente->optimizacion?>
                      </div>
                    </div>
                  </li>
                  <li> 
                    <div class="row">
                      <div class="col col-xs-5 text-right">
                        <b>Total Neto Facturas:</b>
                      </div>
                      <div class="col col-xs-7">
                        $ <?=$cliente->neto?>
                      </div>
                    </div> 
                  </li>
                </ul>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <a href="<?=base_url('gestion/cliente/?token=' . $cliente->id)?>" title="Ver más">Ver Más</a>
              </div>
              <!-- /.box-footer-->
            </div>
            <!-- /.box -->
        </div>
        <?php endforeach; ?>
        <?php endif; ?>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->