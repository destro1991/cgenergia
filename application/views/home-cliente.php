<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Home
        <small>Plataforma de Ahorro</small>
      </h1>
      <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Inicio</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="callout callout-info">
        <h4>¡Bienvenido!</h4>
        <p>
        <?php if(empty($this->data->cliente->message)) : ?>
          Esta es la plataforma de Gestión Energética proporcionada por GC Energía.
        <?php else : ?>
          <?=$this->data->cliente->message;?>
        <?php endif; ?>
        </p>
      </div>

      <?php if(empty($this->data->cliente->suministros)) : ?>

      <div class="callout callout-success">
        <h4>Información</h4>

        <p>
            Aquí se desplegará tu lista de suministros.

            Aún no tienes suministros asociados.
        </p>
      </div>

      <?php endif; ?>

      <div class="row">
        <div class="col col-xs-12">
            <!-- Default box -->
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Estadísticas Generales año <?=$this->data->anno?></h3>
              </div>
              <div class="box-body">
                <div class="row">
                      <div class="col col-xs-12">
                          <ul class="detail">
                              <li class="margin-left"><b>Suministros:</b> <?=$this->data->estadisticas->suministros;?></li>
                              <li class="margin-left"><b>Total en ahorros:</b> $ <?=$this->data->estadisticas->ahorro;?></li>
                              <li class="margin-left"><b>Total en optimización:</b> $ <?=$this->data->estadisticas->optimizacion;?></li>
                              <li class="margin-left"><b>Total neto Facturas:</b> $ <?=$this->data->estadisticas->neto;?></li>
                              <li class="margin-left"><b>Total Facturas:</b> $ <?=$this->data->estadisticas->facturado;?></li>
                          </ul>
                      </div>
                  </div>
                </div>
              <!-- /.box-body -->
              <div class="box-footer">
              </div>
              <!-- /.box-footer-->
            </div>
            <!-- /.box -->
        </div>
        <?php if(!empty($this->data->cliente->suministros)) : ?>
        <?php foreach ($this->data->cliente->suministros as $suministro) : ?>
        <div class="col col-xs-12 col-md-4">
            <!-- Default box -->
            <div class="box box-primary">
              <div class="box-header with-border text-center">
                <div class="row">
                  <div class="col col-xs-12">
                    <a href="<?=base_url('gestion/suministro/?token=' . $suministro->id)?>" class="box-title" title="Ver más"><b><?=$suministro->nombre?> <?=$this->data->anno?></b></a>
                  </div>
                  <div class="col col-xs-12">
                    <span class="text-14"><b>$ <?=$suministro->gestion?></b><span>
                  </div>
                </div>                
              </div>
              <div class="box-body">
                <p></p>
                <ul class="favorite-data">
                    <div class="row">
                      <div class="col col-xs-5 text-right">
                        <b>Ahorro:</b>
                      </div>
                      <div class="col col-xs-7">
                        $ <?=$suministro->ahorro?>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="row">
                      <div class="col col-xs-5 text-right">
                        <b>Optimización:</b> 
                      </div>
                      <div class="col col-xs-7">
                        $ <?=$suministro->optimizacion?>
                      </div>
                    </div>
                  </li>
                  <li> 
                    <div class="row">
                      <div class="col col-xs-5 text-right">
                        <b>Total Neto Facturas:</b>
                      </div>
                      <div class="col col-xs-7">
                        $ <?=$suministro->neto?>
                      </div>
                    </div> 
                  </li>
                </ul>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <a href="<?=base_url('gestion/suministro/?token=' . $suministro->id)?>" title="Ver más">Ver Más</a>
              </div>
              <!-- /.box-footer-->
            </div>
            <!-- /.box -->
        </div>
        <?php endforeach; ?>
        <?php endif; ?>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->