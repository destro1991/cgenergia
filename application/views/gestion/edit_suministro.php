<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Modal -->
<div id="ajax-response" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ajax-response-label" aria-hidden="true">
    <div class="modal-dialog">
        <div id="ajax-content" class="modal-content">
            
        </div>
    </div>
</div>
<!-- Modal -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión
        <small>Modificar Suministro</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="<?=base_url('gestion/suministros');?>"><i class="fa fa-edit"></i> Suministros</a></li>
        <li><a href="<?=base_url('gestion/suministro/?token=' . $this->data->suministro->id);?>"><i class="fa fa-search"></i> Detalle Suminsitro</a></li>
        <li class="active">Editar Suminstro</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <?php if(empty($this->data->suministro) && $this->data->user->id_perfil != '4') :?>
        <div class="callout callout-danger">
            <h4><i class="fa fa-exclamation-circle"></i> Error</h4>

            <p>Imposible recuperar la información del suministro solicitado</p>
        </div>
    <?php else :?>
        <?php if(empty($this->data->suministro->fec_ini_con)) : ?>
            <div id="alert-con" class="callout callout-warning">
                <h4> <i class="fa fa-exclamation-triangle"></i> Alerta!</h4>

                <p>
                    El suministro requiere el ingreso del contrato con GC Energía.<br>
                </p> 
            </div>
        <?php endif; ?>
        <?php if($this->data->suministro->sumi  == 0) : ?>
            <div id="alert-sum" class="callout callout-danger">
                <h4> <i class="fa fa-exclamation-triangle"></i> Alerta!</h4>

                <p>
                    El suministro requiere el ingreso de los datos asociados al contrato de suministro eléctrico vigente.<br>
                </p> 
            </div>
        <?php endif; ?>
      <form id="suministro_edit_form" action="<?=base_url()?>gestion/edit_suministro" method="post">
      <!-- Modal -->
        <div class="modal fade" id="saveSuministroModal" tabindex="-1" role="dialog" aria-labelledby="saveSuministroModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="saveSuministroModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                </div>
                <div class="modal-body">
                    <p>¿Realmente desea modificar el suministro?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button id="save" type="submit" class="btn btn-primary">Sí</button>
                </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
            <div class="col-xs-4 col-sm-8">
                <h3 class="box-title">Editando suministro <b><?=$this->data->suministro->nombre;?></b></h3>
            </div>
            <div class="col-xs-6 col-sm-4 text-right">
                <a href="<?=base_url('gestion/suministro/?token=' . $this->data->suministro->id);?>" class="text-18 margin-left" title="Cancelar">
                    <i class="fa fa-window-close"></i>
                </a>
            </div>
        </div>
        <div class="box-body">
        <div class="row">
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Compañia:
                                <select id="compania" name="compania"  class="form-control" required="required">
                                    <?php foreach($this->data->companias as $compania) :?>
                                        <option value="<?=$compania->id;?>" <?php if($compania->nombre == $this->data->suministro->compania) :?>selected <?php endif;?>><?=$compania->nombre?></option>
                                    <?php endforeach;?>
                                </select>
                            </label>
                        </div>
                        <div class="form-group">
                        <label>
                            Sector Tarifario:
                            <select id="tarifario" name="tarifario" class="form-control" required="required">
                                <?php foreach($this->data->sectores as $sector) :?>
                                    <option value="<?=$sector->codigo;?>" <?php if($sector->nombre == $this->data->suministro->zona) :?>selected <?php endif;?>><?=$sector->nombre?></option>
                                <?php endforeach;?>
                            </select>
                        </label>
                        </div>   
                        
                        <div class="form-group">
                            <label>
                                Nombre:
                                <input name="nombre" type="text" class="form-control" value="<?=$this->data->suministro->nombre?>" placeholder="nombre" required="required">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                N° Cliente:
                                <input name="numcli" type="text" class="form-control" value="<?=$this->data->suministro->num_cli?>" placeholder="Número de cliente" required="required">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label>
                                Dirección
                                <input name="direccion" type="text" class="form-control" value="<?=$this->data->suministro->direccion?>" placeholder="Dirección" required="required">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Región:
                                <select id="regiones" name="region" class="form-control" required="required">
                                    <option value="0">Selecionar...</option>
                                    <!-- Cargadas por Ajax-->
                                </select>
                                <input id="reg" type="hidden" value="<?=$this->data->suministro->region?>">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Ciudad:
                                <select id="ciudades" name="ciudad"  class="form-control" required="required" disabled>
                                    <!-- Cargadas por Ajax-->
                                </select>
                                <input id="cid" type="hidden" value="<?=$this->data->suministro->ciudad?>">
                            </label>
                        </div>

                        <div class="form-group">
                            <label for="observacion">
                                Observación:
                                <textarea name="observacion" class="form-control" cols="24" rows="4"><?=$this->data->suministro->observacion?></textarea>
                            </label>
                        </div>
                    </div>
                    
                    <div class="col col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label>
                                Contacto:
                                <input name="contacto" type="text" class="form-control" value="<?=$this->data->suministro->nom_contacto?>" placeholder="Nombre Contacto">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Cargo:
                                <input name="cargo" type="text" class="form-control" value="<?=$this->data->suministro->cargo_contacto?>" placeholder="Cargo">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Correo:
                                <input name="correo" type="mail" class="form-control"value="<?=$this->data->suministro->email_contacto?>" placeholder="example@mail.com" pattern="^[-\w.%+]{1,64}@(?:[A-Za-z0-9-]{1,63}\.){1,125}[A-Za-z]{2,63}$">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Teléfono:
                                <input name="telefono" type="tel" class="form-control" value="<?=$this->data->suministro->telefono_contacto?>" placeholder="912345678" minlength="9" maxlength="9" pattern="^[0-9]{9}$">
                            </label>
                        </div>
                    </div>
                    
                    <div class="col col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Ter. Cont. Stro.:
                                <input type="date" name="fec_ter_sum" class="form-control" value="<?=$this->data->suministro->fec_ter?>" required="required">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Fec. Lmte. Cmbio. Tarif.:
                                <input type="date" name="fec_cm_tarif" class="form-control" value="<?=$this->data->suministro->fec_mod?>" required="required">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Max. Inv.:
                                <input id="max_inv" name="max_inv" type="text" class="form-control" value="<?=$this->data->suministro->limit_inv?>" placeholder="" pattern="^[0-9]*$">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Año Base:
                                <input id="anno" name="anno_base" type="year" class="form-control" value="<?=$this->data->suministro->anno_base?>" placeholder="YYYY" minlength="4" maxlength="4" pattern="^[0-9]{4}$">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="row">
                <div class="col col-xs-8 col-xs-offset-1 col-sm-offset-2">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="token" value="<?=$this->data->suministro->id;?>">
                                <button id="saveSuministro" type="button" class="btn btn-primary btn-flat">
                                    <i class="fa fa-floppy-o"></i> Guardar
                                </button>
                                <a href="<?=base_url('gestion/suministro/?token=' . $this->data->suministro->id);?>" class="btn btn-default btn-flat margin-left">
                                    <i class="fa fa-window-close"></i> Volver
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      </form>
      <form id="contrato_form" action="<?=base_url()?>gestion/edit_contrato" method="post">
        <!-- Modal -->
        <div class="modal fade" id="saveContratoModal" tabindex="-1" role="dialog" aria-labelledby="saveContratoModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="saveContratoModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                </div>
                <div class="modal-body">
                    <p>¿Realmente desea modificar el contrato?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button id="saveCon" type="submit" class="btn btn-primary">Sí</button>
                </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-xs-4 col-sm-8">
                <h3 class="box-title">Contrato GC Energía</h3>
            </div>
        </div>
        <div class="box-body">
        <div class="row">
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Inicio:
                                <input type="date" name="fec_ini_gc" class="form-control" value="<?=$this->data->suministro->fec_ini_con?>">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Termino:
                                <input type="date" name="fec_ter_gc" class="form-control" value="<?=$this->data->suministro->fec_ter_con?>">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Ini. Ahorro:
                                <input type="date" name="fec_ini_save" class="form-control" value="<?=$this->data->suministro->fec_cambio?>">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                % Contrato:
                                <input type="number" name="pct_con" min="0" max="100" value="<?=$this->data->suministro->pct_contrato?>" class="form-control">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Estado Publicación:
                            </label>
                            <div class="row">
                                <div class="col col-xs-12">
                                    <span>Si</span> <input type="radio" name="estado" value="1" <?php if($this->data->suministro->publicacion == 1) :?> checked="checked" <?php endif;?>>
                                    <span>No</span> <input type="radio" name="estado" value="0" <?php if($this->data->suministro->publicacion != 1) :?> checked="checked" <?php endif;?>>
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Observación:
                                <textarea name="observacion3" class="form-control" cols="24" rows="4"><?=$this->data->suministro->obs_con?></textarea>
                            </label>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col col-xs-8 col-xs-offset-1 col-sm-offset-2">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="tcon" value="<?=$this->data->suministro->id;?>">
                                <input type="hidden" name="con_mod" value="<?=$this->data->suministro->con_mod;?>">
                                <button id="saveContrato" type="button" class="btn btn-primary btn-flat">
                                    <i class="fa fa-floppy-o"></i> Guardar
                                </button>
                                <a href="<?=base_url('gestion/suministro/?token=' . $this->data->suministro->id);?>" class="btn btn-default btn-flat margin-left">
                                    <i class="fa fa-window-close"></i> Volver
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      </form>
       <!-- Modal -->
       <div class="modal fade" id="deleteAhorroModal" tabindex="-1" role="dialog" aria-labelledby="deleteAhorroModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="deleteAhorroModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                </div>
                <div class="modal-body">
                    <p>¿Realmente desea eliminar el contrato de ahorro seleccionado?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button id="deleteAhorro" type="button" class="btn btn-primary">Sí</button>
                </div>
            </div>
        </div>
      </div>
      <!-- Modal -->
        <!-- Modal -->
        <div class="modal fade" id="editAhorroModal" tabindex="-1" role="dialog" aria-labelledby="editAhorroModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title" id="editAhorroModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                    </div>
                    <div class="modal-body">
                        <p>¿Realmente desea modificar el contrato de ahorro seleccionado?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button id="editAhorro" type="button" class="btn btn-primary">Sí</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
      <!-- Default box -->
      <div class="box box-success">
        <div class="box-header with-border">
            <div class="col-xs-4 col-sm-8">
                <h3 class="box-title">Contrato Ahorro</h3>
            </div>
            <div class="col-xs-6 col-sm-4 text-right">
                <a id="addaho" href="javascript:void(0);" data-token="<?=$this->data->suministro->id?>" class="text-18 margin-left"  title="Añadir contrato ahorro">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="box-body">
        <div class="row">
        <?php if(!empty($this->data->suministro->ahorro)) :?>
            <div id="cona" class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
            <?php $h = 0; foreach ($this->data->suministro->ahorro as $ahorro) : $h++;?>
            <form id="aho-<?=$h?>" action="<?=base_url('gestion/edit_ahorro')?>" method="post">
               
                <div class="row">
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Inicio:
                                <input type="date" name="fec_ini_con_save" class="form-control" value="<?=$ahorro->fec_ini?>" disabled>
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Termino:
                                <input type="date" name="fec_ter_con_save" class="form-control" value="<?=$ahorro->fec_ter?>" disabled>
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>
                                Porcentaje:
                                <input type="number" name="pct_con_save" min="0" max="100" value="<?=$ahorro->pct?>" class="form-control" disabled>
                            </label>
                            
                            <a href="javascript:void(0);" data-tar="aho-<?=$h?>"  title="Modificar" class="margin-left edit">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="javascript:void(0);" data-tar="aho-<?=$h?>" title="Eliminar" class="margin-left delete-ahorro">
                                <i class="fa fa-trash"></i>
                            </a>

                            <a href="javascript:void(0);" data-tar="aho-<?=$h?>"  title="Guardar" class="margin-left hidden eaho">
                                <i class="fa fa-save"></i>
                            </a>
                            <a href="javascript:void(0);" data-tar="aho-<?=$h?>" title="Cancelar" class="margin-left hidden cancel">
                                <i class="fa fa-times"></i>
                            </a>
                            <input type="hidden" name="eaho" value="<?=$ahorro->id;?>">
                        </div>
                    </div>
                </div>
                
            </form>
            <?php endforeach;?>
            </div>

            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
            <form id="ahorro_form" action="<?=base_url('gestion/add_ahorro');?>" method="post">
            <!-- Modal -->
            <div class="modal fade" id="addAhorroModal" tabindex="-1" role="dialog" aria-labelledby="addAhorroModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                            <h5 class="modal-title" id="addAhorroModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                        </div>
                        <div class="modal-body">
                            <p>¿Realmente desea agregar un nuevo contrato de ahorro?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                            <button id="saveAhorro" type="submit" class="btn btn-primary">Sí</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
                <div id="aho-box" class="row hidden">
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Inicio:
                                <input type="date" name="fec_ini_con_save" class="form-control" value="">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Termino:
                                <input type="date" name="fec_ter_con_save" class="form-control" value="">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>
                                Porcentaje:
                                <input type="number" name="pct_con_save" min="0" max="100" value="0" class="form-control">
                            </label>
                            <a id="adahos" href="javascript:void(0);" title="Guardar" class="margin-left">
                                <i class="fa fa-save"></i>
                            </a>
                            <a id="adahoc" href="javascript:void(0);" title="Cancelar" class="margin-left">
                                <i class="fa fa-times"></i>
                            </a>
                            <input type="hidden" name="adaho" value="<?=$this->data->suministro->id?>">
                        </div>
                    </div>
                </div>
            </form>
            </div>
        <?php else :?>
            <div class="col col-xs-12">
                <p><em>(No existen elementos)</em></p>
            </div>
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
            <form id="ahorro_form" action="<?=base_url('gestion/add_ahorro');?>" method="post">
            <!-- Modal -->
            <div class="modal fade" id="addAhorroModal" tabindex="-1" role="dialog" aria-labelledby="addAhorroModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                            <h5 class="modal-title" id="addAhorroModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                        </div>
                        <div class="modal-body">
                            <p>¿Realmente desea agregar un nuevo contrato de ahorro?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                            <button id="saveAhorro" type="submit" class="btn btn-primary">Sí</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
                <div id="aho-box" class="row hidden">
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Inicio:
                                <input type="date" name="fec_ini_con_save" class="form-control" value="">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Termino:
                                <input type="date" name="fec_ter_con_save" class="form-control" value="">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>
                                Porcentaje:
                                <input type="number" name="pct_con_save" min="0" max="100" value="0" class="form-control">
                            </label>
                            <a id="adahos" href="javascript:void(0);" title="Guardar" class="margin-left">
                                <i class="fa fa-save"></i>
                            </a>
                            <a id="adahoc" href="javascript:void(0);" title="Cancelar" class="margin-left">
                                <i class="fa fa-times"></i>
                            </a>
                            <input type="hidden" name="adaho" value="<?=$this->data->suministro->id?>">
                        </div>
                    </div>
                </div>
            </form>
            </div>
        <?php endif;?>
        </div>
        </div>
        <div class="box-footer">
          
        </div>
      </div>
        <!-- Modal -->
        <div class="modal fade" id="deleteOptimizacionModal" tabindex="-1" role="dialog" aria-labelledby="deleteOptimizacionModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title" id="deleteAhorroModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                    </div>
                    <div class="modal-body">
                        <p>¿Realmente desea eliminar el contrato de optimización seleccionado?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button id="deleteOptimizacion" type="button" class="btn btn-primary">Sí</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <!-- Modal -->
        <div class="modal fade" id="editOptimizacionModal" tabindex="-1" role="dialog" aria-labelledby="editOptimizacionModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title" id="editAhorroModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                    </div>
                    <div class="modal-body">
                        <p>¿Realmente desea modificar el contrato de optimización seleccionado?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button id="editOptimizacion" type="button" class="btn btn-primary">Sí</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
      <div class="box box-warning">
        <div class="box-header with-border">
            <div class="col-xs-4 col-sm-8">
                <h3 class="box-title">Contrato Optimización</h3>
            </div>
            <div class="col-xs-6 col-sm-4 text-right">
                <a id="addopt" href="javascript:void(0);" data-token="<?=$this->data->suministro->id?>" class="text-18 margin-left" title="Añadir contrato optimización">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="box-body">
        <div class="row">
        <?php if(!empty($this->data->suministro->optimizacion)) :?>
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
            <?php $o = 0; foreach ($this->data->suministro->optimizacion as $optimizacion) : $o++;?>
            <form id="opt-<?=$o?>" action="<?=base_url('gestion/edit_optimizacion')?>" method="post">
                <div class="row">
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Inicio:
                                <input type="date" name="fec_ini_con_opt" class="form-control" value="<?=$optimizacion->fec_ini?>" disabled>
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Termino:
                                <input type="date" name="fec_ter_con_opt" class="form-control" value="<?=$optimizacion->fec_ter?>" disabled>
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>
                                Porcentaje:
                                <input type="number" name="pct_con_opt" min="0" max="100" class="form-control" value="<?=$optimizacion->pct?>" disabled>
                            </label>
                            <a href="javascript:void(0);" data-tar="opt-<?=$o?>"  title="Modificar" class="margin-left edit">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="javascript:void(0);" data-tar="opt-<?=$o?>" title="Eliminar" class="margin-left delete-optimizacion">
                                <i class="fa fa-trash"></i>
                            </a>

                            <a href="javascript:void(0);" data-tar="opt-<?=$o?>"  title="Guardar" class="margin-left hidden eopt">
                                <i class="fa fa-save"></i>
                            </a>
                            <a href="javascript:void(0);" data-tar="opt-<?=$o?>" title="Cancelar" class="margin-left hidden cancel">
                                <i class="fa fa-times"></i>
                            </a>
                            <input type="hidden" name="eopt" value="<?=$optimizacion->id;?>">
                        </div>
                    </div>

                </div>
            </form>
            <?php endforeach;?>
            </div>
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
            <form id="optimizacion_form" action="<?=base_url('gestion/add_optimizacion');?>" method="post">
                <!-- Modal -->
                <div class="modal fade" id="addOptimizacionModal" tabindex="-1" role="dialog" aria-labelledby="addOptimizacionModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                                <h5 class="modal-title" id="addOptimizacionModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                            </div>
                            <div class="modal-body">
                                <p>¿Realmente desea agregar un nuevo contrato de optimización?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                <button id="saveOptimizacion" type="submit" class="btn btn-primary">Sí</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div id="opt-box" class="row hidden">
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Inicio:
                                <input type="date" name="fec_ini_con_opt" class="form-control" value="">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Termino:
                                <input type="date" name="fec_ter_con_opt" class="form-control" value="">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>
                                Porcentaje:
                                <input type="number" name="pct_con_opt" min="0" max="100" value="0" class="form-control">
                            </label>
                            <a id="adopts" href="javascript:void(0);" title="Guardar" class="margin-left">
                                <i class="fa fa-save"></i>
                            </a>
                            <a id="adoptc" href="javascript:void(0);" title="Cancelar" class="margin-left">
                                <i class="fa fa-times"></i>
                            </a>
                            <input type="hidden" name="adopt" value="<?=$this->data->suministro->id?>">
                        </div>
                    </div>
                </div>
            </form>
            </div>
        <?php else :?>
            <div class="col col-xs-12">
                <p><em>(No existen elementos)</em></p>
            </div>
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
            <form id="optimizacion_form" action="<?=base_url('gestion/add_optimizacion');?>" method="post">
                <!-- Modal -->
                <div class="modal fade" id="addOptimizacionModal" tabindex="-1" role="dialog" aria-labelledby="addOptimizacionModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                                <h5 class="modal-title" id="addOptimizacionModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                            </div>
                            <div class="modal-body">
                                <p>¿Realmente desea agregar un nuevo contrato de optimización?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                <button id="saveOptimizacion" type="submit" class="btn btn-primary">Sí</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div id="opt-box" class="row hidden">
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Inicio:
                                <input type="date" name="fec_ini_con_opt" class="form-control" value="">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Termino:
                                <input type="date" name="fec_ter_con_opt" class="form-control" value="">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>
                                Porcentaje:
                                <input type="number" name="pct_con_opt" min="0" max="100" value="0" class="form-control">
                            </label>
                            <a id="adopts" href="javascript:void(0);" title="Guardar" class="margin-left">
                                <i class="fa fa-save"></i>
                            </a>
                            <a id="adoptc" href="javascript:void(0);" title="Cancelar" class="margin-left">
                                <i class="fa fa-times"></i>
                            </a>
                            <input type="hidden" name="adopt" value="<?=$this->data->suministro->id?>">
                        </div>
                    </div>
                </div>
            </form>
            </div>
        <?php endif;?>
        </div>
        </div>
        <div class="box-footer">
          
        </div>
      </div>
        <!-- Modal -->
        <div class="modal fade" id="editConSumModal" tabindex="-1" role="dialog" aria-labelledby="editConSumModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title" id="editConSumModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                    </div>
                    <div class="modal-body">
                        <p>¿Realmente desea modificar el contrato de suministro seleccionado?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button id="editConSum" type="button" class="btn btn-primary">Sí</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
      <!-- Default box -->
      <div class="box box-danger">
        <div class="box-header with-border">
            <div class="col-xs-4 col-sm-8">
                <h3 class="box-title">Contrato de Suministro</h3>
            </div>
            <div class="col-xs-6 col-sm-4 text-right">
                <a id="addconsum" href="javascript:void(0);" data-token="<?=$this->data->suministro->id?>" class="text-18 margin-left"  title="Añadir contrato ahorro">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="box-body">
        <div class="row">
        <?php if(!empty($this->data->suministro->periodos)) :?>
            <div id="consumi" class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
            <?php $x = 0; foreach ($this->data->suministro->periodos as $periodo) : $x++;?>
            <form id="cosu-<?=$x?>" action="<?=base_url('gestion/edit_contrato_suministro')?>" method="post">
               
                <div class="row">
                    <div class="col col-xs-9">
                        <div class="form-group">
                            <label>
                                PERIODO <?=$x?>
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-3 text-right text-18">
                        <a href="javascript:void(0);" data-tar="cosu-<?=$x?>"  title="Modificar" class="margin-left edit">
                            <i class="fa fa-edit"></i>
                        </a>

                        <a href="javascript:void(0);" data-tar="cosu-<?=$x?>"  title="Guardar" class="margin-left hidden econsu">
                            <i class="fa fa-save"></i>
                        </a>

                        <a href="javascript:void(0);" data-tar="cosu-<?=$x?>" title="Cancelar" class="margin-left hidden cancel">
                            <i class="fa fa-times"></i>
                        </a>
                        <input type="hidden" name="ecosu" value="<?=$periodo->id;?>">
                        <input type="hidden" name="ecosusu" value="<?=$periodo->id_suministro;?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Inicio:
                                <input type="date" name="fec_ini_con_sum" class="form-control" value="<?=$periodo->fec_ini?>" disabled>
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Termino:
                                <input type="date" name="fec_ter_con_sum" class="form-control" value="<?=$periodo->fec_ter?>" disabled>
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Tarifa Contratada:
                                <select id="tipo_tarifa" name="tipo_tarifa"  class="form-control" required="required" disabled>
                                    <?php foreach($this->data->tarifas as $tarifa) :?>
                                        <option value="<?=$tarifa->id;?>" <?php if($tarifa->id == $periodo->id_tarifa) :?>selected <?php endif;?>><?=$tarifa->nombre?></option>
                                    <?php endforeach;?>
                                </select>
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Potencia Conectada:
                                <input type="text" name="potencia" class="form-control" value="<?=$periodo->potencia?>" disabled>
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3 col-md-offset-3">
                        <div class="form-group">
                            <label>
                                [AC] Número Medidor:
                                <input type="text" name="ac_num" class="form-control" value="<?=$periodo->ac_num?>" disabled>
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                [AC] Propiedad Medidor:
                                <select id="ac_prop" name="ac_prop"  class="form-control" required="required" disabled>
                                    <option value="COMPAÑIA" <?php if($periodo->ac_prop == "COMPAÑIA") : ?>selected <?php endif;?>> Compañia</option>
                                    <option value="DUEÑO" <?php if($periodo->ac_prop == "DUEÑO") : ?>selected <?php endif;?>>Dueño</option>
                                </select>
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                [AC] Cosntante Medidor:
                                <input type="text" name="ac_cons" class="form-control" value="<?=$periodo->ac_cons?>" disabled>
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-3">
                        <div class="form-group">
                            <label>
                                [RE] Número Medidor:
                                <input type="text" name="re_num" class="form-control" value="<?=$periodo->re_num?>" disabled>
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                [RE] Propiedad Medidor:
                                <select id="re_prop" name="re_prop"  class="form-control" required="required" disabled>
                                    <option value="COMPAÑIA" <?php if($periodo->re_prop == "COMPAÑIA") : ?>selected <?php endif;?>> Compañia</option>
                                    <option value="DUEÑO" <?php if($periodo->re_prop == "DUEÑO") : ?>selected <?php endif;?>>Dueño</option>
                                </select>
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                [RE] Cosntante Medidor:
                                <input type="text" name="re_cons" class="form-control" value="<?=$periodo->re_cons?>" disabled>
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-3">
                        <div class="form-group">
                            <label>
                                [FP] Número Medidor:
                                <input type="text" name="fp_num" class="form-control" value="<?=$periodo->fp_num?>" disabled>
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                [FP] Propiedad Medidor:
                                <select id="fp_prop" name="fp_prop"  class="form-control" required="required" disabled>
                                    <option value="COMPAÑIA" <?php if($periodo->fp_prop == "COMPAÑIA") : ?>selected <?php endif;?>> Compañia</option>
                                    <option value="DUEÑO" <?php if($periodo->fp_prop == "DUEÑO") : ?>selected <?php endif;?>>Dueño</option>
                                </select>
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                [FP] Cosntante Medidor:
                                <input type="text" name="fp_cons" class="form-control" value="<?=$periodo->fp_cons?>" disabled>
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-3">
                        <div class="form-group">
                            <label>
                                [EP] Número Medidor:
                                <input type="text" name="ep_num" class="form-control" value="<?=$periodo->ep_num?>" disabled>
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                [EP] Propiedad Medidor:
                                <select id="ep_prop" name="ep_prop"  class="form-control" required="required" disabled>
                                    <option value="COMPAÑIA" <?php if($periodo->ep_prop == "COMPAÑIA") : ?>selected <?php endif;?>> Compañia</option>
                                    <option value="DUEÑO" <?php if($periodo->ep_prop == "DUEÑO") : ?>selected <?php endif;?>>Dueño</option>
                                </select>
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                [EP] Constante Medidor:
                                <input type="text" name="ep_cons" value="<?=$periodo->ep_cons?>" class="form-control" disabled>
                            </label>                            
                        </div>
                    </div>
                </div>

                <div class="box-footer">
          
                </div>
                
            </form>
            <?php endforeach;?>
            
            </div>
        <?php else :?>
            <div class="col col-xs-12">
                <p><em>(No existen elementos)</em></p>
            </div>
            
            
        <?php endif;?>
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
            <form id="consu_form" action="<?=base_url('gestion/add_contrato_suministro');?>" method="post">
            <!-- Modal -->
            <div class="modal fade" id="addConSumModal" tabindex="-1" role="dialog" aria-labelledby="addConSumModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                            <h5 class="modal-title" id="addConSumModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                        </div>
                        <div class="modal-body">
                            <p>¿Realmente desea agregar un nuevo contrato de suministro?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                            <button id="saveConSum" type="submit" class="btn btn-primary">Sí</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
                <div id="consu-box" class="row hidden">
                    <div class="row">
                        <div class="col col-xs-9">
                            <div class="form-group">
                                <label>
                                    NUEVO PERIODO
                                </label>
                            </div>
                        </div>
                        <div class="col col-xs-3 text-right text-18">
                            <a id="adconsus" href="javascript:void(0);" title="Guardar" class="margin-left text-18">
                                <i class="fa fa-save"></i>
                            </a>
                            <a id="adconsuc" href="javascript:void(0);" title="Cancelar" class="margin-left text-18">
                                <i class="fa fa-times"></i>
                            </a>
                            <input type="hidden" name="adconsu" value="<?=$this->data->suministro->id?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>
                                    Fec. Inicio:
                                    <input type="date" name="fec_ini_con_sum" class="form-control" required="required">
                                </label>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>
                                    Fec. Termino:
                                    <input type="date" name="fec_ter_con_sum" class="form-control" required="required">
                                </label>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>
                                    Tarifa Contratada:
                                    <select id="tipo_tarifa" name="tipo_tarifa"  class="form-control" required="required">
                                        <?php foreach($this->data->tarifas as $tarifa) :?>
                                            <option value="<?=$tarifa->id;?>"><?=$tarifa->nombre?></option>
                                        <?php endforeach;?>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>
                                    Potencia Conectada:
                                    <input type="text" name="potencia" class="form-control number" value="0" required="required">
                                </label>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-3 col-md-offset-3">
                            <div class="form-group">
                                <label>
                                    [AC] Número Medidor:
                                    <input type="text" name="ac_num" class="form-control" required="required">
                                </label>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>
                                    [AC] Propiedad Medidor:
                                    <select id="ac_prop" name="ac_prop"  class="form-control" required="required">
                                        <option value="COMPAÑIA" selected> Compañia</option>
                                        <option value="DUEÑO">Dueño</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>
                                    [AC] Cosntante Medidor:
                                    <input type="text" name="ac_cons" class="form-control number" value="0" required="required">
                                </label>
                            </div>
                        </div>

                        <div class="col col-xs-12 col-md-3 col-md-offset-3">
                            <div class="form-group">
                                <label>
                                    [RE] Número Medidor:
                                    <input type="text" name="re_num" class="form-control" required="required">
                                </label>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>
                                    [RE] Propiedad Medidor:
                                    <select id="re_prop" name="re_prop"  class="form-control" required="required">
                                        <option value="COMPAÑIA"selected> Compañia</option>
                                        <option value="DUEÑO">Dueño</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>
                                    [RE] Cosntante Medidor:
                                    <input type="text" name="re_cons" class="form-control number" value="0" required="required">
                                </label>
                            </div>
                        </div>

                        <div class="col col-xs-12 col-md-3 col-md-offset-3">
                            <div class="form-group">
                                <label>
                                    [FP] Número Medidor:
                                    <input type="text" name="fp_num" class="form-control" required="required">
                                </label>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>
                                    [FP] Propiedad Medidor:
                                    <select id="fp_prop" name="fp_prop"  class="form-control" required="required">
                                        <option value="COMPAÑIA" selected> Compañia</option>
                                        <option value="DUEÑO">Dueño</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>
                                    [FP] Cosntante Medidor:
                                    <input type="text" name="fp_cons" class="form-control number " value="0" required="required">
                                </label>
                            </div>
                        </div>

                        <div class="col col-xs-12 col-md-3 col-md-offset-3">
                            <div class="form-group">
                                <label>
                                    [EP] Número Medidor:
                                    <input type="text" name="ep_num" class="form-control" required="required">
                                </label>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>
                                    [EP] Propiedad Medidor:
                                    <select id="ep_prop" name="ep_prop"  class="form-control" required="required">
                                        <option value="COMPAÑIA" selected> Compañia</option>
                                        <option value="DUEÑO">Dueño</option>
                                    </select>
                                </label>
                            </div>
                        </div>

                        <div class="col col-xs-12 col-md-3">
                            <div class="form-group">
                                <label>
                                    [EP] Constante Medidor:
                                    <input type="text" name="ep_cons" value="0" class="form-control number" required="required">
                                </label>                            
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
        </div>
        <div class="box-footer">
            <div class="col-xs-12 text-right">
                <a id="addconsum2" href="javascript:void(0);" data-token="<?=$this->data->suministro->id?>" class="text-18 margin-left"  title="Añadir contrato ahorro">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
      </div>
    <?php endif;?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->