<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión
        <small>Nuevo Suministro</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="<?=base_url('gestion/suministros');?>"><i class="fa fa-edit"></i> Suministros</a></li>
        <li class="active">Nuevo</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="callout callout-info">
        <h4>Dato útil!</h4>

        <p>
            Después de ingresar la información general del suminstro, podrás añadir los contratos asociados al mismo.
        </p>
      </div>

      <form id="suministro_form" action="<?=base_url()?>gestion/agregar_suministro" method="post">
      <!-- Modal -->
        <div class="modal fade" id="saveSuministroModal" tabindex="-1" role="dialog" aria-labelledby="saveSuministroModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="saveSuministroModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                </div>
                <div class="modal-body">
                    <p>¿Realmente desea ingresar un nuevo suministro?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button id="save" type="submit" class="btn btn-primary">Sí</button>
                </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
         
            <div class="col-xs-4 col-sm-8">
                <h3 class="box-title">Nuevo suministro para <b><?=$this->data->cliente->nombre;?></b></h3>
            </div>
            <div class="col-xs-6 col-sm-4 text-right">
                <a href="javascript:void(0);" onclick="window.history.back();" class="btn btn-default btn-flat margin-left">
                    <i class="fa fa-window-close"></i> Volver
                </a>
            </div>
        </div>
        <div class="box-body">
        <div class="row">
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Compañia:
                                <select id="compania" name="compania"  class="form-control" required="required">
                                    <option value="0">Selecionar...</option>
                                    <?php foreach($this->data->companias as $compania) :?>
                                        <option value="<?=$compania->id;?>"><?=$compania->nombre?></option>
                                    <?php endforeach;?>
                                </select>
                            </label>
                        </div>
                        <div class="form-group">
                        <label>
                            Sector Tarifario:
                            <select id="tarifario" name="tarifario" class="form-control" required="required" disabled>
                                <!-- cargados por Ajax-->
                            </select>
                        </label>
                        </div>   
                        
                        <div class="form-group">
                            <label>
                                Nombre:
                                <input name="nombre" type="text" class="form-control" placeholder="nombre" required="required">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                N° Cliente:
                                <input name="numcli" type="text" class="form-control" placeholder="Número de cliente" required="required">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label>
                                Dirección
                                <input name="direccion" type="text" class="form-control" placeholder="Dirección" required="required">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Región:
                                <select id="regiones" name="region" class="form-control" required="required">
                                    <option value="0">Selecionar...</option>
                                    <!-- Cargadas por Ajax-->
                                </select>
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Ciudad:
                                <select id="ciudades" name="ciudad"  class="form-control" required="required" disabled>
                                    <!-- Cargadas por Ajax-->
                                </select>
                            </label>
                        </div>

                        <div class="form-group">
                            <label for="observacion">
                                Observación:
                                <textarea name="observacion" class="form-control" cols="24" rows="4"></textarea>
                            </label>
                        </div>
                    </div>
                    
                    <div class="col col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label>
                                Contacto:
                                <input name="contacto" type="text" class="form-control" placeholder="Nombre Contacto">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Cargo:
                                <input name="cargo" type="text" class="form-control" placeholder="Cargo">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Correo:
                                <input name="correo" type="mail" class="form-control" placeholder="example@mail.com" pattern="^[-\w.%+]{1,64}@(?:[A-Za-z0-9-]{1,63}\.){1,125}[A-Za-z]{2,63}$">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Teléfono:
                                <input name="telefono" type="tel" class="form-control" placeholder="912345678" minlength="9" maxlength="9" pattern="^[0-9]{9}$">
                            </label>
                        </div>
                    </div>
                    
                    <div class="col col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label>
                                Fec. Ter. Cont. Stro.:
                                <input type="date" name="fec_ter_sum" class="form-control" required="required">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Fec. Lmte. Cmbio. Tarif.:
                                <input type="date" name="fec_cm_tarif" class="form-control" required="required">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Max. Inv.:
                                <input id="max_inv" name="max_inv" type="text" class="form-control" value="0" placeholder="" pattern="^[0-9]*$">
                            </label>
                        </div>

                        <div class="form-group">
                            <label>
                                Año Base:
                                <input id="anno" name="anno_base" type="year" class="form-control" value="<?=$this->data->anno;?>" placeholder="YYYY" minlength="4" maxlength="4" pattern="^[0-9]{4}$">
                            </label>
                        </div>
                    </div>
                </div>
            </div>            

            <div class="col col-xs-8 col-xs-offset-1 col-sm-offset-2">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group text-right">
                            <input type="hidden" name="token" value="<?=$this->data->token;?>">
                            <button id="saveSuministro" type="button" class="btn btn-primary btn-flat">
                                <i class="fa fa-floppy-o"></i> Guardar
                            </button>
                            <a href="javascript:void(0);" onclick="window.history.back();" class="btn btn-default btn-flat margin-left">
                                <i class="fa fa-window-close"></i> Volver
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="ajax-respond" class="col-xs-12">
                        
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>        
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  