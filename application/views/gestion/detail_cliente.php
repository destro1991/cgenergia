<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Modal -->
<div class="modal fade" id="addFavoritoModal" tabindex="-1" role="dialog" aria-labelledby="addFavoritoModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="addFavoritoModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
        </div>
        <div class="modal-body">
            <p>¿Realmente desea añadir al cliente a favoritos?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button id="addFavorito" type="button" class="btn btn-primary" data-url="<?=base_url('favoritos/add')?>">Sí</button>
        </div>
    </div>
</div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="deleteFavoritoModal" tabindex="-1" role="dialog" aria-labelledby="deleteFavoritoModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="deleteFavoritoModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
        </div>
        <div class="modal-body">
            <p>¿Realmente desea eliminar al cliente de sus favoritos?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button id="deleteFavorito" type="button" class="btn btn-primary" data-url="<?=base_url('favoritos/delete')?>">Sí</button>
        </div>
    </div>
</div>
</div>
<!-- Modal -->
<!-- Modal -->
<div id="ajax-response" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ajax-response-label" aria-hidden="true">
    <div class="modal-dialog">
        <div id="ajax-content" class="modal-content">
            
        </div>
    </div>
</div>
<!-- Modal -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión
        <small>Detalle Cliente</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="<?=base_url()?>gestion/clientes"><i class="fa fa-address-book-o"></i> Clientes</a></li>
        <li class="active">Ficha Cliente</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <?php if (!empty($this->data->cliente) && empty($this->data->suministros)) : ?>
        <div class="callout callout-info">
            <h4>Dato útil!</h4>

            <p>
                El cliente no cuenta con suministros, puedes agregarlos desde el siguiente enlace.
            </p>

            <a href="<?=base_url('gestion/nuevo_suministro/?token=' . $this->data->cliente->id)?>" class="btn btn-success btn-flat" title="Agregar Suministro">
                <i class="fa fa-plus"></i> Nuevo Suministro
            </a>
        </div>
    <?php endif; ?>
    <?php if (!empty($this->data->cliente) && $this->data->user->id_perfil != '4') : ?>
        <?php if (empty($this->data->cliente->id_user)) : ?>
            <div class="callout callout-info">
                <h4>Dato útil!</h4>

                <p>
                    El cliente no cuenta con un usuario registrado en el sistema, puedes agregarlos desde el siguiente enlace.
                </p>

                <a href="<?=base_url('cuentas/new_cliente/?token=' . $this->data->cliente->id)?>" class="btn btn-success btn-flat" title="Añadir Usuario">
                    <i class="fa fa-user-plus"></i> Crear Usuario
                </a>
            </div>
        <?php endif;?>
    <?php endif;?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <div class="row">
            <div class="col-xs-10">
                <h3 class="box-title">Cliente</h3>
            </div>
            <?php if (!empty($this->data->cliente) && $this->data->user->id_perfil != '4') : ?>
            <div class="col-xs-2 text-right">
            <?php if (!empty($this->data->cliente->id_user)) : ?>
                <a href="<?=base_url('cuentas/view_cliente/?token=' . $this->data->cliente->id_user)?>" class="margin-left text-18" title="Ver usuario">
                    <i class="fa fa-user"></i>
                </a>
            <?php else : ?>
                <a href="<?=base_url('cuentas/new_cliente/?token=' . $this->data->cliente->id)?>" class="margin-left text-18" title="Añadir usuario">
                    <i class="fa fa-user-plus"></i>
                </a>
            <?php endif;?>
                <a href="<?=base_url('clientes/view/?token=' . $this->data->cliente->id)?>" class="margin-left text-18" title="Modificar">
                    <i class="fa fa-edit"></i>
                </a>

                <?php if($this->data->cliente->fav == 0) : ?>

                    <a href="javascript:void(0)" class="fav-add margin-left text-18" data-target="<?=$this->data->cliente->id?>" title="Agregar a Favoritos">
                        <i class="fa fa-star-o"></i>
                    </a>

                <?php else : ?>

                    <a href="javascript:void(0)" class="fav-remove margin-left text-18" data-target="<?=$this->data->cliente->id?>" title="Quitar de Favoritos">
                        <i class="fa fa-star"></i>
                    </a>

                <?php endif; ?>
            </div>
            <?php endif; ?>
          </div>
        </div>
        <div class="box-body">
            <?php if (!empty($this->data->cliente)) : ?>
                <div class="row">
                    <div class="col col-xs-12">
                        <ul class="detail">
                            <li><b>Cliente:</b> <?=$this->data->cliente->nombre;?></li>
                            <li class="margin-left"><b>Rut.:</b> <?=$this->data->cliente->rut;?></li>
                            <li class="margin-left"><b>Contacto:</b> <?=$this->data->cliente->contacto;?></li>
                            <li class="margin-left"><b>Correo:</b> <?=$this->data->cliente->correo;?></li>
                            <li class="margin-left"><b>Telefono:</b> <?=$this->data->cliente->num_tel;?></li>

                            <li class="margin-left"><b>Suministros:</b> <?=$this->data->cliente->total;?></li>

                        </ul>
                    </div>
                </div>
            <?php else: ?>
                <h4><em> (No se encontró el elemento)</em></h4>
            <?php endif; ?>  
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border pointer">
            <div class="col-xs-3" data-toggle="collapse" data-target="#collapse" aria-expanded="false" aria-controls="collapse">
                <h3 class="box-title">Suministros Asociados</h3>
            </div>
            <div class="col-xs-4">
                <form id="filtrarSumCli" action="<?=base_url('gestion/cliente')?>" method="get">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Buscar..." <?php if(!empty($_GET['q'])) : ?> value="<?=$_GET['q'];?>" <?php endif; ?>>
                        <input type="hidden" name="token" value="<?=$this->data->cliente->id?>">
                        <span class="input-group-btn">
                            <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
            <?php if($this->data->filtrado):?>
            <div class="col-xs-2">
                <a href="<?=base_url('gestion/cliente/?token=' . $this->data->cliente->id)?>" type="submit" class="btn btn-default btn-flat margin-left">
                    <i class="fa fa-plus"></i> Mostrar Todos
                </a>
            </div>
            <?php endif;?>
            <div class="col-xs-2">
                <?php if($this->data->export) : ?>
                    <a href="<?=base_url('export/suministros/?&token=' . $this->data->cliente->id)?>" class="btn btn-default btn-flat margin-left">
                        <i class="fa fa-file-excel-o"></i> Exportar
                    </a>
                <?php endif; ?>
            </div>
            <?php if (!empty($this->data->cliente) && $this->data->user->id_perfil != '4') : ?>
            <div class="col-xs-1 <? if($this->data->filtrado) : ?> col-sm-1 <?else :?> col-sm-3 <?endif;?> text-right">
                <a href="<?=base_url('gestion/nuevo_suministro/?token=' . $this->data->cliente->id)?>" class="margin-left text-18" title="Agregar suministro">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
            <?php endif; ?>
        </div>
        <div id="collapse" class="box-body collapse in">
            <?php if (!empty($this->data->suministros)) : ?>
            <table id="example3" class="table table-hover">
                <thead>
                    <tr>
                        <th>Suministro</th>
                        <th>Nro. Cliente</th>
                        <th>Ult. Mes</th>
                        <th>Neto</th>
                        <th>Total a Pagar</th>
                        <th>Ahorro</th>
                        <th></th>                    
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->data->suministros as $suministro) : ?>

                        <tr <?php if($suministro->etapa == 0) : ?> class="bg-hover bg-red" <?php endif;?>>
                            <td><?=$suministro->nombre;?></td>
                            <td><?=$suministro->num_cli;?></td>
                            <td><?=$suministro->last_month;?></td>
                            <td><?=$suministro->last_neto;?></td>
                            <td><?=$suministro->last_fact;?></td>
                            <td><?=$suministro->ahorro;?></td>
                            <td style="word-wrap: break-word;">
                                <a href="<?=base_url('gestion/suministro/?token=' . $suministro->id);?>" title="Ver">
                                    <i class="fa fa-folder-open"></i>
                                </a>
                                
                                <a href="<?=base_url('gestion/nueva_factura/?token=' . $suministro->id);?>" title="Subir factura" class="margin-left">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Suministro</th>
                        <th>Nro. Cliente</th>
                        <th>Ult. Mes</th>
                        <th>Ult. Mes</th>
                        <th>Neto</th>
                        <th>Total a Pagar</th>
                        <th>Ahorro</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
            <?php else: ?>
                <h4><em> (No se encontraron suministros asociados)</em></h4>
            <?php endif; ?>  
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  