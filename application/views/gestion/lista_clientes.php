<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Modal -->
<div class="modal fade" id="addFavoritoModal" tabindex="-1" role="dialog" aria-labelledby="addFavoritoModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="addFavoritoModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
        </div>
        <div class="modal-body">
            <p>¿Realmente desea añadir al cliente a favoritos?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button id="addFavorito" type="button" class="btn btn-primary" data-url="<?=base_url('favoritos/add')?>">Sí</button>
        </div>
    </div>
</div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="deleteFavoritoModal" tabindex="-1" role="dialog" aria-labelledby="deleteFavoritoModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="deleteFavoritoModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
        </div>
        <div class="modal-body">
            <p>¿Realmente desea eliminar al cliente de sus favoritos?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button id="deleteFavorito" type="button" class="btn btn-primary" data-url="<?=base_url('favoritos/delete')?>">Sí</button>
        </div>
    </div>
</div>
</div>
<!-- Modal -->
<!-- Modal -->
<div id="ajax-response" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ajax-response-label" aria-hidden="true">
    <div class="modal-dialog">
        <div id="ajax-content" class="modal-content">
            
        </div>
    </div>
</div>
<!-- Modal -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión
        <small>Listado de clientes</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active"> <i class="fa fa-address-book-o"></i> Clientes</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-xs-2 col-sm-4">
                    <h3 class="box-title">Clientes</h3>
                </div>
                <?php if($this->data->user->id_perfil != '4') :?>
                <div class="col-xs-2">
                    <a href="<?=base_url('clientes/new_client')?>" type="submit" class="btn btn-primary btn-flat margin-left">
                        <i class="fa fa-plus"></i> Nuevo Cliente
                    </a>
                </div>
                <?php endif;?>
                <div class="col-xs-6 col-sm-4">
                    <form id="filtrarCli" action="<?=base_url('gestion/clientes')?>" method="get">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Buscar..." <?php if(!empty($_GET['q'])) : ?> value="<?=$_GET['q'];?>" <?php endif; ?>>
                            <span class="input-group-btn">
                                <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
                <?php if($this->data->filtrado):?>
                <div class="col-xs-2">
                    <a href="<?=base_url('gestion/clientes')?>" type="submit" class="btn btn-default btn-flat margin-left">
                        <i class="fa fa-window-close"></i> Mostrar Todos
                    </a>
                </div>
                <?php endif;?>
            </div>
        </div>
        <div class="box-body">
            
            <?php if (!empty($this->data->clientes)) : ?>
            <table id="example2" class="table table-hover">
                <thead>
                    <tr>
                        <th>Cliente</th>
                        <th>Rut</th>
                        <th>Correo</th>                    
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->data->clientes as $cliente) : ?>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col col-xs-1">
                                    <?php if($cliente->fav == 0) : ?>

                                        <a href="javascript:void(0)" class="fav-add" data-target="<?=$cliente->id?>" title="Agregar a Favoritos">
                                            <i class="fa fa-star-o"></i>
                                        </a>

                                    <?php else : ?>

                                        <a href="javascript:void(0)" class="fav-remove" data-target="<?=$cliente->id?>" title="Quitar de Favoritos">
                                            <i class="fa fa-star"></i>
                                        </a>

                                    <?php endif; ?>
                                    </div>

                                    <div class="col col-xs-6 col-sm-8 col-md-9">
                                        <span><?=$cliente->nombre;?></span>
                                    </div>
                                </div>
                            </td>
                            <td><?=$cliente->rut;?></td>
                            <td style="word-wrap: break-word;">
                                <div class="row">
                                    <div class="col col-xs-8 col-sm-9">
                                        <span><?=$cliente->correo;?></span>
                                    </div>

                                    <div class="col col-xs-4 col-sm-3">
                                        <a href="<?php site_url()?>cliente/?token=<?=$cliente->id;?>" title="Ver">
                                            <i class="fa fa-folder-open"></i>
                                        </a>

                                        <?php if($this->data->user->id_perfil != '4') :?>
                                        
                                        <a href="<?php base_url();?>nuevo_suministro/?token=<?=$cliente->id;?>" title="Agregar suministro" class="margin-left">
                                            <i class="fa fa-plus"></i>
                                        </a>

                                        <?php endif;?>
                                    </div>

                                </div>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Cliente</th>
                        <th>Rut</th>
                        <th>Correo</th>
                    </tr>
                </tfoot>
            </table>
            <?php else: ?>
                <h4><em> (No se encontraron resultados) </em></h4>
            <?php endif; ?>  
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  