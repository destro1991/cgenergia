<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión
        <small>Editando Factura</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="<?=base_url('gestion/suministros');?>"><i class="fa fa-edit"></i> Suministros</a></li>
        <li><a href="<?=base_url('gestion/suministro/?token=' . $this->data->factura->id_suministro);?>"><i class="fa fa-search"></i> Detalle Suminsitro</a></li>
        <li class="active">Factura</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <?php if(!empty($this->data->factura)) : ?>
      <form id="factura_form" name="facturaForm" action="<?=base_url('gestion/edit_factura')?>" method="post" enctype="multipart/form-data">
        <!-- Modal -->
        <div class="modal fade" id="saveFacturaModal" tabindex="-1" role="dialog" aria-labelledby="saveFacturaModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
                  <h5 class="modal-title" id="saveFacturaModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
              </div>
              <div class="modal-body">
                  <p>¿Realmente desea actualizar la factura?</p>
                  <span class="important-text">Total neto: </span><span id="conf-neto" class="important-text"><?=number_format($this->data->factura->tot_neto_libre, 0, ',', '.')?></span>
                  <br>
                  <span class="important-text">Total a pagar: </span><span id="conf-tot" class="important-text"><?=number_format($this->data->factura->total_factura_libre, 0, ',', '.')?></span>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                  <button id="save" type="submit" class="btn btn-primary">Sí</button>
              </div>
              </div>
          </div>
        </div>
        <!-- Modal -->
      <div class="box">
        <div class="box-header with-border">
          <div class="col-xs-4 col-sm-8">
            <h3 class="box-title">Factura de <b><?=$this->data->factura->suministro;?></b></h3>
          </div>
          <div class="col-xs-6 col-sm-4 text-right">
            <button id="saveFactura-n" type="button" class="btn btn-primary btn-flat">
              <i class="fa fa-floppy-o"></i> Guardar
            </button>
            <a href="<?=base_url('gestion/suministro/?token=' . $this->data->factura->id_suministro);?>" class="btn btn-default btn-flat margin-left">
              <i class="fa fa-window-close"></i> Volver
            </a>
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1 text-12">
              <div class="row">
                <fieldset>
                  <legend>General</legend>
                <?php if($this->data->factura->factura_mode == 'simple') :?>
                  <div class="col col-xs-12 col-md-3 documento_simple">
                        <div class="form-group">
                            <label class="">
                                Nro.:
                                <input name="nro_factura_libre" type="text" class="form-control" value="<?=$this->data->factura->num_fact?>" placeholder="987654321" required="required">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3 documento_simple">
                        <div class="form-group">
                            <label class="">
                                Fecha Documento:
                                <input type="date" name="fec_doc_libre" class="form-control" value="<?=$this->data->factura->fec_doc?>" required="required">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3 documento_simple">
                        <div class="form-group">
                            <label class="">
                                Desde:
                                <input type="date" name="fec_desde_libre" class="form-control" value="<?=$this->data->factura->fec_ini?>" required="required">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3 documento_simple">
                        <div class="form-group">
                            <label class="">
                                Hasta:
                                <input type="date" name="fec_hasta_libre" class="form-control" value="<?=$this->data->factura->fec_ter?>" required="required">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 documento_simple">
                        <div class="form-group">
                            <label>
                                Prox. Lectura:
                                <input type="date" name="fec_prox_lec_libre" class="form-control" value="<?=$this->data->factura->fec_prox?>" required="required">
                            </label>
                        </div>
                    </div>
                <?php endif;?>
                  <div class="col col-xs-12 col-md-3">
                      <div class="form-group">
                          <label class="">
                            Proyectado:
                              <input name="valor_produccion" type="text" class="form-control" value="<?=$this->data->factura->produccion?>">
                          </label>
                      </div>
                  </div>
                  <div class="col col-xs-12 col-md-3">
                    <div class="form-group">
                      <label>
                        Mes:
                        <input type="month" name="mes" class="form-control" value="<?=$this->data->factura->mes?>" required="required">
                      </label>
                    </div>
                  </div>
                  <div class="col col-xs-12 col-md-3">
                    <div class="form-group">
                      <label>
                        SUMINISTRO EN ETAPA <?=$this->data->factura->etapa;?>
                      </label>
                      <br>
                      <label class="<?php if($this->data->factura->etapa == 1) : ?> oculto <?php endif;?>">
                        Tarifa aplicada:
                        <input id="tarB" type="text" name="tarB" class="form-control" value="<?=$this->data->factura->tarB?>">
                      </label>
                    </div>
                  </div>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- Generacion -->
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <div class="col-xs-12">
            <h3 class="box-title">Generación</h3>
          </div>
        </div>
        <div class="box-body">
        <div class="row">
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1 text-12">
                <?php if($this->data->factura->factura_mode == 'doble') :?>
                <div class="row resumen_doble">
                <fieldset>
                    <legend>Documento</legend>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Nro.:
                                <input name="nro_factura_gen" type="text" class="form-control" value="<?=$this->data->factura->num_fact_gen?>" required="required">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Fecha Documento:
                                <input type="date" name="fec_doc_gen" class="form-control" value="<?=$this->data->factura->fec_doc?>" required="required">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Desde:
                                <input type="date" name="fec_desde_gen" class="form-control" value="<?=$this->data->factura->fec_ini?>" required="required">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Hasta:
                                <input type="date" name="fec_hasta_gen" class="form-control" value="<?=$this->data->factura->fec_ter?>" required="required">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Prox. Lectura:
                                <input type="date" name="fec_prox_lec_gen" class="form-control" value="<?=$this->data->factura->fec_prox?>" required="required">
                            </label>
                        </div>
                    </div>
                </fieldset>
                </div>
                <?php endif;?>

                <div class="row">
                <fieldset>
                    <legend>Lectura</legend>
                    <div class="col col-xs-12 col-md-2">
                        <br>
                            <label class="">
                                ACTIVA
                            </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Lec. Anterior:
                                <input name="ac_lec_ant_gen" type="text" class="form-control" value="<?=$this->data->factura->lec1_activa_gen?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Lec. Actual:
                                <input type="text" name="ac_lec_act_gen" class="form-control" value="<?=$this->data->factura->lec2_activa_gen?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Consumo:
                                <input id="ac_consumo_gen" type="text" name="ac_consumo_gen" class="form-control" value="<?=$this->data->factura->consumo_ac_gen?>" pattern="^[0-9]+(\.[0-9]{1,})?$" data-target="ene_consumo_gen">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2">
                        <br>
                            <label class="">
                                REACTIVA
                            </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Lec. Anterior:
                                <input name="re_lec_ant_gen" type="text" class="form-control" value="<?=$this->data->factura->lec1_reactiva_gen?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Lec. Actual:
                                <input type="text" name="re_lec_act_gen" class="form-control" value="<?=$this->data->factura->lec2_reactiva_gen?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Consumo:
                                <input type="text" name="re_consumo_gen" class="form-control" value="<?=$this->data->factura->consumo_re_gen?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <br>
                        <label class="">
                            POTENCIA EN HORA PUNTA
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Consumo:
                                <input id="ep_consumo_gen" type="text" name="ep_consumo_gen" class="form-control" value="<?=$this->data->factura->ep_pot?>" pattern="^[0-9]+(\.[0-9]{1,})?$" data-target="deman_ep_lec_gen">
                            </label>
                        </div>
                    </div>
                    
                </fieldset>
                </div>

                <div class="row">
                <fieldset>
                    <legend>Detalle de la Cuenta</legend>

                    <div class="col col-xs-12 col-md-2 col-md-offset-3">
                        <div class="form-group">
                            <br>
                            <label>
                                ENERGÍA
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Ene. Fact.:
                                <input id="ene_consumo_gen" type="text" name="ene_consumo_gen" class="form-control" value="<?=$this->data->factura->consumo_ac_gen?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Precio:
                                <input id="ene_precio_gen" type="text" name="ene_precio_gen" class="form-control sumar_libre" value="<?=$this->data->factura->energia_gen?>" data-sum="sumar_libre" data-result="neto_ene_gen" onBlur = "setTotalServ('sumar_libre','neto_ene_gen')">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2">
                        <br>
                            <label class="">
                                POTENCIA HP
                            </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Pot. Leída:
                                <input id="deman_ep_lec_gen" name="deman_ep_lec_gen" type="text" class="form-control" disabled value="<?=$this->data->factura->ep_pot_gen?>">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Pot. Fact.:
                                <input type="text" name="deman_ep_fact_gen" class="form-control" value="<?=$this->data->factura->ep_deman_fact_gen?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Precio:
                                <input type="text" name="deman_ep_prec_gen" class="form-control sumar_libre" value="<?=$this->data->factura->ep_deman_prec_gen?>" data-sum="sumar_libre" data-result="neto_ene_gen" onBlur = "setTotalServ('sumar_libre','neto_ene_gen')">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            CARGO L.G.S.E
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_lgse" class="form-control sumar_libre" value="<?=$this->data->factura->cargo_lgse?>" data-sum="sumar_libre" data-result="neto_ene_gen" onBlur = "setTotalServ('sumar_libre','neto_ene_gen')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            CARGO MIN. TEC.
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_min_tec" class="form-control sumar_libre" value="<?=$this->data->factura->cargo_tecnico?>" data-sum="sumar_libre" data-result="neto_ene_gen" onBlur = "setTotalServ('sumar_libre','neto_ene_gen')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <div class="form-group">
                            <label class="">
                              CARGO SERV. COM.
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3"> 
                        <div class="form-group">
                            <input name="cargo_serv_com" type="text" class="form-control sumar_libre" value="<?=$this->data->factura->cargo_ser_com?>" data-sum="sumar_libre" data-result="neto_ene_gen" onBlur = "setTotalServ('sumar_libre','neto_ene_gen')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <div class="form-group">
                            <label class="">
                              CARGO ARMO. TAR.
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3"> 
                        <div class="form-group">
                            <input name="cargo_armonizacion" type="text" class="form-control sumar_libre" value="<?=$this->data->factura->cargo_armonizacion?>" data-sum="sumar_libre" data-result="neto_ene_gen" onBlur = "setTotalServ('sumar_libre','neto_ene_gen')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            CARGO SERV. PÚBLICO
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_serv_pub" class="form-control sumar_libre" value="<?=$this->data->factura->serv_pub?>" data-sum="sumar_libre" data-result="neto_ene_gen" onBlur = "setTotalServ('sumar_libre','neto_ene_gen')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            INTERESES
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_interes_gen" class="form-control sumar_libre" value="<?=$this->data->factura->interes_gen?>" data-sum="sumar_libre" data-result="neto_ene_gen" onBlur = "setTotalServ('sumar_libre','neto_ene_gen')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            OTROS RECARGOS
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_otros_gen" class="form-control sumar_libre" value="<?=$this->data->factura->otros_cargos_gen?>" data-sum="sumar_libre" data-result="neto_ene_gen" onBlur = "setTotalServ('sumar_libre','neto_ene_gen')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            OTROS DESCUENTOS
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_desc_gen" class="form-control sumar_libre" value="<?=$this->data->factura->otros_desc_gen?>" data-sum="sumar_libre" data-result="neto_ene_gen" onBlur = "setTotalServ('sumar_libre','neto_ene_gen')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            TOTAL GENERACIÓN
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input id="neto_ene_gen" type="text" name="neto_ene_gen" class="form-control" value="<?=$this->data->factura->total_serv_gen?>" readonly>
                        </div>
                    </div>

                </fieldset>
                </div>
                <?php if($this->data->factura->factura_mode == 'doble') :?>
                <div class="row resumen_doble">
                    <fieldset>
                    <legend>Generación</legend>
                    <div class="col col-xs-12 col-md-2">
                        <div class="form-group">
                            <label class="">
                                Total Exento:
                                <input id="monto_exento_gen" type="text" name="monto_exento_gen" class="form-control" value="<?=$this->data->factura->tot_exento_gen?>" onblur="setTotalNetoAndFact('neto_ene_gen', 'monto_exento_gen', 'total_neto_gen', 'total_fact_gen')">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-2">
                        <div class="form-group">
                            <label class="">
                                Total Neto:
                                <input id="total_neto_gen" type="text" name="total_neto_gen" class="form-control" readonly value="<?=$this->data->factura->tot_neto_gen?>">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2">
                        <div class="form-group">
                            <label class="">
                                Total Factura:
                                <input id="total_fact_gen" type="text" name="total_fact_gen" class="form-control" disabled value="<?=$this->data->factura->total_factura_gen?>">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2">
                        <div class="form-group">
                            <label class="">
                                Otros:
                                <input id="cargo_otr_gen" type="text" name="cargo_otr_gen" class="form-control" value="<?=$this->data->factura->otros_gen?>" onblur="setTotalPagar('total_fact_gen', 'cargo_otr_gen', 'saldo_ant_gen', 'total_pagar_gen')">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2">
                        <div class="form-group">
                            <label class="">
                                Saldo Anterior:
                                <input id="saldo_ant_gen" type="text" name="saldo_ant_gen" class="form-control" value="<?=$this->data->factura->saldo_anterior_gen?>" onblur="setTotalPagar('total_fact_gen', 'cargo_otr_gen', 'saldo_ant_gen', 'total_pagar_gen')">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2">
                        <div class="form-group">
                            <label class="">
                                Total a Pagar:
                                <input id="total_pagar_gen" type="text" name="total_pagar_gen" class="form-control" value="<?=$this->data->factura->tot_fac_gen?>" readonly>
                            </label>
                        </div>
                    </div>
                    </fieldset>
                </div>
                <?php endif;?>
                <?php if($this->data->factura->factura_mode == 'doble') :?>
                <div class="row resumen_doble">
                    <fieldset>
                    <legend>Documento</legend>

                    <div class="col col-xs-12 col-md-5">
                        <div class="form-group">
                            <label class="extendido">
                                Archivo:
                                <input type="file" id="facturafile_gen" name="facturafile_gen" class="facturafile form-control" value="<?=$this->data->factura->archivo_gen?>">
                                <input type="hidden" name="file_name_gen" value="<?=$this->data->factura->archivo_gen?>">
                            </label>
                        </div>
                    </div>
                    </fieldset>
                </div>
                <?php endif;?>
            </div>
        </div>        
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      <!-- Distribucion -->
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <div class="col-xs-12">
            <h3 class="box-title">Distribución</h3>
          </div>
        </div>
        <div class="box-body">
        <div class="row">
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1 text-12">
            <?php if($this->data->factura->factura_mode == 'doble') :?>
                <div class="row resumen_doble">
                <fieldset>
                    <legend>Documento</legend>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Nro.:
                                <input name="nro_factura" type="text" class="form-control" value="<?=$this->data->factura->num_fact?>" required="required">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Fecha Documento:
                                <input type="date" name="fec_doc" class="form-control" value="<?=$this->data->factura->fec_doc?>" required="required">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Desde:
                                <input type="date" name="fec_desde" class="form-control" value="<?=$this->data->factura->fec_ini?>" required="required">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Hasta:
                                <input type="date" name="fec_hasta" class="form-control" value="<?=$this->data->factura->fec_ter?>" required="required">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Prox. Lectura:
                                <input type="date" name="fec_prox_lec" class="form-control" value="<?=$this->data->factura->fec_prox?>" required="required">
                            </label>
                        </div>
                    </div>
                </fieldset>
                </div>
            <?php endif;?>

                <div class="row">
                <fieldset>
                    <legend>Lectura</legend>
                    <div class="col col-xs-12 col-md-2">
                        <br>
                            <label class="">
                                ACTIVA
                            </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Lec. Anterior:
                                <input name="ac_lec_ant" type="text" class="form-control" value="<?=$this->data->factura->lec1_activa?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Lec. Actual:
                                <input type="text" name="ac_lec_act" class="form-control" value="<?=$this->data->factura->lec2_activa?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Consumo:
                                <input id="ac_consumo" type="text" name="ac_consumo" class="form-control" value="<?=$this->data->factura->consumo_ac?>" pattern="^[0-9]+(\.[0-9]{1,})?$" data-target="ene_consumo">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2">
                        <br>
                            <label class="">
                                REACTIVA
                            </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Lec. Anterior:
                                <input name="re_lec_ant" type="text" class="form-control" value="<?=$this->data->factura->lec1_reactiva?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Lec. Actual:
                                <input type="text" name="re_lec_act" class="form-control" value="<?=$this->data->factura->lec2_reactiva?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Consumo:
                                <input type="text" name="re_consumo" class="form-control" value="<?=$this->data->factura->consumo_re?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <br>
                        <label class="">
                            POTENCIA FUERA DE PUNTA
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Consumo:
                                <input id="fp_consumo" type="text" name="fp_consumo" class="form-control" value="<?=$this->data->factura->fp_pot?>" pattern="^[0-9]+(\.[0-9]{1,})?$" data-target="deman_fp_lec">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <br>
                        <label class="">
                            POTENCIA EN HORA PUNTA
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Consumo:
                                <input id="ep_consumo" type="text" name="ep_consumo" class="form-control" value="<?=$this->data->factura->ep_pot?>" pattern="^[0-9]+(\.[0-9]{1,})?$" data-target="deman_ep_lec">
                            </label>
                        </div>
                    </div>
                    
                </fieldset>
                </div>

                <div class="row">
                <fieldset>
                    <legend>Detalle de la Cuenta</legend>
                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            CARGO FIJO
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_fijo" class="form-control sumar" value="<?=$this->data->factura->cargo_fijo?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <div class="form-group">
                            <label class="">
                                DESPACHO POSTAL
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3"> 
                        <div class="form-group">
                            <input name="cargo_postal" type="text" class="form-control sumar" value="<?=$this->data->factura->cargo_despacho?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2 col-md-offset-3">
                        <div class="form-group">
                            <br>
                            <label>
                                ENERGÍA
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Ene. Fact.:
                                <input id="ene_consumo" type="text" name="ene_consumo" class="form-control" value="<?=$this->data->factura->consumo_ac?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Precio:
                                <input id="ene_precio" type="text" name="ene_precio" class="form-control sumar" value="<?=$this->data->factura->energia?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2">
                        <br>
                            <label class="">
                                POTENCIA FP
                            </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Pot. Leída:
                                <input id="deman_fp_lec" name="deman_fp_lec" type="text" class="form-control" disabled value="<?=$this->data->factura->fp_pot?>">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Pot. Fact.:
                                <input type="text" name="deman_fp_fact" class="form-control" value="<?=$this->data->factura->fp_deman_fact?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Precio:
                                <input type="text" name="deman_fp_prec" class="form-control sumar" value="<?=$this->data->factura->fp_deman_prec?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2">
                        <br>
                            <label class="">
                                POTENCIA HP
                            </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Pot. Leída:
                                <input id="deman_ep_lec" name="deman_ep_lec" type="text" class="form-control" disabled value="<?=$this->data->factura->ep_pot?>">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Pot. Fact.:
                                <input type="text" name="deman_ep_fact" class="form-control" value="<?=$this->data->factura->ep_deman_fact?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Precio:
                                <input type="text" name="deman_ep_prec" class="form-control sumar" value="<?=$this->data->factura->ep_deman_prec?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2 col-md-offset-3">
                        <div class="form-group">
                            <br>
                            <label>
                                COMPRA POTENCIA
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                CP. Fact.:
                                <input id="cp_consumo" type="text" name="cp_consumo" class="form-control" value="<?=$this->data->factura->cp_consumo?>" pattern="^[0-9]+(\.[0-9]{1,})?$">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="">
                                Precio:
                                <input id="cp_precio" type="text" name="cp_precio" class="form-control sumar" value="<?=$this->data->factura->cp_precio?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            FACTOR POTENCIA
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="factor_potencia" class="form-control sumar" value="<?=$this->data->factura->recargo_factor?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            ARRIENDO
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_arriendo" class="form-control sumar" value="<?=$this->data->factura->arriendo?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            TRANSMISION
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_trans" class="form-control sumar" value="<?=$this->data->factura->transmision?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            INTERESES
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_interes" class="form-control sumar" value="<?=$this->data->factura->interes?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            DOMICILIO
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_domi" class="form-control sumar" value="<?=$this->data->factura->domicilio?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            CORTE Y REPOSICIÓN
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_corte" class="form-control sumar" value="<?=$this->data->factura->corte?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            OTROS RECARGOS
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_otros" class="form-control sumar" value="<?=$this->data->factura->otros_cargos?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            RELIQUIDACIONES
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_reliq" class="form-control sumar" value="<?=$this->data->factura->reliquidaciones?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                        <label class="">
                            OTROS DESCUENTOS
                        </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input type="text" name="cargo_desc" class="form-control sumar" value="<?=$this->data->factura->otros_desc?>" data-sum="sumar" data-result="neto_ene" onBlur = "setTotalServ('sumar','neto_ene')">
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3 col-md-offset-5">
                            <label class="">
                                TOTAL DISTRIBUCIÓN
                            </label>
                    </div>
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <input id="neto_ene" type="text" name="neto_ene" class="form-control" value="<?=$this->data->factura->total_serv?>" readonly>
                        </div>
                    </div>

                </fieldset>
                </div>
                <?php if($this->data->factura->factura_mode == 'doble') :?>
                <div class="row resumen_doble">
                    <fieldset>
                    <legend>Distribución</legend>
                    <div class="col col-xs-12 col-md-2">
                        <div class="form-group">
                            <label class="">
                                Total Exento:
                                <input id="monto_exento" type="text" name="monto_exento" class="form-control" value="<?=$this->data->factura->tot_exento?>" onblur="setTotalNetoAndFact('neto_ene', 'monto_exento', 'total_neto', 'total_fact')">
                            </label>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-2">
                        <div class="form-group">
                            <label class="">
                                Total Neto:
                                <input id="total_neto" type="text" name="total_neto" class="form-control" readonly value="<?=$this->data->factura->tot_neto?>">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2">
                        <div class="form-group">
                            <label class="">
                                Total Factura:
                                <input id="total_fact" type="text" name="total_fact" class="form-control" disabled value="<?=$this->data->factura->total_factura?>">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2">
                        <div class="form-group">
                            <label class="">
                                Otros:
                                <input id="cargo_otr" type="text" name="cargo_otr" class="form-control" value="<?=$this->data->factura->otros?>" onblur="setTotalPagar('total_fact', 'cargo_otr', 'saldo_ant', 'total_pagar')">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2">
                        <div class="form-group">
                            <label class="">
                                Saldo Anterior:
                                <input id="saldo_ant" type="text" name="saldo_ant" class="form-control" value="<?=$this->data->factura->saldo_anterior?>" onblur="setTotalPagar('total_fact', 'cargo_otr', 'saldo_ant', 'total_pagar')">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-2">
                        <div class="form-group">
                            <label class="">
                                Total a Pagar:
                                <input id="total_pagar" type="text" name="total_pagar" class="form-control" value="<?=$this->data->factura->tot_fac?>" readonly>
                            </label>
                        </div>
                    </div>
                    </fieldset>
                </div>
                <?php endif;?>
                
                <?php if($this->data->factura->factura_mode == 'doble') :?>
                <div class="row resumen_doble">
                    <fieldset>
                    <legend>Documento</legend>

                    <div class="col col-xs-12 col-md-5">
                        <div class="form-group">
                            <label class="extendido">
                                Archivo:
                                <input type="file" id="facturafile" name="facturafile" class="facturafile form-control" value="<?=$this->data->factura->archivo?>">
                                <input type="hidden" name="file_name" value="<?=$this->data->factura->archivo?>">
                            </label>
                        </div>
                    </div>
                    </fieldset>
                </div>
                <?php endif;?>
            </div>
        </div>        
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      <!-- Contabilidad -->
      <div class="box">
        <!-- <div class="box-header">

        </div> -->
        <div class="box-body">
          <div class="row">
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1 text-12">
              <div class="row">
                <fieldset>
                <legend>Resumen</legend>
                <div class="col col-xs-12 col-md-2">
                    <div class="form-group">
                        <label class="">
                            Total Exento:
                            <input id="monto_exento_libre" type="text" name="monto_exento_libre" class="form-control" value="<?=$this->data->factura->tot_exento_libre?>" onblur="setTotalNetoAndFact('neto_ene', 'monto_exento_libre', 'total_neto_libre', 'total_fact_libre')">
                        </label>
                    </div>
                </div>
                <div class="col col-xs-12 col-md-2">
                    <div class="form-group">
                        <label class="">
                            Total Neto:
                            <input id="total_neto_libre" type="text" name="total_neto_libre" class="form-control" disabled value="<?=$this->data->factura->tot_neto_libre?>">
                        </label>
                    </div>
                </div>

                <div class="col col-xs-12 col-md-2">
                    <div class="form-group">
                        <label class="">
                            Total Factura:
                            <input id="total_fact_libre" type="text" name="total_fact_libre" class="form-control" disabled value="<?=$this->data->factura->total_factura_libre?>">
                        </label>
                    </div>
                </div>

                <div class="col col-xs-12 col-md-2">
                    <div class="form-group">
                        <label class="">
                            Otros:
                            <input id="cargo_otr_libre" type="text" name="cargo_otr_libre" class="form-control" value="<?=$this->data->factura->otros_libre?>" onblur="setTotalPagar('total_fact_libre', 'cargo_otr_libre', 'saldo_ant_libre', 'total_pagar_libre')">
                        </label>
                    </div>
                </div>

                <div class="col col-xs-12 col-md-2">
                    <div class="form-group">
                        <label class="">
                            Saldo Anterior:
                            <input id="saldo_ant_libre" type="text" name="saldo_ant_libre" class="form-control" value="<?=$this->data->factura->saldo_anterior_libre?>" onblur="setTotalPagar('total_fact_libre', 'cargo_otr_libre', 'saldo_ant_libre', 'total_pagar_libre')">
                        </label>
                    </div>
                </div>

                <div class="col col-xs-12 col-md-2">
                    <div class="form-group">
                        <label class="">
                            Total a Pagar:
                            <input id="total_pagar_libre" type="text" name="total_pagar_libre" class="form-control" disabled value="<?=$this->data->factura->tot_fac_libre?>">
                        </label>
                    </div>
                </div>
                </fieldset>
              </div>

              <?php if($this->data->factura->factura_mode == 'simple') :?>
              <div class="row documento_simple">
                <fieldset>
                <legend>Documento</legend>

                <div class="col col-xs-12 col-md-5">
                    <div class="form-group">
                        <label class="extendido">
                            Archivo:
                            <input type="file" id="facturafile_libre" name="facturafile_libre" class="facturafile form-control" value="<?=$this->data->factura->archivo?>">
                            <input type="hidden" name="file_name" value="">
                        </label>
                    </div>
                </div>
                </fieldset>
              </div>
              <?php endif;?>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      <!-- Tools -->
      <div class="box">
        <!-- <div class="box-header">

        </div> -->
        <div class="box-body">
          <div class="row">
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1 text-12">
              <div class="row">
                <fieldset>
                  <legend>Análisis</legend>

                  <div class="col col-xs-12 col-md-2">
                      <div class="form-group">
                          <label>
                              Estado:
                          </label>
                          <div class="row">
                              <div class="col col-xs-12">
                                  <input type="radio" name="estado" value="1"><span class="margin-right">In</span> 
                                  <input type="radio" name="estado" value="0" checked="checked"><span>Out</span> 
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="col col-xs-12 col-md-2">
                      <div class="form-group">
                          <label class="">
                              Tipo:
                              <select id="tipo_ana" name="tipo_ana"  class="form-control">
                                  <option value="0" selected>Seleccionar....</option>
                                  <?php foreach($this->data->tipo_ana as $tipo) :?>
                                      <option value="<?=$tipo->id;?>"><?=$tipo->nombre?></option>
                                  <?php endforeach;?>
                              </select>
                          </label>
                      </div>
                  </div>

                  <div class="col col-xs-12 col-md-4">
                      <div class="form-group">
                          <label class="extendido">
                              Observación Interna:
                              <input id="obs_in" type="text" name="obs_in" class="form-control">
                          </label>
                      </div>
                  </div>

                  <div class="col col-xs-12 col-md-4">
                      <div class="form-group">
                          <label class="extendido">
                              Observación Cliente:
                              <input id="obs_cli" type="text" name="obs_cli" class="form-control">
                          </label>
                      </div>
                  </div>
                </fieldset>
              </div>

              <div class="row">
                <fieldset>
                  <legend>Alerta</legend>

                  <div class="col col-xs-12 col-md-2">
                      <div class="form-group">
                          <label>
                              Estado:
                          </label>
                          <div class="row">
                              <div class="col col-xs-12">
                                  <input type="radio" name="alerta" value="1"><span class="margin-right">Activa</span>
                                  <input type="radio" name="alerta" value="0" checked="checked"><span>Inactiva</span>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="col col-xs-12 col-md-4">
                      <div class="form-group">
                          <label class="extendido">
                              Observación:
                              <input id="obs_alert" type="text" name="obs_alert" class="form-control">
                          </label>
                      </div>
                  </div>

                </fieldset>
              </div>
            </div>
            <div class="col col-xs-8 col-xs-offset-1 col-sm-offset-2">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group text-right">
                            <input type="hidden" name="token" value="<?=$this->data->factura->id?>">
                            <input type="hidden" name="tarifa" value="<?=$this->data->factura->tipo_tarifa?>">
                            <input type="hidden" name="facturaMode" value="<?=$this->data->factura->factura_mode?>">
                            <button id="saveFactura" type="button" class="btn btn-primary btn-flat">
                                <i class="fa fa-floppy-o"></i> Guardar
                            </button>
                            <a href="<?=base_url('gestion/suministro/?token=' . $this->data->factura->id_suministro);?>" class="btn btn-default btn-flat margin-left">
                                <i class="fa fa-window-close"></i> Volver
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="ajax-respond" class="col-xs-12">
                        
                    </div>
                </div>
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      </form>
    <?php endif;?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  