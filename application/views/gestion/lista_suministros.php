<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión
        <small>Listado de suministros</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active"> <i class="fa fa-edit"></i> Suministros</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-xs-4 col-sm-6">
                    <h3 class="box-title">Suministros</h3>
                </div>
                <div class="col-xs-4">
                    <form id="filtrarSum" action="<?=base_url('gestion/suministros')?>" method="get">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Buscar..." <?php if(!empty($_GET['q'])) : ?> value="<?=$_GET['q'];?>" <?php endif; ?>>
                            <span class="input-group-btn">
                                <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
                <?php if($this->data->filtrado):?>
                <div class="col-xs-2">
                    <a href="<?=base_url('gestion/suministros')?>" type="submit" class="btn btn-default btn-flat margin-left">
                        <i class="fa fa-plus"></i> Mostrar Todos
                    </a>
                </div>
                <?php endif;?>
                <div class="col-xs-2">
                    <?php if($this->data->export) : ?>
                        <a href="<?=base_url('export/suministros/?&token=' . $this->data->cliente->id)?>" class="btn btn-default btn-flat margin-left">
                            <i class="fa fa-file-excel-o"></i> Exportar
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="box-body">
            <?php if (!empty($this->data->suministros)) : ?>
            <table id="example2" class="table table-hover" data-page-length="30">
                <thead>
                    <tr>
                        <th>Suministro</th>
                        <th>Nro. Cliente</th>
                        <th>Ult. Mes</th>
                        <th>Neto</th>
                        <th>Total a Pagar</th>
                        <th>Ahorro</th>
                        <td></td>                    
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->data->suministros as $suministro) : ?>

                        <tr>
                        <td><?=$suministro->nombre;?></td>
                            <td><?=$suministro->num_cli;?></td>
                            <td><?=$suministro->last_month;?></td>
                            <td><?=$suministro->last_neto;?></td>
                            <td><?=$suministro->last_fact;?></td>
                            <td><?=$suministro->ahorro;?></td>
                            <td>
                                <a href="<?=base_url('gestion/suministro/?token=' . $suministro->id)?>" title="Ver">
                                    <i class="fa fa-folder-open"></i>
                                </a>

                                <?php if($this->data->user->id_perfil != '3') : ?>
                                <a href="<?=base_url('gestion/nueva_factura/?token=' . $suministro->id);?>" title="Subir factura" class="margin-left">
                                    <i class="fa fa-plus"></i>

                                </a>
                                <?php endif; ?>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Suministro</th>
                        <th>Nro. Cliente</th>
                        <th>Ult. Mes</th>
                        <th>Neto</th>
                        <th>Total a Pagar</th>
                        <th>Ahorro</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
            <?php else: ?>
                <h4><em> (No se encontraron resultados) </em></h4>
            <?php endif; ?>  
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  