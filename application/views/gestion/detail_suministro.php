<?php
defined('BASEPATH') OR exit('No direct script access allowed');
function cal_colspan(array $props = []) {
  $i = 0;
  foreach($props as $value) {
    if($value == 1) $i++;
  }

  return $i;
}
?>
<?php if($this->data->user->id_perfil != '3') : ?>
<!-- Modal -->
<div class="modal fade" id="saveColumnFacturaModal" tabindex="-1" role="dialog" aria-labelledby="saveColumnFacturaModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="saveColumnFacturaModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
        </div>
        <div class="modal-body">
            <p>¿Realmente desea modificar la vizualización de columnas para las facturas?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-primary saveColumnFactura" data-mdl="saveColumnFacturaModal" data-target="columnas_factura_form">Sí</button>
        </div>
        </div>
    </div>
</div>
<!-- Modal -->

<!-- Modal -->
<div class="modal fade" id="saveColumnFacturaModalLibre" tabindex="-1" role="dialog" aria-labelledby="saveColumnFacturaModalLibreLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="saveColumnFacturaModalLibreLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
        </div>
        <div class="modal-body">
            <p>¿Realmente desea modificar la vizualización de columnas para las facturas?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-primary saveColumnFactura" data-mdl="saveColumnFacturaModalLibre" data-target="columnas_factura_form_libre">Sí</button>
        </div>
        </div>
    </div>
</div>
<!-- Modal -->

<!-- Modal -->
<div class="modal fade" id="saveColumnRatioModal" tabindex="-1" role="dialog" aria-labelledby="saveColumnRatioModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="saveColumnRatioModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
        </div>
        <div class="modal-body">
            <p>¿Realmente desea modificar la vizualización de columnas para los ratios?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-primary saveColumnRatio" data-mdl="saveColumnRatioModal" data-target="columnas_ratio_form">Sí</button>
        </div>
        </div>
    </div>
</div>
<!-- Modal -->

<!-- Modal -->
<div class="modal fade" id="saveColumnRatioModalLibre" tabindex="-1" role="dialog" aria-labelledby="saveColumnRatioModalLibreLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="saveColumnRatioModalLibreLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
        </div>
        <div class="modal-body">
            <p>¿Realmente desea modificar la vizualización de columnas para los ratios?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-primary saveColumnRatio" data-mdl="saveColumnRatioModalLibre" data-target="columnas_ratio_form_libre">Sí</button>
        </div>
        </div>
    </div>
</div>
<!-- Modal -->

<!-- Modal -->
<div class="modal fade" id="deleteFacturaModal" tabindex="-1" role="dialog" aria-labelledby="deleteFacturaModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="deleteFacturaModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
      </div>
      <div class="modal-body">
        <p>¿Realmente desea eliminar la factura seleccionada?</p>
      </div>
      <div class="modal-footer">
        <form id="factura_delete" action="<?=base_url('gestion/delete_factura');?>">
            <input id="token" name="token" type="hidden" value="">
        </form>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button id="deleteFactura" type="button" class="btn btn-primary">Sí</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<?php endif; ?>
<?php if($this->data->user->id_perfil != '3' && $this->data->user->id_perfil != '4') : ?>
<!-- Modal -->
<div class="modal fade" id="addIntervencionModal" tabindex="-1" role="dialog" aria-labelledby="addIntervencionModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="addIntervencionModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
        </div>
        <div class="modal-body">
            <p>¿Realmente desea ingresar la interveción?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button id="saveIntervencion" type="button" class="btn btn-primary">Sí</button>
        </div>
    </div>
</div>
</div>
<!-- Modal -->
<?php endif; ?>
<!-- Modal -->
<div id="ajax-response" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ajax-response-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div id="ajax-content" class="modal-content">
        
    </div>
  </div>
</div>
<!-- Modal -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión
        <small>Detalle Suministro</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="<?=base_url()?>gestion/suministros"><i class="fa fa-address-book-o"></i> Suministros</a></li>
        <li class="active">Detalle Suministro</li>
      </ol>
    </section>

    <!-- Main content --> 
    <section class="content">
    <?php if(!empty($this->data->suministro) && $this->data->user->id_perfil != '3' && $this->data->user->id_perfil != '4') : ?>
        <?php if(empty($this->data->suministro->fec_ini_con)) : ?>
            <div class="callout callout-warning">
                <h4> <i class="fa fa-exclamation-triangle"></i> Alerta!</h4>

                <p>
                    El suministro requiere el ingreso del contrato con GC Energía<br>
                </p>

                <a href="<?=base_url('gestion/suministro_edit/?token=' . $this->data->suministro->id)?>" class="btn btn-primary btn-flat" title="Editar Suministro">
                    <i class="fa fa-edit"></i> Modificar
                </a> 
            </div>
        <?php endif; ?>
        <?php if($this->data->suministro->sumi  == 0) : ?>
            <div id="alert-sum" class="callout callout-danger">
                <h4> <i class="fa fa-exclamation-triangle"></i> Alerta!</h4>

                <p>
                    El suministro requiere el ingreso de los datos asociados al contrato de suministro eléctrico vigente.<br>
                </p>
                
                <a href="<?=base_url('gestion/suministro_edit/?token=' . $this->data->suministro->id)?>" class="btn btn-primary btn-flat" title="Editar Suministro">
                    <i class="fa fa-edit"></i> Modificar
                </a>
            </div>
        <?php endif; ?>
    <?php endif; ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <div class="row">
            <div class="col-xs-10">
                <h3 class="box-title">Suministro</h3>
            </div>
            <?php if (!empty($this->data->suministro) && $this->data->user->id_perfil != '3' && $this->data->user->id_perfil != '4') : ?>
            <div class="col-xs-2 text-right">
                <a href="<?=base_url('gestion/suministro_edit/?token=' . $this->data->suministro->id)?>" class="margin-left text-18" title="Modificar">
                    <i class="fa fa-edit"></i>
                </a>
            </div>
            <?php endif; ?>
          </div>
        </div>
        <div class="box-body">
            <?php if (!empty($this->data->suministro)) : ?>
                <div class="row">
                    <div class="col col-xs-12">
                        <ul class="detail">
                            <li class="margin-left"><b>Cliente:</b> <?=$this->data->suministro->cliente;?></li>
                            <li class="margin-left"><b>Suministro:</b> <?=$this->data->suministro->nombre;?></li>
                            <li class="margin-left"><b>Zona Tarifaria:</b> <?=$this->data->suministro->zona;?></li>
                            <li class="margin-left"><b>Compañia:</b> <?=$this->data->suministro->compania;?></li>
                            <li class="margin-left"><b>Tarifa Actual:</b> <?=$this->data->suministro->tarifa;?></li>
                            <li class="margin-left"><b>Etapa:</b> <?=$this->data->suministro->etapa;?></li>
                            <li class="margin-left"><b>Año Base:</b> <?=$this->data->suministro->anno_base_str;?></li>
                        </ul>
                    </div>
                </div>
            <?php else: ?>
                <h4><em> (No se encontró el elemento)</em></h4>
            <?php endif; ?>  
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      <!-- Default box -->
      <?php if (!empty($this->data->suministro)) : ?>
      <div class="box">
        <div class="box-header pointer" >
          <div class="row">
            <div id="facturas" class="col-xs-8">
                <ul class="pagination no-margin">
                    <li class="paginate_button previus">
                        <a id="ant" href="<?=base_url('gestion/suministro/?y=' . ($this->data->anno - 1) . '&token=' . $this->data->suministro->id);?>"> <i class="fa fa-angle-double-left"></i> </a>
                    </li>
                    <li>
                        <span class="title-box">Facturas año <?=$this->data->anno;?></span>
                    </li>
                    <li class="paginate_button next">
                        <a id="sgt" href="<?=base_url('gestion/suministro/?y=' . ($this->data->anno + 1) . '&token=' . $this->data->suministro->id);?>"><i class="fa fa-angle-double-right"> </i></a>
                    </li>
                </ul> 
            </div>
            <div class="col-xs-4">
                <?php if (!empty($this->data->suministro->facturas) || !empty($this->data->suministro->facturas_libre)) : ?>
                <a href="<?=base_url('export/factura/?y=' . $this->data->anno . '&token=' . $this->data->suministro->id)?>" class="btn btn-default btn-flat margin-left">
                    <i class="fa fa-file-excel-o"></i> Exportar
                </a>
                <?php endif; ?>

                <?php if (!empty($this->data->suministro->fec_ini_con) && $this->data->user->id_perfil != '3' && $this->data->suministro->tarifa != "N/A") : ?>
                <a href="<?=base_url('gestion/nueva_factura/?token=' . $this->data->suministro->id)?>" class="btn btn-primary btn-flat margin-left">
                    <i class="fa fa-plus"></i> Nueva Factura
                </a>
                <?php endif; ?>
            </div>
          </div>
          <div class="box-footer">
          
          </div>
          <!-- /.box-footer-->
        </div>
      </div>
      <!-- Cliente regulado -->
      <?php if (!empty($this->data->suministro->facturas)) : ?>
      <div class="box box-primary">
        <div class="box-header with-border pointer" >
          <div class="row">
            <div id="facturas-r" class="col-xs-12" data-toggle="collapse" data-target="#collapse" aria-expanded="true" aria-controls="collapse">
              <span class="box-title">Cliente Regulado</span>
            </div>
          </div>
        </div>
        <div id="collapse" class="box-body collapse in text-12">
            <table id="example2" class="table table-hover dark text-center">
                <thead>
                    <tr id="factura_columns">
                        <th>Mes</th>

                        <th>Nro. Fact.</th>

                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->factura_3 == 1) :?>
                        <th>Csmo. <br> (kWh)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_4 == 1) :?>
                        <th>Gasto <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_5 == 1) :?>
                        <th>PLFP <br> (kW)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_6 == 1) :?>
                        <th>PFFP <br> (kW)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_7 == 1) :?>
                        <th>PLHP <br> (kW)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_8 == 1) :?>
                        <th>PFHP <br> (kW)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_17 == 1) :?>
                        <th>CPHP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_9 == 1) :?>
                        <th>FP <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_10 == 1) :?>
                        <th>HP <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_18 == 1) :?>
                        <th>CP <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_11 == 1) :?>
                        <th>Req. <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_12 == 1) :?>
                        <th>Arr/tra <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_13 == 1) :?>
                        <th>MFP <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_14 == 1) :?>  
                        <th>OC <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_15 == 1) :?>
                        <th>Neto <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_16 == 1) :?>
                        <th>Total a Pagar <br> ($)</th>
                        <?php endif; ?>
                        <th></th>
                    <?php else : ?> 
                        <form id="columnas_factura_form" action="<?=base_url('gestion/set_columnas_factura');?>">
                            <input type="hidden" name="token" value="<?=$this->data->suministro->id?>">
                                                                      
                        <th>Csmo. <input class="hidden" type="checkbox" name="consumo" value="factura_3" <?php if($this->data->suministro->factura_3 == 1) :?>checked<?php endif; ?>> <br> (kWh)</th>
                        
                        <th>Gasto. <input class="hidden" type="checkbox" name="gasto" value="factura_4" <?php if($this->data->suministro->factura_4 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                                                
                        <th>PLFP <input class="hidden" type="checkbox" name="pl_fp" value="factura_5" <?php if($this->data->suministro->factura_5 == 1) :?>checked<?php endif; ?>> <br> (kW)</th>
                        
                        <th>PFFP <input class="hidden" type="checkbox" name="pf_fp" value="factura_6" <?php if($this->data->suministro->factura_6 == 1) :?>checked<?php endif; ?>> <br> (kW)</th>
                       
                        <th>PLHP <input class="hidden" type="checkbox" name="pl_ep" value="factura_7" <?php if($this->data->suministro->factura_7 == 1) :?>checked<?php endif; ?>> <br> (kW)</th>
                        
                        <th>PFHP <input class="hidden" type="checkbox" name="pf_ep" value="factura_8" <?php if($this->data->suministro->factura_8 == 1) :?>checked<?php endif; ?>> <br> (kW)</th>

                        <th>CPHP <input class="hidden" type="checkbox" name="cp_consumo" value="factura_17" <?php if($this->data->suministro->factura_17 == 1) :?>checked<?php endif; ?>></th>
                        
                        <th>FP <input class="hidden" type="checkbox" name="fp_precio" value="factura_9" <?php if($this->data->suministro->factura_9 == 1) :?>checked<?php endif; ?>><br> ($)</th>
                        
                        <th>HP <input class="hidden" type="checkbox" name="ep_precio" value="factura_10" <?php if($this->data->suministro->factura_10 == 1) :?>checked<?php endif; ?>><br> ($)</th>

                        <th>CP <input class="hidden" type="checkbox" name="cp_precio" value="factura_18" <?php if($this->data->suministro->factura_18 == 1) :?>checked<?php endif; ?>><br> ($)</th>
                       
                        <th>Req. <input class="hidden" type="checkbox" name="req" value="factura_11" <?php if($this->data->suministro->factura_11 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                       
                        <th>Arr/tra <input class="hidden" type="checkbox" name="ah_ta" value="factura_12" <?php if($this->data->suministro->factura_12 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                        
                        <th>MFP <input class="hidden" type="checkbox" name="factor" value="factura_13" <?php if($this->data->suministro->factura_13 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                        
                        <th>OC <input class="hidden" type="checkbox" name="oc" value="factura_14" <?php if($this->data->suministro->factura_14 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                        
                        <th>Neto <input class="hidden" type="checkbox" name="neto" value="factura_15" <?php if($this->data->suministro->factura_15 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                        
                        <th>Total a Pagar <input class="hidden" type="checkbox" name="total" value="factura_16" <?php if($this->data->suministro->factura_16 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                        
                        <th> 
                            <a id="setColumnasFactura" class="setColumnas hidden" href="javascript:void(0)" title="Guardar Visualización">
                                <i class="fa fa-save" data-target="saveColumnFacturaModal"></i>
                            </a>

                            <a id="cancelColumnasFactura" class="cancelColumnas margin-left hidden" href="javascript:void(0)" title="Cancelar">
                                <i class="fa fa-times" data-target="factura_columns" data-set-target="setColumnasFactura" data-edit-target="editColumnasFactura" data-form-target="columnas_factura_form"></i>
                            </a>

                            <a id="editColumnasFactura" class="editColumnas" href="javascript:void(0)" title="Modificar Visualización">
                                <i class="fa fa-edit" data-target="factura_columns" data-set-target="setColumnasFactura" data-cancel-target="cancelColumnasFactura"></i>
                            </a>
                        </th>
                        </form>
                    <?php endif; ?>              
                    </tr>
                </thead>
                <tbody>
                    <?php $i=0; foreach ($this->data->suministro->facturas as $factura) : $i++?>
                        <tr id="<?=$factura->id;?>" <?php if($this->data->user->id_perfil != '3' && $factura->fac_class != "") :?>class="<?=$factura->fac_class;?>"<?php endif;?>>
                            <td>
                                <b><?=substr($factura->mes, 5, 2);?></b>
                                <?php if(!empty($factura->archivo)) : ?>
                                <a id="v-doc" href="<?=base_url('factura/view_factura/?token=' . $factura->id);?>" target="blank" title="Documento" class="margin-left view">
                                    <i class="fa fa-search-plus"></i>
                                </a>
                                <?php endif;?>
                            </td>
                            <td>
                                <span><?=$factura->num_fact;?></span>
                            </td>
                        <?php if($this->data->user->id_perfil == 3) : ?> 
                            <?php if($this->data->suministro->factura_3 == 1) :?>
                            <td><?=$factura->consumo_ac;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_4 == 1) :?>
                            <td><?=$factura->energia;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_5 == 1) :?>
                            <td><?=$factura->fp_pot;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_6 == 1) :?>
                            <td><?=$factura->fp_deman_fact;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_7 == 1) :?>
                            <td <?php if($factura->remark) : ?>class="negrita"<?php endif;?>><?=$factura->ep_pot;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_8 == 1) :?>
                            <td><?=$factura->ep_deman_fact;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_17 == 1) :?>
                            <td><?=$factura->cp_consumo;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_9 == 1) :?>
                            <td><?=$factura->fp_deman_prec;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_10 == 1) :?>
                            <td><?=$factura->ep_deman_prec;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_18 == 1) :?>
                            <td><?=$factura->cp_precio;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_11 == 1) :?>
                            <td><?=$factura->reliquidaciones;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_12 == 1) :?>
                            <td><?=$factura->arr_tra;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_13 == 1) :?>
                            <td><?=$factura->recargo_factor;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_14 == 1) :?>
                            <td><?=$factura->otros_cargos;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_15 == 1) :?>
                            <td><?=$factura->tot_neto;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_16 == 1) :?>
                            <td style="word-wrap: break-word;">
                                <?=$factura->tot_fac;?>
                            </td>
                            <?php endif;?>
                            <td></td>
                        <?php else : ?>
                            <td><?=$factura->consumo_ac;?></td>
                            
                            <td><?=$factura->energia;?></td>
                            
                            <td><?=$factura->fp_pot;?></td>
                            
                            <td><?=$factura->fp_deman_fact;?></td>
                            
                            <td <?php if($factura->remark) : ?>class="negrita"<?php endif;?>><?=$factura->ep_pot;?></td>
                            
                            <td><?=$factura->ep_deman_fact;?></td>

                            <td><?=$factura->cp_consumo;?></td>
                            
                            <td><?=$factura->fp_deman_prec;?></td>
                            
                            <td><?=$factura->ep_deman_prec;?></td>

                            <td><?=$factura->cp_precio;?></td>
                           
                            <td><?=$factura->reliquidaciones;?></td>
                            
                            <td><?=$factura->arr_tra;?></td>
                            
                            <td><?=$factura->recargo_factor;?></td>
                            
                            <td><?=$factura->otros_cargos;?></td>
                           
                            <td><?=$factura->tot_neto;?></td>
                           
                            <td style="word-wrap: break-word;">
                                <?=$factura->tot_fac;?>
                            </td>

                            <td>
                                <a href="<?=base_url('gestion/factura/?token=' . $factura->id)?>" title="Ver">
                                    <i class="fa fa-edit"></i>
                                </a>
                                
                                <a href="<?=base_url('gestion/delete_factura');?>" data-token="<?=$factura->id;?>" title="Eliminar" class="margin-left delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        <?php endif;?>                        
                        </tr>

                    <?php endforeach; ?>
                        
                </tbody>
                <tfoot>
                    <?php if(empty($this->data->suministro->facturas_libre)) : ?>
                    <tr>
                        <td colspan="2" class="text-right"><b>Totales:</b></td>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->factura_3 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_consumo_ac;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_4 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_energia;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_5 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_fp_pot;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_6 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_fp_deman_fact;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_7 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_ep_pot;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_8 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_ep_deman_fact;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_17 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_cp_consumo;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_9 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_fp_deman_prec;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_10 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_ep_deman_prec;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_18 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_cp_precio;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_11 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_reliquidaciones;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_12 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_arr_tra;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_13 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_recargo_factor;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_14 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_otros_rec;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_15 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_tot_neto;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_16 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_tot_fac;?></b></td>
                        <?php endif;?>
                    <?php else : ?>
                        <td><b><?=$this->data->suministro->tot_consumo_ac;?></b></td>
                        <td><b><?=$this->data->suministro->tot_energia;?></b></td>
                        <td><b><?=$this->data->suministro->tot_fp_pot;?></b></td>
                        <td><b><?=$this->data->suministro->tot_fp_deman_fact;?></b></td>
                        <td><b><?=$this->data->suministro->tot_ep_pot;?></b></td>
                        <td><b><?=$this->data->suministro->tot_ep_deman_fact;?></b></td>
                        <td><b><?=$this->data->suministro->tot_cp_consumo;?></b></td>
                        <td><b><?=$this->data->suministro->tot_fp_deman_prec;?></b></td>
                        <td><b><?=$this->data->suministro->tot_ep_deman_prec;?></b></td>
                        <td><b><?=$this->data->suministro->tot_cp_precio;?></b></td>
                        <td><b><?=$this->data->suministro->tot_reliquidaciones;?></b></td>
                        <td><b><?=$this->data->suministro->tot_arr_tra;?></b></td>
                        <td><b><?=$this->data->suministro->tot_recargo_factor;?></b></td>
                        <td><b><?=$this->data->suministro->tot_otros_rec;?></b></td>
                        <td><b><?=$this->data->suministro->tot_tot_neto;?></b></td>
                        <td><b><?=$this->data->suministro->tot_tot_fac;?></b></td>
                    <?php endif;?>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-right"><b>Promedios:</b></td>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->factura_3 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_consumo_ac;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_4 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_energia;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_5 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_fp_pot;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_6 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_fp_deman_fact;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_7 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_ep_pot;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_8 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_ep_deman_fact;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_17 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_cp_consumo;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_9 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_fp_deman_prec;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_10 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_ep_deman_prec;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_18 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_cp_precio;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_11 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_reliquidaciones;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_12 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_arr_tra;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_13 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_recargo_factor;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_14 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_otros_rec;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_15 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_tot_neto;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_16 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_tot_fac;?></b></td>
                        <?php endif;?>
                    <?php else : ?>
                        <td><b><?=$this->data->suministro->prom_consumo_ac;?></b></td>
                        <td><b><?=$this->data->suministro->prom_energia;?></b></td>
                        <td><b><?=$this->data->suministro->prom_fp_pot;?></b></td>
                        <td><b><?=$this->data->suministro->prom_fp_deman_fact;?></b></td>
                        <td><b><?=$this->data->suministro->prom_ep_pot;?></b></td>
                        <td><b><?=$this->data->suministro->prom_ep_deman_fact;?></b></td>
                        <td><b><?=$this->data->suministro->prom_cp_consumo;?></b></td>
                        <td><b><?=$this->data->suministro->prom_fp_deman_prec;?></b></td>
                        <td><b><?=$this->data->suministro->prom_ep_deman_prec;?></b></td>
                        <td><b><?=$this->data->suministro->prom_cp_precio;?></b></td>
                        <td><b><?=$this->data->suministro->prom_reliquidaciones;?></b></td>
                        <td><b><?=$this->data->suministro->prom_arr_tra;?></b></td>
                        <td><b><?=$this->data->suministro->prom_recargo_factor;?></b></td>
                        <td><b><?=$this->data->suministro->prom_otros_rec;?></b></td>
                        <td><b><?=$this->data->suministro->prom_tot_neto;?></b></td>
                        <td><b><?=$this->data->suministro->prom_tot_fac;?></b></td>
                    <?php endif;?>
                        <td></td>
                    </tr>
                    <?php endif;?>
                    <tr>
                        <th>Mes</th>
                        <th>Nro. Fact.</th>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->factura_3 == 1) :?>
                        <th>Csmo.</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_4 == 1) :?>
                        <th>Gasto.</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_5 == 1) :?>
                        <th>PLFP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_6 == 1) :?>
                        <th>PFFP </th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_7 == 1) :?>
                        <th>PLHP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_8 == 1) :?>
                        <th>PFHP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_17 == 1) :?>
                        <th>CPHP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_9 == 1) :?>
                        <th>FP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_10 == 1) :?>
                        <th>HP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_18 == 1) :?>
                        <th>CP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_11 == 1) :?>
                        <th>Req. </th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_12 == 1) :?>
                        <th>Arr/tra</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_13 == 1) :?>
                        <th>MFP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_14 == 1) :?>  
                        <th>OC</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_15 == 1) :?>
                        <th>Neto</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_16 == 1) :?>
                        <th>Total a Pagar</th>
                        <?php endif; ?>
                    <?php else : ?>
                        <th>Csmo.</th>
                        <th>Gasto.</th>
                        <th>PLFP</th>
                        <th>PFFP</th> 
                        <th>PLHP</th>
                        <th>PFHP</th> 
                        <th>CPHP</th>
                        <th>FP</th>
                        <th>HP</th>
                        <th>CP</th>
                        <th>Req.</th>
                        <th>Arr/tra</th>
                        <th>MFP</th>   
                        <th>OC</th> 
                        <th>Neto</th>
                        <th>Total a Pagar</th>
                    <?php endif;?>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->      
      <?php endif; ?>
      <!-- Cliente libre -->
      <?php if (!empty($this->data->suministro->facturas_libre)) : ?>
      <div class="box box-primary">
        <div class="box-header with-border pointer" >
          <div class="row">
            <div id="facturas-l" class="col-xs-12" data-toggle="collapse" data-target="#collapse_libre" aria-expanded="true" aria-controls="collapse">
              <span class="box-title">Cliente Libre</span>
            </div>
          </div>
        </div>
        <?php
          $colspan_gen_a = cal_colspan([$this->data->suministro->factura_19, $this->data->suministro->factura_20]);
          $colspan_dis_a = cal_colspan([$this->data->suministro->factura_3, $this->data->suministro->factura_4]);
          $colspan_gen_b = cal_colspan([$this->data->suministro->factura_21, $this->data->suministro->factura_22]);
          $colspan_dis_b = cal_colspan([$this->data->suministro->factura_5, $this->data->suministro->factura_6, $this->data->suministro->factura_7, $this->data->suministro->factura_8, $this->data->suministro->factura_17]);
          $colspan_gen_c = cal_colspan([$this->data->suministro->factura_23]);
          $colspan_dis_c = cal_colspan([$this->data->suministro->factura_9, $this->data->suministro->factura_10, $this->data->suministro->factura_18]);
        ?>
        <div id="collapse_libre" class="box-body collapse in text-12">
            <table id="example2_libre" class="table table-hover dark text-center">
                <thead>
                  <?php if($this->data->user->id_perfil == 3) : ?>
                    <tr>
                      <th colspan="2"></th>
                      <?php if($colspan_gen_a > 0) :?>
                      <th colspan="<?=$colspan_gen_a;?>">Generación</th>
                      <?php endif; ?>
                      <?php if($colspan_dis_a > 0) :?>
                      <th colspan="<?=$colspan_dis_a;?>">Distribución</th>
                      <?php endif; ?>
                      <?php if($colspan_gen_b > 0) :?>
                      <th colspan="<?=$colspan_gen_b;?>">Generación</th>
                      <?php endif; ?>
                      <?php if($colspan_dis_b > 0) :?>
                      <th colspan="<?=$colspan_dis_b;?>">Distribución</th>
                      <?php endif; ?>
                      <?php if($colspan_gen_c > 0) :?>
                      <th>Generación</th>
                      <?php endif; ?>
                      <?php if($colspan_dis_c > 0) :?>
                      <th colspan="<?=$colspan_dis_c;?>">Distribución</th>
                      <?php endif; ?>
                      <th colspan='7'></th>
                    </tr>
                  <?php else : ?>
                    <tr>
                      <th colspan='2'></th>
                      <th colspan='2'>Generación</th>
                      <th colspan='2'>Distribución</th>
                      <th colspan='2'>Generación</th>
                      <th colspan='5'>Distribución</th>
                      <th>Generación</th>
                      <th colspan='3'>Distribución</th>
                      <th colspan='7'></th>
                    </tr>
                  <?php endif; ?>
                    <tr id="factura_columns_libre">
                        <th>Mes</th>

                        <th>Nro. Fact.</th>

                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <!-- Generacion -->
                        <?php if($this->data->suministro->factura_19 == 1) :?>
                        <th>Csmo. <br> (kWh)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_20 == 1) :?>
                        <th>Gasto <br> ($)</th>
                        <?php endif; ?>
                        <!-- Distribucion -->
                        <?php if($this->data->suministro->factura_3 == 1) :?>
                        <th>Csmo. <br> (kWh)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_4 == 1) :?>
                        <th>Gasto <br> ($)</th>
                        <?php endif; ?>
                        <!-- Generacion -->
                        <?php if($this->data->suministro->factura_21 == 1) :?>
                        <th>PLHP <br> (kW)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_22 == 1) :?>
                        <th>PFHP <br> (kW)</th>
                        <?php endif; ?>
                        <!-- Distribucion -->
                        <?php if($this->data->suministro->factura_5 == 1) :?>
                        <th>PLFP <br> (kW)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_6 == 1) :?>
                        <th>PFFP <br> (kW)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_7 == 1) :?>
                        <th>PLHP <br> (kW)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_8 == 1) :?>
                        <th>PFHP <br> (kW)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_17 == 1) :?>
                        <th>CPHP </th>
                        <?php endif; ?>
                        <!-- Generacion -->
                        <?php if($this->data->suministro->factura_23 == 1) :?>
                        <th>HP <br> ($)</th>
                        <?php endif; ?>
                        <!-- Distribucion -->
                        <?php if($this->data->suministro->factura_9 == 1) :?>
                        <th>FP <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_10 == 1) :?>
                        <th>HP <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_18 == 1) :?>
                        <th>CP <br> ($)</th>
                        <?php endif; ?>
                        <!-- Otros -->
                        <?php if($this->data->suministro->factura_11 == 1) :?>
                        <th>Req. <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_12 == 1) :?>
                        <th>Arr/tra <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_13 == 1) :?>
                        <th>MFP <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_14 == 1) :?>  
                        <th>OC <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_15 == 1) :?>
                        <th>Neto <br> ($)</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_16 == 1) :?>
                        <th>Total a Pagar <br> ($)</th>
                        <?php endif; ?>
                        <th></th>
                    <?php else : ?>
                        <form id="columnas_factura_form_libre" action="<?=base_url('gestion/set_columnas_factura');?>">
                            <input type="hidden" name="token" value="<?=$this->data->suministro->id?>">
                        
                        <th>Csmo. <input class="hidden" type="checkbox" name="consumo_gen" value="factura_19" <?php if($this->data->suministro->factura_19 == 1) :?>checked<?php endif; ?>> <br> (kWh)</th>
                        <th>Gasto. <input class="hidden" type="checkbox" name="gasto_gen" value="factura_20" <?php if($this->data->suministro->factura_20 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                        
                        <th>Csmo. <input class="hidden" type="checkbox" name="consumo" value="factura_3" <?php if($this->data->suministro->factura_3 == 1) :?>checked<?php endif; ?>> <br> (kWh)</th>
                        <th>Gasto. <input class="hidden" type="checkbox" name="gasto" value="factura_4" <?php if($this->data->suministro->factura_4 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                        
                        <th>PLHP <input class="hidden" type="checkbox" name="pl_ep_gen" value="factura_21" <?php if($this->data->suministro->factura_21 == 1) :?>checked<?php endif; ?>> <br> (kW)</th>
                        <th>PFHP <input class="hidden" type="checkbox" name="pf_ep_gen" value="factura_22" <?php if($this->data->suministro->factura_22 == 1) :?>checked<?php endif; ?>> <br> (kW)</th>

                        <th>PLFP <input class="hidden" type="checkbox" name="pl_fp" value="factura_5" <?php if($this->data->suministro->factura_5 == 1) :?>checked<?php endif; ?>> <br> (kW)</th>
                        <th>PFFP <input class="hidden" type="checkbox" name="pf_fp" value="factura_6" <?php if($this->data->suministro->factura_6 == 1) :?>checked<?php endif; ?>> <br> (kW)</th>
                        <th>PLHP <input class="hidden" type="checkbox" name="pl_ep" value="factura_7" <?php if($this->data->suministro->factura_7 == 1) :?>checked<?php endif; ?>> <br> (kW)</th>
                        <th>PFHP <input class="hidden" type="checkbox" name="pf_ep" value="factura_8" <?php if($this->data->suministro->factura_8 == 1) :?>checked<?php endif; ?>> <br> (kW)</th>
                        <th>CPHP <input class="hidden" type="checkbox" name="cp_consumo" value="factura_17" <?php if($this->data->suministro->factura_17 == 1) :?>checked<?php endif; ?>></th>
                        
                        <th>HP <input class="hidden" type="checkbox" name="ep_precio_gen" value="factura_23" <?php if($this->data->suministro->factura_23 == 1) :?>checked<?php endif; ?>><br> ($)</th>
                        
                        <th>FP <input class="hidden" type="checkbox" name="fp_precio" value="factura_9" <?php if($this->data->suministro->factura_9 == 1) :?>checked<?php endif; ?>><br> ($)</th>
                        <th>HP <input class="hidden" type="checkbox" name="ep_precio" value="factura_10" <?php if($this->data->suministro->factura_10 == 1) :?>checked<?php endif; ?>><br> ($)</th>
                        <th>CP <input class="hidden" type="checkbox" name="cp_precio" value="factura_18" <?php if($this->data->suministro->factura_18 == 1) :?>checked<?php endif; ?>><br> ($)</th>
                        
                        <th>Req. <input class="hidden" type="checkbox" name="req" value="factura_11" <?php if($this->data->suministro->factura_11 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                        <th>Arr/tra <input class="hidden" type="checkbox" name="ah_ta" value="factura_12" <?php if($this->data->suministro->factura_12 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                        <th>MFP <input class="hidden" type="checkbox" name="factor" value="factura_13" <?php if($this->data->suministro->factura_13 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                        <th>OC <input class="hidden" type="checkbox" name="oc" value="factura_14" <?php if($this->data->suministro->factura_14 == 1) :?>checked<?php endif; ?>> <br> ($)</th>

                        <th>Neto <input class="hidden" type="checkbox" name="neto" value="factura_15" <?php if($this->data->suministro->factura_15 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                        <th>Total a Pagar <input class="hidden" type="checkbox" name="total" value="factura_16" <?php if($this->data->suministro->factura_16 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                        
                        <th> 
                            <a id="setColumnasFactura_libre" class="setColumnas hidden" href="javascript:void(0)" title="Guardar Visualización">
                                <i class="fa fa-save" data-target="saveColumnFacturaModalLibre"></i>
                            </a>

                            <a id="cancelColumnasFactura_libre" class="cancelColumnas margin-left hidden" href="javascript:void(0)" title="Cancelar">
                                <i class="fa fa-times" data-target="factura_columns_libre" data-set-target="setColumnasFactura_libre" data-edit-target="editColumnasFactura_libre" data-form-target="columnas_factura_form_libre"></i>
                            </a>

                            <a id="editColumnasFactura_libre" class="editColumnas" href="javascript:void(0)" title="Modificar Visualización">
                                <i class="fa fa-edit" data-target="factura_columns_libre" data-set-target="setColumnasFactura_libre" data-cancel-target="cancelColumnasFactura_libre"></i>
                            </a>
                        </th>
                        </form>
                    <?php endif; ?>              
                    </tr>
                </thead>
                <tbody>
                    <?php $i=0; foreach ($this->data->suministro->facturas_libre as $factura) : $i++?>
                        <tr id="<?=$factura->id;?>" <?php if($this->data->user->id_perfil != '3' && $factura->fac_class != "") :?>class="<?=$factura->fac_class;?>"<?php endif;?>>
                            <td>
                                <b><?=substr($factura->mes, 5, 2);?></b>
                                <?php if(!empty($factura->archivo_gen)) : ?>
                                <a id="v-doc-l2" href="<?=base_url('factura/view_factura/?token=' . $factura->id . '&file=2');?>" target="blank" title="Documento Generación" class="margin-left view">
                                    <i class="fa fa-search-plus"></i>
                                </a>
                                <?php endif;?>
                                <?php if(!empty($factura->archivo)) : ?>
                                <a id="v-doc-l1" href="<?=base_url('factura/view_factura/?token=' . $factura->id . '&file=1');?>" target="blank" title="Documento Distribución" class="margin-left view">
                                    <i class="fa fa-search-plus"></i>
                                </a>
                                <?php endif;?>
                            </td>
                            <td>
                                <?php if($factura->num_fact != $factura->num_fact_gen) :?>
                                  <span>G-<?=$factura->num_fact_gen;?></span>/<span>D-<?=$factura->num_fact;?></span>
                                <?php else :?>
                                  <span><?=$factura->num_fact;?></span>
                                <?php endif;?>
                            </td>
                        <?php if($this->data->user->id_perfil == 3) : ?>
                          <?php if($this->data->suministro->factura_19 == 1) :?>
                            <td><?=$factura->consumo_ac_gen;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_20 == 1) :?>
                            <td><?=$factura->energia_gen;?></td>
                            <?php endif;?>

                            <?php if($this->data->suministro->factura_3 == 1) :?>
                            <td><?=$factura->consumo_ac;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_4 == 1) :?>
                            <td><?=$factura->energia;?></td>
                            <?php endif;?>

                            <?php if($this->data->suministro->factura_21 == 1) :?>
                            <td><?=$factura->ep_pot_gen;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_22 == 1) :?>
                            <td><?=$factura->ep_deman_fact_gen;?></td>
                            <?php endif;?>

                            <?php if($this->data->suministro->factura_5 == 1) :?>
                            <td><?=$factura->fp_pot;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_6 == 1) :?>
                            <td><?=$factura->fp_deman_fact;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_7 == 1) :?>
                            <td <?php if($factura->remark) : ?>class="negrita"<?php endif;?>><?=$factura->ep_pot;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_8 == 1) :?>
                            <td><?=$factura->ep_deman_fact;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_17 == 1) :?>
                            <td><?=$factura->cp_consumo;?></td>
                            <?php endif;?>

                            <?php if($this->data->suministro->factura_23 == 1) :?>
                            <td><?=$factura->ep_deman_prec_gen;?></td>
                            <?php endif;?>

                            <?php if($this->data->suministro->factura_9 == 1) :?>
                            <td><?=$factura->fp_deman_prec;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_10 == 1) :?>
                            <td><?=$factura->ep_deman_prec;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_18 == 1) :?>
                            <td><?=$factura->cp_precio;?></td>
                            <?php endif;?>
                    
                            <?php if($this->data->suministro->factura_11 == 1) :?>
                            <td><?=$factura->reliquidaciones;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_12 == 1) :?>
                            <td><?=$factura->arr_tra;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_13 == 1) :?>
                            <td><?=$factura->recargo_factor;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_14 == 1) :?>
                            <td><?=$factura->otros_cargos;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_15 == 1) :?>
                            <td><?=$factura->tot_neto_libre;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->factura_16 == 1) :?>
                            <td style="word-wrap: break-word;">
                                <?=$factura->tot_fac_libre;?>
                            </td>
                            <?php endif;?>
                            <td></td>
                        <?php else : ?>
                            <td><?=$factura->consumo_ac_gen;?></td>
                            <td><?=$factura->energia_gen;?></td>

                            <td><?=$factura->consumo_ac;?></td>
                            <td><?=$factura->energia;?></td>

                            <td><?=$factura->ep_pot_gen;?></td>
                            <td><?=$factura->ep_deman_fact_gen;?></td>
                            
                            <td><?=$factura->fp_pot;?></td>
                            <td><?=$factura->fp_deman_fact;?></td>
                            <td <?php if($factura->remark) : ?>class="negrita"<?php endif;?>><?=$factura->ep_pot;?></td>
                            <td><?=$factura->ep_deman_fact;?></td>
                            <td><?=$factura->cp_consumo;?></td>

                            <td><?=$factura->ep_deman_prec_gen;?></td>

                            <td><?=$factura->fp_deman_prec;?></td>
                            <td><?=$factura->ep_deman_prec;?></td>
                            <td><?=$factura->cp_precio;?></td>

                            <td><?=$factura->reliquidaciones;?></td>
                            <td><?=$factura->arr_tra;?></td>
                            <td><?=$factura->recargo_factor;?></td>
                            <td><?=$factura->otros_cargos;?></td>
                            <td><?=$factura->tot_neto_libre;?></td>
                            <td style="word-wrap: break-word;">
                                <?=$factura->tot_fac_libre;?>
                            </td>

                            <td>
                                <a href="<?=base_url('gestion/factura/?token=' . $factura->id)?>" title="Ver">
                                    <i class="fa fa-edit"></i>
                                </a>
                                
                                <a href="<?=base_url('gestion/delete_factura');?>" data-token="<?=$factura->id;?>" title="Eliminar" class="margin-left delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        <?php endif;?>                        
                        </tr>

                    <?php endforeach; ?>
                        
                </tbody>
                <tfoot>
                    <?php if(empty($this->data->suministro->facturas)) : ?>
                    <tr>
                        <td colspan="2" class="text-right"><b>Totales:</b></td>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->factura_19 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_consumo_ac_gen;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_20 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_energia_gen;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->factura_3 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_consumo_ac;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_4 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_energia;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->factura_21 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_ep_pot_gen;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_22 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_ep_deman_fact_gen;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->factura_5 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_fp_pot;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_6 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_fp_deman_fact;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_7 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_ep_pot;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_8 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_ep_deman_fact;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_17 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_cp_consumo;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->factura_23 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_ep_deman_prec_gen;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->factura_9 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_fp_deman_prec;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_10 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_ep_deman_prec;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_18 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_cp_precio;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->factura_11 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_reliquidaciones;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_12 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_arr_tra;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_13 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_recargo_factor;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_14 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_otros_rec;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_15 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_tot_neto;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_16 == 1) :?>
                        <td><b><?=$this->data->suministro->tot_tot_fac;?></b></td>
                        <?php endif;?>
                    <?php else : ?>
                        <td><b><?=$this->data->suministro->tot_consumo_ac_gen;?></b></td>
                        <td><b><?=$this->data->suministro->tot_energia_gen;?></b></td>

                        <td><b><?=$this->data->suministro->tot_consumo_ac;?></b></td>
                        <td><b><?=$this->data->suministro->tot_energia;?></b></td>

                        <td><b><?=$this->data->suministro->tot_ep_pot_gen;?></b></td>
                        <td><b><?=$this->data->suministro->tot_ep_deman_fact_gen;?></b></td>

                        <td><b><?=$this->data->suministro->tot_fp_pot;?></b></td>
                        <td><b><?=$this->data->suministro->tot_fp_deman_fact;?></b></td>
                        <td><b><?=$this->data->suministro->tot_ep_pot;?></b></td>
                        <td><b><?=$this->data->suministro->tot_ep_deman_fact;?></b></td>
                        <td><b><?=$this->data->suministro->tot_cp_consumo;?></b></td>

                        <td><b><?=$this->data->suministro->tot_ep_deman_prec_gen;?></b></td>

                        <td><b><?=$this->data->suministro->tot_fp_deman_prec;?></b></td>
                        <td><b><?=$this->data->suministro->tot_ep_deman_prec;?></b></td>
                        <td><b><?=$this->data->suministro->tot_cp_precio;?></b></td>

                        <td><b><?=$this->data->suministro->tot_reliquidaciones;?></b></td>
                        <td><b><?=$this->data->suministro->tot_arr_tra;?></b></td>
                        <td><b><?=$this->data->suministro->tot_recargo_factor;?></b></td>
                        <td><b><?=$this->data->suministro->tot_otros_rec;?></b></td>
                        <td><b><?=$this->data->suministro->tot_tot_neto;?></b></td>
                        <td><b><?=$this->data->suministro->tot_tot_fac;?></b></td>
                    <?php endif;?>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-right"><b>Promedios:</b></td>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->factura_19 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_consumo_ac_gen;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_20 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_energia_gen;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->factura_3 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_consumo_ac;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_4 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_energia;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->factura_21 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_ep_pot_gen;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_22 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_ep_deman_fact_gen;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->factura_5 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_fp_pot;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_6 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_fp_deman_fact;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_7 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_ep_pot;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_8 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_ep_deman_fact;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_17 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_cp_consumo;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->factura_23 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_ep_deman_prec_gen;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->factura_9 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_fp_deman_prec;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_10 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_ep_deman_prec;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_18 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_cp_precio;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->factura_11 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_reliquidaciones;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_12 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_arr_tra;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_13 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_recargo_factor;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_14 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_otros_rec;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_15 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_tot_neto;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->factura_16 == 1) :?>
                        <td><b><?=$this->data->suministro->prom_tot_fac;?></b></td>
                        <?php endif;?>
                    <?php else : ?>
                        <td><b><?=$this->data->suministro->prom_consumo_ac_gen;?></b></td>
                        <td><b><?=$this->data->suministro->prom_energia_gen;?></b></td>

                        <td><b><?=$this->data->suministro->prom_consumo_ac;?></b></td>
                        <td><b><?=$this->data->suministro->prom_energia;?></b></td>

                        <td><b><?=$this->data->suministro->prom_ep_pot_gen;?></b></td>
                        <td><b><?=$this->data->suministro->prom_ep_deman_fact_gen;?></b></td>

                        <td><b><?=$this->data->suministro->prom_fp_pot;?></b></td>
                        <td><b><?=$this->data->suministro->prom_fp_deman_fact;?></b></td>
                        <td><b><?=$this->data->suministro->prom_ep_pot;?></b></td>
                        <td><b><?=$this->data->suministro->prom_ep_deman_fact;?></b></td>
                        <td><b><?=$this->data->suministro->prom_cp_consumo;?></b></td>

                        <td><b><?=$this->data->suministro->prom_ep_deman_prec_gen;?></b></td>

                        <td><b><?=$this->data->suministro->prom_fp_deman_prec;?></b></td>
                        <td><b><?=$this->data->suministro->prom_ep_deman_prec;?></b></td>
                        <td><b><?=$this->data->suministro->prom_cp_precio;?></b></td>

                        <td><b><?=$this->data->suministro->prom_reliquidaciones;?></b></td>
                        <td><b><?=$this->data->suministro->prom_arr_tra;?></b></td>
                        <td><b><?=$this->data->suministro->prom_recargo_factor;?></b></td>
                        <td><b><?=$this->data->suministro->prom_otros_rec;?></b></td>
                        <td><b><?=$this->data->suministro->prom_tot_neto;?></b></td>
                        <td><b><?=$this->data->suministro->prom_tot_fac;?></b></td>
                    <?php endif;?>
                        <td></td>
                    </tr>
                    <?php endif;?>
                    <tr>
                        <th>Mes</th>
                        <th>Nro. Fact.</th>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->factura_19 == 1) :?>
                        <th>Csmo.</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_20 == 1) :?>
                        <th>Gasto.</th>
                        <?php endif; ?>

                        <?php if($this->data->suministro->factura_3 == 1) :?>
                        <th>Csmo.</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_4 == 1) :?>
                        <th>Gasto.</th>
                        <?php endif; ?>

                        <?php if($this->data->suministro->factura_21 == 1) :?>
                        <th>PLHP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_22 == 1) :?>
                        <th>PFHP</th>
                        <?php endif; ?>

                        <?php if($this->data->suministro->factura_5 == 1) :?>
                        <th>PLFP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_6 == 1) :?>
                        <th>PFFP </th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_7 == 1) :?>
                        <th>PLHP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_8 == 1) :?>
                        <th>PFHP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_17 == 1) :?>
                        <th>CPHP</th>
                        <?php endif; ?>

                        <?php if($this->data->suministro->factura_23 == 1) :?>
                        <th>HP</th>
                        <?php endif; ?>

                        <?php if($this->data->suministro->factura_9 == 1) :?>
                        <th>FP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_10 == 1) :?>
                        <th>HP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_18 == 1) :?>
                        <th>CP</th>
                        <?php endif; ?>

                        <?php if($this->data->suministro->factura_11 == 1) :?>
                        <th>Req. </th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_12 == 1) :?>
                        <th>Arr/tra</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_13 == 1) :?>
                        <th>MFP</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_14 == 1) :?>  
                        <th>OC</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_15 == 1) :?>
                        <th>Neto</th>
                        <?php endif; ?>
                        <?php if($this->data->suministro->factura_16 == 1) :?>
                        <th>Total a Pagar</th>
                        <?php endif; ?>
                    <?php else : ?>
                        <th>Csmo.</th>
                        <th>Gasto.</th>

                        <th>Csmo.</th>
                        <th>Gasto.</th>

                        <th>PLHP</th>
                        <th>PFHP</th>

                        <th>PLFP</th>
                        <th>PFFP</th> 
                        <th>PLHP</th>
                        <th>PFHP</th> 
                        <th>CPHP</th>

                        <th>HP</th>

                        <th>FP</th>
                        <th>HP</th>
                        <th>CP</th>

                        <th>Req.</th>
                        <th>Arr/tra</th>
                        <th>MFP</th>   
                        <th>OC</th> 
                        <th>Neto</th>
                        <th>Total a Pagar</th>
                    <?php endif;?>
                        <th></th>
                    </tr>
                  <?php if($this->data->user->id_perfil == 3) : ?>
                    <tr>
                      <th colspan="2"></th>
                      <?php if($colspan_gen_a > 0) :?>
                      <th colspan="<?=$colspan_gen_a;?>">Generación</th>
                      <?php endif; ?>
                      <?php if($colspan_dis_b > 0) :?>
                      <th colspan="<?=$colspan_dis_a;?>">Distribución</th>
                      <?php endif; ?>
                      <?php if($colspan_gen_b > 0) :?>
                      <th colspan="<?=$colspan_gen_b;?>">Generación</th>
                      <?php endif; ?>
                      <?php if($colspan_dis_b > 0) :?>
                      <th colspan="<?=$colspan_dis_b;?>">Distribución</th>
                      <?php endif; ?>
                      <?php if($colspan_gen_c > 0) :?>
                      <th>Generación</th>
                      <?php endif; ?>
                      <?php if($colspan_dis_c > 0) :?>
                      <th colspan="<?=$colspan_dis_c;?>">Distribución</th>
                      <?php endif; ?>
                      <th colspan='7'></th>
                    </tr>
                  <?php else : ?>
                    <tr>
                      <th colspan='2'></th>
                      <th colspan='2'>Generación</th>
                      <th colspan='2'>Distribución</th>
                      <th colspan='2'>Generación</th>
                      <th colspan='5'>Distribución</th>
                      <th>Generación</th>
                      <th colspan='3'>Distribución</th>
                      <th colspan='7'></th>
                    </tr>
                  <?php endif; ?>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->      
      <?php endif; ?>

      <?php if (!empty($this->data->suministro->facturas) && !empty($this->data->suministro->facturas_libre)) : ?>
      <div class="box box-primary">
        <div class="box-header with-border pointer">
          <div class="row">
            <div class="col-xs-12">
              <span class="box-title">Resumen Facturas</span>
            </div>
          </div>
        </div>
        <div class="box-body text-12">
          <table class="table table-hover dark text-center">
            <tbody>
              <?php if($this->data->user->id_perfil == 3) : ?>
                <tr>
                  <th colspan="2"></th>
                  <?php if($colspan_gen_a > 0) :?>
                  <th colspan="<?=$colspan_gen_a;?>">Generación</th>
                  <?php endif; ?>
                  <?php if($colspan_dis_b > 0) :?>
                  <th colspan="<?=$colspan_dis_a;?>">Distribución</th>
                  <?php endif; ?>
                  <?php if($colspan_gen_b > 0) :?>
                  <th colspan="<?=$colspan_gen_b;?>">Generación</th>
                  <?php endif; ?>
                  <?php if($colspan_dis_b > 0) :?>
                  <th colspan="<?=$colspan_dis_b;?>">Distribución</th>
                  <?php endif; ?>
                  <?php if($colspan_gen_c > 0) :?>
                  <th>Generación</th>
                  <?php endif; ?>
                  <?php if($colspan_dis_c > 0) :?>
                  <th colspan="<?=$colspan_dis_c;?>">Distribución</th>
                  <?php endif; ?>
                  <th colspan='7'></th>
                </tr>
              <?php else : ?>
                <tr>
                  <th colspan='2'></th>
                  <th colspan='2'>Generación</th>
                  <th colspan='2'>Distribución</th>
                  <th colspan='2'>Generación</th>
                  <th colspan='5'>Distribución</th>
                  <th>Generación</th>
                  <th colspan='3'>Distribución</th>
                  <th colspan='7'></th>
                </tr>
              <?php endif; ?>
              <tr>
                  <th colspan="2"></th>
              <?php if($this->data->user->id_perfil == 3) : ?>
                  <?php if($this->data->suministro->factura_19 == 1) :?>
                  <th>Csmo. <br> (kWh)</th>
                  <?php endif; ?>
                  <?php if($this->data->suministro->factura_20 == 1) :?>
                  <th>Gasto. <br> ($)</th>
                  <?php endif; ?>

                  <?php if($this->data->suministro->factura_3 == 1) :?>
                  <th>Csmo. <br> (kWh)</th>
                  <?php endif; ?>
                  <?php if($this->data->suministro->factura_4 == 1) :?>
                  <th>Gasto. <br> ($)</th>
                  <?php endif; ?>

                  <?php if($this->data->suministro->factura_21 == 1) :?>
                  <th>PLHP <br> (kW)</th>
                  <?php endif; ?>
                  <?php if($this->data->suministro->factura_22 == 1) :?>
                  <th>PFHP <br> (kW)</th>
                  <?php endif; ?>

                  <?php if($this->data->suministro->factura_5 == 1) :?>
                  <th>PLFP <br> (kW)</th>
                  <?php endif; ?>
                  <?php if($this->data->suministro->factura_6 == 1) :?>
                  <th>PFFP <br> (kW)</th>
                  <?php endif; ?>
                  <?php if($this->data->suministro->factura_7 == 1) :?>
                  <th>PLHP <br> (kW)</th>
                  <?php endif; ?>
                  <?php if($this->data->suministro->factura_8 == 1) :?>
                  <th>PFHP <br> (kW)</th>
                  <?php endif; ?>
                  <?php if($this->data->suministro->factura_17 == 1) :?>
                  <th>CPHP</th>
                  <?php endif; ?>

                  <?php if($this->data->suministro->factura_23 == 1) :?>
                  <th>HP <br> ($)</th>
                  <?php endif; ?>

                  <?php if($this->data->suministro->factura_9 == 1) :?>
                  <th>FP <br> ($)</th>
                  <?php endif; ?>
                  <?php if($this->data->suministro->factura_10 == 1) :?>
                  <th>HP <br> ($)</th>
                  <?php endif; ?>
                  <?php if($this->data->suministro->factura_18 == 1) :?>
                  <th>CP <br> ($)</th>
                  <?php endif; ?>

                  <?php if($this->data->suministro->factura_11 == 1) :?>
                  <th>Req. <br> ($)</th>
                  <?php endif; ?>
                  <?php if($this->data->suministro->factura_12 == 1) :?>
                  <th>Arr/tra <br> ($)</th>
                  <?php endif; ?>
                  <?php if($this->data->suministro->factura_13 == 1) :?>
                  <th>MFP <br> ($)</th>
                  <?php endif; ?>
                  <?php if($this->data->suministro->factura_14 == 1) :?>  
                  <th>OC <br> ($)</th>
                  <?php endif; ?>
                  <?php if($this->data->suministro->factura_15 == 1) :?>
                  <th>Neto <br> ($)</th>
                  <?php endif; ?>
                  <?php if($this->data->suministro->factura_16 == 1) :?>
                  <th>Total a Pagar <br> ($)</th>
                  <?php endif; ?>
              <?php else : ?>
                  <th>Csmo.<br> (kWh)</th>
                  <th>Gasto.<br> ($)</th>

                  <th>Csmo. <br> (kWh)</th>
                  <th>Gasto. <br> ($)</th>

                  <th>PLHP <br> (kW)</th>
                  <th>PFHP <br> (kW)</th>

                  <th>PLFP <br> (kW)</th>
                  <th>PFFP <br> (kW)</th> 
                  <th>PLHP <br> (kW)</th>
                  <th>PFHP <br> (kW)</th> 
                  <th>CPHP</th>

                  <th>HP <br> ($)</th>

                  <th>FP <br> ($)</th>
                  <th>HP <br> ($)</th>
                  <th>CP <br> ($)</th>

                  <th>Req. <br> ($)</th>
                  <th>Arr/tra <br> ($)</th>
                  <th>MFP <br> ($)</th>   
                  <th>OC <br> ($)</th> 
                  <th>Neto <br> ($)</th>
                  <th>Total a Pagar <br> ($)</th>
              <?php endif;?>
                  <th></th>
              </tr>
              <tr>
                  <td colspan="2" class="text-right"><b>Totales:</b></td>
              <?php if($this->data->user->id_perfil == 3) : ?>
                  <?php if($this->data->suministro->factura_19 == 1) :?>
                  <td><?=$this->data->suministro->tot_consumo_ac_gen;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_20 == 1) :?>
                  <td><?=$this->data->suministro->tot_energia_gen;?></td>
                  <?php endif;?>

                  <?php if($this->data->suministro->factura_3 == 1) :?>
                  <td><?=$this->data->suministro->tot_consumo_ac;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_4 == 1) :?>
                  <td><?=$this->data->suministro->tot_energia;?></td>
                  <?php endif;?>

                  <?php if($this->data->suministro->factura_21 == 1) :?>
                  <td><?=$this->data->suministro->tot_ep_pot_gen;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_22 == 1) :?>
                  <td><?=$this->data->suministro->tot_ep_deman_fact_gen;?></td>
                  <?php endif;?>

                  <?php if($this->data->suministro->factura_5 == 1) :?>
                  <td><?=$this->data->suministro->tot_fp_pot;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_6 == 1) :?>
                  <td><?=$this->data->suministro->tot_fp_deman_fact;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_7 == 1) :?>
                  <td><?=$this->data->suministro->tot_ep_pot;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_8 == 1) :?>
                  <td><?=$this->data->suministro->tot_ep_deman_fact;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_17 == 1) :?>
                  <td><?=$this->data->suministro->tot_cp_consumo;?></td>
                  <?php endif;?>

                  <?php if($this->data->suministro->factura_23 == 1) :?>
                  <td><?=$this->data->suministro->tot_ep_deman_prec_gen;?></td>
                  <?php endif;?>

                  <?php if($this->data->suministro->factura_9 == 1) :?>
                  <td><?=$this->data->suministro->tot_fp_deman_prec;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_10 == 1) :?>
                  <td><?=$this->data->suministro->tot_ep_deman_prec;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_18 == 1) :?>
                  <td><?=$this->data->suministro->tot_cp_precio;?></td>
                  <?php endif;?>

                  <?php if($this->data->suministro->factura_11 == 1) :?>
                  <td><?=$this->data->suministro->tot_reliquidaciones;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_12 == 1) :?>
                  <td><?=$this->data->suministro->tot_arr_tra;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_13 == 1) :?>
                  <td><?=$this->data->suministro->tot_recargo_factor;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_14 == 1) :?>
                  <td><?=$this->data->suministro->tot_otros_rec;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_15 == 1) :?>
                  <td><?=$this->data->suministro->tot_tot_neto;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_16 == 1) :?>
                  <td><?=$this->data->suministro->tot_tot_fac;?></td>
                  <?php endif;?>
              <?php else : ?>
                  <td><?=$this->data->suministro->tot_consumo_ac_gen;?></td>
                  <td><?=$this->data->suministro->tot_energia_gen;?></td>

                  <td><?=$this->data->suministro->tot_consumo_ac;?></td>
                  <td><?=$this->data->suministro->tot_energia;?></td>

                  <td><?=$this->data->suministro->tot_ep_pot_gen;?></td>
                  <td><?=$this->data->suministro->tot_ep_deman_fact_gen;?></td>

                  <td><?=$this->data->suministro->tot_fp_pot;?></td>
                  <td><?=$this->data->suministro->tot_fp_deman_fact;?></td>
                  <td><?=$this->data->suministro->tot_ep_pot;?></td>
                  <td><?=$this->data->suministro->tot_ep_deman_fact;?></td>
                  <td><?=$this->data->suministro->tot_cp_consumo;?></td>

                  <td><?=$this->data->suministro->tot_ep_deman_prec_gen;?></td>

                  <td><?=$this->data->suministro->tot_fp_deman_prec;?></td>
                  <td><?=$this->data->suministro->tot_ep_deman_prec;?></td>
                  <td><?=$this->data->suministro->tot_cp_precio;?></td>

                  <td><?=$this->data->suministro->tot_reliquidaciones;?></td>
                  <td><?=$this->data->suministro->tot_arr_tra;?></td>
                  <td><?=$this->data->suministro->tot_recargo_factor;?></td>
                  <td><?=$this->data->suministro->tot_otros_rec;?></td>
                  <td><?=$this->data->suministro->tot_tot_neto;?></td>
                  <td><?=$this->data->suministro->tot_tot_fac;?></td>
              <?php endif;?>
                  <td></td>
              </tr>
              <tr>
                  <td colspan="2" class="text-right"><b>Promedios:</b></td>
              <?php if($this->data->user->id_perfil == 3) : ?>
                  <?php if($this->data->suministro->factura_19 == 1) :?>
                  <td><?=$this->data->suministro->prom_consumo_ac_gen;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_20 == 1) :?>
                  <td><?=$this->data->suministro->prom_energia_gen;?></td>
                  <?php endif;?>

                  <?php if($this->data->suministro->factura_3 == 1) :?>
                  <td><?=$this->data->suministro->prom_consumo_ac;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_4 == 1) :?>
                  <td><?=$this->data->suministro->prom_energia;?></td>
                  <?php endif;?>

                  <?php if($this->data->suministro->factura_21 == 1) :?>
                  <td><?=$this->data->suministro->prom_ep_pot_gen;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_22 == 1) :?>
                  <td><?=$this->data->suministro->prom_ep_deman_fact_gen;?></td>
                  <?php endif;?>

                  <?php if($this->data->suministro->factura_5 == 1) :?>
                  <td><?=$this->data->suministro->prom_fp_pot;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_6 == 1) :?>
                  <td><?=$this->data->suministro->prom_fp_deman_fact;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_7 == 1) :?>
                  <td><?=$this->data->suministro->prom_ep_pot;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_8 == 1) :?>
                  <td><?=$this->data->suministro->prom_ep_deman_fact;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_17 == 1) :?>
                  <td><?=$this->data->suministro->prom_cp_consumo;?></td>
                  <?php endif;?>

                  <?php if($this->data->suministro->factura_23 == 1) :?>
                  <td><?=$this->data->suministro->prom_ep_deman_prec_gen;?></td>
                  <?php endif;?>

                  <?php if($this->data->suministro->factura_9 == 1) :?>
                  <td><?=$this->data->suministro->prom_fp_deman_prec;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_10 == 1) :?>
                  <td><?=$this->data->suministro->prom_ep_deman_prec;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_18 == 1) :?>
                  <td><?=$this->data->suministro->prom_cp_precio;?></td>
                  <?php endif;?>

                  <?php if($this->data->suministro->factura_11 == 1) :?>
                  <td><?=$this->data->suministro->prom_reliquidaciones;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_12 == 1) :?>
                  <td><?=$this->data->suministro->prom_arr_tra;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_13 == 1) :?>
                  <td><?=$this->data->suministro->prom_recargo_factor;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_14 == 1) :?>
                  <td><?=$this->data->suministro->prom_otros_rec;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_15 == 1) :?>
                  <td><?=$this->data->suministro->prom_tot_neto;?></td>
                  <?php endif;?>
                  <?php if($this->data->suministro->factura_16 == 1) :?>
                  <td><?=$this->data->suministro->prom_tot_fac;?></td>
                  <?php endif;?>
              <?php else : ?>
                  <td><?=$this->data->suministro->prom_consumo_ac_gen;?></td>
                  <td><?=$this->data->suministro->prom_energia_gen;?></td>

                  <td><?=$this->data->suministro->prom_consumo_ac;?></td>
                  <td><?=$this->data->suministro->prom_energia;?></td>

                  <td><?=$this->data->suministro->prom_ep_pot_gen;?></td>
                  <td><?=$this->data->suministro->prom_ep_deman_fact_gen;?></td>

                  <td><?=$this->data->suministro->prom_fp_pot;?></td>
                  <td><?=$this->data->suministro->prom_fp_deman_fact;?></td>
                  <td><?=$this->data->suministro->prom_ep_pot;?></td>
                  <td><?=$this->data->suministro->prom_ep_deman_fact;?></td>
                  <td><?=$this->data->suministro->prom_cp_consumo;?></td>

                  <td><?=$this->data->suministro->prom_ep_deman_prec_gen;?></td>

                  <td><?=$this->data->suministro->prom_fp_deman_prec;?></td>
                  <td><?=$this->data->suministro->prom_ep_deman_prec;?></td>
                  <td><?=$this->data->suministro->prom_cp_precio;?></td>

                  <td><?=$this->data->suministro->prom_reliquidaciones;?></td>
                  <td><?=$this->data->suministro->prom_arr_tra;?></td>
                  <td><?=$this->data->suministro->prom_recargo_factor;?></td>
                  <td><?=$this->data->suministro->prom_otros_rec;?></td>
                  <td><?=$this->data->suministro->prom_tot_neto;?></td>
                  <td><?=$this->data->suministro->prom_tot_fac;?></td>
              <?php endif;?>
                  <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <?php endif;?>

      <?php if (!empty($this->data->ratios) || !empty($this->data->ratios_libre)) : ?>
      <div class="box">
        <div class="box-header with-border pointer">
          <div class="row">
            <div class="col-xs-8">
                <h3 class="box-title">Ratios año <?=$this->data->anno;?></h3>
            </div>
            <div class="col-xs-4">
                <a href="<?=base_url('export/ratio/?y=' . $this->data->anno . '&token=' . $this->data->suministro->id)?>" class="btn btn-default btn-flat margin-left">
                  <i class="fa fa-file-excel-o"></i> Exportar
                </a>
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if (!empty($this->data->ratios)) : ?>
      <div class="box box-warning">
        <div class="box-header with-border pointer">
          <div class="row">
            <div id="ratios-r" class="col-xs-8" data-toggle="collapse" data-target="#collapse2-r" aria-expanded="true" aria-controls="collapse">
              <span class="box-title">Cliente Regulado</span>
            </div>
          </div>
        </div>
        <div id="collapse2-r" class="box-body collapse in text-12">
            <table id="example2-r" class="table table-hover dark text-center">
                <thead>
                    <tr id="ratio_columns">
                        <th>Mes</th>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->ratio_2 == 1) :?>
                        <th>Energía <br> ($/kWh)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_3 == 1) :?>
                        <th>Fuera de Punta <br> ($/kW)</th>
                        <?php endif;?> 
                        <?php if($this->data->suministro->ratio_4 == 1) :?>
                        <th>Hora Punta <br> ($/kW)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_10 == 1) :?>
                        <th>Compra Potencia <br> ($/kW)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_5 == 1) :?>
                        <th>Monómico <br> ($/kWh)</th>
                        <?php endif;?> 
                        <?php if($this->data->suministro->ratio_6 == 1) :?>
                        <th>Proyectado  <br> (kWh)</th>
                        <?php endif;?> 
                        <?php if($this->data->suministro->ratio_7 == 1) :?>
                        <th>Indicador <br> (kWh/día)</th>
                        <?php endif;?> 
                        <?php if($this->data->suministro->ratio_8 == 1) :?> 
                        <th>Ahorro <br> ($)</th>
                        <?php endif;?> 
                        <?php if($this->data->suministro->ratio_9 == 1) :?>
                        <th>Optimización <br> ($)</th>
                        <?php endif;?> 
                        <th></th>
                    <?php else : ?>
                    <form id="columnas_ratio_form" action="<?=base_url('gestion/set_columnas_ratio');?>">
                            <input type="hidden" name="token" value="<?=$this->data->suministro->id?>">
                        <th>Energía <input class="hidden" type="checkbox" name="ratio_2" value="1" <?php if($this->data->suministro->ratio_2 == 1) :?>checked<?php endif; ?>> <br> ($/kWh)</th>
                        <th>Fuera de Punta <input class="hidden" type="checkbox" name="ratio_3" value="1" <?php if($this->data->suministro->ratio_3 == 1) :?>checked<?php endif; ?>> <br> ($/kW)</th>
                        <th>Hora Punta <input class="hidden" type="checkbox" name="ratio_4" value="1" <?php if($this->data->suministro->ratio_4 == 1) :?>checked<?php endif; ?>> <br> ($/kW)</th>
                        <th>Compra Potencia <input class="hidden" type="checkbox" name="ratio_10" value="1" <?php if($this->data->suministro->ratio_10 == 1) :?>checked<?php endif; ?>> <br> ($/kW)</th>
                        <th>Monómico <input class="hidden" type="checkbox" name="ratio_5" value="1" <?php if($this->data->suministro->ratio_5 == 1) :?>checked<?php endif; ?>> <br> ($/kWh)</th>
                        <th>Proyectado <input class="hidden" type="checkbox" name="ratio_6" value="1" <?php if($this->data->suministro->ratio_6 == 1) :?>checked<?php endif; ?>>  <br> (kWh)</th>
                        <th>Indicador <input class="hidden" type="checkbox" name="ratio_7" value="1" <?php if($this->data->suministro->ratio_7 == 1) :?>checked<?php endif; ?>> <br> (kWh/día)</th> 
                        <th>Ahorro <br> ($)</th>
                        <th>Optimización <br> ($)</th>   
                        <th>
                            <a id="setColumnasRatio" class="setColumnas hidden" href="javascript:void(0)" title="Guardar Visualización">
                                <i class="fa fa-save" data-target="saveColumnRatioModal"></i>
                            </a>

                            <a id="cancelColumnasRatio" class="cancelColumnas margin-left hidden" href="javascript:void(0)" title="Cancelar">
                                <i class="fa fa-times" data-target="ratio_columns" data-set-target="setColumnasRatio" data-edit-target="editColumnasRatio" data-form-target="columnas_ratio_form"></i>
                            </a>

                            <a id="editColumnasRatio" class="editColumnas" href="javascript:void(0)" title="Modificar Visualización">
                                <i class="fa fa-edit" data-target="ratio_columns" data-set-target="setColumnasRatio" data-cancel-target="cancelColumnasRatio"></i>
                            </a>
                        </th>
                    </form>
                    <?php endif;?>              
                    </tr>
                </thead>
                <tbody>
                    <?php $i=0; foreach ($this->data->ratios as $ratio) : $i++?>
                        <tr id="rat-<?=$i;?>" <?php if($this->data->user->id_perfil != '3' && $ratio->mod) :?>class="bg-yellow"<?php endif;?>>
                            <td><b><?=substr($ratio->mes, 5, 2);?></b></td>
                        <?php if($this->data->user->id_perfil == 3) : ?>
                            <?php if($this->data->suministro->ratio_2 == 1) :?>
                            <td><?=$ratio->energia_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_3 == 1) :?>
                            <td><?=$ratio->f_punta_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_4 == 1) :?>
                            <td><?=$ratio->e_punta_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_10 == 1) :?>
                            <td><?=$ratio->cp_ratio_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_5 == 1) :?>
                            <td><?=$ratio->monomico_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_6 == 1) :?>
                            <td><?=$ratio->produccion_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_7 == 1) :?>
                            <td><?=$ratio->indicador_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_8 == 1) :?>
                            <td><?=$ratio->ahorro_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_9 == 1) :?>
                            <td><?=$ratio->optimizacion_str;?></td>
                            <?php endif;?>
                        <?php else : ?>
                            <td><?=$ratio->energia_str;?></td>
                            <td><?=$ratio->f_punta_str;?></td>
                            <td><?=$ratio->e_punta_str;?></td>
                            <td><?=$ratio->cp_ratio_str;?></td>
                            <td><?=$ratio->monomico_str;?></td>
                            <td><?=$ratio->produccion_str;?></td>
                            <td><?=$ratio->indicador_str;?></td>
                            <td><?=$ratio->ahorro_str;?></td>
                            <td><?=$ratio->optimizacion_str;?></td>
                        <?php endif;?>
                        <?php if($this->data->user->id_perfil != '3') :?>
                            <td>
                                <a href="javascript:void(0);" class="mostrar" data-tar="rat-<?=$i?>" data-tar-f="rat-<?=$i?>-f" data-tar-c="rat-<?=$i?>-c" data-tar-i="rat-<?=$i?>-i" title="Ver Intervención">
                                    <i class="fa fa-eye"></i>
                                </a>

                                <a href="javascript:void(0);" class="ocultar hidden" data-tar="rat-<?=$i?>" data-tar-f="rat-<?=$i?>-f" data-tar-c="rat-<?=$i?>-c" data-tar-i="rat-<?=$i?>-i" title="Ocultar Intervención">
                                    <i class="fa fa-eye-slash"></i>
                                </a>
                            </td>
                        <?php else : ?>
                            <td></td>
                        <?php endif;?>
                        </tr>

                    <?php if($this->data->user->id_perfil != '3') :?>
                        <tr id="rat-<?=$i?>-f" class="bg-hover hidden">
                            <td class="text-right"><b>Ahorro Tarifa  =</b></td>
                            <td class="text-left" colspan="4">
                                TarA[<?=$ratio->tar_a_str?>] + TarB[<?=$ratio->tar_b_str?>] + TarC [<?=$ratio->tar_c_str?>] <br>
                                Donde: <br>
                                TarA = (Base FP[<?=$ratio->fp_base_str;?>] - FP Mes[<?=$ratio->fp_mes_str?>]) * Ratio FP Mes[<?=$ratio->f_punta_str?>] <br>
                                <?php if($ratio->r_etapa == 1) :?>
                                TarB = (Base EP[<?=$ratio->ep_base_str;?>] - EP Mes[<?=$ratio->ep_mes_str?>]) * Ratio EP Mes[<?=$ratio->e_punta_str?>] <?php if($ratio->tipo_tarifa == 20) :?> + (CP Mes[<?=$ratio->cp_mes_str;?>] * Ratio CP Mes[<?=$ratio->cp_ratio_str;?>])<?php endif;?> <br>
                                <?php else : ?>
                                TarB = (Base EP[<?=$ratio->ep_base_str;?>] * Tarifa Mes[<?=$ratio->tarifa_mes_str?>]) - (EP Mes[<?=$ratio->ep_mes_str?>] * Ratio EP Mes[<?=$ratio->e_punta_str?>]) <?php if($ratio->tipo_tarifa == 20) :?> + (CP Mes[<?=$ratio->cp_mes_str;?>] * Ratio CP Mes[<?=$ratio->cp_ratio_str;?>])<?php endif;?> <br>
                                <?php endif;?>
                                TarC = Base Prom. Varios[<?=$ratio->varios_base_str;?>] - Varios Mes[<?=$ratio->varios_mes_str?>] <br>

                            </td>
                            <td class="text-right"><b>Optimización  =</b></td>
                            <td class="text-left" colspan="5">
                                (Indicador Mes Base[<?=$ratio->indicador_base_str?>] - Indicador Mes[<?=$ratio->indicador_str?>]) * Proyectado [<?=$ratio->produccion_str?>] * Precio Energía[<?=$ratio->energia_str;?>]<br>
                                
                                <?php if($ratio->mes >= '2018-04') :?>
                                + ((Indicador Mes Base[<?=$ratio->indicador_base_str?>] - Indicador Mes[<?=$ratio->indicador_str?>]) * Valor Transmisión[<?=$ratio->transmision_mes_str?>])
                                <?php endif;?>
                            </td>
                        </tr>

                        <tr id="rat-<?=$i?>-c" class="bg-hover hidden text-left">
                            <td class="text-right"><b>Calculado</b></td>
                            <td><?=$ratio->energia;?></td>
                            <td><?=$ratio->f_punta;?></td>
                            <td><?=$ratio->e_punta;?></td>
                            <td><?=$ratio->cp_ratio;?></td>
                            <td><?=$ratio->monomico;?></td>
                            <td><?=$ratio->produccion;?></td>
                            <td><?=$ratio->indicador;?></td>
                            <td><?=$ratio->ahorro;?></td>
                            <td><?=$ratio->optimizacion;?></td>
                            <td></td>
                        </tr>
                        <?php if($this->data->user->id_perfil != '4') :?>
                            <form id="rat-<?=$i?>-form" action="<?=base_url('gestion/set_intervencion');?>">

                            <tr id="rat-<?=$i?>-i" class="bg-hover hidden">
                                <td class="text-right"><b>Intervenido</b></td>
                            <?php if(!empty($ratio->energia_in)) :?>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="energia" class="num" value="<?=$ratio->energia_in;?>" data-target="rat-<?=$i?>-i" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="fp" class="num" value="<?=$ratio->f_punta_in;?>" data-target="rat-<?=$i?>-i" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="ep" class="num" value="<?=$ratio->e_punta_in;?>" data-target="rat-<?=$i?>-i" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="cp" class="num" value="<?=$ratio->cp_in;?>" data-target="rat-<?=$i?>-i" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="monomico" class="num" value="<?=$ratio->monomico_in;?>" data-target="rat-<?=$i?>-i" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="produccion" class="num" value="<?=$ratio->produccion_in;?>" data-target="rat-<?=$i?>-i" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="indicador" class="num" value="<?=$ratio->indicador_in;?>" data-target="rat-<?=$i?>-i" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td class="ahorro_in" data-value="<?=$ratio->ahorro_in;?>">
                                    <span><?=$ratio->ahorro_in;?></span>
                                    <input type="hidden" name="ahorro" value="<?=$ratio->ahorro_in;?>">
                                </td>
                                <td class="optimizacion_in" data-value="<?=$ratio->optimizacion_in;?>">
                                    <span><?=$ratio->optimizacion_in;?></span>
                                    <input type="hidden" name="optimizacion" value="<?=$ratio->optimizacion_in;?>">
                                </td>
                            <?php else :?>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="energia" class="num" value="<?=$ratio->energia;?>" data-target="rat-<?=$i?>-i" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td> 
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="fp" class="num" value="<?=$ratio->f_punta;?>" data-target="rat-<?=$i?>-i" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="ep" class="num" value="<?=$ratio->e_punta;?>" data-target="rat-<?=$i?>-i" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="cp" class="num" value="<?=$ratio->cp_ratio;?>" data-target="rat-<?=$i?>-i" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="monomico" class="num" value="<?=$ratio->monomico;?>" data-target="rat-<?=$i?>-i" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="produccion" class="num" value="<?=$ratio->produccion;?>" data-target="rat-<?=$i?>-i" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="indicador" class="num" value="<?=$ratio->indicador;?>" data-target="rat-<?=$i?>-i" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td class="ahorro_in" data-value="<?=$ratio->ahorro;?>">
                                    <span><?=$ratio->ahorro;?></span>
                                    <input type="hidden" name="ahorro" value="<?=$ratio->ahorro;?>">
                                </td>
                                <td class="optimizacion_in" data-value="<?=$ratio->optimizacion;?>">
                                    <span><?=$ratio->optimizacion;?></span>
                                    <input type="hidden" name="optimizacion" value="<?=$ratio->optimizacion;?>">
                                </td>
                            <?php endif;?>
                                <td>
                                    <input type="hidden" name="etapa" value="<?=$this->data->suministro->etapa;?>">
                                    <input type="hidden" name="fp_base" value="<?=$ratio->fp_base;?>">
                                    <input type="hidden" name="ep_base" value="<?=$ratio->ep_base;?>">
                                    <input type="hidden" name="varios_base" value="<?=$ratio->varios_base;?>">
                                    <input type="hidden" name="indicador_base" value="<?=$ratio->indicador_base;?>">
                                    <input type="hidden" name="fp_mes" value="<?=$ratio->fp_mes;?>">
                                    <input type="hidden" name="ep_mes" value="<?=$ratio->ep_mes;?>">
                                    <input type="hidden" name="cp_mes" value="<?=$ratio->cp_mes;?>">
                                    <input type="hidden" name="varios_mes" value="<?=$ratio->varios_mes;?>">
                                    <input type="hidden" name="tarifa_mes" value="<?=$ratio->tarifa_mes;?>">
                                    <input type="hidden" name="transmision_mes" value="<?=$ratio->transmision_mes;?>">
                                    <input type="hidden" name="mes" value="<?=$ratio->mes;?>">
                                    <input type="hidden" name="tipo_tarifa" value="<?=$ratio->tipo_tarifa;?>">
                                    <input type="hidden" name="token" value="<?=$ratio->id;?>">
                                    <a href="javascript:void(0);" class="irat" data-target="rat-<?=$i?>-form" data-tar-c="rat-<?=$i?>-c" data-tar-i="rat-<?=$i?>-i" title="Guardar">
                                        <i class="fa fa-save"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr id="rat-<?=$i?>-io" class="bg-hover hidden">
                                <td class="text-right"><b>Observación</b></td>
                                <td class="text-left" colspan="10">
                                    <div class="form-group">
                                        <input type="text" name="observacion" class="large" value="<?=$ratio->observacion;?>">
                                    </div>
                                </td>
                            </tr>
                            </form>
                        <?php else : ?>
                            <tr id="rat-<?=$i?>-i" class="bg-hover hidden">
                                <td class="text-right"><b>Intervenido</b></td>
                            <?php if(!empty($ratio->energia_in)) :?>
                                <td>
                                    <?=$ratio->energia_in;?>
                                </td>
                                <td>
                                    <?=$ratio->f_punta_in;?>
                                </td>
                                <td>
                                    <?=$ratio->e_punta_in;?>
                                </td>
                                <td>
                                    <?=$ratio->cp_in;?>
                                </td>
                                <td>
                                    <?=$ratio->monomico_in;?>
                                </td>
                                <td>
                                    <?=$ratio->produccion_in;?>
                                </td>
                                <td>
                                    <?=$ratio->indicador_in;?>
                                </td>
                                <td class="ahorro_in">
                                    <?=$ratio->ahorro_in;?>
                                </td>
                                <td class="optimizacion_in">
                                    <?=$ratio->optimizacion_in;?>
                                </td>
                            <?php else :?>
                                <td>
                                    <?=$ratio->energia;?>
                                </td> 
                                <td>
                                    <?=$ratio->f_punta;?>
                                </td>
                                <td>
                                    <?=$ratio->e_punta;?>
                                </td>
                                <td>
                                    <?=$ratio->cp_ratio;?>
                                </td>
                                <td>
                                    <?=$ratio->monomico;?>
                                </td>
                                <td>
                                    <?=$ratio->produccion;?>
                                </td>
                                <td>
                                    <?=$ratio->indicador;?>
                                </td>
                                <td class="ahorro_in">
                                    <?=$ratio->ahorro;?>
                                </td>
                                <td class="optimizacion_in">
                                    <?=$ratio->optimizacion;?>
                                </td>
                            <?php endif;?>
                                <td>
                                </td>
                            </tr>
                            <tr id="rat-<?=$i?>-io" class="bg-hover hidden">
                                <td class="text-right"><b>Observación</b></td>
                                <td class="text-left" colspan="10">
                                    <?=$ratio->observacion;?>
                                </td>
                            </tr>
                        <?php endif;?>
                    <?php endif;?>
                    <?php endforeach; ?>
                        
                </tbody>
                <tfoot>
                    <?php if(empty($this->data->ratios_libre)) : ?>
                    <tr>
                        <td class="text-right"><b>Totales:</b></td>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->ratio_2 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_energia;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_3 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_f_punta;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_4 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_e_punta;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_10 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_cp;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_5 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_monomico;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_6 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_produccion;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_7 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_indicador;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_8 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_ahorro;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_9 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_optimizacion;?></b></td>
                        <?php endif;?>
                    <?php else : ?>
                   
                        <td><b><?=$this->data->suministro->ratio->tot_energia;?></b></td>
                       
                        <td><b><?=$this->data->suministro->ratio->tot_f_punta;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->tot_e_punta;?></b></td>

                        <td><b><?=$this->data->suministro->ratio->tot_cp;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->tot_monomico;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->tot_produccion;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->tot_indicador;?></b></td>
                       
                        <td><b><?=$this->data->suministro->ratio->tot_ahorro;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->tot_optimizacion;?></b></td>

                    <?php endif;?>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right"><b>Promedios:</b></td>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->ratio_2 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_energia;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_3 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_f_punta;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_4 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_e_punta;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_10 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_cp;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_5 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_monomico;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_6 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_produccion;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_7 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_indicador;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_8 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_ahorro;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_9 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_optimizacion;?></b></td>
                        <?php endif;?>
                    <?php else : ?>

                        <td><b><?=$this->data->suministro->ratio->prom_energia;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->prom_f_punta;?></b></td>
                    
                        <td><b><?=$this->data->suministro->ratio->prom_e_punta;?></b></td>

                        <td><b><?=$this->data->suministro->ratio->prom_cp;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->prom_monomico;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->prom_produccion;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->prom_indicador;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->prom_ahorro;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->prom_optimizacion;?></b></td>
 
                    <?php endif;?>
                        <td></td>
                    </tr>
                    <?php endif;?>
                    <tr>
                        <th>Mes</th>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->ratio_2 == 1) :?>
                        <th>Energía</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_3 == 1) :?>
                        <th>Fuera de Punta</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_4 == 1) :?>
                        <th>Hora Punta</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_10 == 1) :?>
                        <th>Compra Potencia</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_5 == 1) :?>
                        <th>Monómico</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_6 == 1) :?>
                        <th>Prod</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_7 == 1) :?>
                        <th>Indicador</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_8 == 1) :?> 
                        <th>Ahorro</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_9 == 1) :?>
                        <th>Optimización</th>
                        <?php endif;?>
                    <?php else : ?>
                        <th>Energía</th>
                        <th>Fuera de Punta</th>
                        <th>Hora Punta</th>
                        <th>Compra Potencia</th>
                        <th>Monómico</th>
                        <th>Proyectado</th>
                        <th>Indicador</th> 
                        <th>Ahorro</th>
                        <th>Optimización</th>
                    <?php endif;?>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
              
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      <?php endif; ?>


      <?php if (!empty($this->data->ratios_libre)) : ?>
      <div class="box box-warning">
        <div class="box-header with-border pointer">
          <div class="row">
            <div id="ratios-l" class="col-xs-12" data-toggle="collapse" data-target="#collapse2-r_libre" aria-expanded="true" aria-controls="collapse">
              <span class="box-title">Cliente Libre</span>
            </div>
          </div>
        </div>
        <?php
          $colspan_gen_r_a = cal_colspan([$this->data->suministro->ratio_11, $this->data->suministro->ratio_12, $this->data->suministro->ratio_13, $this->data->suministro->ratio_21, $this->data->suministro->ratio_20]);
          $colspan_dis_r_a = cal_colspan([$this->data->suministro->ratio_2, $this->data->suministro->ratio_3, $this->data->suministro->ratio_4, $this->data->suministro->ratio_10, $this->data->suministro->ratio_5, $this->data->suministro->ratio_19]);
          $colspan_gen_r_b = cal_colspan([$this->data->suministro->ratio_14, $this->data->suministro->ratio_16, $this->data->suministro->ratio_18]);
          $colspan_dis_r_b = cal_colspan([$this->data->suministro->ratio_7, $this->data->suministro->ratio_15, $this->data->suministro->ratio_17]);
          $colspan_rr_a = cal_colspan([$this->data->suministro->ratio_6]);
        ?>
        <div id="collapse2-r_libre" class="box-body collapse in text-12">
            <table id="example2-r_libre" class="table table-hover dark text-center">
                <thead>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                      <tr>
                        <th colspan="1"></th>
                        <?php if($colspan_gen_r_a > 0) :?>
                        <th colspan="<?=$colspan_gen_a;?>">Generación</th>
                        <?php endif; ?>
                        <?php if($colspan_dis_r_a > 0) :?>
                        <th colspan="<?=$colspan_dis_a;?>">Distribución</th>
                        <?php endif; ?>
                        <?php if($colspan_rr_a > 0) :?>
                        <th colspan="<?=$colspan_rr_a;?>"></th>
                        <?php endif; ?>
                        <?php if($colspan_gen_r_b > 0) :?>
                        <th colspan="<?=$colspan_gen_b;?>">Generación</th>
                        <?php endif; ?>
                        <?php if($colspan_dis_r_b > 0) :?>
                        <th colspan="<?=$colspan_dis_b;?>">Distribución</th>
                        <th colspan="1"></th>
                        <?php endif; ?>
                      </tr>
                    <?php else : ?>
                      <tr>
                        <th colspan='1'></th>
                        <th colspan='5'>Generación</th>
                        <th colspan='6'>Distribución</th>
                        <th colspan='1'></th>
                        <th colspan='3'>Generación</th>
                        <th colspan='3'>Distribución</th>
                        <th colspan='2'></th>
                        <th colspan="1"></th>
                      </tr>
                    <?php endif; ?>
                    <tr id="ratio_columns_libre">
                        <th>Mes</th>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->ratio_11 == 1) :?>
                        <th>Energía <br> ($/kWh)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_21 == 1) :?>
                        <th>Mínimo Técnico <br> ($/kWh)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_20 == 1) :?>
                        <th>Armonización <br> ($/kWh)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_12 == 1) :?>
                        <th>Hora Punta <br> ($/kW)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_13 == 1) :?>
                        <th>Monómico <br> ($/kWh)</th>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_2 == 1) :?>
                        <th>Energía <br> ($/kWh)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_19 == 1) :?>
                        <th>Transmisión <br> ($/kWh)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_3 == 1) :?>
                        <th>Fuera de Punta <br> ($/kW)</th>
                        <?php endif;?> 
                        <?php if($this->data->suministro->ratio_4 == 1) :?>
                        <th>Hora Punta <br> ($/kW)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_10 == 1) :?>
                        <th>Compra Potencia <br> ($/kW)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_5 == 1) :?>
                        <th>Monómico <br> ($/kWh)</th>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_6 == 1) :?>
                        <th>Proyectado  <br> (kWh)</th>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_14 == 1) :?>
                        <th>Indicador <br> (kWh/día)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_16 == 1) :?> 
                        <th>Ahorro <br> ($)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_18 == 1) :?>
                        <th>Optimización <br> ($)</th>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_7 == 1) :?>
                        <th>Indicador <br> (kWh/día)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_15 == 1) :?> 
                        <th>Ahorro <br> ($)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_17 == 1) :?>
                        <th>Optimización <br> ($)</th>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_8 == 1) :?> 
                        <th>Tot. Ahorro <br> ($)</th>
                        <?php endif;?> 
                        <?php if($this->data->suministro->ratio_9 == 1) :?>
                        <th>Tot. Optimización <br> ($)</th>
                        <?php endif;?> 
                        <th></th>
                    <?php else : ?>
                    <form id="columnas_ratio_form_libre" action="<?=base_url('gestion/set_columnas_ratio');?>">
                        <input type="hidden" name="token" value="<?=$this->data->suministro->id?>">
                        
                        <th>Energía <input class="hidden" type="checkbox" name="ratio_11" value="1" <?php if($this->data->suministro->ratio_11 == 1) :?>checked<?php endif; ?>> <br> ($/kWh)</th>
                        <th>Mínimo Técnico <input class="hidden" type="checkbox" name="ratio_21" value="1" <?php if($this->data->suministro->ratio_21 == 1) :?>checked<?php endif; ?>> <br> ($/kWh)</th>
                        <th>Armonización <input class="hidden" type="checkbox" name="ratio_20" value="1" <?php if($this->data->suministro->ratio_20 == 1) :?>checked<?php endif; ?>> <br> ($/kWh)</th>
                        <th>Hora Punta <input class="hidden" type="checkbox" name="ratio_12" value="1" <?php if($this->data->suministro->ratio_12 == 1) :?>checked<?php endif; ?>> <br> ($/kW)</th>
                        <th>Monómico <input class="hidden" type="checkbox" name="ratio_13" value="1" <?php if($this->data->suministro->ratio_13 == 1) :?>checked<?php endif; ?>> <br> ($/kWh)</th>

                        <th>Energía <input class="hidden" type="checkbox" name="ratio_2" value="1" <?php if($this->data->suministro->ratio_2 == 1) :?>checked<?php endif; ?>> <br> ($/kWh)</th>
                        <th>Transmisión <input class="hidden" type="checkbox" name="ratio_19" value="1" <?php if($this->data->suministro->ratio_19 == 1) :?>checked<?php endif; ?>> <br> ($/kWh)</th>
                        <th>Fuera de Punta <input class="hidden" type="checkbox" name="ratio_3" value="1" <?php if($this->data->suministro->ratio_3 == 1) :?>checked<?php endif; ?>> <br> ($/kW)</th>
                        <th>Hora Punta <input class="hidden" type="checkbox" name="ratio_4" value="1" <?php if($this->data->suministro->ratio_4 == 1) :?>checked<?php endif; ?>> <br> ($/kW)</th>
                        <th>Compra Potencia <input class="hidden" type="checkbox" name="ratio_10" value="1" <?php if($this->data->suministro->ratio_10 == 1) :?>checked<?php endif; ?>> <br> ($/kW)</th>
                        <th>Monómico <input class="hidden" type="checkbox" name="ratio_5" value="1" <?php if($this->data->suministro->ratio_5 == 1) :?>checked<?php endif; ?>> <br> ($/kWh)</th>
                        
                        <th>Proyectado <input class="hidden" type="checkbox" name="ratio_6" value="1" <?php if($this->data->suministro->ratio_6 == 1) :?>checked<?php endif; ?>>  <br> (kWh)</th>

                        <th>Indicador <input class="hidden" type="checkbox" name="ratio_14" value="1" <?php if($this->data->suministro->ratio_14 == 1) :?>checked<?php endif; ?>> <br> (kWh/día)</th> 
                        <th>Ahorro <input class="hidden" type="checkbox" name="ratio_16" value="1" <?php if($this->data->suministro->ratio_16 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                        <th>Optimización <input class="hidden" type="checkbox" name="ratio_18" value="1" <?php if($this->data->suministro->ratio_18 == 1) :?>checked<?php endif; ?>> <br> ($)</th>

                        <th>Indicador <input class="hidden" type="checkbox" name="ratio_7" value="1" <?php if($this->data->suministro->ratio_7 == 1) :?>checked<?php endif; ?>> <br> (kWh/día)</th> 
                        <th>Ahorro <input class="hidden" type="checkbox" name="ratio_15" value="1" <?php if($this->data->suministro->ratio_15 == 1) :?>checked<?php endif; ?>> <br> ($)</th>
                        <th>Optimización <input class="hidden" type="checkbox" name="ratio_17" value="1" <?php if($this->data->suministro->ratio_17 == 1) :?>checked<?php endif; ?>> <br> ($)</th>

                        <th>Tot. Ahorro<br> ($)</th>
                        <th>Tot. Optimización<br> ($)</th>  
                        <th>
                            <a id="setColumnasRatio_libre" class="setColumnas hidden" href="javascript:void(0)" title="Guardar Visualización">
                                <i class="fa fa-save" data-target="saveColumnRatioModalLibre"></i>
                            </a>

                            <a id="cancelColumnasRatio_libre" class="cancelColumnas margin-left hidden" href="javascript:void(0)" title="Cancelar">
                                <i class="fa fa-times" data-target="ratio_columns_libre" data-set-target="setColumnasRatio_libre" data-edit-target="editColumnasRatio_libre" data-form-target="columnas_ratio_form_libre"></i>
                            </a>

                            <a id="editColumnasRatio_libre" class="editColumnas" href="javascript:void(0)" title="Modificar Visualización">
                                <i class="fa fa-edit" data-target="ratio_columns_libre" data-set-target="setColumnasRatio_libre" data-cancel-target="cancelColumnasRatio_libre"></i>
                            </a>
                        </th>
                    </form>
                    <?php endif;?>              
                    </tr>
                </thead>
                <tbody>
                    <?php $i=0; foreach ($this->data->ratios_libre as $ratio) : $i++?>
                        <tr id="rat-<?=$i;?>-libre" <?php if($this->data->user->id_perfil != '3' && $ratio->mod) :?>class="bg-yellow"<?php endif;?>>
                            <td><b><?=substr($ratio->mes, 5, 2);?></b></td>
                        <?php if($this->data->user->id_perfil == 3) : ?>
                            <?php if($this->data->suministro->ratio_11 == 1) :?>
                            <td><?=$ratio->energia_str_gen;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_21 == 1) :?>
                            <td><?=$ratio->tec_ratio_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_20 == 1) :?>
                            <td><?=$ratio->armo_ratio_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_12 == 1) :?>
                            <td><?=$ratio->e_punta_str_gen;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_13 == 1) :?>
                            <td><?=$ratio->monomico_str;?></td>
                            <?php endif;?>

                            <?php if($this->data->suministro->ratio_2 == 1) :?>
                            <td><?=$ratio->energia_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_19 == 1) :?>
                            <td><?=$ratio->trans_ratio_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_3 == 1) :?>
                            <td><?=$ratio->f_punta_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_4 == 1) :?>
                            <td><?=$ratio->e_punta_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_10 == 1) :?>
                            <td><?=$ratio->cp_ratio_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_5 == 1) :?>
                            <td><?=$ratio->monomico_str;?></td>
                            <?php endif;?>

                            <?php if($this->data->suministro->ratio_6 == 1) :?>
                            <td><?=$ratio->produccion_str;?></td>
                            <?php endif;?>

                            <?php if($this->data->suministro->ratio_14 == 1) :?>
                            <td><?=$ratio->indicador_str_gen;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_16 == 1) :?>
                            <td><?=$ratio->ahorro_str_gen;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_18 == 1) :?>
                            <td><?=$ratio->optimizacion_str_gen;?></td>
                            <?php endif;?>

                            <?php if($this->data->suministro->ratio_7 == 1) :?>
                            <td><?=$ratio->indicador_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_15 == 1) :?>
                            <td><?=$ratio->ahorro_str_dis;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_17 == 1) :?>
                            <td><?=$ratio->optimizacion_str_dis;?></td>
                            <?php endif;?>

                            <?php if($this->data->suministro->ratio_15 == 1) :?>
                            <td><?=$ratio->ahorro_str;?></td>
                            <?php endif;?>
                            <?php if($this->data->suministro->ratio_17 == 1) :?>
                            <td><?=$ratio->optimizacion_str;?></td>
                            <?php endif;?>
                        <?php else : ?>
                            <td><?=$ratio->energia_str_gen;?></td>
                            <td><?=$ratio->tec_ratio_str;?></td>
                            <td><?=$ratio->armo_ratio_str;?></td>
                            <td><?=$ratio->e_punta_str_gen;?></td>
                            <td><?=$ratio->monomico_str_gen;?></td>

                            <td><?=$ratio->energia_str;?></td>
                            <td><?=$ratio->trans_ratio_str;?></td>
                            <td><?=$ratio->f_punta_str;?></td>
                            <td><?=$ratio->e_punta_str;?></td>
                            <td><?=$ratio->cp_ratio_str;?></td>
                            <td><?=$ratio->monomico_str;?></td>

                            <td><?=$ratio->produccion_str;?></td>

                            <td><?=$ratio->indicador_str_gen;?></td>
                            <td><?=$ratio->ahorro_str_gen;?></td>
                            <td><?=$ratio->optimizacion_str_gen;?></td>

                            <td><?=$ratio->indicador_str;?></td>
                            <td><?=$ratio->ahorro_str_dis;?></td>
                            <td><?=$ratio->optimizacion_str_dis;?></td>

                            <td><?=$ratio->ahorro_str;?></td>
                            <td><?=$ratio->optimizacion_str;?></td>
                        <?php endif;?>
                        <?php if($this->data->user->id_perfil != '3') :?>
                            <td>
                                <a href="javascript:void(0);" class="mostrar" data-tar="rat-<?=$i?>-libre" data-tar-f="rat-<?=$i?>-f-libre" data-tar-c="rat-<?=$i?>-c-libre" data-tar-i="rat-<?=$i?>-i-libre" title="Ver Intervención">
                                    <i class="fa fa-eye"></i>
                                </a>

                                <a href="javascript:void(0);" class="ocultar hidden" data-tar="rat-<?=$i?>-libre" data-tar-f="rat-<?=$i?>-f-libre" data-tar-c="rat-<?=$i?>-c-libre" data-tar-i="rat-<?=$i?>-i-libre" title="Ocultar Intervención">
                                    <i class="fa fa-eye-slash"></i>
                                </a>
                            </td>
                        <?php else : ?>
                            <td></td>
                        <?php endif;?>
                        </tr>

                    <?php if($this->data->user->id_perfil != '3') :?>
                        <tr id="rat-<?=$i?>-f-libre" class="bg-hover hidden">
                            <td class="text-right"><b>Ahorro Tarifa  =</b></td>
                            <td class="text-left" colspan="9">
                                TarA[<?=$ratio->tar_a_str?>] + TarBG[<?=$ratio->tar_bg_str?>] + TarBD[<?=$ratio->tar_b_str?>] + TarCD [<?=$ratio->tar_c_str?>] <br>
                                Donde: <br>
                                TarA = (Base FPD[<?=$ratio->fp_base_str;?>] - FPD Mes[<?=$ratio->fp_mes_str?>]) * Ratio FPD Mes[<?=$ratio->f_punta_str?>] <br>
                                TarBG = (Base HPG[<?=$ratio->ep_base_str;?>] - HPG Mes[<?=$ratio->ep_mes_str_gen?>]) * Ratio HPG Mes[<?=$ratio->e_punta_str_gen?>] <br>
                                TarBD = (Base HPD[<?=$ratio->ep_base_str;?>] - HPD Mes[<?=$ratio->ep_mes_str?>]) * Ratio HPD Mes[<?=$ratio->e_punta_str?>] <br>
                                TarCD = Base Prom. Varios[<?=$ratio->varios_base_str;?>] - Varios Mes[<?=$ratio->varios_mes_str?>] <br>

                            </td>
                            <td class="text-right"><b>Optimización =</b></td>
                            <td class="text-left" colspan="11">
                                OPTDG[<?=$ratio->optimizacion_str_gen?>] + OPTD[<?=$ratio->optimizacion_str_dis?>] <br>
                                Donde: <br>
                                OPTDG = (Indicador Mes Base[<?=$ratio->indicador_base_str?>] - Indicador Mes[<?=$ratio->indicador_str_gen?>]) * (Precio Energía[<?=$ratio->energia_str_gen;?>] + Precio Min. Tec.[<?=$ratio->tec_ratio_str?>] + Precio Armonización.[<?=$ratio->armo_ratio_str?>]) * Proyectado [<?=$ratio->produccion_str?>] <br>
                                OPTD = (Indicador Mes Base[<?=$ratio->indicador_base_str?>] - Indicador Mes[<?=$ratio->indicador_str?>]) * (Precio Energía[<?=$ratio->energia_str;?>] + Precio Transmisión[<?=$ratio->trans_ratio_str?>]) * Proyectado [<?=$ratio->produccion_str?>] <br>
                            </td>
                        </tr>

                        <tr id="rat-<?=$i?>-c-libre" class="bg-hover hidden text-left">
                            <td class="text-right"><b>Calculado</b></td>
                            <td><?=$ratio->energia_gen;?></td>
                            <td><?=$ratio->tec_ratio;?></td>
                            <td><?=$ratio->armo_ratio;?></td>
                            <td><?=$ratio->e_punta_gen;?></td>
                            <td><?=$ratio->monomico_gen;?></td>

                            <td><?=$ratio->energia;?></td>
                            <td><?=$ratio->trans_ratio;?></td>
                            <td><?=$ratio->f_punta;?></td>
                            <td><?=$ratio->e_punta;?></td>
                            <td><?=$ratio->cp_ratio;?></td>
                            <td><?=$ratio->monomico;?></td>

                            <td><?=$ratio->produccion;?></td>

                            <td><?=$ratio->indicador_gen;?></td>
                            <td><?=$ratio->ahorro_gen;?></td>
                            <td><?=$ratio->optimizacion_gen;?></td>

                            <td><?=$ratio->indicador;?></td>
                            <td><?=$ratio->ahorro_dis;?></td>
                            <td><?=$ratio->optimizacion_dis;?></td>

                            <td><?=$ratio->ahorro;?></td>
                            <td><?=$ratio->optimizacion;?></td>
                            <td></td>
                        </tr>
                        <?php if($this->data->user->id_perfil != '4') :?>
                            <form id="rat-<?=$i?>-form-libre" action="<?=base_url('gestion/set_intervencion');?>">

                            <tr id="rat-<?=$i?>-i-libre" class="bg-hover hidden">
                                <td class="text-right"><b>Intervenido</b></td>
                            <?php if(!empty($ratio->energia_in)) :?>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="energia_gen" class="num" value="<?=$ratio->energia_in_gen;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="tec_r" class="num" value="<?=$ratio->tec_ratio_in;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="armo_r" class="num" value="<?=$ratio->armo_ratio_in;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="ep_gen" class="num" value="<?=$ratio->e_punta_in_gen;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="monomico_gen" class="num" value="<?=$ratio->monomico_in_gen;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="text" name="energia" class="num" value="<?=$ratio->energia_in;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="trans_r" class="num" value="<?=$ratio->trans_ratio_in;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="fp" class="num" value="<?=$ratio->f_punta_in;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="ep" class="num" value="<?=$ratio->e_punta_in;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="cp" class="num" value="<?=$ratio->cp_in;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="monomico" class="num" value="<?=$ratio->monomico_in;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="text" name="produccion" class="num" value="<?=$ratio->produccion_in;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="text" name="indicador_gen" class="num" value="<?=$ratio->indicador_in_gen;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td class="ahorro_in_gen" data-value="<?=$ratio->ahorro_in_gen;?>">
                                    <span><?=$ratio->ahorro_in_gen;?></span>
                                    <input type="hidden" name="ahorro_gen" value="<?=$ratio->ahorro_in_gen;?>">
                                </td>
                                <td class="optimizacion_in_gen" data-value="<?=$ratio->optimizacion_in_gen;?>">
                                    <span><?=$ratio->optimizacion_in_gen;?></span>
                                    <input type="hidden" name="optimizacion_gen" value="<?=$ratio->optimizacion_in_gen;?>">
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="text" name="indicador" class="num" value="<?=$ratio->indicador_in;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td class="ahorro_in_dis" data-value="<?=$ratio->ahorro_in_dis;?>">
                                    <span><?=$ratio->ahorro_in_dis;?></span>
                                    <input type="hidden" name="ahorro_dis" value="<?=$ratio->ahorro_in_dis;?>">
                                </td>
                                <td class="optimizacion_in_dis" data-value="<?=$ratio->optimizacion_in_dis;?>">
                                    <span><?=$ratio->optimizacion_in_dis;?></span>
                                    <input type="hidden" name="optimizacion_dis" value="<?=$ratio->optimizacion_in_dis;?>">
                                </td>

                                <td class="ahorro_in" data-value="<?=$ratio->ahorro_in;?>">
                                    <span><?=$ratio->ahorro_in;?></span>
                                    <input type="hidden" name="ahorro" value="<?=$ratio->ahorro_in;?>">
                                </td>
                                <td class="optimizacion_in" data-value="<?=$ratio->optimizacion_in;?>">
                                    <span><?=$ratio->optimizacion_in;?></span>
                                    <input type="hidden" name="optimizacion" value="<?=$ratio->optimizacion_in;?>">
                                </td>
                            <?php else :?>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="energia_gen" class="num" value="<?=$ratio->energia_gen;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="tec_r" class="num" value="<?=$ratio->tec_ratio;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="armo_r" class="num" value="<?=$ratio->armo_ratio;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="ep_gen" class="num" value="<?=$ratio->e_punta_gen;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="monomico_gen" class="num" value="<?=$ratio->monomico_gen;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="text" name="energia" class="num" value="<?=$ratio->energia;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="trans_r" class="num" value="<?=$ratio->trans_ratio;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="fp" class="num" value="<?=$ratio->f_punta;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="ep" class="num" value="<?=$ratio->e_punta;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="cp" class="num" value="<?=$ratio->cp_ratio;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="monomico" class="num" value="<?=$ratio->monomico;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="text" name="produccion" class="num" value="<?=$ratio->produccion;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="text" name="indicador_gen" class="num" value="<?=$ratio->indicador_gen;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td class="ahorro_in_gen" data-value="<?=$ratio->ahorro_gen;?>">
                                    <span><?=$ratio->ahorro_gen;?></span>
                                    <input type="hidden" name="ahorro_gen" value="<?=$ratio->ahorro_gen;?>">
                                </td>
                                <td class="optimizacion_in_gen" data-value="<?=$ratio->optimizacion_gen;?>">
                                    <span><?=$ratio->optimizacion_gen;?></span>
                                    <input type="hidden" name="optimizacion_gen" value="<?=$ratio->optimizacion_gen;?>">
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="text" name="indicador" class="num" value="<?=$ratio->indicador_in;?>" data-target="rat-<?=$i?>-i-libre" onblur="setRatio(event)" pattern="^\d*(\.\d{0,2})?$" disabled>
                                    </div>
                                </td>
                                <td class="ahorro_in_dis" data-value="<?=$ratio->ahorro_dis;?>">
                                    <span><?=$ratio->ahorro_dis;?></span>
                                    <input type="hidden" name="ahorro_dis" value="<?=$ratio->ahorro_dis;?>">
                                </td>
                                <td class="optimizacion_in_dis" data-value="<?=$ratio->optimizacion_dis;?>">
                                    <span><?=$ratio->optimizacion_dis;?></span>
                                    <input type="hidden" name="optimizacion_dis" value="<?=$ratio->optimizacion_dis;?>">
                                </td>

                                <td class="ahorro_in" data-value="<?=$ratio->ahorro;?>">
                                    <span><?=$ratio->ahorro;?></span>
                                    <input type="hidden" name="ahorro" value="<?=$ratio->ahorro;?>">
                                </td>
                                <td class="optimizacion_in" data-value="<?=$ratio->optimizacion;?>">
                                    <span><?=$ratio->optimizacion;?></span>
                                    <input type="hidden" name="optimizacion" value="<?=$ratio->optimizacion;?>">
                                </td>
                            <?php endif;?>
                                <td>
                                    <input type="hidden" name="etapa" value="<?=$this->data->suministro->etapa;?>">
                                    <input type="hidden" name="fp_base" value="<?=$ratio->fp_base;?>">
                                    <input type="hidden" name="ep_base" value="<?=$ratio->ep_base;?>">
                                    <input type="hidden" name="varios_base" value="<?=$ratio->varios_base;?>">
                                    <input type="hidden" name="indicador_base" value="<?=$ratio->indicador_base;?>">
                                    <input type="hidden" name="fp_mes" value="<?=$ratio->fp_mes;?>">
                                    <input type="hidden" name="ep_mes" value="<?=$ratio->ep_mes;?>">
                                    <input type="hidden" name="cp_mes" value="<?=$ratio->cp_mes;?>">
                                    <input type="hidden" name="varios_mes" value="<?=$ratio->varios_mes;?>">
                                    <input type="hidden" name="tarifa_mes" value="<?=$ratio->tarifa_mes;?>">
                                    <input type="hidden" name="transmision_mes" value="<?=$ratio->transmision_mes;?>">
                                    <input type="hidden" name="mes" value="<?=$ratio->mes;?>">
                                    <input type="hidden" name="tipo_tarifa" value="<?=$ratio->tipo_tarifa;?>">
                                    <input type="hidden" name="token" value="<?=$ratio->id;?>">
                                    <input type="hidden" name="ep_mes_gen" value="<?=$ratio->ep_mes_gen;?>">
                                    <input type="hidden" name="c_armo_mes" value="<?=$ratio->c_armo_mes;?>">
                                    <input type="hidden" name="c_tec_mes" value="<?=$ratio->c_tec_mes;?>">
                                    <a href="javascript:void(0);" class="irat" data-target="rat-<?=$i?>-form-libre" data-tar-c="rat-<?=$i?>-c-libre" data-tar-i="rat-<?=$i?>-i-libre" title="Guardar">
                                        <i class="fa fa-save"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr id="rat-<?=$i?>-io-libre" class="bg-hover hidden">
                                <td class="text-right"><b>Observación</b></td>
                                <td class="text-left" colspan="10">
                                    <div class="form-group">
                                        <input type="text" name="observacion" class="large" value="<?=$ratio->observacion;?>">
                                    </div>
                                </td>
                            </tr>
                            </form>
                        <?php else : ?>
                            <tr id="rat-<?=$i?>-i-libre" class="bg-hover hidden">
                                <td class="text-right"><b>Intervenido</b></td>
                            <?php if(!empty($ratio->energia_in)) :?>
                                <td>
                                    <?=$ratio->energia_in_gen;?>
                                </td>
                                <td>
                                    <?=$ratio->tec_ratio_in;?>
                                </td>
                                <td>
                                    <?=$ratio->armo_ratio_in;?>
                                </td>
                                <td>
                                    <?=$ratio->e_punta_in_gen;?>
                                </td>
                                <td>
                                    <?=$ratio->monomico_in_gen;?>
                                </td>

                                <td>
                                    <?=$ratio->energia_in;?>
                                </td>
                                <td>
                                    <?=$ratio->trans_ratio_in;?>
                                </td>
                                <td>
                                    <?=$ratio->f_punta_in;?>
                                </td>
                                <td>
                                    <?=$ratio->e_punta_in;?>
                                </td>
                                <td>
                                    <?=$ratio->cp_in;?>
                                </td>
                                <td>
                                    <?=$ratio->monomico_in;?>
                                </td>

                                <td>
                                    <?=$ratio->produccion_in;?>
                                </td>

                                <td>
                                    <?=$ratio->indicador_in_gen;?>
                                </td>
                                <td class="ahorro_in">
                                    <?=$ratio->ahorro_in_gen;?>
                                </td>
                                <td class="optimizacion_in">
                                    <?=$ratio->optimizacion_in_gen;?>
                                </td>

                                <td>
                                    <?=$ratio->indicador_in;?>
                                </td>
                                <td class="ahorro_in">
                                    <?=$ratio->ahorro_in_dis;?>
                                </td>
                                <td class="optimizacion_in">
                                    <?=$ratio->optimizacion_in_dis;?>
                                </td>

                                <td class="ahorro_in">
                                    <?=$ratio->ahorro_in;?>
                                </td>
                                <td class="optimizacion_in">
                                    <?=$ratio->optimizacion_in;?>
                                </td>
                            <?php else :?>
                                <td>
                                    <?=$ratio->energia_gen;?>
                                </td>
                                <td>
                                    <?=$ratio->tec_ratio;?>
                                </td>
                                <td>
                                    <?=$ratio->armo_ratio;?>
                                </td>
                                <td>
                                    <?=$ratio->e_punta_gen;?>
                                </td>
                                <td>
                                    <?=$ratio->monomico_gen;?>
                                </td>

                                <td>
                                    <?=$ratio->energia;?>
                                </td>
                                <td>
                                    <?=$ratio->trans_ratio;?>
                                </td>
                                <td>
                                    <?=$ratio->f_punta;?>
                                </td>
                                <td>
                                    <?=$ratio->e_punta;?>
                                </td>
                                <td>
                                    <?=$ratio->cp;?>
                                </td>
                                <td>
                                    <?=$ratio->monomico;?>
                                </td>

                                <td>
                                    <?=$ratio->produccion;?>
                                </td>

                                <td>
                                    <?=$ratio->indicador_gen;?>
                                </td>
                                <td class="ahorro_in">
                                    <?=$ratio->ahorro_gen;?>
                                </td>
                                <td class="optimizacion_in">
                                    <?=$ratio->optimizacion_gen;?>
                                </td>

                                <td>
                                    <?=$ratio->indicador;?>
                                </td>
                                <td class="ahorro_in">
                                    <?=$ratio->ahorro_dis;?>
                                </td>
                                <td class="optimizacion_in">
                                    <?=$ratio->optimizacion_dis;?>
                                </td>

                                <td class="ahorro_in">
                                    <?=$ratio->ahorro;?>
                                </td>
                                <td class="optimizacion_in">
                                    <?=$ratio->optimizacion;?>
                                </td>
                            <?php endif;?>
                                <td>
                                </td>
                            </tr>
                            <tr id="rat-<?=$i?>-io" class="bg-hover hidden">
                                <td class="text-right"><b>Observación</b></td>
                                <td class="text-left" colspan="10">
                                    <?=$ratio->observacion;?>
                                </td>
                            </tr>
                        <?php endif;?>
                    <?php endif;?>
                    <?php endforeach; ?>
                        
                </tbody>
                <tfoot>
                    <?php if(empty($this->data->ratios)) : ?>
                    <tr>
                        <td class="text-right"><b>Totales:</b></td>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->ratio_11 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_energia_gen;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_21 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_tec_ratio;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_20 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_armo_ratio;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_12 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_e_punta_gen;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_13 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_monomico_gen;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_2 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_energia;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_19 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_trans_ratio;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_3 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_f_punta;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_4 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_e_punta;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_10 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_cp;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_5 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_monomico;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_6 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_produccion;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_14 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_indicador_gen;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_16 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_ahorro_gen;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_18 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_optimizacion_gen;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_7 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_indicador;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_15 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_ahorro_dis;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_17 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_optimizacion_dis;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_8 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_ahorro;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_9 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->tot_optimizacion;?></b></td>
                        <?php endif;?>
                    <?php else : ?>
                        <td><b><?=$this->data->suministro->ratio->tot_energia_gen;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->tot_tec_ratio;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->tot_armo_ratio;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->tot_e_punta_gen;?></b></td>               
                        <td><b><?=$this->data->suministro->ratio->tot_monomico_gen;?></b></td>

                        <td><b><?=$this->data->suministro->ratio->tot_energia;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->tot_trans_ratio;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->tot_f_punta;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->tot_e_punta;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->tot_cp;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->tot_monomico;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->tot_produccion;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->tot_indicador_gen;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->tot_ahorro_gen;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->tot_optimizacion_gen;?></b></td>

                        <td><b><?=$this->data->suministro->ratio->tot_indicador;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->tot_ahorro_dis;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->tot_optimizacion_dis;?></b></td>

                        <td><b><?=$this->data->suministro->ratio->tot_ahorro;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->tot_optimizacion;?></b></td>
                    <?php endif;?>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right"><b>Promedios:</b></td>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->ratio_11 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_energia_gen;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_21 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_tec_ratio;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_20 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_armo_ratio;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_12 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_e_punta_gen;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_13 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_monomico_gen;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_2 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_energia;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_19 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_trans_ratio;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_3 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_f_punta;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_4 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_e_punta;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_10 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_cp;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_5 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_monomico;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_6 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_produccion;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_14 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_indicador_gen;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_16 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_ahorro_gen;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_18 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_optimizacion_gen;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_7 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_indicador;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_15 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_ahorro_dis;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_17 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_optimizacion_dis;?></b></td>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_8 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_ahorro;?></b></td>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_9 == 1) :?>
                        <td><b><?=$this->data->suministro->ratio->prom_optimizacion;?></b></td>
                        <?php endif;?>
                    <?php else : ?>
                        <td><b><?=$this->data->suministro->ratio->prom_energia_gen;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->prom_tec_ratio;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->prom_armo_ratio;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->prom_e_punta_gen;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->prom_monomico_gen;?></b></td>

                        <td><b><?=$this->data->suministro->ratio->prom_energia;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->prom_trans_ratio;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->prom_f_punta;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->prom_e_punta;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->prom_cp;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->prom_monomico;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->prom_produccion;?></b></td>
                        
                        <td><b><?=$this->data->suministro->ratio->prom_indicador_gen;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->prom_ahorro_gen;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->prom_optimizacion_gen;?></b></td>

                        <td><b><?=$this->data->suministro->ratio->prom_indicador;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->prom_ahorro_dis;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->prom_optimizacion_dis;?></b></td>

                        <td><b><?=$this->data->suministro->ratio->prom_ahorro;?></b></td>
                        <td><b><?=$this->data->suministro->ratio->prom_optimizacion;?></b></td>
                    <?php endif;?>
                        <td></td>
                    </tr>
                    <?php endif;?>
                    <tr>
                        <th>Mes</th>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                        <?php if($this->data->suministro->ratio_11 == 1) :?>
                        <th>Energía</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_21 == 1) :?>
                        <th>Mínimo Técnico <br> ($/kWh)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_20 == 1) :?>
                        <th>Armonización <br> ($/kWh)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_12 == 1) :?>
                        <th>Hora Punta</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_13 == 1) :?>
                        <th>Monómico</th>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_2 == 1) :?>
                        <th>Energía</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_19 == 1) :?>
                        <th>Transmisión <br> ($/kWh)</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_3 == 1) :?>
                        <th>Fuera de Punta</th>
                        <?php endif;?> 
                        <?php if($this->data->suministro->ratio_4 == 1) :?>
                        <th>Hora Punta</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_10 == 1) :?>
                        <th>Compra Potencia</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_5 == 1) :?>
                        <th>Monómico</th>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_6 == 1) :?>
                        <th>Proyectado</th>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_14 == 1) :?>
                        <th>Indicador</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_16 == 1) :?> 
                        <th>Ahorro</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_18 == 1) :?>
                        <th>Optimización</th>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_7 == 1) :?>
                        <th>Indicador</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_15 == 1) :?> 
                        <th>Ahorro</th>
                        <?php endif;?>
                        <?php if($this->data->suministro->ratio_17 == 1) :?>
                        <th>Optimización</th>
                        <?php endif;?>

                        <?php if($this->data->suministro->ratio_8 == 1) :?> 
                        <th>Tot. Ahorro</th>
                        <?php endif;?> 
                        <?php if($this->data->suministro->ratio_9 == 1) :?>
                        <th>Tot. Optimización</th>
                        <?php endif;?>
                    <?php else : ?>
                        <th>Energía</th>
                        <th>Mínimo Técnico</th>
                        <th>Armonización</th>
                        <th>Hora Punta</th>
                        <th>Monómico</th>

                        <th>Energía</th>
                        <th>Transmisión</th>
                        <th>Fuera de Punta</th>
                        <th>Hora Punta</th>
                        <th>Compra Potencia</th>
                        <th>Monómico</th>

                        <th>Prod</th>

                        <th>Indicador</th> 
                        <th>Ahorro</th>
                        <th>Optimización</th>

                        <th>Indicador</th> 
                        <th>Ahorro</th>
                        <th>Optimización</th>

                        <th>Tot. Ahorro</th>
                        <th>Tot. Optimización</th>
                    <?php endif;?>
                        <th></th>
                    </tr>
                    <?php if($this->data->user->id_perfil == 3) : ?>
                      <tr>
                        <th></th>
                        <?php if($colspan_gen_r_a > 0) :?>
                        <th colspan="<?=$colspan_gen_a;?>">Generación</th>
                        <?php endif; ?>
                        <?php if($colspan_dis_r_a > 0) :?>
                        <th colspan="<?=$colspan_dis_a;?>">Distribución</th>
                        <?php endif; ?>
                        <?php if($colspan_rr_a > 0) :?>
                        <th colspan="<?=$colspan_rr_a;?>"></th>
                        <?php endif; ?>
                        <?php if($colspan_gen_r_b > 0) :?>
                        <th colspan="<?=$colspan_gen_b;?>">Generación</th>
                        <?php endif; ?>
                        <?php if($colspan_dis_r_b > 0) :?>
                        <th colspan="<?=$colspan_dis_b;?>">Distribución</th>
                        <?php endif; ?>
                        <th colspan="1"></th>
                      </tr>
                    <?php else : ?>
                      <tr>
                        <th colspan='1'></th>
                        <th colspan='5'>Generación</th>
                        <th colspan='6'>Distribución</th>
                        <th colspan='1'></th>
                        <th colspan='3'>Generación</th>
                        <th colspan='3'>Distribución</th>
                        <th colspan='2'></th>
                        <th colspan="1"></th>
                      </tr>
                    <?php endif; ?>
                </tfoot>
            </table>
              
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      <?php endif; ?>
      <?php if(!empty($this->data->ratios) && !empty($this->data->ratios_libre)) : ?>
      <div class="box box-warning">
        <div class="box-header with-border pointer">
          <div class="row">
            <div class="col-xs-12">
              <span class="box-title">Resumen Ratios</span>
            </div>
          </div>
        </div>
        <div class="box-body text-12">
          <table class="table table-hover dark text-center">
            <tbody>
              <?php if($this->data->user->id_perfil == 3) : ?>
                <tr>
                  <th colspan="1"></th>
                  <?php if($colspan_gen_r_a > 0) :?>
                  <th colspan="<?=$colspan_gen_a;?>">Generación</th>
                  <?php endif; ?>
                  <?php if($colspan_dis_r_a > 0) :?>
                  <th colspan="<?=$colspan_dis_a;?>">Distribución</th>
                  <?php endif; ?>
                  <?php if($colspan_rr_a > 0) :?>
                  <th colspan="<?=$colspan_rr_a;?>"></th>
                  <?php endif; ?>
                  <?php if($colspan_gen_r_b > 0) :?>
                  <th colspan="<?=$colspan_gen_b;?>">Generación</th>
                  <?php endif; ?>
                  <?php if($colspan_dis_r_b > 0) :?>
                  <th colspan="<?=$colspan_dis_b;?>">Distribución</th>
                  <?php endif; ?>
                  <th colspan='2'></th>
                </tr>
              <?php else : ?>
                <tr>
                  <th colspan='1'></th>
                  <th colspan='5'>Generación</th>
                  <th colspan='6'>Distribución</th>
                  <th colspan='1'></th>
                  <th colspan='3'>Generación</th>
                  <th colspan='3'>Distribución</th>
                  <th colspan='2'></th>
                </tr>
              <?php endif; ?>
                <tr>
                    <th></th>
                <?php if($this->data->user->id_perfil == 3) : ?>
                    <?php if($this->data->suministro->ratio_11 == 1) :?>
                    <th>Energía <br> ($/kWh)</th>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_21 == 1) :?>
                    <th>Mínimo Técnico <br> ($/kWh)</th>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_20 == 1) :?>
                    <th>Armonización <br> ($/kWh)</th>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_12 == 1) :?>
                    <th>Hora Punta <br> ($/kWh)</th>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_13 == 1) :?>
                    <th>Monómico <br> ($/kWh)</th>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_2 == 1) :?>
                    <th>Energía <br> ($/kWh)</th>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_19 == 1) :?>
                    <th>Transmisión <br> ($/kWh)</th>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_3 == 1) :?>
                    <th>Fuera de Punta <br> ($/kWh)</th>
                    <?php endif;?> 
                    <?php if($this->data->suministro->ratio_4 == 1) :?>
                    <th>Hora Punta <br> ($/kWh)</th>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_10 == 1) :?>
                    <th>Compra Potencia <br> ($/kW)</th>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_5 == 1) :?>
                    <th>Monómico <br> ($/kWh)</th>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_6 == 1) :?>
                    <th>Proyectado <br> (kWh)</th>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_14 == 1) :?>
                    <th>Indicador <br> (kWh/día)</th>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_16 == 1) :?> 
                    <th>Ahorro <br> ($)</th>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_18 == 1) :?>
                    <th>Optimización <br> ($)</th>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_7 == 1) :?>
                    <th>Indicador <br> (kWh/día)</th>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_15 == 1) :?> 
                    <th>Ahorro <br> ($)</th>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_17 == 1) :?>
                    <th>Optimización <br> ($)</th>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_8 == 1) :?> 
                    <th>Tot. Ahorro <br> ($)</th>
                    <?php endif;?> 
                    <?php if($this->data->suministro->ratio_9 == 1) :?>
                    <th>Tot. Optimización <br> ($)</th>
                    <?php endif;?>
                <?php else : ?>
                    <th>Energía ($/kWh)</th>
                    <th>Mínimo Técnico ($/kWh)</th>
                    <th>Armonización ($/kWh)</th>
                    <th>Hora Punta ($/kWh)</th>
                    <th>Monómico ($/kWh)</th>

                    <th>Energía ($/kWh)</th>
                    <th>Transmisión ($/kWh)</th>
                    <th>Fuera de Punta ($/kWh)</th>
                    <th>Hora Punta ($/kWh)</th>
                    <th>Compra Potencia ($/kW)</th>
                    <th>Monómico ($/kWh)</th>

                    <th>Proyectado <br> (kWh)</th>

                    <th>Indicador  <br> (kWh/día)</th> 
                    <th>Ahorro <br> ($)</th>
                    <th>Optimización <br> ($)</th>

                    <th>Indicador <br> (kWh/día)</th>
                    <th>Ahorro <br> ($)</th>
                    <th>Optimización <br> ($)</th>

                    <th>Tot. Ahorro <br> ($)</th>
                    <th>Tot. Optimización <br> ($)</th>
                <?php endif;?>
                    <th></th>
                </tr>
                <tr>
                    <td class="text-right"><b>Totales:</b></td>
                <?php if($this->data->user->id_perfil == 3) : ?>
                    <?php if($this->data->suministro->ratio_11 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_energia_gen;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_21 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_tec_ratio;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_20 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_armo_ratio;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_12 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_e_punta_gen;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_13 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_monomico_gen;?></td>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_2 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_energia;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_19 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_trans_ratio;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_3 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_f_punta;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_4 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_e_punta;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_10 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_cp;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_5 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_monomico;?></td>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_6 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_produccion;?></td>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_14 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_indicador_gen;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_16 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_ahorro_gen;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_18 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_optimizacion_gen;?></td>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_7 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_indicador;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_15 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_ahorro_dis;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_17 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_optimizacion_dis;?></td>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_8 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_ahorro;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_9 == 1) :?>
                    <td><?=$this->data->suministro->ratio->tot_optimizacion;?></td>
                    <?php endif;?>
                <?php else : ?>
                    <td><?=$this->data->suministro->ratio->tot_energia_gen;?></td>
                    <td><?=$this->data->suministro->ratio->tot_tec_ratio;?></td>
                    <td><?=$this->data->suministro->ratio->tot_armo_ratio;?></td>
                    <td><?=$this->data->suministro->ratio->tot_e_punta_gen;?></td>               
                    <td><?=$this->data->suministro->ratio->tot_monomico_gen;?></td>

                    <td><?=$this->data->suministro->ratio->tot_energia;?></td>
                    <td><?=$this->data->suministro->ratio->tot_trans_ratio;?></td>
                    <td><?=$this->data->suministro->ratio->tot_f_punta;?></td>
                    <td><?=$this->data->suministro->ratio->tot_e_punta;?></td>
                    <td><?=$this->data->suministro->ratio->tot_cp;?></td>
                    <td><?=$this->data->suministro->ratio->tot_monomico;?></td>
                    
                    <td><?=$this->data->suministro->ratio->tot_produccion;?></td>
                    
                    <td><?=$this->data->suministro->ratio->tot_indicador_gen;?></td>
                    <td><?=$this->data->suministro->ratio->tot_ahorro_gen;?></td>
                    <td><?=$this->data->suministro->ratio->tot_optimizacion_gen;?></td>

                    <td><?=$this->data->suministro->ratio->tot_indicador;?></td>
                    <td><?=$this->data->suministro->ratio->tot_ahorro_dis;?></td>
                    <td><?=$this->data->suministro->ratio->tot_optimizacion_dis;?></td>

                    <td><?=$this->data->suministro->ratio->tot_ahorro;?></td>
                    <td><?=$this->data->suministro->ratio->tot_optimizacion;?></td>
                <?php endif;?>
                    <td></td>
                </tr>
                <tr>
                    <td class="text-right"><b>Promedios:</b></td>
                <?php if($this->data->user->id_perfil == 3) : ?>
                    <?php if($this->data->suministro->ratio_11 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_energia_gen;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_21 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_tec_ratio;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_20 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_armo_ratio;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_12 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_e_punta_gen;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_13 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_monomico_gen;?></td>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_2 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_energia;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_19 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_trans_ratio;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_3 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_f_punta;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_4 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_e_punta;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_10 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_cp;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_5 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_monomico;?></td>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_6 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_produccion;?></td>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_14 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_indicador_gen;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_16 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_ahorro_gen;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_18 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_optimizacion_gen;?></td>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_7 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_indicador;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_15 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_ahorro_dis;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_17 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_optimizacion_dis;?></td>
                    <?php endif;?>

                    <?php if($this->data->suministro->ratio_8 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_ahorro;?></td>
                    <?php endif;?>
                    <?php if($this->data->suministro->ratio_9 == 1) :?>
                    <td><?=$this->data->suministro->ratio->prom_optimizacion;?></td>
                    <?php endif;?>
                <?php else : ?>
                    <td><?=$this->data->suministro->ratio->prom_energia_gen;?></td>
                    <td><?=$this->data->suministro->ratio->prom_tec_ratio;?></td>
                    <td><?=$this->data->suministro->ratio->prom_armo_ratio;?></td>
                    <td><?=$this->data->suministro->ratio->prom_e_punta_gen;?></td>
                    <td><?=$this->data->suministro->ratio->prom_monomico_gen;?></td>

                    <td><?=$this->data->suministro->ratio->prom_energia;?></td>
                    <td><?=$this->data->suministro->ratio->prom_trans_ratio;?></td>
                    <td><?=$this->data->suministro->ratio->prom_f_punta;?></td>
                    <td><?=$this->data->suministro->ratio->prom_e_punta;?></td>
                    <td><?=$this->data->suministro->ratio->prom_cp;?></td>
                    <td><?=$this->data->suministro->ratio->prom_monomico;?></td>
                    
                    <td><?=$this->data->suministro->ratio->prom_produccion;?></td>
                    
                    <td><?=$this->data->suministro->ratio->prom_indicador_gen;?></td>
                    <td><?=$this->data->suministro->ratio->prom_ahorro_gen;?></td>
                    <td><?=$this->data->suministro->ratio->prom_optimizacion_gen;?></td>

                    <td><?=$this->data->suministro->ratio->prom_indicador;?></td>
                    <td><?=$this->data->suministro->ratio->prom_ahorro_dis;?></td>
                    <td><?=$this->data->suministro->ratio->prom_optimizacion_dis;?></td>

                    <td><?=$this->data->suministro->ratio->prom_ahorro;?></td>
                    <td><?=$this->data->suministro->ratio->prom_optimizacion;?></td>
                <?php endif;?>
                    <td></td>
                </tr>
            </tbody>
          </table>
        </div>
      </div>
      <?php endif; ?>
      <?php if(empty($this->data->suministro->facturas) && empty($this->data->suministro->facturas_libre)) : ?>
        <div class="callout callout-info">
            <h4> <i class="fa fa-info"></i> Información</h4>

            <p>
                No se encontraron facturas asociadas para el preríodo<br>
            </p>
        </div>
      <?php endif;?>
      <?php endif; ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  