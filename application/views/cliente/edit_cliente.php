<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Modal -->
<div id="ajax-response" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ajax-response-label" aria-hidden="true">
    <div class="modal-dialog">
        <div id="ajax-content" class="modal-content">
            
        </div>
    </div>
</div>
<!-- Modal -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Clientes
        <small>Modificar Cliente</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="<?=base_url('gestion/clientes');?>"><i class="fa fa-edit"></i> Clientes</a></li>
        <?php if(!empty($this->data->cliente)) :?>
            <li><a href="<?=base_url('gestion/cliente/?token=' . $this->data->cliente->id);?>"><i class="fa fa-search"></i> Detalle Cliente</a></li>
        <?php endif;?>
        <li class="active">Editar Cliente</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <?php if(empty($this->data->cliente)) :?>
        <div class="callout callout-danger">
            <h4><i class="fa fa-exclamation-circle"></i> Error</h4>

            <p>Imposible recuperar la información del cliente solicitado</p>
        </div>
    <?php else :?>
      <form id="cliente_form" action="<?=base_url('clientes/edit')?>" method="post">
      <!-- Modal -->
        <div class="modal fade" id="saveClienteModal" tabindex="-1" role="dialog" aria-labelledby="saveClienteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="saveClienteModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                </div>
                <div class="modal-body">
                    <p>¿Realmente desea modificar la información del cliente?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button id="save" type="submit" class="btn btn-primary">Sí</button>
                </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
            <div class="col-xs-4 col-sm-8">
                <h3 class="box-title">Editando cliente <b><?=$this->data->cliente->nombre;?></b></h3>
            </div>
            <div class="col-xs-6 col-sm-4 text-right">
                <a href="<?=base_url('gestion/cliente/?token=' . $this->data->cliente->id);?>" class="text-18 margin-left" title="Cancelar">
                    <i class="fa fa-window-close"></i>
                </a>
            </div>
        </div>
        <div class="box-body">
        <div class="row">
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label>
                                Rut:
                                <input name="rut" type="text" class="form-control" value="<?=$this->data->cliente->rut?>" placeholder="12345678-k" pattern="^[0-9]+[-|‐]{1}[0-9kK]{1}$" required="required">
                            </label>
                        </div>
                    </div>
                        
                    <div class="col col-xs-12 col-md-9">
                        <div class="form-group">
                            <label>
                                Nombre:
                                <input name="nombre" type="text" class="form-control" value="<?=$this->data->cliente->nombre?>" placeholder="nombre" required="required">
                            </label>
                        </div>
                    </div>
                    
                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Contacto:
                                <input name="contacto" type="text" class="form-control" value="<?=$this->data->cliente->contacto?>" placeholder="nombre contacto">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Correo contacto: 
                                <input name="correo" type="email" class="form-control" value="<?=$this->data->cliente->correo?>" placeholder="mail@ejemplo.com" pattern="^[-\w.%+]{1,64}@(?:[A-Za-z0-9-]{1,63}\.){1,125}[A-Za-z]{2,63}$">
                            </label>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Teléfono contacto:
                                <input type="tel" name="telefono" value="<?=$this->data->cliente->num_tel?>" placeholder="912345678" minlength="9" maxlength="9" pattern="^[0-9]*$">
                            </label>
                        </div>
                    </div> 

                    <div class="col col-xs-12 col-md-3">
                        <div class="form-group">
                            <label>
                                Sitio web:
                                <input type="text" name="web" value="<?=$this->data->cliente->sitio_web?>" placeholder="ejemplo.com" pattern="^(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?$">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="row">
                <div class="col col-xs-8 col-xs-offset-1 col-sm-offset-2">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="token" value="<?=$this->data->cliente->id;?>">
                                <button id="saveCliente" type="button" class="btn btn-primary btn-flat">
                                    <i class="fa fa-floppy-o"></i> Guardar
                                </button>
                                <a href="<?=base_url('gestion/cliente/?token=' . $this->data->cliente->id);?>" class="btn btn-default btn-flat margin-left">
                                    <i class="fa fa-window-close"></i> Cerrar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      </form>

      <form id="avatar_form" action="<?=base_url('clientes/change_avatar')?>" method="post">
      <!-- Modal -->
        <div class="modal fade" id="saveAvatarModal" tabindex="-1" role="dialog" aria-labelledby="saveAvatarModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="saveAvatarModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                </div>
                <div class="modal-body">
                    <p>¿Realmente desea modificar avatar del cliente?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button id="save2" type="submit" class="btn btn-primary">Sí</button>
                </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-xs-4 col-sm-8">
                <h3 class="box-title">Avatar</h3>
            </div>
            <div class="col-xs-6 col-sm-4 text-right">
                <a href="<?=base_url();?>" class="text-18 margin-left" title="Cancelar">
                    <i class="fa fa-window-close"></i>
                </a>
            </div>
        </div>
        <div class="box-body">
        <div class="row">
            <div class="col col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col col-xs-12 col-md-4">
                        <div class="form-group">
                            <img class="img-thumbnail" src="<?=base_url('assets/img/'. $this->data->cliente->avatar);?>" height="150px" width="150px" alt="Avatar Cliente">
                        </div>
                    </div>
                    <div class="col col-xs-12 col-md-8">
                        <div class="form-group">
                            <label class="extendido">
                                Imagen:
                                <input type="file" id="avatarfile" name="avatarfile" class="form-control" required="required">
                                <input type="hidden" name="file_name" value="<?=$this->data->cliente->avatar?>">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="row">
                <div class="col col-xs-8 col-xs-offset-1 col-sm-offset-2">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="token" value="<?=$this->data->cliente->id?>">
                                <button id="saveAvatar" type="button" class="btn btn-primary btn-flat">
                                    <i class="fa fa-floppy-o"></i> Guardar
                                </button>
                                <a href="<?=base_url();?>" class="btn btn-default btn-flat margin-left">
                                    <i class="fa fa-window-close"></i> Cerrar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      </form>

      <form id="cliente_message_form" action="<?=base_url('clientes/set_message')?>" method="post">
      <!-- Modal -->
        <div class="modal fade" id="saveClienteMessageModal" tabindex="-1" role="dialog" aria-labelledby="saveClienteMessageModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="saveClienteMessageModalLabel"><i class="fa fa-exclamation-circle"></i> Confirmación</h5>
                </div>
                <div class="modal-body">
                    <p>¿Realmente desea modificar el mensaje para el cliente?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button id="saveCliMsg" type="submit" class="btn btn-primary">Sí</button>
                </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
            <div class="col-xs-4 col-sm-8">
                <h3 class="box-title">Mensaje</h3>
            </div>
            <div class="col-xs-6 col-sm-4 text-right">
                <a href="<?=base_url('gestion/cliente/?token=' . $this->data->cliente->id);?>" class="text-18 margin-left" title="Cancelar">
                    <i class="fa fa-window-close"></i>
                </a>
            </div>
        </div>
        <div class="box-body">
        <div class="box-body pad">
              <form>
                <textarea id="message" name="message" class="textarea" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php if(!empty($this->data->cliente->message)) : ?><?=$this->data->cliente->message;?><?php endif; ?></textarea>
              </form>
            </div> 
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="row">
                <div class="col col-xs-8 col-xs-offset-1 col-sm-offset-2">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="token" value="<?=$this->data->cliente->id;?>">
                                <button id="saveClienteMessage" type="button" class="btn btn-primary btn-flat">
                                    <i class="fa fa-floppy-o"></i> Guardar
                                </button>
                                <a href="<?=base_url('gestion/cliente/?token=' . $this->data->cliente->id);?>" class="btn btn-default btn-flat margin-left">
                                    <i class="fa fa-window-close"></i> Cerrar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      </form>
    <?php endif;?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->